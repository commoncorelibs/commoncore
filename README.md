# Vinland Solutions CommonCore Library
> A Common Core to Simplify Development

[![License](https://commoncorelibs.gitlab.io/commoncore/badge/license.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/)
[![Platform](https://commoncorelibs.gitlab.io/commoncore/badge/platform.svg)](https://docs.microsoft.com/en-us/dotnet/standard/net-standard)
[![Gitlab Repository](https://commoncorelibs.gitlab.io/commoncore/badge/repository.svg)](https://gitlab.com/commoncorelibs/commoncore/master)
[![Gitlab Documentation](https://commoncorelibs.gitlab.io/commoncore/badge/documentation.svg)](https://commoncorelibs.gitlab.io/commoncore/)
[![Nuget Package](https://badgen.net/nuget/v/VinlandSolutions.CommonCore/latest?icon)](https://www.nuget.org/packages/VinlandSolutions.CommonCore/)  
[![Dev Version](https://commoncorelibs.gitlab.io/commoncore/badge/version/dev.svg)](https://gitlab.com/commoncorelibs/commoncore/commits/master)
[![Pipeline Status](https://gitlab.com/commoncorelibs/commoncore/badges/master/pipeline.svg)](https://gitlab.com/commoncorelibs/commoncore/pipelines)
[![Coverage Status](https://gitlab.com/commoncorelibs/commoncore/badges/master/coverage.svg)](https://commoncorelibs.gitlab.io/commoncore/reports/index.html)
[![Tests Status](https://commoncorelibs.gitlab.io/commoncore/badge/tests.svg)](https://commoncorelibs.gitlab.io/commoncore/reports/index.html)

Vinland Solutions CommonCore is a .Net Standard library focused on collecting and organizing various common and boilerplate tasks, simplifying them as much as possible, and wrapping them into reusable code.

## Installation

The official release versions of the CommonCore package are hosted on [NuGet](https://www.nuget.org/packages/VinlandSolutions.CommonCore/) and can be installed using the standard console means, or found through the Visual Studio NuGet Package Managers.

The library targets both `netstandard2.0` and `netstandard2.1`, so is consumable from projects that target **.Net Framework 4.8** or the range of **.Net Core** versions and beyond.

## Contents

The CommonCore repository is composed of four projects:

* CommonCore: The actual library project.
* CommonCore.Docs: A [DocFX](https://github.com/dotnet/docfx) project to build the api documentation.
* CommonCore.Tests: A [xUnit](https://github.com/xunit/xunit) unit testing project.

## Dependencies

The number of dependencies is kept as low as possible, but they do exist.

* CommonCore: [System.Memory](https://www.nuget.org/packages/System.Memory/) and [Microsoft.Bcl.HashCode](https://www.nuget.org/packages/Microsoft.Bcl.HashCode)
* CommonCore.Docs: [DocFX](https://github.com/dotnet/docfx)
* CommonCore.Tests: [xUnit](https://github.com/xunit/xunit) and [FluentAssertions](https://github.com/fluentassertions/fluentassertions)

## Features

The CommonCore Library features can be broken down into several categories: Extensions, Helpers, Objects, Collections.

### Extensions

Extension methods are the primary means of wrapping common code. Related extension methods are collected into static *Extensions classes.

* Object Extensions: Methods that operate on the base `object`, such as `IsNull()`, `IsNotNull()`, `Is<T>()`, `IsNot<T>()`, `As<T>()`, and `Cast<T>()`.
* Enum Extensions: Contains various methods that simplify checking enum flag values, such as `HasFlag()`, `HasAnyFlag()`, and `HasAllFlags()`.
* Char Extensions: Wrap static `char` methods -- such as `IsDigit()`, `IsLetter()`, `IsLower()`, etc -- so they are callable from `char` instances. Ultimately, the goal is to have full parity between the static methods and the instance-accessible extension methods.
* Numeric Extensions: Similar to the Char Extensions, the Numeric Extensions focus mainly on wrapping the static `Math` methods -- such as `Abs()`, `Min()`, and `Max()` -- into equivalent instance-accessible extension methods. However, other useful methods (eg `Clamp()`) are also included. All numeric extension methods are written as overloads to avoid any wasting cycles on type comparisons and other conversions.
* String Extensions: The String Extensions (strive to) bring parity between `string`s and `StringBuilder`s, with the secondary goal of also providing `char` equivalent functionality. On top of that, various higher level functionality has been implemented, such as line-ending normalization and wildcard pattern matching. Be warned, this functionality of course comes with all the downsides implied by complex operations on a immutable 'string' type; though one goal of these extensions is to keeping that overhead as low as possible.
* StringBuilder Extensions: The StringBuilder Extensions (strive to) bring parity between `string`s and `StringBuilder`s, with the secondary goal of also providing `char` equivalent functionality. On top of that, various higher level functionality has been implemented, such as line-ending normalization and wildcard pattern matching.
* Other Extensions: Extension methods for various other types and kinds are also implemented and new ones will be introduced as functionality for them is found.

### Helpers

Helpers are static classes that contain static methods. Each static *Helper class collects related methods that don't fit as extension methods, though some times functionality is implemented as a helper method and the helper method is called from extension methods.

* Common Helper: This helper contains methods that are generally useful or simply don't (yet) belong any where else.
* Enum Helper: Wraps and simplifies some static `Enum` methods like `GetName()` and `IsDefined()` aw well as implementing some common code like the ubiquitous `GetDescription()` method.
* Date Helper: Implements common code for various common cases, such as finding the first, second, third, ..., Nth weekday. Additionally, the helper contains methods to find the date of over thirty holidays.
* Throw Helper: This helper simplifies throwing exceptions and, more importantly, provides consistency to the messages those exceptions carry. For example, the `ThrowHelper.IfSelfNull()` method is used extensively throughout this library to ensure that extension methods aren't called from a `null` reference.

### Objects

The library provides some classes and structs that fill various logical and semantic holes in the framework.

* AObservableObject: Implements the `INotifyPropertyChanged` interface and provides a base for building bindable objects.

### Collections

The library provides collections that supplement and build upon those in the framework.

* ObservableItemCollection: An `ObservableCollection` derived class that raises an event when its contained elements raise their `PropertyChanged` event.

## Links

- Project: https://gitlab.com/commoncorelibs/commoncore
- Issues: https://gitlab.com/commoncorelibs/commoncore/issues
- Docs: https://commoncorelibs.gitlab.io/commoncore/index.html

### Related Libraries

The other CommonLibs/Common.* libraries are in various stages of being ported; however, those will have to wait until this project is further along.

  * CommonCore.Drawing Library: Confirmed for Porting!
  * CommonCore.Math Library: Planned for Porting.
  * CommonCore.Logging Library: Planned for Porting.
  * CommonCore.Settings Library: Planned for Porting.
  * CommonCore.Security Library: Planned for Porting.
  * CommonCore.WinForms Library: Considered for Porting.

## Licensing

All contents of this repository, unless otherwise stated, are released under [The MIT License (MIT)](https://opensource.org/licenses/MIT) license.

### Notices:

No warranties are given. You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation.
