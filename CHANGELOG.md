## 2.0.0

* Relicensed the repo from CC-BY-NC-SA to MIT. #36
* Retargeted the project to netstandard2.0 and netstandard2.1 #49, #59
* Removed legacy CHM docs project. #30
* Removed [Scripty](https://github.com/daveaglick/Scripty) package dependency. #54

### Added

* Introduce CommonCore.Security #39
* Introduce CommonCore.Numerics #40
* Add `FileInfo` `WriteAll*` extension methods. #47

### Refactored

* Changed `FileHelper.FormatFileSize` format parameter to be more permissive. #45

## 1.3.0

### Added

* Add `ThrowHelper.IList` static extension class.
* Add `Reflection.AssemblyExtensions` static extension class. #33
* Add `Reflection.AssemblyHelper` static helper class. #33
* Add `IO.DirectoryInfoExtensions` static extension class. #34
* Add `IO.FileInfoExtensions` static extension class. #34
* Add `IO.FileHelper` static helper class. #34

## 1.2.0

### Added

* Add `IO.ExtensionFilter` classes. #29
* Add `Rand.MarkovWordGenerator` classes. #29
* Add more `StringBuilder` extension methods. #9

## 1.0.0

* Initial project creation.

### Added

* Add `AObservableObject` abstract class.
* Add `ObjectExtensions` static extension class.
