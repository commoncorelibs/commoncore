﻿#region Copyright (c) 2014 Jay Jeckel
// Copyright (c) 2014 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CommonCore.Experimental.Rand
{
    /// <summary>
    /// The <see cref="RandomExtensions"/> <c>static</c> <c>class</c> provides
    /// extension methods related to <see cref="System.Random"/> objects.
    /// </summary>
    public static partial class RandomExtensions
    {

        #region GetBool Methods

        /// <summary>
        /// Returns a random <see cref="bool"/> value with a 50% chance of <c>true</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>Random <see cref="bool"/> value.</returns>
        public static bool GetBool(this System.Random self)
        {
            Throw.If.Self.IsNull(self);
            return (self.NextDouble() < 0.5);
        }

        /// <summary>
        /// Returns a random <see cref="bool"/> value with the specified <paramref name="probability"/> for <c>true</c>.
        /// The <paramref name="probability"/> must be in the range <c>0.0</c> to <c>1.0</c>, inclusive.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="probability">Probability in the range <c>0.0</c> to <c>1.0</c> of a result of <c>true</c>.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="probability"/> is less than <c>0.0</c> or greater than <c>1.0</c>.</exception>
        /// <returns>Random <see cref="bool"/> value.</returns>
        public static bool GetBool(this System.Random self, double probability)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsLesser(nameof(probability), probability, 0);
            Throw.If.Arg.IsGreater(nameof(probability), probability, 1);
            return (self.NextDouble() < probability);
        }

        #endregion

        #region GetDateTime Methods

        /// <summary>
        /// Returns a random <see cref="DateTime"/> greater than or equal to <paramref name="min"/>
        /// and less than <paramref name="max"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="min">Minimum returnable value, inclusive.</param>
        /// <param name="max">Maximum returnable value, exclusive.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>Random <see cref="DateTime"/> between <paramref name="min"/> and <paramref name="max"/>.</returns>
        public static DateTime GetDateTime(this Random self, DateTime min, DateTime max)
        {
            Throw.If.Self.IsNull(self);
            return DateTime.FromOADate(self.GetDouble(min.ToOADate(), max.ToOADate()));
        }

        /// <summary>
        /// Returns a random <see cref="DateTime"/> greater than or equal to <see cref="DateTime.MinValue"/>
        /// and less than <see cref="DateTime.MaxValue"/>.
        /// 
        /// Note, what the maximum being exclusive means in the context of <see cref="DateTime"/> values
        /// is not as clear as it is with, say, integers. In this case, the underlying operations are
        /// performed using <see cref="double"/> calculations. At the heart, a <see cref="double"/> is
        /// generated in the common range <c>0</c> to <c>1</c>, inclusive and exclusive, respectively.
        /// This random unit is then linearly interpolated (lerp) across the range between minimum and
        /// maximum. While <c>0</c>is a possible result for the unit, meaning minimum can be returned, the
        /// unit will never be <c>1</c>, so maximum can never be returned. However, due to the lerping,
        /// the returned value can get as arbitrarily close the maximum as dictated by the precision of
        /// <see cref="double"/> and the size of the lerped range. Tests need to be done to determine what
        /// this difference is in practical application.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>Random <see cref="DateTime"/> between <see cref="DateTime.MinValue"/> and <see cref="DateTime.MaxValue"/>.</returns>
        public static DateTime GetDateTime(this Random self)
            => self.GetDateTime(DateTime.MinValue, DateTime.MaxValue);

        /// <summary>
        /// Returns a random <see cref="DateTime"/> greater than or equal to <see cref="DateTime.Now"/>
        /// and less than <see cref="DateTime.MaxValue"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>Random <see cref="DateTime"/> between <see cref="DateTime.Now"/> and <see cref="DateTime.MaxValue"/>.</returns>
        public static DateTime GetDateTimeFuture(this Random self)
            => GetDateTime(self, DateTime.Now, DateTime.MaxValue);

        /// <summary>
        /// Returns a random <see cref="DateTime"/> greater than or equal to <paramref name="now"/>
        /// and less than <see cref="DateTime.MaxValue"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="now">Minimum returnable value, inclusive.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>Random <see cref="DateTime"/> between <paramref name="now"/> and <see cref="DateTime.MaxValue"/>.</returns>
        public static DateTime GetDateTimeFuture(this Random self, DateTime now)
            => GetDateTime(self, now, DateTime.MaxValue);

        /// <summary>
        /// Returns a random <see cref="DateTime"/> greater than or equal to <see cref="DateTime.MinValue"/>
        /// and less than <see cref="DateTime.Now"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>Random <see cref="DateTime"/> between <see cref="DateTime.MinValue"/> and <see cref="DateTime.Now"/>.</returns>
        public static DateTime GetDateTimePast(this Random self)
            => GetDateTime(self, DateTime.MinValue, DateTime.Now);

        /// <summary>
        /// Returns a random <see cref="DateTime"/> greater than or equal to <see cref="DateTime.MinValue"/>
        /// and less than <paramref name="now"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="now">Maximum returnable value, exclusive.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>Random <see cref="DateTime"/> between <see cref="DateTime.MinValue"/> and <paramref name="now"/>.</returns>
        public static DateTime GetDateTimePast(this Random self, DateTime now)
            => GetDateTime(self, DateTime.MinValue, now);

        #endregion

        #region GetTimeSpan Methods

        /// <summary>
        /// Returns a random <see cref="TimeSpan"/> greater than or equal to <see cref="TimeSpan.MinValue"/>
        /// and less than <see cref="TimeSpan.MaxValue"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>Random <see cref="TimeSpan"/> between <see cref="TimeSpan.MinValue"/> and <see cref="TimeSpan.MaxValue"/>.</returns>
        public static TimeSpan GetTimeSpan(this Random self)
        {
            Throw.If.Self.IsNull(self);
            return TimeSpan.FromMilliseconds(self.GetDouble(TimeSpan.MinValue.TotalMilliseconds, TimeSpan.MaxValue.TotalMilliseconds));
        }

        /// <summary>
        /// Returns a random <see cref="TimeSpan"/> greater than or equal to <paramref name="min"/>
        /// and less than <paramref name="max"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="min">Minimum returnable value, inclusive.</param>
        /// <param name="max">Maximum returnable value, exclusive.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>Random <see cref="TimeSpan"/> between <paramref name="min"/> and <paramref name="max"/>.</returns>
        public static TimeSpan GetTimeSpan(this Random self, TimeSpan min, TimeSpan max)
        {
            Throw.If.Self.IsNull(self);
            return TimeSpan.FromMilliseconds(self.GetDouble(min.TotalMilliseconds, max.TotalMilliseconds));
        }

        #endregion

        #region GetItem Methods

        /// <summary>
        /// Returns a random <typeparamref name="TItem"/> element from the <paramref name="items"/> sequence.
        /// </summary>
        /// <typeparam name="TItem">Type of items in the sequence.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="items">Sequence to choose an item from.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="items"/> is <c>null</c>.</exception>
        /// <returns>Random <typeparamref name="TItem"/> element from the sequence.</returns>
        public static TItem GetItem<TItem>(this System.Random self, params TItem[] items)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNull(nameof(items), items);
            return items[self.Next(items.Length)];
        }

        /// <summary>
        /// Returns a random <typeparamref name="TItem"/> element from the <paramref name="items"/> sequence.
        /// </summary>
        /// <typeparam name="TItem">Type of items in the sequence.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="items">Sequence to choose an item from.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="items"/> is <c>null</c>.</exception>
        /// <returns>Random <typeparamref name="TItem"/> element from the sequence.</returns>
        public static TItem GetItem<TItem>(this System.Random self, IList<TItem> items)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNull(nameof(items), items);
            return items[self.Next(items.Count)];
        }

        /// <summary>
        /// Returns a random <typeparamref name="TItem"/> element from the <paramref name="items"/> sequence.
        /// </summary>
        /// <typeparam name="TItem">Type of items in the enumerable.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="items">Sequence to choose an item from.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="items"/> is <c>null</c>.</exception>
        /// <returns>Random <typeparamref name="TItem"/> element from the sequence.</returns>
        public static TItem GetItem<TItem>(this System.Random self, ICollection<TItem> items)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNull(nameof(items), items);
            return items.ElementAt(self.Next(items.Count));
        }

        /// <summary>
        /// Returns a random <typeparamref name="TItem"/> element from the <paramref name="enumerable"/> sequence.
        /// 
        /// Note, <see cref="System.Linq.Enumerable.Count{TSource}(IEnumerable{TSource})"/> is used by this method.
        /// If a specified <see cref="IEnumerable{T}"/> doesn't support this method, such as in the case of an
        /// infinite enumerable that cannot logically implement Count(), then this method is not an appropriate choice.
        /// </summary>
        /// <typeparam name="TItem">Type of items in the enumerable.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="enumerable">Enumerable to choose an item from.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="enumerable"/> is <c>null</c>.</exception>
        /// <returns>Random <typeparamref name="TItem"/> element from the enumerable.</returns>
        public static TItem GetItem<TItem>(this System.Random self, IEnumerable<TItem> enumerable)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNull(nameof(enumerable), enumerable);
            return enumerable.ElementAt(self.Next(enumerable.Count()));
        }

        #endregion

        #region GetDict Methods

        /// <summary>
        /// Returns a random <typeparamref name="TKey"/> element from the <paramref name="dict"/>
        /// <see cref="IDictionary{TKey, TValue}.Keys"/> sequence.
        /// </summary>
        /// <typeparam name="TKey">Type of keys in the dictionary.</typeparam>
        /// <typeparam name="TValue">Type of values in the dictionary.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="dict">Dictionary to choose a key from.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="dict"/> is <c>null</c>.</exception>
        /// <returns>Random <typeparamref name="TKey"/> element from the dict keys sequence.</returns>
        public static TKey GetDictKey<TKey, TValue>(this System.Random self, IDictionary<TKey, TValue> dict)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNull(nameof(dict), dict);
            return GetItem<TKey>(self, dict.Keys);
        }

        /// <summary>
        /// Returns a random <typeparamref name="TValue"/> element from the <paramref name="dict"/>
        /// <see cref="IDictionary{TKey, TValue}.Values"/> sequence.
        /// </summary>
        /// <typeparam name="TKey">Type of keys in the dictionary.</typeparam>
        /// <typeparam name="TValue">Type of values in the dictionary.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="dict">Dictionary to choose a value from.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="dict"/> is <c>null</c>.</exception>
        /// <returns>Random <typeparamref name="TValue"/> element from the dict values sequence.</returns>
        public static TValue GetDictValue<TKey, TValue>(this System.Random self, IDictionary<TKey, TValue> dict)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNull(nameof(dict), dict);
            return GetItem<TValue>(self, dict.Values);
        }

        #endregion

        #region Shuffle Methods

        /// <summary>
        /// Randomly reorders the specified <paramref name="items"/> sequence in-place.
        /// </summary>
        /// <typeparam name="TItem">Type of items in the sequence.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="items">Sequence to randomly reorder.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="items"/> is <c>null</c>.</exception>
        public static void Shuffle<TItem>(this System.Random self, TItem[] items)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNull(nameof(items), items);
            for (int index = items.Length; index > 1; )
            {
                int num = self.Next(index--);
                if (index != num)
                {
                    TItem temp = items[num];
                    items[num] = items[index];
                    items[index] = temp;
                }
            }
        }

        /// <summary>
        /// Randomly reorders the specified <paramref name="items"/> sequence in-place.
        /// </summary>
        /// <typeparam name="TItem">Type of items in the sequence.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="items">Sequence to randomly reorder.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="items"/> is <c>null</c>.</exception>
        public static void Shuffle<TItem>(this System.Random self, IList<TItem> items)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNull(nameof(items), items);
            for (int index = items.Count; index > 1; )
            {
                int num = self.Next(index--);
                if (index != num)
                {
                    TItem temp = items[num];
                    items[num] = items[index];
                    items[index] = temp;
                }
            }
        }

        #endregion

        #region GetShuffled Methods

        /// <summary>
        /// Returns a <see cref="IEnumerable{TItem}"/> sequence with <paramref name="items"/> elements randomly ordered.
        /// </summary>
        /// <typeparam name="TItem">Type of items in the sequence.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="items">Sequence to randomly reorder.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="items"/> is <c>null</c>.</exception>
        /// <returns>Sequence with <paramref name="items"/> elements randomly ordered.</returns>
        public static IEnumerable<TItem> GetShuffled<TItem>(this System.Random self, params TItem[] items)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNull(nameof(items), items);
            foreach (var item in items.OrderBy(item => self.NextDouble())) { yield return item; }
        }

        /// <summary>
        /// Returns a <see cref="IEnumerable{TItem}"/> sequence with <paramref name="enumerable"/> elements randomly ordered.
        /// </summary>
        /// <typeparam name="TItem">Type of items in the enumerable.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="enumerable">Enumerable to randomly reorder.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="enumerable"/> is <c>null</c>.</exception>
        /// <returns>Sequence with <paramref name="enumerable"/> elements randomly ordered.</returns>
        public static IEnumerable<TItem> GetShuffled<TItem>(this System.Random self, IEnumerable<TItem> enumerable)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNull(nameof(enumerable), enumerable);
            foreach (var item in enumerable.OrderBy(item => self.NextDouble())) { yield return item; }
        }

        #endregion

        #region GetChar Methods

        /// <summary>
        /// Range of alphabetic lower case characters (a-z);
        /// </summary>
        public static (char First, char Last) AlphabeticLowerCharacterRange { get; } = ('a', 'z');

        /// <summary>
        /// Range of alphabetic upper case characters (A-Z);
        /// </summary>
        public static (char First, char Last) AlphabeticUpperCharacterRange { get; } = ('A', 'Z');

        /// <summary>
        /// Range of numeric characters (0-9);
        /// </summary>
        public static (char First, char Last) NumericCharacterRange { get; } = ('0', '9');

        /// <summary>
        /// Range of symbolic characters, first of four (space-slash);
        /// </summary>
        public static (char First, char Last) SymbolicCharacterRange0 { get; } = (' ', '/');

        /// <summary>
        /// Range of symbolic characters, second of four (colon-at);
        /// </summary>
        public static (char First, char Last) SymbolicCharacterRange1 { get; } = (':', '@');

        /// <summary>
        /// Range of symbolic characters, third of four (left bracket-tick);
        /// </summary>
        public static (char First, char Last) SymbolicCharacterRange2 { get; } = ('[', '`');

        /// <summary>
        /// Range of symbolic characters, fourth of four (left brace-tilde);
        /// </summary>
        public static (char First, char Last) SymbolicCharacterRange3 { get; } = ('{', '~');

        /// <summary>
        /// Returns a random <see cref="char"/> from the alphanumeric character range.
        /// The range includes digits (0-9), lower case letters (a-z), and upper case letters (A-Z).
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>Random <see cref="char"/> from the alphanumeric character range.</returns>
        public static char GetChar(this Random self)
        {
            Throw.If.Self.IsNull(self);
            return self.GetChar(ECharRange.AlphanumericAny);
        }

        /// <summary>
        /// Returns a random <see cref="char"/> from the specified <see cref="ECharRange"/>
        /// character <paramref name="range"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="range">Enumeration value that indicates the character range.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>Random <see cref="char"/> from the specified <paramref name="range"/>.</returns>
        public static char GetChar(this Random self, ECharRange range)
        {
            Throw.If.Self.IsNull(self);
            switch (range)
            {
                case ECharRange.Numeric:
                { return (char)self.Next(NumericCharacterRange.First, NumericCharacterRange.Last); }
                case ECharRange.AlphabeticUpper:
                { return (char)self.Next(AlphabeticUpperCharacterRange.First, AlphabeticUpperCharacterRange.Last); }
                case ECharRange.AlphabeticLower:
                { return (char)self.Next(AlphabeticLowerCharacterRange.First, AlphabeticLowerCharacterRange.Last); }
                case ECharRange.AlphabeticAny:
                {
                    return GetChar(self, AlphabeticLowerCharacterRange, AlphabeticUpperCharacterRange);
                    //return (char)(self.GetBool() ? self.Next(97, 123) : self.Next(65, 91));
                }
                case ECharRange.AlphanumericUpper:
                {
                    return GetChar(self, NumericCharacterRange, AlphabeticUpperCharacterRange);
                    //if (self.GetBool(RandomExtensions.AlphanumericProbabilityNumericCased)) { return (char)self.Next(48, 58); }
                    //else { return (char)self.Next(65, 91); }
                }
                case ECharRange.AlphanumericLower:
                {
                    return GetChar(self, NumericCharacterRange, AlphabeticLowerCharacterRange);
                    //if (self.GetBool(RandomExtensions.AlphanumericProbabilityNumericCased)) { return (char)self.Next(48, 58); }
                    //else { return (char)self.Next(97, 123); }
                }
                case ECharRange.AlphanumericAny:
                {
                    return GetChar(self, NumericCharacterRange, AlphabeticUpperCharacterRange, AlphabeticLowerCharacterRange);
                    //if (self.GetBool(RandomExtensions.AlphanumericProbabilityNumericAny)) { return (char)self.Next(48, 58); }
                    //else { return (char)(self.GetBool() ? self.Next(97, 123) : self.Next(65, 91)); }
                }
                case ECharRange.Symbolic:
                {
                    return GetChar(self, SymbolicCharacterRange0, SymbolicCharacterRange1, SymbolicCharacterRange2, SymbolicCharacterRange3);
                    //// TODO: This does not give a proper distribution through the available
                    ////       characters. Should rewrite so each char has the same chance.
                    //int sub = self.Next(4);
                    //if (sub == 0) { return (char)self.Next(32, 48); }// !"#$%&'()*+,-./
                    //else if (sub == 1) { return (char)self.Next(58, 65); }// :;<=>?@
                    //else if (sub == 2) { return (char)self.Next(91, 97); }// [\]^_`
                    //else if (sub == 3) { return (char)self.Next(123, 127); }// {|}~
                    //else { throw new InvalidOperationException("This should never happen."); }
                }
                case ECharRange.Any:
                default:
                {
                    return GetChar(self, NumericCharacterRange, AlphabeticUpperCharacterRange, AlphabeticLowerCharacterRange, SymbolicCharacterRange0, SymbolicCharacterRange1, SymbolicCharacterRange2, SymbolicCharacterRange3);
                    //// TODO: This does not give a proper distribution through the available
                    ////       characters. Should rewrite so each char has the same chance.
                    //if (self.GetBool()) { return self.GetChar(ECharTypes.AlphanumericAny); }
                    //else { return self.GetChar(ECharTypes.Symbolic); }
                }
            }
        }

        /// <summary>
        /// Returns a random <see cref="char"/> from the specified <paramref name="sequences"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="sequences">Range tuples specifying the available character sets.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="sequences"/> is <c>null</c>.</exception>
        /// <returns>Random <see cref="char"/> from the specified range <paramref name="sequences"/>.</returns>
        public static char GetChar(this System.Random self, params (char First, char Last)[] sequences)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNull(nameof(sequences), sequences);

            int count = 0;
            for (int index = 0; index < sequences.Length; index++)
            { count += sequences[index].Last - sequences[index].First + 1; }

            int rand = self.Next(count);
            count = 0;
            for (int index = 0; index < sequences.Length; index++)
            {
                var sequence = sequences[index];
                int range = sequence.Last - sequence.First + 1;
                if (rand < count + range) { return (char)(rand - count + sequence.First); }
                count += range;
            }
            throw new InvalidOperationException($"Argument '{nameof(sequences)}' must contain at least one element that is non-null and non-empty.");
        }


        //public static char GetChar(this System.Random self, params IList<char>[] sequences)
        //{
        //    Throw.If.Self.IsNull(self);
        //    Throw.If.Arg.IsNull(nameof(sequences), sequences);

        //    int count = 0;
        //    for (int index = 0; index < sequences.Length; index++)
        //    { if (sequences[index].IsNotNull() && sequences[index].Count > 0) { count += sequences[index].Count; } }

        //    if (count == 0)
        //    {
        //        throw new InvalidOperationException($"Argument '{nameof(sequences)}' must contain at least one element that is non-null and non-empty.");
        //    }

        //    int rand = self.Next(count);
        //    count = 0;
        //    for (int index = 0; index < sequences.Length; index++)
        //    {
        //        var sequence = sequences[index];
        //        if (sequence.IsNull() || sequence.Count == 0) { continue; }
        //        if (rand < count + sequence.Count)
        //        {
        //            return sequence[rand - count];
        //        }
        //        count += sequences[index].Count;
        //    }
        //    throw new InvalidOperationException($"Argument '{nameof(sequences)}' must contain at least one element that is non-null and non-empty.");
        //}

        #endregion

        #region GetString Methods

        /// <summary>
        /// Returns a <see cref="string"/> composed of random <see cref="char"/> elements from the alphanumeric character range.
        /// The range includes digits (0-9), lower case letters (a-z), and upper case letters (A-Z).
        /// 
        /// Warning, this method allocates a char[length] each time it is called. If profiling shows these
        /// allocations as a bottleneck, then consider alternative methods instead.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="length">Length of generated string.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="length"/> is less than <c>0</c>.</exception>
        /// <returns>Random <see cref="string"/> composed of <see cref="char"/> from the alphanumeric character range.</returns>
        public static string GetString(this Random self, int length)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsLesser(nameof(length), length, 0);
            return self.GetString(length, ECharRange.AlphanumericAny);
        }

        /// <summary>
        /// Returns a <see cref="string"/> composed of random <see cref="char"/> elements from the specified
        /// <see cref="ECharRange"/> character <paramref name="range"/>.
        /// 
        /// Warning, this method allocates a char[length] each time it is called. If profiling shows these
        /// allocations as a bottleneck, then consider alternative methods instead.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="length">Length of generated string.</param>
        /// <param name="range">Enumeration value that indicates the character range.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="length"/> is less than <c>0</c>.</exception>
        /// <returns>Random <see cref="string"/> composed of <see cref="char"/> from the specified character range.</returns>
        /// <remarks>
        /// When porting to NET CORE 2.0 and NET STANDARD 2.1 the internals should be replaced
        /// with a string.Create() implementation.
        /// </remarks>
        public static string GetString(this Random self, int length, ECharRange range)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsLesser(nameof(length), length, 0);
            char[] chars = new char[length];
            for (int index = 0; index < length; ++index) { chars[index] = self.GetChar(range); }
            return new string(chars);
        }

        #endregion

    }
}
