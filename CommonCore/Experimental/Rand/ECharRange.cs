﻿#region Copyright (c) 2016 Jay Jeckel
// Copyright (c) 2016 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

namespace CommonCore.Experimental.Rand
{
    /// <summary>
    /// Specifies specific ranges of characters.
    /// </summary>
    /// <remarks>
    /// 0-9 == 48-57
    /// A-Z == 65-90
    /// a-z == 97-122
    ///  -/ == 32-47
    /// :-@ == 58-64
    /// [-` == 91-96
    /// {-~ == 123-126
    /// </remarks>
    public enum ECharRange : int
    {
        /// <summary>Indicates the range including all other <see cref="ECharRange"/> ranges.</summary>
        /// <remarks>Char range: </remarks>
        Any = 0,

        /// <summary>Indicates the alphabetic lower case range (a-z).</summary>
        /// <remarks>Char range: 97-122</remarks>
        AlphabeticLower,

        /// <summary>Indicates the alphabetic upper case range (A-Z).</summary>
        /// <remarks>Char range: 65-90</remarks>
        AlphabeticUpper,

        /// <summary>Indicates the alphabetic mixed case range (a-z and A-Z).</summary>
        /// <remarks>Char range: 65-90 and 97-122</remarks>
        AlphabeticAny,

        /// <summary>Indicates the alphanumeric lower case range (0-9 and a-z).</summary>
        /// <remarks>Char range: 48-57 and 97-122</remarks>
        AlphanumericLower,

        /// <summary>Indicates the alphanumeric upper case range (0-9 and A-Z).</summary>
        /// <remarks>Char range: 48-57 and 65-90</remarks>
        AlphanumericUpper,

        /// <summary>Indicates the alphanumeric mixed case range (0-9, a-z, and A-Z).</summary>
        /// <remarks>Char range: 48-57, 65-90, and 97-122</remarks>
        AlphanumericAny,

        /// <summary>Indicates the numeric range (0-9).</summary>
        /// <remarks>Char range: 48-57</remarks>
        Numeric,

        /// <summary>Indicates the symbolic range (!-/, :-@, [-`, and {-~).</summary>
        /// <remarks>Char range: 33-47, 58-64, 91-96, and 123-126</remarks>
        Symbolic
    }
}
