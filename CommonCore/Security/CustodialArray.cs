﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using System.Security;

namespace CommonCore.Security
{
    /// <summary>
    /// The <see cref="CustodialArray{TValue}" /> <c>disposable</c> <c>generic</c> <c>class</c> is
    /// meant to replace raw <see cref="byte" /> and <see cref="char" /> arrays of sensitive clear
    /// text data, such as when processing <see cref="SecureString" /> secrets as unencrypted clear text.
    /// <para>
    /// Several steps are taken to reduce the risk space of the exposed data. First,
    /// <see cref="CustodialArray{TValue}" /> is disposable so that the life cycle of its data can
    /// be controlled; secondly, the enclosed array is pinned in memory when it is created, so that
    /// the Garbage Collector will refrain from moving it around and leaving ghost copies of the
    /// sensitive information; lastly, when the object is disposed, efforts are taken to destroy the
    /// contained data.
    /// </para>
    /// </summary>
    public sealed class CustodialArray<TValue> : IDisposable, IEnumerable<TValue>
    {
        /// <summary>
        /// Initializes a <see cref="CustodialArray{TValue}" /> instance hosting a new pinned array
        /// that will have its data destroyed when this instance is disposed.
        /// </summary>
        /// <param name="length">The length of the hosted array.</param>
        public CustodialArray(int length)
        {
            Throw.If.Arg.IsLesser(nameof(length), length, 0);
            this.Content = new TValue[length];
            this.ContentHandle = GCHandle.Alloc(this.Content, GCHandleType.Pinned);
        }

        /// <summary>
        /// Gets the <see cref="GCHandle"/> of the hosted pinned array.
        /// </summary>
        public GCHandle ContentHandle { get; }

        /// <summary>
        /// Gets the hosted pinned array.
        /// </summary>
        public TValue[] Content { get; private set; }

        /// <summary>
        /// Gets the length of the hosted pinned array.
        /// </summary>
        public int Length => this.Content.Length;

        /// <summary>
        /// Gets or sets the element of the hosted pinned array at the specified <paramref name="index"/>.
        /// </summary>
        /// <param name="index">Index of the target element.</param>
        public TValue this[int index] { get => this.Content[index]; set => this.Content[index] = value; }

        /// <summary>
        /// Implicitly convert a <see cref="CustodialArray{TValue}"/> object to its hosted pinned array.
        /// </summary>
        /// <param name="source">PinnedArray object to be converted.</param>
        public static implicit operator TValue[](CustodialArray<TValue> source) => source.Content;

        #region IDisposable Support

        /// <summary>
        /// Finalizer ensuring unmanaged resources are disposed.
        /// </summary>
        [ExcludeFromCodeCoverage]
        ~CustodialArray() { this.Dispose(false); }

        /// <summary>
        /// Gets whether the current object has been disposed.
        /// </summary>
        public bool Disposed { get; private set; } = false;
        
        /// <summary>
        /// Releases all resources used by the current object.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases managed and/or unmanaged resources used by the current object.
        /// </summary>
        /// <param name="disposing">Should all resources be released, else only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (!this.Disposed)
            {
                if (disposing) { }
                
                if (this.Content != null) { Array.Clear(this.Content, 0, this.Content.Length); }

                this.ContentHandle.Free();
                this.Content = null;

                this.Disposed = true;
            }
        }

        #endregion

        #region IEnumerable Support

        /// <summary>
        /// Returns an enumerator that iterates through the array elements.
        /// </summary>
        [ExcludeFromCodeCoverage]
        public IEnumerator<TValue> GetEnumerator() => ((IEnumerable<TValue>)this.Content).GetEnumerator();

        /// <summary>
        /// Returns an enumerator that iterates through the array elements.
        /// </summary>
        [ExcludeFromCodeCoverage]
        IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable<TValue>)this.Content).GetEnumerator();

        #endregion
    }
}
