﻿//#region Copyright (c) 2017 Jay Jeckel
//// Copyright (c) 2017 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//using System;
//using System.Security;
//using System.Text;

//namespace CommonCore.Security
//{
//    /// <summary>
//    /// The <see cref="SecureStringReader"/> disposable class encapsulates the
//    /// <see cref="char"/> and <see cref="byte"/> arrays representing the clear
//    /// text data of a <see cref="SecureString"/> secret, allowing control over
//    /// the life cycle in memory of the sensitive clear text data through the use
//    /// of <see cref="SecurePinnedArray{TValue}"/>.
//    /// </summary>
//    // ToDo: Implement lazy creation on the byte and char arrays.
//    public sealed class SecureStringReader : IDisposable
//    {
//        /// <summary>
//        /// Initializes a <see cref="SecureStringReader"/> instance from a <see cref="SecureString"/>.
//        /// A copy of the secret will be made and responsibility for disposal of the passed instance
//        /// is left to the calling code.
//        /// </summary>
//        /// <param name="secret">The secure secret used to initialize the reader.</param>
//        public SecureStringReader(SecureString secret)
//        {
//            this.Secret = secret.Copy();
//            this.Bytes = secret.ToByteArray();
//            this.Chars = secret.ToCharArray();
//        }

//        /// <summary>
//        /// Initializes a <see cref="SecureStringReader"/> instance from a raw <see cref="char"/> array.
//        /// Using this constructor gives ownership of the passed array to the reader
//        /// and the array's data will be destroyed at disposal time as if the reader
//        /// created the array internally.
//        /// </summary>
//        /// <param name="chars">The clear text array used to initialize the reader.</param>
//        public SecureStringReader(char[] chars)
//        {
//            this.Chars = new SecurePinnedArray<char>(chars);
//            this.Bytes = new SecurePinnedArray<byte>(Encoding.Unicode.GetBytes(this.Chars));
//            this.Secret = new SecureString().AppendChars(this.Chars, true);
//        }

//        /// <summary>
//        /// Initializes a <see cref="SecureStringReader"/> instance from a raw <see cref="byte"/> array.
//        /// Using this constructor gives ownership of the passed array to the reader
//        /// and the array's data will be destroyed at disposal time as if the reader
//        /// created the array internally.
//        /// <para>
//        /// The passed bytes are assumed to be encoded in UTF16 <see cref="Encoding.Unicode"/>.
//        /// If that is not the case, then the appropriate <see cref="Encoding"/> should
//        /// be specified to convert the bytes to chars.
//        /// </para>
//        /// </summary>
//        /// <param name="bytes">The clear text array used to initialize the reader.</param>
//        /// <param name="encoding">
//        /// Optional encoding for conversion from bytes to chars.
//        /// If omitted, <see cref="Encoding.Unicode"/>, ie UTF16, will be used.
//        /// </param>
//        public SecureStringReader(byte[] bytes, Encoding encoding = null)
//        {
//            this.Bytes = new SecurePinnedArray<byte>(bytes);
//            this.Chars = new SecurePinnedArray<char>((encoding ?? Encoding.Unicode).GetChars(this.Bytes));
//            this.Secret = new SecureString().AppendChars(this.Chars, true);
//        }

//        /// <summary>
//        /// Gets the <see cref="SecureString"/> representing the encrypted secret.
//        /// </summary>
//        public SecureString Secret { get; private set; } = null;

//        /// <summary>
//        /// Gets the <see cref="SecurePinnedArray{Byte}"/> representing the clear text bytes
//        /// of the secret.
//        /// </summary>
//        public SecurePinnedArray<byte> Bytes { get; private set; } = null;

//        /// <summary>
//        /// Gets the <see cref="SecurePinnedArray{Char}"/> representing the clear text chars
//        /// of the secret.
//        /// </summary>
//        public SecurePinnedArray<char> Chars { get; private set; } = null;

//        /// <summary>
//        /// Gets the length of the <see cref="SecureString"/> secret.
//        /// </summary>
//        public int Length => this.Secret?.Length ?? 0;

//        /// <summary>
//        /// Gets the length of the <see cref="byte"/> array.
//        /// </summary>
//        public int ByteLength => this.Bytes?.Length ?? 0;

//        /// <summary>
//        /// Gets the length of the <see cref="char"/> array.
//        /// </summary>
//        public int CharLength => this.Chars?.Length ?? 0;

//#if DEBUG

//        /// <summary>
//        /// Gets a new <see cref="string"/> representation of the <see cref="byte"/> array for debug purposes.
//        /// This property should NEVER be used in production.
//        /// </summary>
//        public string DebugByteText => Encoding.Unicode.GetString(this.Bytes);

//        /// <summary>
//        /// Gets a new <see cref="string"/> representation of the <see cref="char"/> array for debug purposes.
//        /// This property should NEVER be used in production.
//        /// </summary>
//        public string DebugCharText => new string(this.Chars);

//#endif

//        /// <summary>
//        /// Implicitly convert a <see cref="SecureStringReader"/> instance to its hosted <see cref="SecureString"/> secret.
//        /// </summary>
//        /// <param name="reader">SecureStringReader instance to be converted.</param>
//        public static implicit operator SecureString(SecureStringReader reader) => reader.Secret;

//        /// <summary>
//        /// Implicitly convert a <see cref="SecureStringReader"/> instance to its hosted <see cref="byte"/> array.
//        /// </summary>
//        /// <param name="reader">SecureStringReader instance to be converted.</param>
//        public static implicit operator SecurePinnedArray<byte>(SecureStringReader reader) => reader.Bytes;

//        /// <summary>
//        /// Implicitly convert a <see cref="SecureStringReader"/> instance to its hosted <see cref="char"/> array.
//        /// </summary>
//        /// <param name="reader">SecureStringReader instance to be converted.</param>
//        public static implicit operator SecurePinnedArray<char>(SecureStringReader reader) => reader.Chars;

//        #region IDisposable Support

//        /// <summary>
//        /// Gets whether the current instance has been disposed.
//        /// </summary>
//        public bool Disposed { get; private set; } = false;

//        /// <summary>
//        /// Releases all resources used by the current instance.
//        /// </summary>
//        public void Dispose() => this.Dispose(true);

//        /// <summary>
//        /// Releases managed and/or unmanaged resources used by the current instance.
//        /// </summary>
//        /// <param name="disposing">Should all resources be released, else only unmanaged resources.</param>
//        private void Dispose(bool disposing)
//        {
//            if (!this.Disposed)
//            {
//                if (disposing)
//                {
//                    this.Secret?.Dispose();
//                    this.Secret = null;

//                    this.Bytes?.Dispose();
//                    this.Bytes = null;

//                    this.Chars?.Dispose();
//                    this.Chars = null;
//                }

//                this.Disposed = true;
//            }
//        }

//        #endregion
//    }
//}
