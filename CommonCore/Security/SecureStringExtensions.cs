﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security;

namespace CommonCore.Security
{
    /// <summary>
    /// The <see cref="SecureStringExtensions"/> <c>static</c> <c>class</c> provides
    /// <see cref="SecureString"/> related extension methods.
    /// </summary>
    public static class SecureStringExtensions
    {
        /// <summary>
        /// Appends the content of the specified <see cref="char" /> array to this
        /// <see cref="SecureString" />, optionally marking the this instance as readonly and
        /// preventing further modifications.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="chars">Array of <see cref="char" /> to append.</param>
        /// <param name="makeReadOnly">
        /// Optionally specifies whether this instance should be made readonly after appending.
        /// </param>
        /// <returns>This <see cref="SecureString" /> instance.</returns>
        public static SecureString AppendChars(this SecureString self, char[] chars, bool makeReadOnly = true)
        {
            for (int index = 0; index < chars.Length; index++) { self.AppendChar(chars[index]); }
            if (makeReadOnly) { self.MakeReadOnly(); }
            return self;
        }

        /// <summary>
        /// Creates a <see cref="CustodialArray{Byte}"/> instance representing the clear text secret.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The clear text secret as a <see cref="CustodialArray{Byte}"/> instance.</returns>
        public static CustodialArray<byte> ToByteArray(this SecureString self)
        {
            var bytes = new CustodialArray<byte>(self.Length * 2);
            var pointer = IntPtr.Zero;
            try
            {
                pointer = Marshal.SecureStringToGlobalAllocUnicode(self);
                Marshal.Copy(pointer, bytes, 0, bytes.Length);
                return bytes;
            }
            finally { if (pointer != IntPtr.Zero) { Marshal.ZeroFreeGlobalAllocUnicode(pointer); } }
        }

        /// <summary>
        /// Creates a <see cref="CustodialArray{Char}"/> instance representing the clear text secret.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The clear text secret as a <see cref="CustodialArray{Char}"/> instance.</returns>
        public static CustodialArray<char> ToCharArray(this SecureString self)
        {
            var chars = new CustodialArray<char>(self.Length);
            var pointer = IntPtr.Zero;
            try
            {
                pointer = Marshal.SecureStringToGlobalAllocUnicode(self);
                Marshal.Copy(pointer, chars, 0, chars.Length);
                return chars;
            }
            finally { if (pointer != IntPtr.Zero) { Marshal.ZeroFreeGlobalAllocUnicode(pointer); } }
        }

        /// <summary>
        /// Provides an enumeration over each <see cref="byte" /> of the clear text secret. Warning,
        /// the enumerator must have its <see cref="IDisposable.Dispose" /> method called, such as
        /// by a using statement, otherwise this method can leak pointers.
        /// </summary>
        /// <remarks>
        /// Enumerating through the secret provides a non-zero reduction in risk surface by
        /// marshaling only one clear text <see cref="byte" /> at a time into managed memory. This
        /// is in contrast to marshaling the entire clear text secret into managed memory in the
        /// form of an array.
        /// </remarks>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Enumerable suitable for looping through the clear text of the secret.</returns>
        public static IEnumerable<byte> AsByteSequence(this SecureString self)
        {
            IntPtr pointer = IntPtr.Zero;
            try
            {
                pointer = Marshal.SecureStringToGlobalAllocUnicode(self);
                int length = self.Length * 2;
                for (int offset = 0; offset < length; offset++)
                {
                    yield return Marshal.ReadByte(pointer, offset);
                }
            }
            finally { if (pointer != IntPtr.Zero) { Marshal.ZeroFreeGlobalAllocUnicode(pointer); } }
        }

        /// <summary>
        /// Provides an enumeration over each <see cref="char" /> of the clear text secret. Warning,
        /// the enumerator must have its <see cref="IDisposable.Dispose" /> method called, such as
        /// by a using statement, otherwise this method can leak pointers.
        /// </summary>
        /// <remarks>
        /// Enumerating through the secret provides a non-zero reduction in risk surface by
        /// marshaling only one clear text <see cref="char" /> at a time into managed memory. This
        /// is in contrast to marshaling the entire clear text secret into managed memory in the
        /// form of an array.
        /// </remarks>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Enumerable suitable for looping through the clear text of the secret.</returns>
        public static IEnumerable<char> AsCharSequence(this SecureString self)
        {
            IntPtr pointer = IntPtr.Zero;
            try
            {
                pointer = Marshal.SecureStringToGlobalAllocUnicode(self);
                int length = self.Length * 2;
                for (int offset = 0; offset < length; offset += 2)
                {
                    yield return (char)Marshal.ReadInt16(pointer, offset);
                }
            }
            finally { if (pointer != IntPtr.Zero) { Marshal.ZeroFreeGlobalAllocUnicode(pointer); } }
        }

        //public static SecureString AppendEncryptedString(this SecureString self, string encrypted, byte[] entropy, bool makeReadOnly = false)
        //{
        //    using (var desecured = new SecureStringReader(DataProtectHelper.DecryptData(Convert.FromBase64String(encrypted), entropy)))
        //    { self.AppendChars(desecured); }
        //    if (makeReadOnly) { self.MakeReadOnly(); }
        //    return self;
        //}

        //public static string ToEncryptedString(this SecureString self, byte[] entropy)
        //    => DataProtectHelper.EncryptString(self, entropy);

    }
}
