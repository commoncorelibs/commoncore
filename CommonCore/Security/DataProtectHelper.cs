﻿//#region Copyright (c) 2017 Jay Jeckel
//// Copyright (c) 2017 Jay Jeckel
//// Licensed under the MIT license: https://opensource.org/licenses/MIT
//// Permission is granted to use, copy, modify, and distribute the work.
//// Full license information available in the project LICENSE file.
//#endregion

//using System;
//using System.Security;
//using System.Security.Cryptography;

//namespace CommonCore.Security
//{
//    /// <summary>
//    /// A static helper class to simplify encrypting and decrypting sensitive data using
//    /// the <see cref="ProtectedData.Protect"/> and <see cref="ProtectedData.Unprotect"/> methods.
//    /// </summary>
//    public static class DataProtectHelper
//    {
//        /// <summary>
//        /// Create an array of 16 random bytes using the <see cref="RNGCryptoServiceProvider"/>.
//        /// When entropy is used to encrypt data, then that same entropy must be used in decryption.
//        /// See <see cref="DataProtectHelper"/> for information about entropy and its use.
//        /// </summary>
//        public static byte[] CreateRandomEntropy()
//        {
//            byte[] entropy = new byte[16];
//            using (var rand = new RNGCryptoServiceProvider()) { rand.GetBytes(entropy); }
//            return entropy;
//        }

//        /// <summary>
//        /// Encrypt the byte array data using the DPAPI.
//        /// </summary>
//        /// <param name="data">Unencrypted data to be encrypted.</param>
//        /// <param name="entropy">
//        /// Bytes to increase the complexity of the encryption.
//        /// When entropy is used to encrypt data, then that same entropy must be used in decryption.
//        /// See <see cref="DataProtectHelper"/> for information about entropy and its use.
//        /// </param>
//        /// <param name="scope">Scope the data will be encrypted under.</param>
//        public static byte[] EncryptData(byte[] data, byte[] entropy, DataProtectionScope scope = DataProtectionScope.CurrentUser)
//            => ProtectedData.Protect(data, entropy, scope);

//        /// <summary>
//        /// Encrypt the <see cref="SecureString"/> data to a base-64 encoded string using the DPAPI.
//        /// See <see cref="DataProtectHelper"/> for information about <see cref="SecureString"/> and its use.
//        /// </summary>
//        /// <param name="data">
//        /// Unencrypted data to be encrypted.
//        /// See <see cref="DataProtectHelper"/> for information about <see cref="SecureString"/> and its use.
//        /// </param>
//        /// <param name="entropy">
//        /// Bytes to increase the complexity of the encryption.
//        /// When entropy is used to encrypt data, then that same entropy must be used in decryption.
//        /// See <see cref="DataProtectHelper"/> for information about entropy and its use.
//        /// </param>
//        /// <param name="scope">Scope the data will be encrypted under.</param>
//        public static string EncryptString(SecureString data, byte[] entropy, DataProtectionScope scope = DataProtectionScope.CurrentUser)
//        { using (var desecured = new SecureStringReader(data)) { return Convert.ToBase64String(EncryptData(desecured.Bytes, entropy, scope)); } }






//        /// <summary>
//        /// Decrypt the encrypted byte array data using the DPAPI.
//        /// </summary>
//        /// <param name="data">Encrypted data to be unencrypted.</param>
//        /// <param name="entropy">
//        /// Bytes to increase the complexity of the encryption.
//        /// When entropy is used to encrypt data, then that same entropy must be used in decryption.
//        /// See <see cref="DataProtectHelper"/> for information about entropy and its use.
//        /// </param>
//        /// <param name="scope">Scope the data was encrypted under.</param>
//        public static byte[] DecryptData(byte[] data, byte[] entropy, DataProtectionScope scope = DataProtectionScope.CurrentUser)
//            => ProtectedData.Unprotect(data, entropy, scope);

//        /// <summary>
//        /// Decrypt the encrypted base-64 encoded string data using the DPAPI.
//        /// </summary>
//        /// <param name="data">Encrypted data to be unencrypted.</param>
//        /// <param name="entropy">
//        /// Bytes to increase the complexity of the encryption.
//        /// When entropy is used to encrypt data, then that same entropy must be used in decryption.
//        /// See <see cref="DataProtectHelper"/> for information about entropy and its use.
//        /// </param>
//        /// <param name="scope">Scope the data was encrypted under.</param>
//        public static SecureString DecryptString(string data, byte[] entropy, DataProtectionScope scope = DataProtectionScope.CurrentUser)
//        {
//            // Note: The byte[] returned by Convert.FromBase64String(data) is an array of the
//            // encrypted bytes, so it doesn't need to be manually cleared in a try/catch/finally.
//            using (var desecured = new SecureStringReader(DecryptData(Convert.FromBase64String(data), entropy, scope)))
//            { return desecured.Secret.Copy(); }
//        }
//    }
//}
