﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;

namespace CommonCore.Security.Passwords.Analysis
{

    #region Bonus Analysis Rules

    /// <inheritdoc/>
    public class LengthBonusAnalysisRule : AAnalysisRule
    {
        /// <inheritdoc cref="AAnalysisRule.AAnalysisRule(string, string, string, string)"/>
        public LengthBonusAnalysisRule()
            : base(nameof(LengthBonusAnalysisRule), "Password Length", "", "+(length * 4)") { }

        /// <inheritdoc/>
        public override AnalysisRuleResult Analyze(char[] password)
            => new AnalysisRuleResult(this, password.Length, password.Length * 4);
    }

    /// <summary>
    /// A <see cref="AAnalysisRule"/> derived class representing a rule used to analyze a password.
    /// </summary>
    public class LetterUpperBonusAnalysisRule : AAnalysisRule
    {
        /// <inheritdoc cref="AAnalysisRule.AAnalysisRule(string, string, string, string)"/>
        public LetterUpperBonusAnalysisRule()
            : base(nameof(LetterUpperBonusAnalysisRule), "Uppercase Letters", "", "(count > 0) +((length - count) * 2)") { }

        /// <inheritdoc/>
        public override AnalysisRuleResult Analyze(char[] password)
        {
            int count = AnalysisHelper.CountMatches(password, AnalysisHelper.IsLetterUpper);
            int value = (count == 0 ? 0 : ((password.Length - count) * 2));
            return new AnalysisRuleResult(this, count, value);
        }
    }

    /// <summary>
    /// A <see cref="AAnalysisRule"/> derived class representing a rule used to analyze a password.
    /// </summary>
    public class LetterLowerBonusAnalysisRule : AAnalysisRule
    {
        /// <inheritdoc cref="AAnalysisRule.AAnalysisRule(string, string, string, string)"/>
        public LetterLowerBonusAnalysisRule()
            : base(nameof(LetterLowerBonusAnalysisRule), "Lowercase Letters", "", "(count > 0) +((length - count) * 2)") { }

        /// <inheritdoc/>
        public override AnalysisRuleResult Analyze(char[] password)
        {
            int count = AnalysisHelper.CountMatches(password, AnalysisHelper.IsLetterLower);
            int value = (count == 0 ? 0 : ((password.Length - count) * 2));
            return new AnalysisRuleResult(this, count, value);
        }
    }

    /// <summary>
    /// A <see cref="AAnalysisRule"/> derived class representing a rule used to analyze a password.
    /// </summary>
    public class DigitBonusAnalysisRule : AAnalysisRule
    {
        /// <inheritdoc cref="AAnalysisRule.AAnalysisRule(string, string, string, string)"/>
        public DigitBonusAnalysisRule()
            : base(nameof(DigitBonusAnalysisRule), "Numbers", "", "+(count * 4)") { }

        /// <inheritdoc/>
        public override AnalysisRuleResult Analyze(char[] password)
        {
            int count = AnalysisHelper.CountMatches(password, AnalysisHelper.IsDigit);
            int value = (count * 4);
            return new AnalysisRuleResult(this, count, value);
        }
    }

    /// <summary>
    /// A <see cref="AAnalysisRule"/> derived class representing a rule used to analyze a password.
    /// </summary>
    public class SymbolBonusAnalysisRule : AAnalysisRule
    {
        /// <inheritdoc cref="AAnalysisRule.AAnalysisRule(string, string, string, string)"/>
        public SymbolBonusAnalysisRule()
            : base(nameof(SymbolBonusAnalysisRule), "Symbols", "", "+(count * 6)") { }

        /// <inheritdoc/>
        public override AnalysisRuleResult Analyze(char[] password)
        {
            int count = AnalysisHelper.CountMatches(password, AnalysisHelper.IsSymbol);
            int value = (count * 6);
            return new AnalysisRuleResult(this, count, value);
        }
    }

    /// <summary>
    /// A <see cref="AAnalysisRule"/> derived class representing a rule used to analyze a password.
    /// </summary>
    public class MiddleDigitOrSymbolBonusAnalysisRule : AAnalysisRule
    {
        /// <inheritdoc cref="AAnalysisRule.AAnalysisRule(string, string, string, string)"/>
        public MiddleDigitOrSymbolBonusAnalysisRule()
            : base(nameof(MiddleDigitOrSymbolBonusAnalysisRule), "Middle Numbers or Symbols", "", "(count > 0) +(count * 6)") { }

        /// <inheritdoc/>
        public override AnalysisRuleResult Analyze(char[] password)
        {
            if (password.Length < 3) { return new AnalysisRuleResult(this, 0, 0); }
            int count = AnalysisHelper.CountMatches(password, 1, password.Length - 2, AnalysisHelper.IsDigit, AnalysisHelper.IsSymbol);
            int value = (count == 0 ? 0 : (count * 6));
            return new AnalysisRuleResult(this, count, value);
        }
    }

    /// <summary>
    /// A <see cref="AAnalysisRule"/> derived class representing a rule used to analyze a password.
    /// </summary>
    public class RequirementsBonusAnalysisRule : AAnalysisRule
    {
        /// <inheritdoc cref="AAnalysisRule.AAnalysisRule(string, string, string, string)"/>
        public RequirementsBonusAnalysisRule()
            : base(nameof(RequirementsBonusAnalysisRule), "Requirements", "", "+(count * 2)") { }

        /// <inheritdoc/>
        public override AnalysisRuleResult Analyze(char[] password)
        {
            int count = 0;
            if (password.Length >= 8) { count++; }

            for (var index = 0; index < password.Length; index++) { if (AnalysisHelper.IsLetterUpper(password[index])) { count++; break; } }
            for (var index = 0; index < password.Length; index++) { if (AnalysisHelper.IsLetterLower(password[index])) { count++; break; } }
            for (var index = 0; index < password.Length; index++) { if (AnalysisHelper.IsDigit(password[index])) { count++; break; } }
            for (var index = 0; index < password.Length; index++) { if (AnalysisHelper.IsSymbol(password[index])) { count++; break; } }
            int bonus = (count == 0 ? 0 : (count * 2));
            return new AnalysisRuleResult(this, count, bonus);
        }
    }

    #endregion

    #region Malus Analysis Rules

    /// <summary>
    /// A <see cref="AAnalysisRule"/> derived class representing a rule used to analyze a password.
    /// </summary>
    public class LetterOnlyMalusAnalysisRule : AAnalysisRule
    {
        /// <inheritdoc cref="AAnalysisRule.AAnalysisRule(string, string, string, string)"/>
        public LetterOnlyMalusAnalysisRule()
                : base(nameof(LetterOnlyMalusAnalysisRule), "Letters Only", "", "-length") { }

        /// <inheritdoc/>
        public override AnalysisRuleResult Analyze(char[] password)
        {
            var value = 0;
            if (!AnalysisHelper.ContainsNonMatch(password, AnalysisHelper.IsLetterOrWhiteSpace)) { value = password.Length; }
            return new AnalysisRuleResult(this, value, value);
        }
    }

    /// <summary>
    /// A <see cref="AAnalysisRule"/> derived class representing a rule used to analyze a password.
    /// </summary>
    public class DigitOnlyMalusAnalysisRule : AAnalysisRule
    {
        /// <inheritdoc cref="AAnalysisRule.AAnalysisRule(string, string, string, string)"/>
        public DigitOnlyMalusAnalysisRule()
            : base(nameof(DigitOnlyMalusAnalysisRule), "Numbers Only", "", "-length") { }

        /// <inheritdoc/>
        public override AnalysisRuleResult Analyze(char[] password)
        {
            var value = 0;
            if (!AnalysisHelper.ContainsNonMatch(password, AnalysisHelper.IsDigitOrWhiteSpace)) { value = password.Length; }
            return new AnalysisRuleResult(this, value, value);
        }
    }

    /// <summary>
    /// A <see cref="AAnalysisRule"/> derived class representing a rule used to analyze a password.
    /// </summary>
    public class RepeatCharactersMalusAnalysisRule : AAnalysisRule
    {
        /// <inheritdoc cref="AAnalysisRule.AAnalysisRule(string, string, string, string)"/>
        public RepeatCharactersMalusAnalysisRule()
            : base(nameof(RepeatCharactersMalusAnalysisRule), "Repeat Characters (Case Insensitive)", "", "?") { }

        /// <inheritdoc/>
        public override AnalysisRuleResult Analyze(char[] password)
        {
            int count = 0;
            double value = 0.0;
            for (var a = 0; a < password.Length; a++)
            {
                var found = false;
                for (var b = 0; b < password.Length; b++)
                {
                    if (password[a] == password[b] && a != b)
                    {
                        found = true;
                        /* 
                        Calculate increment deduction based on proximity to identical characters.
                        Deduction is incremented each time a new match is discovered.
                        Deduction amount is based on total password length divided by the
                        difference of distance between currently selected match.
                        */
                        value += Math.Abs(password.Length / (double)(b - a));
                    }
                }
                if (found)
                {
                    count++;
                    int unique = password.Length - count;
                    value = Math.Ceiling(unique == 0 ? value : value / (double)unique);
                }
            }
            return new AnalysisRuleResult(this, count, (int)Math.Ceiling(value));
        }
    }

    /// <summary>
    /// A <see cref="AAnalysisRule"/> derived class representing a rule used to analyze a password.
    /// </summary>
    public class ConsecutiveLetterUpperMalusAnalysisRule : AAnalysisRule
    {
        /// <inheritdoc cref="AAnalysisRule.AAnalysisRule(string, string, string, string)"/>
        public ConsecutiveLetterUpperMalusAnalysisRule()
            : base(nameof(ConsecutiveLetterUpperMalusAnalysisRule), "Consecutive Uppercase Letters", "", "-(count * 2)") { }

        /// <inheritdoc/>
        public override AnalysisRuleResult Analyze(char[] password)
        {
            int count = AnalysisHelper.CountConsecutiveMatches(password, AnalysisHelper.IsLetterUpper);
            int value = (count * 2);
            return new AnalysisRuleResult(this, count, value);
        }
    }

    /// <summary>
    /// A <see cref="AAnalysisRule"/> derived class representing a rule used to analyze a password.
    /// </summary>
    public class ConsecutiveLetterLowerMalusAnalysisRule : AAnalysisRule
    {
        /// <inheritdoc cref="AAnalysisRule.AAnalysisRule(string, string, string, string)"/>
        public ConsecutiveLetterLowerMalusAnalysisRule()
            : base(nameof(ConsecutiveLetterLowerMalusAnalysisRule), "Consecutive Lowercase Letters", "", "-(count * 2)") { }

        /// <inheritdoc/>
        public override AnalysisRuleResult Analyze(char[] password)
        {
            int count = AnalysisHelper.CountConsecutiveMatches(password, AnalysisHelper.IsLetterLower);
            int value = (count * 2);
            return new AnalysisRuleResult(this, count, value);
        }
    }

    /// <summary>
    /// A <see cref="AAnalysisRule"/> derived class representing a rule used to analyze a password.
    /// </summary>
    public class ConsecutiveDigitMalusAnalysisRule : AAnalysisRule
    {
        /// <inheritdoc cref="AAnalysisRule.AAnalysisRule(string, string, string, string)"/>
        public ConsecutiveDigitMalusAnalysisRule()
            : base(nameof(ConsecutiveDigitMalusAnalysisRule), "Consecutive Numbers", "", "-(count * 2)") { }

        /// <inheritdoc/>
        public override AnalysisRuleResult Analyze(char[] password)
        {
            int count = AnalysisHelper.CountConsecutiveMatches(password, AnalysisHelper.IsDigit);
            int value = (count * 2);
            return new AnalysisRuleResult(this, count, value);
        }
    }

    /// <summary>
    /// A <see cref="AAnalysisRule"/> derived class representing a rule used to analyze a password.
    /// </summary>
    public class SequentialLetterMalusAnalysisRule : AAnalysisRule
    {
        /// <summary>
        /// Gets the set of characters used for sequence comparison analysis.
        /// </summary>
        public static string Sequence { get; } = "abcdefghijklmnopqrstuvwxyz";

        /// <inheritdoc cref="AAnalysisRule.AAnalysisRule(string, string, string, string)"/>
        public SequentialLetterMalusAnalysisRule()
            : base(nameof(SequentialLetterMalusAnalysisRule), "Sequential Letters (3+)", "", "-(count * 3)") { }

        /// <inheritdoc/>
        public override AnalysisRuleResult Analyze(char[] password)
        {
            int count = 0;
            for (int index = 0; index < password.Length - 2; index++)
            { if (AnalysisHelper.CompareSequence(password, index, SequentialLetterMalusAnalysisRule.Sequence, 3)) { count++; } }
            int value = (count * 3);
            return new AnalysisRuleResult(this, count, value);
        }
    }

    /// <summary>
    /// A <see cref="AAnalysisRule"/> derived class representing a rule used to analyze a password.
    /// </summary>
    public class SequentialDigitMalusAnalysisRule : AAnalysisRule
    {
        /// <summary>
        /// Gets the set of characters used for sequence comparison analysis.
        /// </summary>
        public static string Sequence { get; } = "0123456789";

        /// <inheritdoc cref="AAnalysisRule.AAnalysisRule(string, string, string, string)"/>
        public SequentialDigitMalusAnalysisRule()
            : base(nameof(SequentialDigitMalusAnalysisRule), "Sequential Numbers (3+)", "", "-(count * 3)") { }

        /// <inheritdoc/>
        public override AnalysisRuleResult Analyze(char[] password)
        {
            int count = 0;
            for (int index = 0; index < password.Length - 2; index++)
            { if (AnalysisHelper.CompareSequence(password, index, SequentialDigitMalusAnalysisRule.Sequence, 3)) { count++; } }
            int value = (count * 3);
            return new AnalysisRuleResult(this, count, value);
        }
    }

    /// <summary>
    /// A <see cref="AAnalysisRule"/> derived class representing a rule used to analyze a password.
    /// </summary>
    public class SequentialSymbolMalusAnalysisRule : AAnalysisRule
    {
        /// <summary>
        /// Gets the set of characters used for sequence comparison analysis.
        /// </summary>
        public static string Sequence { get; } = "~!@#$%^&*()_+";

        /// <inheritdoc cref="AAnalysisRule.AAnalysisRule(string, string, string, string)"/>
        public SequentialSymbolMalusAnalysisRule()
            : base(nameof(SequentialSymbolMalusAnalysisRule), "Sequential Symbols (3+)", "", "-(count * 3)") { }

        /// <inheritdoc/>
        public override AnalysisRuleResult Analyze(char[] password)
        {
            int count = 0;
            for (int index = 0; index < password.Length - 2; index++)
            { if (AnalysisHelper.CompareSequence(password, index, SequentialSymbolMalusAnalysisRule.Sequence, 3)) { count++; } }
            int value = (count * 3);
            return new AnalysisRuleResult(this, count, value);
        }
    }

    #endregion

}
