﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Collections.Generic;
using System.Linq;

namespace CommonCore.Security.Passwords.Analysis
{
    /// <summary>
    /// The <see cref="AnalysisResult"/> <c>readonly</c> <c>struct</c> represents the collated
    /// results of analyzing a password with multiple rules.
    /// </summary>
    public readonly struct AnalysisResult
    {
        /// <summary>
        /// Gets the default empty instance of the struct.
        /// </summary>
        public static AnalysisResult Empty { get; } = default;

        /// <summary>
        /// Initializes a <see cref="AnalysisResult"/> instance with the specified
        /// <paramref name="bonuses"/> and <paramref name="maluses"/>
        /// <see cref="AnalysisRuleResult"/> readonly lists.
        /// </summary>
        /// <param name="bonuses">The readonly list of bonus-rule results.</param>
        /// <param name="maluses">The readonly list of malus-rule results.</param>
        public AnalysisResult(IReadOnlyList<AnalysisRuleResult> bonuses, IReadOnlyList<AnalysisRuleResult> maluses)
        {
            this.BonusResults = bonuses;
            this.MalusResults = maluses;
            this.Bonus = this.BonusResults?.Sum(result => result.Value) ?? 0;
            this.Malus = this.MalusResults?.Sum(result => result.Value) ?? 0;
            this.Total = this.Bonus - this.Malus;
            this.Score = this.Total < 0 ? 0 : this.Total > 100 ? 100 : this.Total;
            this.Rating = this.Score == 0 ? EAnalysisRating.None
                : this.Score == 100 ? EAnalysisRating.VeryStrong
                : (EAnalysisRating)((this.Score / 20) + 1);
        }

        /// <summary>
        /// Gets the readonly list of bonus-rule results.
        /// </summary>
        public IReadOnlyList<AnalysisRuleResult> BonusResults { get; }

        /// <summary>
        /// Gets the readonly list of malus-rule results.
        /// </summary>
        public IReadOnlyList<AnalysisRuleResult> MalusResults { get; }

        /// <summary>
        /// Gets the sum of all bonus-rule result values.
        /// </summary>
        public int Bonus { get; }

        /// <summary>
        /// Gets the sum of all malus-rule result values.
        /// </summary>
        public int Malus { get; }

        /// <summary>
        /// Gets the difference of <see cref="Bonus"/> and <see cref="Malus"/>.
        /// </summary>
        public int Total { get; }

        /// <summary>
        /// Gets the value of <see cref="Total"/> clamped to the inclusive range 0...100.
        /// </summary>
        public int Score { get; }

        /// <summary>
        /// Gets a <see cref="EAnalysisRating"/> representing the value of <see cref="Score"/>.
        /// </summary>
        public EAnalysisRating Rating { get; }
    }
}
