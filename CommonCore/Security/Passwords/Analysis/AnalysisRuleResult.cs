﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

namespace CommonCore.Security.Passwords.Analysis
{
    /// <summary>
    /// The <see cref="AnalysisRuleResult"/> <c>readonly</c> <c>struct</c> represents the result
    /// of analyzing a password with a single rule.
    /// </summary>
    public readonly struct AnalysisRuleResult
    {
        /// <summary>
        /// Gets the default empty instance of the struct.
        /// </summary>
        public static AnalysisRuleResult Empty { get; } = default;

        /// <summary>
        /// Initialize a <see cref="AnalysisRuleResult"/> instance.
        /// </summary>
        /// <param name="rule">The rule that generated this result.</param>
        /// <param name="count">The number of rule-related elements analyzed.</param>
        /// <param name="value">The value representing how well the rule was met, higher numbers being better and lower numbers being worse.</param>
        public AnalysisRuleResult(AAnalysisRule rule, int count, int value)
        {
            this.Rule = rule;
            this.Count = count;
            this.Value = value;
        }

        /// <summary>
        /// Gets the rule that generated this result.
        /// </summary>
        public AAnalysisRule Rule { get; }

        /// <summary>
        /// Gets the number of rule-related elements analyzed.
        /// </summary>
        public int Count { get; }

        /// <summary>
        /// Gets the value representing how well the rule was met, higher numbers being better and lower numbers being worse.
        /// </summary>
        public int Value { get; }
    }
}
