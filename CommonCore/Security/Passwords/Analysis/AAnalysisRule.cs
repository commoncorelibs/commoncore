﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

namespace CommonCore.Security.Passwords.Analysis
{
    /// <summary>
    /// The <see cref="AAnalysisRule"/> <c>abstract</c> <c>class</c> provides a base
    /// to define password analysis rules.
    /// </summary>
    public abstract class AAnalysisRule
    {
        /// <summary>
        /// Initialize a <see cref="AAnalysisRule"/> instance.
        /// </summary>
        /// <param name="id">The unique ID of the rule.</param>
        /// <param name="title">The displayable title of the rule.</param>
        /// <param name="description">The displayable description of the rule.</param>
        /// <param name="formula">The displayable formula of the rule used to calculate the <see cref="AnalysisRuleResult"/> value.</param>
        protected AAnalysisRule(string id, string title, string description, string formula)
        {
            this.Id = id;
            this.Title = title;
            this.Description = description;
            this.Formula = formula;
        }

        /// <summary>
        /// Gets the unique ID of the rule.
        /// </summary>
        public string Id { get; }

        /// <summary>
        /// Gets the displayable title of the rule.
        /// </summary>
        public virtual string Title { get; }

        /// <summary>
        /// Gets the displayable description of the rule.
        /// </summary>
        public virtual string Description { get; }

        /// <summary>
        /// Gets the displayable formula used to calculate the <see cref="AnalysisRuleResult"/> value.
        /// </summary>
        public virtual string Formula { get; }

        /// <summary>
        /// Analyzes the specified password.
        /// </summary>
        /// <param name="password">The password to analyze.</param>
        /// <returns>The result of the analysis.</returns>
        public abstract AnalysisRuleResult Analyze(char[] password);
    }

    ///// <summary>
    ///// A <see cref="AAnalysisRule"/> derived class representing a rule used to analyze a password.
    ///// </summary>
    //public class SimpleAnalysisRule : AAnalysisRule
    //{
    //    /// <summary>
    //    /// Initialize a <see cref="SimpleAnalysisRule"/> object.
    //    /// </summary>
    //    /// <param name="id">Unique ID of the rule.</param>
    //    /// <param name="title">Display title of the rule.</param>
    //    /// <param name="description">Display description of the rule.</param>
    //    /// <param name="formula">Display formula of the rule used to calculate the <see cref="AnalysisRuleResult"/> value.</param>
    //    /// <param name="analyzeFunc">Delegate used to analyze a password.</param>
    //    public SimpleAnalysisRule(string id, string title, string description, string formula,
    //        Func<char[], (int, int)> analyzeFunc)
    //        : base(title, description, formula)
    //    {
    //        this.Id = id;
    //        this.AnalyzeFunc = analyzeFunc;
    //    }

    //    /// <summary>
    //    /// Gets the unique ID of the rule.
    //    /// </summary>
    //    public string Id { get; }

    //    /// <summary>
    //    /// Gets the delegate used to analyze a password.
    //    /// </summary>
    //    public Func<char[], (int count, int value)> AnalyzeFunc { get; }

    //    /// <inheritdoc/>
    //    public override AnalysisRuleResult Analyze(char[] password)
    //    {
    //        var (count, value) = this.AnalyzeFunc(password);
    //        return new AnalysisRuleResult(this, count, value);
    //    }
    //}
}
