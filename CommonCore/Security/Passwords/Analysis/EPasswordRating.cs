﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

namespace CommonCore.Security.Passwords.Analysis
{
    /// <summary>
    /// The <see cref="EAnalysisRating" /> <c>enum</c> provides a simplified view of a password
    /// analysis result. Generally maps to the result score.
    /// </summary>
    public enum EAnalysisRating : int
    {
        /// <summary>
        /// The password provides no security.
        /// </summary>
        None,

        /// <summary>
        /// The password is very weak.
        /// </summary>
        VeryWeak,

        /// <summary>
        /// The password is weak.
        /// </summary>
        Weak,

        /// <summary>
        /// The password is good.
        /// </summary>
        Good,

        /// <summary>
        /// The password is strong.
        /// </summary>
        Strong,

        /// <summary>
        /// The password is very strong.
        /// </summary>
        VeryStrong,
    }
}
