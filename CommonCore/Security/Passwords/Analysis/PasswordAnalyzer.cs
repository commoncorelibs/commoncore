﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Collections.Generic;
using System.Security;

namespace CommonCore.Security.Passwords.Analysis
{
    /// <summary>
    /// The <see cref="PasswordAnalyzer" /> <c>class</c> allows rule-based analysis of passwords. Rules are
    /// divided into two sets: Bonus (positive) rules and Malus (negative) rules.
    /// <para>
    /// Rules in both sets work the same way and are both composed of <see cref="AAnalysisRule" />
    /// derived instances. Each one analyzes a password and calculate a value based on how
    /// extensively the data meets the criteria of the rule. The higher the value, the more the data
    /// meets the rule criteria; the lower the value, the less the criteria is met. The two rule
    /// sets differ in that high values are desirable for bonuses and low values are desirable for maluses.
    /// </para>
    /// <para>
    /// In other words, bonuses are good and maluses are bad. Once all rules are calculated, the
    /// difference between the sum of all bonuses and the sum of all maluses is clamped inclusively
    /// between 0 and 100 for a final score for the password. High scores are better than lower scores.
    /// </para>
    /// </summary>
    // Original Source: http://www.passwordmeter.com/ and http://metabetageek.com/software/
    public class PasswordAnalyzer
    {
        /// <summary>
        /// Gets the default <see cref="PasswordAnalyzer" /> instance. Unless implementing rules
        /// other than the defaults, this instance should be suitable for the vast majority of uses.
        /// </summary>
        public static PasswordAnalyzer DefaultAnalyzer { get; } = InitializePasswordAnalyzerDefaultRules(new PasswordAnalyzer());

        /// <summary>
        /// Initializes the specified <see cref="PasswordAnalyzer"/> with the sets of default rules.
        /// </summary>
        /// <param name="analyzer">The <see cref="PasswordAnalyzer"/> to add rules to.</param>
        /// <returns>This specified <see cref="PasswordAnalyzer"/> instance.</returns>
        public static PasswordAnalyzer InitializePasswordAnalyzerDefaultRules(PasswordAnalyzer analyzer)
        {
            analyzer.BonusRules.Add(new LengthBonusAnalysisRule());
            analyzer.BonusRules.Add(new LetterUpperBonusAnalysisRule());
            analyzer.BonusRules.Add(new LetterLowerBonusAnalysisRule());
            analyzer.BonusRules.Add(new DigitBonusAnalysisRule());
            analyzer.BonusRules.Add(new SymbolBonusAnalysisRule());
            analyzer.BonusRules.Add(new MiddleDigitOrSymbolBonusAnalysisRule());
            analyzer.BonusRules.Add(new RequirementsBonusAnalysisRule());

            analyzer.MalusRules.Add(new LetterOnlyMalusAnalysisRule());
            analyzer.MalusRules.Add(new DigitOnlyMalusAnalysisRule());
            analyzer.MalusRules.Add(new RepeatCharactersMalusAnalysisRule());
            analyzer.MalusRules.Add(new ConsecutiveLetterUpperMalusAnalysisRule());
            analyzer.MalusRules.Add(new ConsecutiveLetterLowerMalusAnalysisRule());
            analyzer.MalusRules.Add(new ConsecutiveDigitMalusAnalysisRule());
            analyzer.MalusRules.Add(new SequentialLetterMalusAnalysisRule());
            analyzer.MalusRules.Add(new SequentialDigitMalusAnalysisRule());
            analyzer.MalusRules.Add(new SequentialSymbolMalusAnalysisRule());
            return analyzer;
        }

        /// <summary>
        /// Gets the list of bonus rules. This list represents rules that look for positive aspects
        /// of a text. Higher value results are better and lower value results are worse.
        /// </summary>
        public List<AAnalysisRule> BonusRules { get; } = new List<AAnalysisRule>();

        /// <summary>
        /// Gets the list of malus rules. This list represents rules that look for negative aspects
        /// of a text. Lower value results are better and higher value results are worse.
        /// </summary>
        public List<AAnalysisRule> MalusRules { get; } = new List<AAnalysisRule>();

        /// <summary>
        /// Enumerates through the <see cref="BonusRules"/> list and collates all the
        /// results into the return list.
        /// </summary>
        /// <param name="password">The password to analyze.</param>
        /// <exception cref="System.ArgumentException">If <paramref name="password"/> is <c>null</c> or <c>empty</c>.</exception>
        /// <returns>A new list of <see cref="AnalysisRuleResult"/> objects.</returns>
        public List<AnalysisRuleResult> CalculateBonusResults(char[] password)
        {
            Throw.If.Arg.IsNullOrEmpty(nameof(password), password);
            var output = new List<AnalysisRuleResult>();
            foreach (var rule in this.BonusRules) { output.Add(rule.Analyze(password)); }
            return output;
        }

        /// <summary>
        /// Enumerates through the <see cref="MalusRules"/> list and collates all the
        /// results into the return list.
        /// </summary>
        /// <param name="password">The password to analyze.</param>
        /// <exception cref="System.ArgumentException">If <paramref name="password"/> is <c>null</c> or <c>empty</c>.</exception>
        /// <returns>A new list of <see cref="AnalysisRuleResult"/> objects.</returns>
        public List<AnalysisRuleResult> CalculateMalusResults(char[] password)
        {
            Throw.If.Arg.IsNullOrEmpty(nameof(password), password);
            var output = new List<AnalysisRuleResult>();
            foreach (var rule in this.MalusRules) { output.Add(rule.Analyze(password)); }
            return output;
        }

        /// <summary>
        /// Analyzes the password with both the bonus and malus sets, then returns the collected
        /// result. An empty password will always return <see cref="AnalysisResult.Empty"/>.
        /// </summary>
        /// <param name="password">The password to analyze.</param>
        /// <exception cref="System.ArgumentException">If <paramref name="password"/> is <c>null</c> or <c>empty</c>.</exception>
        /// <returns>The generated <see cref="AnalysisResult"/> object.</returns>
        public AnalysisResult GenerateAnalysis(SecureString password)
        {
            Throw.If.Arg.IsNullOrEmpty(nameof(password), password);
            using var chars = password.ToCharArray();
            return new AnalysisResult(this.CalculateBonusResults(chars), this.CalculateMalusResults(chars));
        }
    }
}
