﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CommonCore.Security.Passwords.Analysis
{
    /// <summary>
    /// The <see cref="AnalysisHelper"/> <c>static</c> <c>class</c> provides helper methods
    /// useful for analyzing passwords.
    /// </summary>
    public static class AnalysisHelper
    {
        /// <summary>
        /// Determines if the specified <see cref="char"/> is a digit.
        /// </summary>
        /// <param name="c">The <see cref="char"/> to evaluate.</param>
        /// <returns><c>true</c> if <paramref name="c"/> is a digit; otherwise <c>false</c>.</returns>
        public static bool IsDigit(char c) => char.IsDigit(c);

        /// <summary>
        /// Determines if the specified <see cref="char"/> is a digit or whitespace.
        /// </summary>
        /// <param name="c">The <see cref="char"/> to evaluate.</param>
        /// <returns><c>true</c> if <paramref name="c"/> is a digit or whitespace; otherwise <c>false</c>.</returns>
        public static bool IsDigitOrWhiteSpace(char c) => char.IsDigit(c) || char.IsWhiteSpace(c);

        /// <summary>
        /// Determines if the specified <see cref="char"/> is a letter.
        /// </summary>
        /// <param name="c">The <see cref="char"/> to evaluate.</param>
        /// <returns><c>true</c> if <paramref name="c"/> is a letter; otherwise <c>false</c>.</returns>
        public static bool IsLetter(char c) => char.IsLetter(c);

        /// <summary>
        /// Determines if the specified <see cref="char"/> is a letter or whitespace.
        /// </summary>
        /// <param name="c">The <see cref="char"/> to evaluate.</param>
        /// <returns><c>true</c> if <paramref name="c"/> is a letter or whitespace; otherwise <c>false</c>.</returns>
        public static bool IsLetterOrWhiteSpace(char c) => char.IsLetter(c) || char.IsWhiteSpace(c);

        /// <summary>
        /// Determines if the specified <see cref="char"/> is an uppercase letter.
        /// </summary>
        /// <param name="c">The <see cref="char"/> to evaluate.</param>
        /// <returns><c>true</c> if <paramref name="c"/> is an uppercase letter; otherwise <c>false</c>.</returns>
        public static bool IsLetterUpper(char c) => char.IsLetter(c) && char.IsUpper(c);

        /// <summary>
        /// Determines if the specified <see cref="char"/> is a lowercase letter.
        /// </summary>
        /// <param name="c">The <see cref="char"/> to evaluate.</param>
        /// <returns><c>true</c> if <paramref name="c"/> is a lowercase letter; otherwise <c>false</c>.</returns>
        public static bool IsLetterLower(char c) => char.IsLetter(c) && char.IsLower(c);

        /// <summary>
        /// Determines if the specified <see cref="char"/> is not a letter or digit.
        /// </summary>
        /// <param name="c">The <see cref="char"/> to evaluate.</param>
        /// <returns><c>true</c> if <paramref name="c"/> is not a letter or digit; otherwise <c>false</c>.</returns>
        public static bool IsSymbol(char c) => !char.IsLetterOrDigit(c);

        /// <summary>
        /// Determines if the specified <paramref name="comparison"/> fails to match every
        /// character in the specified <paramref name="password"/>.
        /// </summary>
        /// <param name="password">The password to analyze.</param>
        /// <param name="comparison">The function to use for analysis.</param>
        /// <returns><c>true</c> if <paramref name="comparison"/> succeeds for all password characters; otherwise <c>false</c>.</returns>
        public static bool ContainsNonMatch(char[] password, Func<char, bool> comparison)
        {
            Throw.If.Arg.IsNull(nameof(password), password);
            for (int index = 0; index < password.Length; index++)
            { if (!comparison(password[index])) { return true; } }
            return false;
        }

        /// <summary>
        /// Counts the number of consecutive matches according to the specified <paramref name="comparison"/>.
        /// </summary>
        /// <param name="password">The password to analyze.</param>
        /// <param name="comparison">The function to use for analysis.</param>
        /// <returns>The number of consecutive matches.</returns>
        public static int CountConsecutiveMatches(char[] password, Func<char, bool> comparison)
        {
            Throw.If.Arg.IsNull(nameof(password), password);
            int count = 0;
            for (int index = 0; index < password.Length - 1; index++)
            { if (comparison(password[index]) && comparison(password[index + 1])) { count++; } }
            return count;
        }

        /// <summary>
        /// Counts the number of matches according to the specified <paramref name="comparison"/>.
        /// </summary>
        /// <param name="password">The password to analyze.</param>
        /// <param name="comparison">The function to use for analysis.</param>
        /// <returns>The number of matches.</returns>
        public static int CountMatches(char[] password, Func<char, bool> comparison)
        {
            Throw.If.Arg.IsNull(nameof(password), password);
            int count = 0;
            for (int index = 0; index < password.Length; index++)
            { if (comparison(password[index])) { count++; } }
            return count;
        }

        /// <summary>
        /// Counts the number of matches according to the specified <paramref name="comparisonA"/>
        /// or <paramref name="comparisonB"/>.
        /// </summary>
        /// <param name="password">The password to analyze.</param>
        /// <param name="comparisonA">The first function to use for analysis.</param>
        /// <param name="comparisonB">The second function to use for analysis.</param>
        /// <returns>The number of matches.</returns>
        public static int CountMatches(char[] password, Func<char, bool> comparisonA, Func<char, bool> comparisonB)
        {
            Throw.If.Arg.IsNull(nameof(password), password);
            int count = 0;
            for (int index = 0; index < password.Length; index++)
            { if (comparisonA(password[index]) || comparisonB(password[index])) { count++; } }
            return count;
        }

        /// <summary>
        /// Counts the number of matches within the specified subsection according to the
        /// specified <paramref name="comparisonA"/> or <paramref name="comparisonB"/>.
        /// </summary>
        /// <param name="password">The password to analyze.</param>
        /// <param name="analysisStartIndex">The password index to start the analysis at.</param>
        /// <param name="analysisCharCount">The number of password chars to analyze.</param>
        /// <param name="comparisonA">The first function to use for analysis.</param>
        /// <param name="comparisonB">The second function to use for analysis.</param>
        /// <returns>The number of matches.</returns>
        public static int CountMatches(char[] password, int analysisStartIndex, int analysisCharCount,
            Func<char, bool> comparisonA, Func<char, bool> comparisonB)
        {
            Throw.If.Arg.IsNull(nameof(password), password);
            Throw.If.Arg.IsIndexOutOfRange(nameof(analysisStartIndex), analysisStartIndex, password);
            Throw.If.Arg.IsLesser(nameof(analysisCharCount), analysisCharCount, 0);
            Throw.If.Arg.IsNull(nameof(comparisonA), comparisonA);
            Throw.If.Arg.IsNull(nameof(comparisonB), comparisonB);
            int count = 0;
            for (int i = analysisStartIndex; i < analysisStartIndex + analysisCharCount; i++)
            { if (comparisonA(password[i]) || comparisonB(password[i])) { count++; } }
            return count;
        }

        /// <summary>
        /// Compares <paramref name="count"/> number of password characters starting at
        /// <paramref name="index"/> against all ascending and descending sub-sequences of
        /// <paramref name="sequence"/>.
        /// </summary>
        /// <param name="password">The password to analyze.</param>
        /// <param name="index">The password index where analysis should start.</param>
        /// <param name="sequence">The series of chars to compare against the password.</param>
        /// <param name="count">The number of sub-sequences to compare to the password.</param>
        /// <returns><c>true</c> if any sub-sequence match if found; otherwise <c>false</c>.</returns>
        public static bool CompareSequence(char[] password, int index, string sequence, int count)
        {
            Throw.If.Arg.IsNull(nameof(password), password);
            Throw.If.Arg.IsIndexOutOfRange(nameof(index), index, password);
            Throw.If.Arg.IsNullOrEmpty(nameof(sequence), sequence);
            Throw.If.Arg.IsLesser(nameof(count), count, 0);
            // Chunk of a password has to be stored in a pinned array
            // so it can be destroyed when disposed.
            using (var passwordChunk = new CustodialArray<char>(count))
            {
                Array.Copy(password, index, passwordChunk, 0, count);
                for (var indexSequence = 0; indexSequence < sequence.Length; indexSequence++)
                {
                    if (passwordChunk.SequenceEqual(AnalysisHelper.GetSubSequence(sequence, indexSequence, count))) { return true; }
                    if (passwordChunk.SequenceEqual(AnalysisHelper.GetSubSequence(string.Concat(sequence.Reverse()), indexSequence, count))) { return true; }
                }
            }
            return false;
        }

        /// <summary>
        /// Provides an enumeration over the sub-sequence of specified <paramref name="sequence"/>
        /// defined by the specified <paramref name="index"/> and <paramref name="count"/>.
        /// Sub-sequences that extend beyond the end of the sequence are appropriately wrapped
        /// back around to valid indexes.
        /// For example, ("0123456789", 4, 3) is the sub-sequence { '4', '5', '6' } and
        /// ("0123456789", 9, 3) is the sub-sequence { '9', '0', '1' }.
        /// </summary>
        /// <param name="sequence">The series of chars used to build the sub-sequence.</param>
        /// <param name="index">The sequence index to start enumerating.</param>
        /// <param name="count">The number of sequence elements to enumerate.</param>
        /// <returns></returns>
        public static IEnumerable<char> GetSubSequence(string sequence, int index, int count)
        {
            Throw.If.Arg.IsNullOrEmpty(nameof(sequence), sequence);
            Throw.If.Arg.IsLesser(nameof(index), index, 0);
            Throw.If.Arg.IsLesser(nameof(count), count, 0);
            for (int offset = 0; offset < count; offset++)
            { yield return sequence[(index + offset) % sequence.Length]; }
        }
    }
}
