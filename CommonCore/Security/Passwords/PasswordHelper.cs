﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Security;
using CommonCore.Security.Passwords.Analysis;
using CommonCore.Security.Passwords.Hashing;

namespace CommonCore.Security.Passwords
{
    /// <summary>
    /// The <see cref="PasswordHelper"/> <c>static</c> <c>class</c> provides helper methods for
    /// common password related actions, such as hashing, validating, and strength testing.
    /// <para>
    /// The API uses <see cref="SecureString"/> instead of <see cref="string"/> for
    /// all handling of passwords and many mechanisms are provided to simplify handling
    /// of these <see cref="SecureString"/> secrets.
    /// </para>
    /// </summary>
    public static class PasswordHelper
    {
        /// <summary>
        /// Calculates the complexity score (0..100) of the specified <paramref name="secret"/>.
        /// See <see cref="PasswordAnalyzer"/> for details.
        /// </summary>
        /// <param name="secret">Password to analyze.</param>
        /// <returns>Complexity score (0..100) of the specified <paramref name="secret"/>.</returns>
        public static int CalculateScore(SecureString secret)
            => PasswordAnalyzer.DefaultAnalyzer.GenerateAnalysis(secret).Score;

        /// <summary>
        /// Analyzes the password with both the bonus and malus sets, then returns the collected
        /// result. An empty password will always return <see cref="AnalysisResult.Empty"/>.
        /// See <see cref="PasswordAnalyzer"/> for details.
        /// </summary>
        /// <param name="secret">The password to analyze.</param>
        /// <returns>The generated <see cref="AnalysisResult"/> object.</returns>
        public static AnalysisResult GenerateAnalysis(SecureString secret)
            => PasswordAnalyzer.DefaultAnalyzer.GenerateAnalysis(secret);

        /// <summary>
        /// Generates a <see cref="Pbkdf2Hash"/> for the specified <paramref name="secret"/>.
        /// See <see cref="PasswordHasher"/> and <see cref="Pbkdf2Hash"/> for details.
        /// </summary>
        /// <param name="secret">The password to hash.</param>
        /// <returns>A <see cref="Pbkdf2Hash"/> for the specified <paramref name="secret"/>.</returns>
        public static Pbkdf2Hash GenerateHash(SecureString secret)
            => PasswordHasher.GenerateHash(secret);

        /// <summary>
        /// Determines if the specified <paramref name="hash"/> is valid for
        /// the specified <paramref name="secret"/>.
        /// See <see cref="PasswordHasher"/> and <see cref="Pbkdf2Hash"/> for details.
        /// </summary>
        /// <param name="secret">The password to compare to the hash.</param>
        /// <param name="hash">The hash to compare to the password.</param>
        /// <returns><c>true</c> if <paramref name="secret"/> and <paramref name="hash"/> are equivalent; otherwise <c>false</c>.</returns>
        public static bool CompareHash(SecureString secret, string hash)
            => PasswordHasher.CompareHash(secret, hash);

        /// <summary>
        /// Performs the user interaction necessary to retrieve a password
        /// through the console. Enter will complete the interaction and backspace
        /// will remove the last character, but all other control keys are ignored.
        /// Read keys are not echoed back to the console and instead a mask composed
        /// of * characters is displayed.
        /// </summary>
        /// <returns>A <see cref="SecureString"/> containing the entered password.</returns>
        public static SecureString ConsoleReadPassword()
        {
            var password = new SecureString();
            for (int num = 0; num < int.MaxValue; num++)
            {
                var input = Console.ReadKey(true);
                if (input.Key == ConsoleKey.Enter) { break; }
                else if (input.Key == ConsoleKey.Backspace)
                {
                    if (password.Length > 0)
                    {
                        password.RemoveAt(password.Length - 1);
                        Console.Write((char)ConsoleKey.Backspace + ' ' + (char)ConsoleKey.Backspace);
                    }
                }
                else if (char.IsControl(input.KeyChar)) { continue; }
                else
                {
                    password.AppendChar(input.KeyChar);
                    Console.Write("*");
                }
            }
            Console.WriteLine();

            password.MakeReadOnly();
            return password;
        }

#if DEBUG
        //while (true)
        //{
        //    Console.WriteLine("WARNING: Do not enter real passwords.");
        //    Console.WriteLine("Enter password for demonstration: ");
        //    using (var password = ConsoleReadPassword())
        //    {
        //        string hash = PasswordHasher.DefaultHasher.GenerateHash(password);
        //        AnalysisResult analysis = PasswordAnalyzer.DefaultAnalyzer.GenerateAnalysis(password);
        //        Console.WriteLine(new string('-', Console.BufferWidth - 1));
        //        Console.WriteLine($"Password Text: {password.ToClearTextString()}");
        //        Console.WriteLine($"Password Hash: {hash}");
        //        Console.WriteLine($"Analysis Bonus: {analysis.Bonus} (higher is better)");
        //        Console.WriteLine($"Analysis Malus: {analysis.Malus} (lower is better)");
        //        Console.WriteLine($"Analysis Total: {analysis.Total} (Bonus - Malus)");
        //        Console.WriteLine($"Analysis Score: {analysis.Score} (0 to 100, inclusive)");
        //        Console.WriteLine($"Analysis Complexity: {GetDisplayText(analysis)}");
        //        Console.WriteLine(new string('-', Console.BufferWidth - 1));
        //    }

        //    Console.WriteLine();
        //    Console.Write("   ... Press ESC to exit, any other key to continue ...   ");
        //    if (Console.ReadKey(true).Key == ConsoleKey.Escape) { break; }
        //    Console.WriteLine();
        //    Console.WriteLine();
        //}

        //public static void Test()
        //{
        //    /*
        //    !!IMPORTANT NOTE!! This is for testing purposes.
        //    A SecureString should not be created from a string
        //    as the damage is already done. It should be created
        //    directly from user input through console ReadChar
        //    or using the WPF PasswordBox and its SecureString
        //    property. This pushes all risk off the application
        //    and onto the framework and OS.

        //    The password should never be held, completely or partly,
        //    as a string. Instead, when it needs to be worked with,
        //    the SecureString should be converted to either a char[]
        //    or byte[]. All of that should be done inside a try/catch
        //    block so that the array can be cleared by Array.Clear()
        //    within the try's finally block.

        //    This minimizes the risk surface of the clear text arrays
        //    to the smallest amount of time possible.
        //    */
        //    string clearPassword = "TestPassword";
        //    string clearData = "This is some data of an arbitrary length that must be securely persisted.";

        //    Debug.WriteLine("------------------------");
        //    Debug.WriteLine($"Original Text: \"{clearData}\"");

        //    byte[] entropy = null;
        //    using (var password = new SecureString().AppendClearTextString(clearPassword, true))
        //    {
        //        entropy = Encoding.Unicode.GetBytes(PasswordHelper.GenerateHash(password));
        //    }

        //    using (var secret = new SecureString().AppendClearTextString(clearData, true))
        //    {
        //        string encrypted = secret.ToEncryptedString(entropy);
        //        File.WriteAllText("Data.dat", encrypted);
        //        Debug.WriteLine("------------------------");
        //        Debug.WriteLine($"Encrypted Text: \"{encrypted}\"");
        //    }

        //    using (var decrypted = new SecureString().AppendEncryptedString(File.ReadAllText("Data.dat"), entropy))
        //    {
        //        using (var desecured = new SecureStringReader(decrypted))
        //        {
        //            Debug.WriteLine("------------------------");
        //            Debug.WriteLine($"Decrypted DebugByteText: \"{desecured.DebugByteText}\"");
        //            Debug.WriteLine($"Decrypted DebugCharText: \"{desecured.DebugCharText}\"");
        //            Debug.WriteLine($"Decrypted ByteSequence: \"{Encoding.Unicode.GetString(desecured.Secret.ToByteSequence().ToArray())}\"");
        //            Debug.WriteLine($"Decrypted CharSequence: \"{string.Concat(desecured.Secret.ToCharSequence())}\"");
        //            Debug.WriteLine("------------------------");
        //        }
        //    }
        //}
#endif
    }
}
