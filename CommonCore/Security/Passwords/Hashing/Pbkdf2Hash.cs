﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace CommonCore.Security.Passwords.Hashing
{
    /// <summary>
    /// The <see cref="Pbkdf2Hash" /> <c>readonly</c> <c>struct</c> represents PBKDF2 hash and
    /// provides validation for parsing of hash strings and creating canonically formatted PBKDF2
    /// hash strings. The canonical hash format is
    /// 'algorithm:iterations:integrity:base64_salt:base64_payload'. For example, a properly
    /// formatted hash for the password 'password' would be 'sha1:64000:18:v741vZLHthpjnKAUZhOoQRT67oopJ14E:NxVUAo1coj+Wtihhju1bqlmp'.
    /// <para>
    /// A PBKDF2 hash is composed of five sections: an algorithm name, an iteration count, an
    /// integrity size, some salt content, and the payload content. When converting the hash to
    /// text, the sections must be delimited with colons.
    /// <list type="bullet">
    /// <item>
    /// <term>Algorithm</term>
    /// <description>
    /// The name of the algorithm used to generate hash content. Currently 'sha1' is the only value supported.
    /// </description>
    /// </item>
    /// <item>
    /// <term>Iterations</term>
    /// <description>
    /// The number of iterations used to generate random data. Value must be greater than <c>0</c>
    /// and should not be lower than <c>64000</c>.
    /// </description>
    /// </item>
    /// <item>
    /// <term>Integrity</term>
    /// <description>
    /// The size of the hash payload. Value must be greater than <c>0</c>. This is a defensive value
    /// and helps to detect accidental truncation of the payload. Note this is the size of the
    /// payload in bytes, not the length of its <c>Base64</c> encoded text.
    /// </description>
    /// </item>
    /// <item>
    /// <term>Salt</term>
    /// <description>
    /// The bytes of salt used to generate the payload. Value must not be <c>null</c> and should not
    /// be <c>empty</c>. When converting the hash to text, the salt bytes must be <c>Base64</c> encoded.
    /// </description>
    /// </item>
    /// <item>
    /// <term>Payload</term>
    /// <description>
    /// The bytes of the salted hash of the password. Value must not be <c>null</c> and should not
    /// be <c>empty</c>. When converting the hash to text, the payload bytes must be <c>Base64</c> encoded.
    /// </description>
    /// </item>
    /// </list>
    /// </para>
    /// </summary>
    public readonly struct Pbkdf2Hash : IEquatable<Pbkdf2Hash>
    {
        /// <summary>
        /// Gets the default instance of <see cref="Pbkdf2Hash"/> representing
        /// an uninitialized, invalid value.
        /// </summary>
        public static Pbkdf2Hash None { get; } = default;

        /// <summary>
        /// Gets the name of the hash function PBKDF2 uses. Currently, only SHA1 is supported. Although SHA1 has been
        /// cryptographically broken as a collision-resistant function, its second preimage
        /// resistance and other PRF-like properties ensure that it is cryptographically sound for
        /// password storage with PBKDF2.
        /// </summary>
        public static string DefaultAlgorithmName { get; } = "sha1";

        /// <summary>
        /// Gets the number of PBKDF2 iterations. By default, it is 64,000. To provide greater protection
        /// of passwords, at the expense of needing more processing power to validate passwords,
        /// increase the number of iterations. The number of iterations should not be decreased.
        /// </summary>
        public static int DefaultIterationCount { get; } = 64000;

        /// <summary>
        /// Gets the number of bytes of salt. By default, 24 bytes, which is 192 bits. This is more than enough.
        /// </summary>
        public static int DefaultSaltByteCount { get; } = 24;

        /// <summary>
        /// Gets the number of PBKDF2 output bytes. By default, 18 bytes, which is 144 bits. While it may
        /// seem useful to increase the number of output bytes, doing so can actually give an
        /// advantage to the attacker, as it introduces unnecessary (avoidable) slowness to the
        /// PBKDF2 computation. 144 bits was chosen because it is (1) Less than SHA1's 160-bit
        /// output (to avoid unnecessary PBKDF2 overhead), and (2) A multiple of 6 bits, so that the
        /// base64 encoding is optimal.
        /// </summary>
        public static int DefaultPayloadByteCount { get; } = 18;

        /// <summary>
        /// Gets the number of colon-delimited sections in the output hash.
        /// </summary>
        public static int SectionCount { get; } = 5;

        /// <summary>
        /// Gets the index of the formatted hash algorithm name section.
        /// </summary>
        public static int SectionIndexAlgorithm { get; } = 0;

        /// <summary>
        /// Gets the index of the formatted hash iteration count section.
        /// </summary>
        public static int SectionIndexIterations { get; } = 1;

        /// <summary>
        /// Gets the index of the formatted hash payload size integrity section.
        /// </summary>
        public static int SectionIndexIntegrity { get; } = 2;

        /// <summary>
        /// Gets the index of the formatted hash salt section.
        /// </summary>
        public static int SectionIndexSalt { get; } = 3;

        /// <summary>
        /// Gets the index of the formatted hash payload section.
        /// </summary>
        public static int SectionIndexPayload { get; } = 4;

        private static string ExceptionFormat { get; } = "Section[{0}]: {1} '{2}' is invalid.";

        /// <summary>
        /// Converts a string representation of a formatted PBKDF2 hash into
        /// its <see cref="Pbkdf2Hash"/> equivalent.
        /// </summary>
        /// <param name="hash">The hash string to parse.</param>
        /// <exception cref="System.ArgumentException">If <paramref name="hash"/> is <c>null</c> or <c>empty</c>.</exception>
        /// <exception cref="FormatException">
        /// If <paramref name="hash"/> does not contain exactly 5 sections;
        /// or algorithm section is not 'sha1';
        /// or iterations section is not a valid <see cref="Int32"/> or is less than <c>1</c>;
        /// or integrity section is not a valid <see cref="Int32"/> or is less than <c>0</c>;
        /// or salt section is not valid <c>Base64</c> encoded text;
        /// or salt size does not match the expected size of <c>24</c>;
        /// or payload section is not valid <c>Base64</c> encoded text;
        /// or payload size does not match the integrity section value.
        /// </exception>
        /// <returns>An <see cref="Pbkdf2Hash"/> object equivalent to the specified <paramref name="hash"/> string.</returns>
        // The hash is of the format 'algorithm:iterations:integrity:salt:payload'.
        public static Pbkdf2Hash ParseString(string hash)
        {
            Throw.If.Arg.IsNull(nameof(hash), hash);
            string[] split = hash.Split(':');
            if (split.Length != Pbkdf2Hash.SectionCount)
            { throw new FormatException($"Hash section count '{split.Length}' is invalid. Hash must contain '{Pbkdf2Hash.SectionCount}' sections."); }

            var algorithm = ParseSectionAlgorithm(split[Pbkdf2Hash.SectionIndexAlgorithm].AsSpan());
            var iterations = ParseSectionIterations(split[Pbkdf2Hash.SectionIndexIterations].AsSpan());
            var integrity = ParseSectionIntegrity(split[Pbkdf2Hash.SectionIndexIntegrity].AsSpan());
            var salt = ParseSectionSalt(split[Pbkdf2Hash.SectionIndexSalt].AsSpan(), Pbkdf2Hash.DefaultSaltByteCount);
            var payload = ParseSectionPayload(split[Pbkdf2Hash.SectionIndexPayload].AsSpan(), integrity);

            return new Pbkdf2Hash(algorithm, iterations, integrity, salt, payload);
        }

        /// <summary>
        /// Converts a string representation of a formatted PBKDF2 hash
        /// algorithm name section into a valid value.
        /// </summary>
        /// <param name="section">The <see cref="ReadOnlySpan{Char}"/> to parse.</param>
        /// <exception cref="FormatException">
        /// If <paramref name="section"/> is not equivalent to <see cref="Pbkdf2Hash.DefaultAlgorithmName"/>.
        /// </exception>
        /// <returns>A valid section value.</returns>
        public static string ParseSectionAlgorithm(ReadOnlySpan<char> section)
        {
            if (!section.SequenceEqual(Pbkdf2Hash.DefaultAlgorithmName.AsSpan()))
            {
                throw new FormatException(ExceptionFormat.AsFormat(Pbkdf2Hash.SectionIndexAlgorithm, "Algorithm", section.ToString()) + $" Only '{Pbkdf2Hash.DefaultAlgorithmName}' is supported.");
            }
            return section.ToString();
        }

        /// <summary>
        /// Converts a string representation of a formatted PBKDF2 hash
        /// iteration count section into a valid value.
        /// </summary>
        /// <param name="section">The <see cref="ReadOnlySpan{Char}"/> to parse.</param>
        /// <exception cref="FormatException">
        /// If <paramref name="section"/> is not a valid <see cref="Int32"/> or is less than <c>1</c>.
        /// </exception>
        /// <returns>A valid section value.</returns>
        public static int ParseSectionIterations(ReadOnlySpan<char> section)
        {
#if NETSTANDARD2_0
            if (!Int32.TryParse(section.ToString(), out int iterations) || iterations < 1)
#else
            if (!Int32.TryParse(section, out int iterations) || iterations < 1)
#endif
            {
                throw new FormatException(ExceptionFormat.AsFormat(Pbkdf2Hash.SectionIndexIterations, "Iteration", section.ToString()) + $" Must be a 32bit integer greater than zero.");
            }
            return iterations;
        }

        /// <summary>
        /// Converts a string representation of a formatted PBKDF2 hash
        /// integrity size section into a valid value.
        /// </summary>
        /// <param name="section">The <see cref="ReadOnlySpan{Char}"/> to parse.</param>
        /// <exception cref="FormatException">
        /// If <paramref name="section"/> is not a valid <see cref="Int32"/> or is less than <c>0</c>.
        /// </exception>
        /// <returns>A valid section value.</returns>
        public static int ParseSectionIntegrity(ReadOnlySpan<char> section)
        {
#if NETSTANDARD2_0
            if (!Int32.TryParse(section.ToString(), out int integrity) || integrity < 0)
#else
            if (!Int32.TryParse(section, out int integrity) || integrity < 0)
#endif
            {
                throw new FormatException(ExceptionFormat.AsFormat(Pbkdf2Hash.SectionIndexIntegrity, "Integrity", section.ToString()) + $" Must be a 32bit integer greater than zero.");
            }
            return integrity;
        }

        /// <summary>
        /// Converts a string representation of a formatted PBKDF2 hash
        /// salt content section into a valid value.
        /// </summary>
        /// <param name="section">The <see cref="ReadOnlySpan{Char}"/> to parse.</param>
        /// <param name="byteCount">The expected number of bytes for the returned array.</param>
        /// <exception cref="FormatException">
        /// If <paramref name="section"/> is not valid <c>Base64</c> encoded text or length does not equal <paramref name="byteCount"/>.
        /// </exception>
        /// <returns>A valid section value.</returns>
        public static byte[] ParseSectionSalt(ReadOnlySpan<char> section, int byteCount)
        {
#if NETSTANDARD2_0
            byte[] salt = null;
            int readByteCount = 0;
            bool success = true;
            try
            {
                salt = Convert.FromBase64String(section.ToString());
                readByteCount = salt.Length;
            }
            catch (FormatException) { success = false; }
            if (!success || salt.Length != byteCount)
#else
            byte[] salt = new byte[byteCount];
            if (!Convert.TryFromBase64Chars(section, salt.AsSpan(), out int readByteCount))
#endif
            {
                throw new FormatException(ExceptionFormat.AsFormat(Pbkdf2Hash.SectionIndexSalt, "Salt", section.ToString()) + $" Must be valid Base64.");
            }
            else if (readByteCount != salt.Length)
            {
                throw new FormatException(ExceptionFormat.AsFormat(Pbkdf2Hash.SectionIndexSalt, "Payload", section.ToString()) + $" Count of decoded bytes '{readByteCount}' must match the number of expected bytes '{byteCount}'.");
            }
            return salt;
        }

        /// <summary>
        /// Converts a string representation of a formatted PBKDF2 hash
        /// payload content section into a valid value.
        /// </summary>
        /// <param name="section">The <see cref="ReadOnlySpan{Char}"/> to parse.</param>
        /// <param name="integrity">The expected number of bytes for the returned array.</param>
        /// <exception cref="FormatException">
        /// If <paramref name="section"/> is not valid <c>Base64</c> encoded text or length does not equal <paramref name="integrity"/>.
        /// </exception>
        /// <returns>A valid section value.</returns>
        public static byte[] ParseSectionPayload(ReadOnlySpan<char> section, int integrity)
        {
#if NETSTANDARD2_0
            byte[] payload = null;
            int readByteCount = 0;
            bool success = true;
            try
            {
                payload = Convert.FromBase64String(section.ToString());
                readByteCount = payload.Length;
            }
            catch (FormatException) { success = false; }
            if (!success || payload.Length != integrity)
#else
            byte[] payload = new byte[integrity];
            if (!Convert.TryFromBase64Chars(section, payload.AsSpan(), out int readByteCount))
#endif
            {
                throw new FormatException(ExceptionFormat.AsFormat(Pbkdf2Hash.SectionIndexPayload, "Payload", section.ToString()) + $" Must be valid Base64.");
            }
            else if (readByteCount != payload.Length)
            {
                throw new FormatException(ExceptionFormat.AsFormat(Pbkdf2Hash.SectionIndexPayload, "Payload", section.ToString()) + $" Count of decoded bytes '{readByteCount}' must match hash integrity section[{Pbkdf2Hash.SectionIndexIntegrity}] value '{integrity}'.");
            }
            return payload;
        }

        /// <summary>
        /// Initializes a <see cref="Pbkdf2Hash"/> instance
        /// with the default algorithm name and iteration count.
        /// </summary>
        /// <param name="salt">The salt <see cref="byte"/> array.</param>
        /// <param name="payload">The payload <see cref="byte"/> array.</param>
        public Pbkdf2Hash(byte[] salt, byte[] payload)
            : this(DefaultAlgorithmName, DefaultIterationCount, payload.Length, salt, payload) { }

        /// <summary>
        /// Initializes a <see cref="Pbkdf2Hash"/> instance.
        /// </summary>
        /// <param name="algorithm">The algorithm name.</param>
        /// <param name="iterations">The iteration count.</param>
        /// <param name="integrity">The payload length.</param>
        /// <param name="salt">The salt <see cref="byte"/> array.</param>
        /// <param name="payload">The payload <see cref="byte"/> array.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="algorithm"/>, <paramref name="salt"/>, or <paramref name="payload"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="iterations"/> or <paramref name="integrity"/> are less than <c>0</c>.</exception>
        public Pbkdf2Hash(string algorithm, int iterations, int integrity, byte[] salt, byte[] payload)
        {
            // These are not meant as validation, only as sanity checks.
            // No nulls, no negatives.
            Throw.If.Arg.IsNull(nameof(algorithm), algorithm);
            Throw.If.Arg.IsLesser(nameof(iterations), iterations, 0);
            Throw.If.Arg.IsLesser(nameof(integrity), integrity, 0);
            Throw.If.Arg.IsNull(nameof(salt), salt);
            Throw.If.Arg.IsNull(nameof(payload), payload);
            this.Algorithm = algorithm;
            this.Iterations = iterations;
            this.Integrity = integrity;
            this.Salt = salt;
            this.Payload = payload;
        }

        /// <summary>
        /// Gets the hash algorithm name.
        /// </summary>
        public string Algorithm { get; }

        /// <summary>
        /// Gets the hash iteration count.
        /// </summary>
        public int Iterations { get; }

        /// <summary>
        /// Gets the hash integrity value.
        /// </summary>
        public int Integrity { get; }

        /// <summary>
        /// Gets the hash salt content.
        /// </summary>
        public byte[] Salt { get; }

        /// <summary>
        /// Gets the hash payload content.
        /// </summary>
        public byte[] Payload { get; }

        /// <summary>
        /// Gets the hash salt content as <c>Base64</c> encoded text.
        /// </summary>
        public string Base64Salt => Convert.ToBase64String(this.Salt);

        /// <summary>
        /// Gets the hash payload content as <c>Base64</c> encoded text.
        /// </summary>
        public string Base64Payload => Convert.ToBase64String(this.Payload);

        /// <summary>
        /// Determines if the <see cref="Pbkdf2Hash"/> is equivalent to <c>default</c>.
        /// </summary>
        /// <returns><c>true</c> if the <see cref="Pbkdf2Hash"/> is equivalent to <c>default</c>; otherwise <c>false</c>.</returns>
        public bool IsNone() => this.Equals(default);

        /// <summary>
        /// Converts the <see cref="Pbkdf2Hash"/> to its canonically formatted <see cref="string"/> representation.
        /// The canonical hash format is 'algorithm:iterations:integrity:base64_salt:base64_payload'.
        /// For example, a properly formatted hash for the password 'password' would be
        /// 'sha1:64000:18:v741vZLHthpjnKAUZhOoQRT67oopJ14E:NxVUAo1coj+Wtihhju1bqlmp'.
        /// </summary>
        /// <returns>A canonically formatted <see cref="string"/> representation of the <see cref="Pbkdf2Hash"/> instance.</returns>
        public override string ToString()
            => $"{this.Algorithm}:{this.Iterations}:{this.Integrity}:{this.Base64Salt}:{this.Base64Payload}";

        /// <inheritdoc/>
        public override int GetHashCode()
            => HashCode.Combine(this.Algorithm, this.Iterations, this.Integrity, this.Salt, this.Payload);

        /// <inheritdoc/>
        public override bool Equals(object obj)
            => obj is Pbkdf2Hash hash && this.Equals(hash);

        /// <inheritdoc/>
        public bool Equals(Pbkdf2Hash other)
            => this.Algorithm == other.Algorithm && this.Iterations == other.Iterations && this.Integrity == other.Integrity && EqualityComparer<byte[]>.Default.Equals(this.Salt, other.Salt) && EqualityComparer<byte[]>.Default.Equals(this.Payload, other.Payload);

        /// <inheritdoc/>
        public static bool operator ==(Pbkdf2Hash left, Pbkdf2Hash right) => left.Equals(right);

        /// <inheritdoc/>
        public static bool operator !=(Pbkdf2Hash left, Pbkdf2Hash right) => !left.Equals(right);
    }
}
