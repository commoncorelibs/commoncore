﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Drawing;
using System.Security;
using System.Security.Cryptography;

namespace CommonCore.Security.Passwords.Hashing
{
    /// <summary>
    /// The <see cref="PasswordHasher"/> <c>static</c> <c>class</c> provides helper methods for
    /// hashing and comparing passwords using the PBKDF2 key derivation function.
    /// </summary>
    /// <remarks>
    /// This module implements the specifications at http://crackstation.net/hashing-security.htm,
    /// specifically the example C# implementation as of commit 4bb7f0b (https://github.com/defuse/password-hashing/blob/4bb7f0bcf478ea91467c90fc471629e6925da481/PasswordStorage.cs).
    /// This implementation differs from the example one in many ways:
    /// <list type="bullet">
    ///     <item><description><see cref="SecureString"/> is used in place of <see cref="string"/> for passwords. Passwords should never be stored in clear text strings.</description></item>
    ///     <item><description>All validation and constants encapsulated in the <see cref="Pbkdf2Hash"/> struct.</description></item>
    ///     <item><description><see cref="FormatException"/> is used instead of a custom <c>InvalidHashException</c> class.</description></item>
    ///     <item><description>Constants have been converted to static readonly properties</description></item>
    ///     <item><description>All static methods are public.</description></item>
    ///     <item><description>Standard C# naming conventions have been enforced</description></item>
    /// </list>
    /// </remarks>
    public static partial class PasswordHasher
    {
        /// <summary>
        /// Generates a <see cref="Pbkdf2Hash"/> for the specified <paramref name="secret"/>.
        /// </summary>
        /// <param name="secret">The secure string to hash.</param>
        /// <exception cref="ArgumentException">If <paramref name="secret"/> is <c>null</c> or <c>empty</c>.</exception>
        /// <returns>A <see cref="Pbkdf2Hash"/> equivalent to the specified <paramref name="secret"/>.</returns>
        public static Pbkdf2Hash GenerateHash(SecureString secret)
        {
            Throw.If.Arg.IsNullOrEmpty(nameof(secret), secret);
            byte[] salt = GenerateSalt(Pbkdf2Hash.DefaultSaltByteCount);
            byte[] payload = GeneratePayload(secret, salt);
            return new Pbkdf2Hash(salt, payload);
        }

        /// <summary>
        /// Determines if the specified <paramref name="hash"/> is valid for
        /// the specified <paramref name="secret"/>.
        /// </summary>
        /// <param name="secret">The secure string to compare.</param>
        /// <param name="hash">The hash to compare.</param>
        /// <exception cref="ArgumentException">If <paramref name="secret"/> is <c>null</c> or <c>empty</c>; or <paramref name="hash"/> is <c>default</c>.</exception>
        /// <returns><c>true</c> if <paramref name="secret"/> and <paramref name="hash"/> are equivalent; otherwise <c>false</c>.</returns>
        public static bool CompareHash(SecureString secret, Pbkdf2Hash hash)
        {
            Throw.If.Arg.IsNullOrEmpty(nameof(secret), secret);
            Throw.If.Arg.IsDefault(nameof(hash), hash);
            byte[] payload = GeneratePayload(secret, hash.Salt, hash.Iterations, hash.Payload.Length);
            return SlowEquals(payload, hash.Payload);
        }

        /// <summary>
        /// Determines if the specified <paramref name="hash"/> is valid for
        /// the specified <paramref name="secret"/>.
        /// </summary>
        /// <param name="secret">The secure string to compare.</param>
        /// <param name="hash">The hash to compare.</param>
        /// <exception cref="ArgumentException">If <paramref name="secret"/> or <paramref name="hash"/> are <c>null</c> or <c>empty</c>.</exception>
        /// <exception cref="FormatException">If <paramref name="hash"/> fails to parse as a <see cref="Pbkdf2Hash"/>.</exception>
        /// <returns><c>true</c> if <paramref name="secret"/> and <paramref name="hash"/> are equivalent; otherwise <c>false</c>.</returns>
        public static bool CompareHash(SecureString secret, string hash)
        {
            Throw.If.Arg.IsNullOrEmpty(nameof(secret), secret);
            Throw.If.Arg.IsNullOrEmpty(nameof(hash), hash);
            var checkHash = Pbkdf2Hash.ParseString(hash);
            //byte[] payload = GeneratePayload(secret, checkHash.Salt, checkHash.Iterations, checkHash.Payload.Length);
            //return SlowEquals(payload, checkHash.Payload);
            return CompareHash(secret, checkHash);
        }

        /// <summary>
        /// Generates a <see cref="byte"/> array representing a salted PBKDF2 hash payload
        /// for the specified <paramref name="secret"/>.
        /// The default iteration and payload byte counts as defined
        /// by <see cref="Pbkdf2Hash"/> will be used.
        /// </summary>
        /// <param name="secret">The password to hash.</param>
        /// <param name="salt">The salt to use for hash generation.</param>
        /// <exception cref="ArgumentException">If <paramref name="secret"/> or <paramref name="salt"/> are <c>null</c> or <c>empty</c>.</exception>
        /// <returns>Array of password hash payload bytes.</returns>
        public static byte[] GeneratePayload(SecureString secret, byte[] salt)
            => GeneratePayload(secret, salt, Pbkdf2Hash.DefaultIterationCount, Pbkdf2Hash.DefaultPayloadByteCount);

        /// <summary>
        /// Generates a <see cref="byte"/> array representing a salted PBKDF2 hash payload
        /// for the specified <paramref name="secret"/>.
        /// </summary>
        /// <param name="secret">The password to hash.</param>
        /// <param name="salt">The salt to use for hash generation.</param>
        /// <param name="iterations">The number of PBKDF2 iterations.</param>
        /// <param name="count">The number of bytes to generate for the hash.</param>
        /// <exception cref="ArgumentException">If <paramref name="secret"/> or <paramref name="salt"/> are <c>null</c> or <c>empty</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="iterations"/> or <paramref name="count"/> are less than <c>1</c>.</exception>
        /// <returns>Array of password hash payload bytes.</returns>
        public static byte[] GeneratePayload(SecureString secret, byte[] salt, int iterations, int count)
        {
            Throw.If.Arg.IsNullOrEmpty(nameof(secret), secret);
            Throw.If.Arg.IsNullOrEmpty(nameof(salt), salt);
            Throw.If.Arg.IsLesser(nameof(iterations), iterations, 1);
            Throw.If.Arg.IsLesser(nameof(count), count, 1);
            using var desecured = secret.ToByteArray();
            using var pbkdf2 = new Rfc2898DeriveBytes(desecured, salt, iterations);
            return pbkdf2.GetBytes(count);
        }

        /// <summary>
        /// Generates a <see cref="byte"/> array representing salt suitable for hashing passwords.
        /// Bytes are generated using <see cref="RandomNumberGenerator"/>.
        /// </summary>
        /// <param name="saltByteCount">The number of salt bytes to generate.</param>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="saltByteCount"/> is less than <c>1</c>.</exception>
        /// <returns>Array of salt bytes.</returns>
        public static byte[] GenerateSalt(int saltByteCount)
        {
            Throw.If.Arg.IsLesser(nameof(saltByteCount), saltByteCount, 1);
            byte[] salt = new byte[saltByteCount];
            using var provider = RandomNumberGenerator.Create();
            provider.GetBytes(salt);
            return salt;
        }

        /// <summary>
        /// Compares two <see cref="byte"/> arrays in length-constant time.
        /// This type of comparison reduces the effectiveness of timing attacks that
        /// utilize timing differences between to success/fail determinations by
        /// ensuring that a failed match takes the same time to calculate as a
        /// successful match.
        /// </summary>
        /// <param name="a">The first <see cref="byte"/> array.</param>
        /// <param name="b">The second <see cref="byte"/> array.</param>
        /// <returns><c>true</c> if both <see cref="byte"/> arrays are equal; otherwise <c>false</c>.</returns>
        public static bool SlowEquals(byte[] a, byte[] b)
        {
            uint diff = (uint)a.Length ^ (uint)b.Length;
            for (int i = 0; i < a.Length && i < b.Length; i++) { diff |= (uint)(a[i] ^ b[i]); }
            return diff == 0;
        }
    }
}
