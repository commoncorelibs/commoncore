﻿#region Copyright (c) 2014 Jay Jeckel
// Copyright (c) 2014 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Reflection;

namespace CommonCore.Reflection
{
    /// <summary>
    /// Static helper class for <see cref="Assembly"/> related operations.
    /// </summary>
    public static class AssemblyHelper
    {
        /// <summary>
        /// Gets the primary <see cref="System.Reflection.Assembly"/>.
        /// In the default application domain, then it will be the entry assembly;
        /// otherwise the assembly of the calling method.
        /// </summary>
        /// <returns>The entry assembly or the calling assembly.</returns>
        public static Assembly GetPrimaryAssembly()
            => Assembly.GetEntryAssembly() ?? Assembly.GetCallingAssembly();
    }
}
