﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.ComponentModel;

namespace CommonCore.Collections
{
    /// <summary>
    /// Provides data for the <see cref="ObservableItemCollection{T}.ItemPropertyChanged"/> event.
    /// </summary>
    public class ItemPropertyChangedEventArgs : PropertyChangedEventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ItemPropertyChangedEventArgs"/> class.
        /// </summary>
        /// <param name="index">The index of the changed item in the collection.</param>
        /// <param name="name">The name of the property that changed.</param>
        public ItemPropertyChangedEventArgs(int index, string name) : base(name) => this.ItemIndex = index;

        /// <summary>
        /// Gets the index of the item in the collection that changed.
        /// </summary>
        /// <value>The index of the item in the collection that changed.</value>
        public int ItemIndex { get; }
    }
}
