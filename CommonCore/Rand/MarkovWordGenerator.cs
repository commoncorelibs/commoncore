﻿#region Copyright (c) 2014 Jay Jeckel
// Copyright (c) 2014 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
//
// The code in this module was written to replace external code created by someone else.
// Original Implementation: http://www.siliconcommandergames.com/MarkovNameGenerator.htm
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;

namespace CommonCore.Rand
{
    /// <summary>
    /// Class implementing random word generation based on statistically weighted
    /// letter sequences in a given collection of sample words.
    /// Wikipedia: https://en.wikipedia.org/wiki/Markov_chain
    /// </summary>
    public class MarkovWordGenerator
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MarkovWordGenerator"/> class.
        /// Using a <paramref name="rootLength"/> of <c>3</c> provides decent results.
        /// </summary>
        /// <param name="samples">Sequence of sample words. Samples should not contain any spaces.</param>
        /// <param name="rootLength">Length of markov chain roots.</param>
        /// <param name="rand">Random object to use when generating words or <c>null</c> to use an internally created <see cref="Random"/> instance.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="samples"/> is <c>null</c>.</exception>
        public MarkovWordGenerator(IEnumerable<string> samples, int rootLength = 3, Random rand = null)
        {
            Throw.If.Arg.IsNull(nameof(samples), samples);
            this.RootLength = (rootLength < 1 ? 1 : rootLength);
            this.Rand = rand ?? new Random(BitConverter.ToInt32(System.Guid.NewGuid().ToByteArray(), 0));
            this.Chains = new Dictionary<string, List<char>>();

            this.InitializeSamples(samples);
            this.BuildChains();
        }

        /// <summary>
        /// The <see cref="Random"/> instance used in generating random words.
        /// </summary>
        public Random Rand { get; set; }

        /// <summary>
        /// Length of the markov chain roots. Using a length of <c>3</c> provides decent results.
        /// </summary>
        public int RootLength { get; protected set; }

        /// <summary>
        /// List of initial sample words. Collection is readonly as samples
        /// should not be arbitrarily changed once the list is initialized.
        /// </summary>
        public ReadOnlyCollection<string> Samples { get; protected set; }

        /// <summary>
        /// Dictionary of markov chains built from the initialized sample words.
        /// </summary>
        protected Dictionary<string, List<char>> Chains { get; set; }

        /// <summary>
        /// Initializes the sample list from the specified sequence of <paramref name="samples"/>.
        /// Sample words with a length less than or equal to the root length will be ignored.
        /// </summary>
        /// <param name="samples">Sample words to build a sample list from.</param>
        protected void InitializeSamples(IEnumerable<string> samples)
        {
            List<string> tokens = new List<string>();
            foreach (string sample in samples)
            {
                string token = sample.Trim().ToUpper();
                if (token.Length > this.RootLength) { tokens.Add(token); }
            }
            this.Samples = tokens.AsReadOnly();
        }

        /// <summary>
        /// Builds the dictionary of markov chains from the initialized sample words.
        /// </summary>
        protected void BuildChains()
        {
            this.Chains.Clear();
            foreach (string sample in this.Samples)
            {
                for (int letter = 0; letter < sample.Length - this.RootLength; letter++)
                {
                    string token = sample.Substring(letter, this.RootLength);
                    if (!this.Chains.ContainsKey(token)) { this.Chains[token] = new List<char>(); }
                    this.Chains[token].Add(sample[letter + this.RootLength]);
                }
            }
        }

        /// <summary>
        /// Generates a random letter from the chain with the token key.
        /// </summary>
        /// <param name="token">Key of the desired chain.</param>
        /// <returns>A random letter from token's chain.</returns>
        protected char GenLetter(string token)
        {
            if (!this.Chains.ContainsKey(token)) { return '\0'; }
            List<char> chain = this.Chains[token];
            return chain[this.Rand.Next(chain.Count)];
        }

        /// <summary>
        /// Generates a random word with the specified <paramref name="minLength"/>.
        /// </summary>
        /// <param name="minLength">Minimum length of the generated word.</param>
        /// <returns>A random markov chain generated word.</returns>
        public string GenWord(int minLength = 3)
        {
            if (minLength < 1) { minLength = 1; }
            var word = string.Empty;
            do
            {
                int index = this.Rand.Next(this.Samples.Count);
                int length = this.Samples[index].Length;
                word = this.Samples[index].Substring(this.Rand.Next(0, length - this.RootLength), this.RootLength);
                while (word.Length < length)
                {
                    string token = word.Substring(word.Length - this.RootLength, this.RootLength);
                    char c = this.GenLetter(token);
                    if (c != '\0') { word += c; }
                    else { break; }
                }
            }
            while (word.Length < minLength);
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(word.ToLower());
        }

        /// <summary>
        /// Generates a number of random words equal to the specified <paramref name="count"/> each
        /// with a the specified <paramref name="minLength"/>.
        /// </summary>
        /// <param name="count">The number of words to generate.</param>
        /// <param name="minLength">Minimum length of each generated word.</param>
        /// <returns>Sequence of random words.</returns>
        public IEnumerable<string> GenWords(int count, int minLength = 3)
        { for (int index = 0; index < count; index++) { yield return this.GenWord(minLength); } }

        /// <summary>
        /// Generates an endless sequence of random words.
        /// WARNING: Looping over this collection will cause an infinite loop unless you break it manually.
        /// It is recommended to use a construct like GenInfiniteWords().Take(100) to avoid this.
        /// </summary>
        /// <param name="minLength">Minimum length of each generated word.</param>
        /// <returns>Sequence of random words.</returns>
        public IEnumerable<string> GenInfiniteWords(int minLength = 3)
        { do { yield return this.GenWord(minLength); } while (true); }
    }
}
