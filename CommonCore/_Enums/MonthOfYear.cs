﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

namespace CommonCore
{
    /// <summary>
    /// Specifies the months of the year.
    /// </summary>
    public enum MonthOfYear
    {
        /// <summary>
        /// Indicates the first month of the year.
        /// </summary>
        January = 1,

        /// <summary>
        /// Indicates the second month of the year.
        /// </summary>
        February = 2,

        /// <summary>
        /// Indicates the third month of the year.
        /// </summary>
        March = 3,

        /// <summary>
        /// Indicates the fourth month of the year.
        /// </summary>
        April = 4,

        /// <summary>
        /// Indicates the fifth month of the year.
        /// </summary>
        May = 5,

        /// <summary>
        /// Indicates the sixth month of the year.
        /// </summary>
        June = 6,

        /// <summary>
        /// Indicates the seventh month of the year.
        /// </summary>
        July = 7,

        /// <summary>
        /// Indicates the eighth month of the year.
        /// </summary>
        August = 8,

        /// <summary>
        /// Indicates the ninth month of the year.
        /// </summary>
        September = 9,

        /// <summary>
        /// Indicates the tenth month of the year.
        /// </summary>
        October = 10,

        /// <summary>
        /// Indicates the eleventh month of the year.
        /// </summary>
        November = 11,

        /// <summary>
        /// Indicates the twelfth month of the year.
        /// </summary>
        December = 12
    }
}
