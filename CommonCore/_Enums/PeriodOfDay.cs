﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

namespace CommonCore
{
    /// <summary>
    /// Specifies the period of day.
    /// </summary>
    public enum PeriodOfDay : int
    {
        /// <summary>
        /// Indicates the period after <see cref="Night"/> and before <see cref="Afternoon"/>.
        /// 6 AM to Noon.
        /// </summary>
        Morning,

        /// <summary>
        /// Indicates the period after <see cref="Morning"/> and before <see cref="Evening"/>.
        /// Noon to 6 PM.
        /// </summary>
        Afternoon,

        /// <summary>
        /// Indicates the period after <see cref="Afternoon"/> and before <see cref="Night"/>.
        /// 6 PM to 10 PM.
        /// </summary>
        Evening,

        /// <summary>
        /// Indicates the period after <see cref="Evening"/> and before <see cref="Morning"/>.
        /// 10 PM to 6 AM.
        /// </summary>
        Night
    }
}
