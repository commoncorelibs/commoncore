﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Globalization;

namespace CommonCore
{
    /// <summary>
    /// Specifies the how to compare two <see cref="char"/> values.
    /// </summary>
    public enum ECharComparison : int
    {
        /// <summary>
        /// Indicates a normal, ordinal case-sensitive comparison.
        /// </summary>
        CaseSensitive,

        /// <summary>
        /// Indicates a case-insensitive comparison using <see cref="CultureInfo.CurrentCulture"/>
        /// language rules.
        /// </summary>
        CurrentCultureIgnoreCase,

        /// <summary>
        /// Indicates a case-insensitive comparison using <see cref="CultureInfo.InvariantCulture"/>
        /// language rules.
        /// </summary>
        InvariantCultureIgnoreCase
    }

    /// <summary>
    /// Static extension class for <see cref="ECharComparison"/> objects.
    /// </summary>
    public static class ECharComparisonExtensions
    {
        /// <summary>
        /// Converts this <see cref="ECharComparison"/> value to the appropriate <see cref="CultureInfo"/>.
        /// The <see cref="ECharComparison.CaseSensitive"/> value returns <c>null</c> as case-sensitive
        /// <see cref="char"/> comparisons are culture-ignorant.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>A <see cref="CultureInfo"/> appropriate to the value; <c>null</c> if value is <see cref="ECharComparison.CaseSensitive"/>.</returns>
        public static CultureInfo ToCultureInfo(this ECharComparison self)
        {
            switch (self)
            {
                case ECharComparison.CurrentCultureIgnoreCase: { return CultureInfo.CurrentCulture; }
                case ECharComparison.InvariantCultureIgnoreCase: { return CultureInfo.InvariantCulture; }
                case ECharComparison.CaseSensitive:
                default: { return null; }
            }
        }
    }
}
