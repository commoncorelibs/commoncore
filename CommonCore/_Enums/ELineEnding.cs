﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

namespace CommonCore
{
    /// <summary>
    /// Specifies the four types of line endings.
    /// </summary>
    public enum ELineEnding : int
    {
        /// <summary>
        /// Indicates the Line Feed (LF) line ending (\n).
        /// This ending is the unofficial universal standard.
        /// Unless otherwise required, this is the default line ending.
        /// </summary>
        N = 0,

        /// <summary>
        /// Indicates the Carriage Return (CR) line ending (\r).
        /// This ending is not common on modern systems, but is
        /// included for legacy compatibility. Unless specifically
        /// required, this value should not be used.
        /// </summary>
        R = 1,

        /// <summary>
        /// Indicates the Carriage Return + Line Feed (CR+LF) line ending (\r\n).
        /// This ending is the official Windows standard.
        /// </summary>
        RN = 2,

        /// <summary>
        /// Indicates the Line Feed + Carriage Return (LF+CR) line ending (\n\r).
        /// This ending has never been common, but is included for compatibility.
        /// Unless specifically required, this value should not be used.
        /// </summary>
        NR = 3
    }

    //public static class ELineEndingExtensions
    //{
    //    public static string ToLineEnding(this ELineEnding self) => StringExtensions.LineEndings[(int)self];
    //}
}
