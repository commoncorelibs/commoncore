﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;

namespace CommonCore
{
    /// <summary>
    /// Static helper class for dealing with <see cref="DateTime"/> objects.
    /// </summary>
    public static class DateHelper
    {
        /// <summary>
        /// Returns <see cref="DateTime"/> representing the first occurrence of <see cref="DayOfWeek"/> within <see cref="MonthOfYear"/>.
        /// </summary>
        /// <param name="year">The year component (1 through 9999).</param>
        /// <param name="month">Enumeration value that indicates the month component (1 through 12).</param>
        /// <param name="weekday">Enumeration value that indicates the day component (0 through 6).</param>
        /// <returns><see cref="DateTime"/> representing the first occurrence of <see cref="DayOfWeek"/> within <see cref="MonthOfYear"/>.</returns>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="year"/> is less than 1 or greater than 9999. <paramref name="month"/> is less than 1 or greater than 12. <paramref name="weekday"/> is less than 0 or greater than 6.</exception>
        public static DateTime FindFirstWeekday(int year, MonthOfYear month, DayOfWeek weekday)
        {
            Throw.If.Arg.IsLesser(nameof(year), year, 1);
            Throw.If.Arg.IsGreater(nameof(year), year, 9999);
            Throw.If.Arg.IsLesser(nameof(month), (int) month, 1);
            Throw.If.Arg.IsGreater(nameof(month), (int) month, 12);
            Throw.If.Arg.IsLesser(nameof(weekday), (int) weekday, 0);
            Throw.If.Arg.IsGreater(nameof(weekday), (int) weekday, 6);

            var date = new DateTime(year, (int)month, 1);
            for (int i = 0; i < 7; i++) { if (date.DayOfWeek == weekday) { break; } else { date = date.AddDays(1); } }
            return date;
        }

        /// <summary>
        /// Returns <see cref="DateTime"/> representing the second occurrence of <see cref="DayOfWeek"/> within <see cref="MonthOfYear"/>.
        /// </summary>
        /// <param name="year">The year component (1 through 9999).</param>
        /// <param name="month">Enumeration value that indicates the month component (1 through 12).</param>
        /// <param name="weekday">Enumeration value that indicates the day component (0 through 6).</param>
        /// <returns><see cref="DateTime"/> representing the first occurrence of <see cref="DayOfWeek"/> within <see cref="MonthOfYear"/>.</returns>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="year"/> is less than 1 or greater than 9999. <paramref name="month"/> is less than 1 or greater than 12. <paramref name="weekday"/> is less than 0 or greater than 6.</exception>
        public static DateTime FindSecondWeekday(int year, MonthOfYear month, DayOfWeek weekday)
        {
            Throw.If.Arg.IsLesser(nameof(year), year, 1);
            Throw.If.Arg.IsGreater(nameof(year), year, 9999);
            Throw.If.Arg.IsLesser(nameof(month), (int) month, 1);
            Throw.If.Arg.IsGreater(nameof(month), (int) month, 12);
            Throw.If.Arg.IsLesser(nameof(weekday), (int) weekday, 0);
            Throw.If.Arg.IsGreater(nameof(weekday), (int) weekday, 6);

            //var date = new DateTime(year, (int)month, 1);
            //for (int i = 0; i < 7; i++) { if (date.DayOfWeek == weekday) { break; } else { date = date.AddDays(1); } }
            //return date.AddDays(7);
            return FindFirstWeekday(year, month, weekday).AddDays(7);
        }

        /// <summary>
        /// Returns <see cref="DateTime"/> representing the third occurrence of <see cref="DayOfWeek"/> within <see cref="MonthOfYear"/>.
        /// </summary>
        /// <param name="year">The year component (1 through 9999).</param>
        /// <param name="month">Enumeration value that indicates the month component (1 through 12).</param>
        /// <param name="weekday">Enumeration value that indicates the day component (0 through 6).</param>
        /// <returns><see cref="DateTime"/> representing the first occurrence of <see cref="DayOfWeek"/> within <see cref="MonthOfYear"/>.</returns>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="year"/> is less than 1 or greater than 9999. <paramref name="month"/> is less than 1 or greater than 12. <paramref name="weekday"/> is less than 0 or greater than 6.</exception>
        public static DateTime FindThirdWeekday(int year, MonthOfYear month, DayOfWeek weekday)
        {
            Throw.If.Arg.IsLesser(nameof(year), year, 1);
            Throw.If.Arg.IsGreater(nameof(year), year, 9999);
            Throw.If.Arg.IsLesser(nameof(month), (int) month, 1);
            Throw.If.Arg.IsGreater(nameof(month), (int) month, 12);
            Throw.If.Arg.IsLesser(nameof(weekday), (int) weekday, 0);
            Throw.If.Arg.IsGreater(nameof(weekday), (int) weekday, 6);

            //var date = new DateTime(year, (int)month, 1);
            //for (int i = 0; i < 7; i++) { if (date.DayOfWeek == weekday) { break; } else { date = date.AddDays(1); } }
            //return date.AddDays(14);
            return FindFirstWeekday(year, month, weekday).AddDays(14);
        }

        /// <summary>
        /// Returns <see cref="DateTime"/> representing the fourth occurrence of <see cref="DayOfWeek"/> within <see cref="MonthOfYear"/>.
        /// </summary>
        /// <param name="year">The year component (1 through 9999).</param>
        /// <param name="month">Enumeration value that indicates the month component (1 through 12).</param>
        /// <param name="weekday">Enumeration value that indicates the day component (0 through 6).</param>
        /// <returns><see cref="DateTime"/> representing the first occurrence of <see cref="DayOfWeek"/> within <see cref="MonthOfYear"/>.</returns>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="year"/> is less than 1 or greater than 9999. <paramref name="month"/> is less than 1 or greater than 12. <paramref name="weekday"/> is less than 0 or greater than 6.</exception>
        public static DateTime FindFourthWeekday(int year, MonthOfYear month, DayOfWeek weekday)
        {
            Throw.If.Arg.IsLesser(nameof(year), year, 1);
            Throw.If.Arg.IsGreater(nameof(year), year, 9999);
            Throw.If.Arg.IsLesser(nameof(month), (int) month, 1);
            Throw.If.Arg.IsGreater(nameof(month), (int) month, 12);
            Throw.If.Arg.IsLesser(nameof(weekday), (int) weekday, 0);
            Throw.If.Arg.IsGreater(nameof(weekday), (int) weekday, 6);

            //var date = new DateTime(year, (int)month, 1);
            //for (int i = 0; i < 7; i++) { if (date.DayOfWeek == weekday) { break; } else { date = date.AddDays(1); } }
            //return date.AddDays(21);
            return FindFirstWeekday(year, month, weekday).AddDays(21);
        }

        /// <summary>
        /// Returns <see cref="DateTime"/> representing the last (either fourth or fifth) occurrence of <see cref="DayOfWeek"/> within <see cref="MonthOfYear"/>.
        /// </summary>
        /// <param name="year">The year component (1 through 9999).</param>
        /// <param name="month">Enumeration value that indicates the month component (1 through 12).</param>
        /// <param name="weekday">Enumeration value that indicates the day component (0 through 6).</param>
        /// <returns><see cref="DateTime"/> representing the first occurrence of <see cref="DayOfWeek"/> within <see cref="MonthOfYear"/>.</returns>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="year"/> is less than 1 or greater than 9999. <paramref name="month"/> is less than 1 or greater than 12. <paramref name="weekday"/> is less than 0 or greater than 6.</exception>
        public static DateTime FindLastWeekday(int year, MonthOfYear month, DayOfWeek weekday)
        {
            Throw.If.Arg.IsLesser(nameof(year), year, 1);
            Throw.If.Arg.IsGreater(nameof(year), year, 9999);
            Throw.If.Arg.IsLesser(nameof(month), (int) month, 1);
            Throw.If.Arg.IsGreater(nameof(month), (int) month, 12);
            Throw.If.Arg.IsLesser(nameof(weekday), (int) weekday, 0);
            Throw.If.Arg.IsGreater(nameof(weekday), (int) weekday, 6);

            DateTime fourth = DateHelper.FindFourthWeekday(year, month, weekday);
            DateTime fifth = fourth.AddDays(7);
            return (fifth.Month == (int)month ? fifth : fourth);
        }

        /// <summary>
        /// Returns <see cref="DateTime"/> representing the Nth occurrence of <see cref="DayOfWeek"/> within <see cref="MonthOfYear"/>.
        /// </summary>
        /// <param name="year">The year component (1 through 9999).</param>
        /// <param name="month">Enumeration value that indicates the month component (1 through 12).</param>
        /// <param name="weekday">Enumeration value that indicates the day component (0 through 6).</param>
        /// <param name="occurrence">The nth occurrence to search for (1 through 5).</param>
        /// <returns><see cref="DateTime"/> representing the first occurrence of <see cref="DayOfWeek"/> within <see cref="MonthOfYear"/>.</returns>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="year"/> is less than 1 or greater than 9999. <paramref name="month"/> is less than 1 or greater than 12. <paramref name="weekday"/> is less than 0 or greater than 6. <paramref name="occurrence"/> is less than 1 or greater than 5 or greater than the count of <paramref name="weekday"/> in <paramref name="month"/>.</exception>
        public static DateTime FindNthWeekday(int year, MonthOfYear month, DayOfWeek weekday, int occurrence)
        {
            Throw.If.Arg.IsLesser(nameof(year), year, 1);
            Throw.If.Arg.IsGreater(nameof(year), year, 9999);
            Throw.If.Arg.IsLesser(nameof(month), (int)month, 1);
            Throw.If.Arg.IsGreater(nameof(month), (int)month, 12);
            Throw.If.Arg.IsLesser(nameof(weekday), (int)weekday, 0);
            Throw.If.Arg.IsGreater(nameof(weekday), (int)weekday, 6);
            Throw.If.Arg.IsLesser(nameof(occurrence), occurrence, 1);
            Throw.If.Arg.IsGreater(nameof(occurrence), occurrence, 5);

            var date = new DateTime(year, (int)month, 1);
            for (int i = 0; i < 7; i++) { if (date.DayOfWeek == weekday) { break; } else { date = date.AddDays(1); } }
            date = date.AddDays((occurrence - 1) * 7);

            if (occurrence == 5 && date.Month != (int)month) { throw new ArgumentOutOfRangeException(nameof(occurrence), occurrence, $"The given month has less than {occurrence} {weekday}s"); }

            return date;
        }
    }
}
