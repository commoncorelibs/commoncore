﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Globalization;

namespace CommonCore
{
    /// <summary>
    /// Static helper class for collecting code that doesn't belong in other more
    /// specific helper or extension classes.
    /// </summary>
    public static class CommonHelper
    {

        #region IsCharEqual Methods
        /// <summary>
        /// Determines if <see cref="char"/> value <paramref name="a"/> is equivalent to <see cref="char"/>
        /// value <paramref name="b"/> according to the specified <paramref name="comparison"/>.
        /// </summary>
        /// <param name="a">The first <see cref="char"/> to compare.</param>
        /// <param name="b">The second <see cref="char"/> to compare.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for the comparison.</param>
        /// <returns><c>true</c> if the two <see cref="char"/> values are equivalent; otherwise <c>false</c>.</returns>
        public static bool IsCharEqual(char a, char b, ECharComparison comparison)
        {
            if (comparison == ECharComparison.CaseSensitive) { return a == b; }
            return IsCharEqual(a, b, comparison.ToCultureInfo());
        }

        /// <summary>
        /// Determines if <see cref="char"/> value <paramref name="a"/> is equivalent to <see cref="char"/>
        /// value <paramref name="b"/> according case-insensitivity rules of the specified <see cref="CultureInfo"/>.
        /// If <paramref name="insensitiveCulture"/> is <c>null</c>, then a typical case-sensitive ordinal
        /// comparison will be performed.
        /// </summary>
        /// <param name="a">The first <see cref="char"/> to compare.</param>
        /// <param name="b">The second <see cref="char"/> to compare.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <returns><c>true</c> if the two <see cref="char"/> values are equivalent; otherwise <c>false</c>.</returns>
        public static bool IsCharEqual(char a, char b, CultureInfo insensitiveCulture)
        {
            if (insensitiveCulture == null) { return a == b; }
            return a == b || (char.ToUpper(a, insensitiveCulture) == char.ToUpper(b, insensitiveCulture) || char.ToLower(a, insensitiveCulture) == char.ToLower(b, insensitiveCulture));
        }

        #endregion

    }
}
