﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;

namespace CommonCore
{
    /// <summary>
    /// Static helper class for dealing with <see cref="DateTime"/> objects and holidays.
    /// </summary>
    public static class HolidayHelper
    {
        /// <summary>
        /// Returns a <see cref="DateTime"/> representing New Year's Day for the specified <paramref name="year"/>.
        /// New Year's Day is always January 1st.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing New Year's Day for the specified <paramref name="year"/>.</returns>
        public static DateTime GetNewYearsDay(int year) => new DateTime(year, (int)MonthOfYear.January, 1);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing New Year's Eve for the specified <paramref name="year"/>.
        /// New Year's Eve is always December 31st.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing New Year's Eve for the specified <paramref name="year"/>.</returns>
        public static DateTime GetNewYearsEve(int year) => new DateTime(year, (int)MonthOfYear.December, 31);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Groundhog's Day for the specified <paramref name="year"/>.
        /// Groundhog's Day is always February 2nd.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Groundhog's Day for the specified <paramref name="year"/>.</returns>
        public static DateTime GetGroundhogsDay(int year) => new DateTime(year, (int)MonthOfYear.February, 2);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Valentine's Day for the specified <paramref name="year"/>.
        /// Valentine's Day is always February 14th.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Valentine's Day for the specified <paramref name="year"/>.</returns>
        public static DateTime GetValentinesDay(int year) => new DateTime(year, (int)MonthOfYear.February, 14);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Saint Patrick's Day for the specified <paramref name="year"/>.
        /// Saint Patrick's Day is always March 17th.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Saint Patrick's Day for the specified <paramref name="year"/>.</returns>
        public static DateTime GetSaintPatricksDay(int year) => new DateTime(year, (int)MonthOfYear.March, 17);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing April Fool's Day for the specified <paramref name="year"/>.
        /// April Fool's Day is always April 1st.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing April Fool's Day for the specified <paramref name="year"/>.</returns>
        public static DateTime GetAprilFoolsDay(int year) => new DateTime(year, (int)MonthOfYear.April, 1);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Earth Day for the specified <paramref name="year"/>.
        /// Earth Day is always April 22nd.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Earth Day for the specified <paramref name="year"/>.</returns>
        public static DateTime GetEarthDay(int year) => new DateTime(year, (int)MonthOfYear.April, 22);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Arbor Day for the specified <paramref name="year"/>.
        /// Arbor Day is last Friday of April.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Arbor Day for the specified <paramref name="year"/>.</returns>
        public static DateTime GetArborDay(int year) => DateHelper.FindLastWeekday(year, MonthOfYear.April, DayOfWeek.Friday);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Cinco de Mayo for the specified <paramref name="year"/>.
        /// Cinco de Mayo is always May 5th.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Cinco de Mayo for the specified <paramref name="year"/>.</returns>
        public static DateTime GetCincodeMayo(int year) => new DateTime(year, (int)MonthOfYear.May, 5);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Mother's Day for the specified <paramref name="year"/>.
        /// Mother's Day is the second Sunday of May.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Mother's Day for the specified <paramref name="year"/>.</returns>
        public static DateTime GetMothersDay(int year) => DateHelper.FindSecondWeekday(year, MonthOfYear.May, DayOfWeek.Sunday);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Memorial Day for the specified <paramref name="year"/>.
        /// Memorial Day is the last Monday of May.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Memorial Day for the specified <paramref name="year"/>.</returns>
        public static DateTime GetMemorialDay(int year) => DateHelper.FindLastWeekday(year, MonthOfYear.May, DayOfWeek.Monday);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Father's Day for the specified <paramref name="year"/>.
        /// Father's Day is the third Sunday of June.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Father's Day for the specified <paramref name="year"/>.</returns>
        public static DateTime GetFathersDay(int year) => DateHelper.FindThirdWeekday(year, MonthOfYear.June, DayOfWeek.Sunday);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Independence Day for the specified <paramref name="year"/>.
        /// Independence Day is always July 4th.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Independence Day for the specified <paramref name="year"/>.</returns>
        public static DateTime GetIndependenceDay(int year) => new DateTime(year, (int)MonthOfYear.July, 4);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Labor Day for the specified <paramref name="year"/>.
        /// Labor Day is the first Monday of September.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Labor Day for the specified <paramref name="year"/>.</returns>
        public static DateTime GetLaborDay(int year) => DateHelper.FindFirstWeekday(year, MonthOfYear.September, DayOfWeek.Monday);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Leif Erikson Day for the specified <paramref name="year"/>.
        /// Leif Erikson Day is always October 9th.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Leif Erikson Day for the specified <paramref name="year"/>.</returns>
        public static DateTime GetLeifEriksonDay(int year) => new DateTime(year, (int)MonthOfYear.October, 9);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Columbus Day for the specified <paramref name="year"/>.
        /// Independence Day is always October 12th.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Columbus Day for the specified <paramref name="year"/>.</returns>
        public static DateTime GetColumbusDay(int year) => new DateTime(year, (int)MonthOfYear.October, 12);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Halloween Day for the specified <paramref name="year"/>.
        /// Halloween Day is always October 31st.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Halloween Day for the specified <paramref name="year"/>.</returns>
        public static DateTime GetHalloweenDay(int year) => new DateTime(year, (int)MonthOfYear.October, 31);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing All Saints' Day for the specified <paramref name="year"/>.
        /// All Saints' Day is always November 1st.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing All Saints' Day for the specified <paramref name="year"/>.</returns>
        public static DateTime GetAllSaintsDay(int year) => new DateTime(year, (int)MonthOfYear.November, 1);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Veterans Day for the specified <paramref name="year"/>.
        /// Veterans Day is always November 11th.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Veterans Day for the specified <paramref name="year"/>.</returns>
        public static DateTime GetVeteransDay(int year) => new DateTime(year, (int)MonthOfYear.November, 11);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Thanksgiving Day for the specified <paramref name="year"/>.
        /// Thanksgiving Day is the fourth Thursday of November.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Thanksgiving Day for the specified <paramref name="year"/>.</returns>
        public static DateTime GetThanksgivingDay(int year) => DateHelper.FindFourthWeekday(year, MonthOfYear.November, DayOfWeek.Thursday);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Black Friday for the specified <paramref name="year"/>.
        /// Black Friday is the fourth Friday of November.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Black Friday for the specified <paramref name="year"/>.</returns>
        public static DateTime GetBlackFriday(int year) => DateHelper.FindFourthWeekday(year, MonthOfYear.November, DayOfWeek.Friday);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Christmas Eve for the specified <paramref name="year"/>.
        /// Christmas Eve is always December 24th.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Christmas Eve for the specified <paramref name="year"/>.</returns>
        public static DateTime GetChristmasEve(int year) => new DateTime(year, (int)MonthOfYear.December, 24);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Christmas Day for the specified <paramref name="year"/>.
        /// Christmas Day is always December 25th.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Christmas Day for the specified <paramref name="year"/>.</returns>
        public static DateTime GetChristmasDay(int year) => new DateTime(year, (int)MonthOfYear.December, 25);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Easter Day for the specified <paramref name="year"/>.
        /// The returned date is only accurate for years greater than 1852.
        /// In simplest terms, Easter Day is the first Sunday after the first full moon after vernal equinox.
        /// For a more accurate description of when Easter Day happens, see: http://www.webexhibits.org/calendars/calendar-christian-easter.html
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Easter Day for the specified <paramref name="year"/>.</returns>
        public static DateTime GetEasterDay(int year)
        {
            Throw.If.Arg.IsLesser(nameof(year), year, 1);
            Throw.If.Arg.IsGreater(nameof(year), year, 9999);

            int century = year / 100;
            int golden = year % 19;

            int epact = (century - century / 4 - ((8 * century + 13) / 25) + 19 * golden + 15) % 30;
            int paschalOffset = epact - (epact / 28) * (1 - (29 / (epact + 1)) * ((21 - golden) / 11));
            int paschalWeekday = (year + (year / 4) + paschalOffset + 2 - century + century / 4) % 7;
            int l = paschalOffset - paschalWeekday;

            int month = 3 + ((l + 40) / 44);
            int day = l + 28 - 31 * (month / 4);

            return new DateTime(year, month, day);
        }

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Mardi Gras (Shrove Tuesday) for the specified <paramref name="year"/>.
        /// Mardi Gras (Shrove Tuesday) is the day before Ash Wednesday and forty-seven days before Easter Day.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Mardi Gras (Shrove Tuesday) for the specified <paramref name="year"/>.</returns>
        public static DateTime GetMardiGras(int year) => HolidayHelper.GetEasterDay(year).AddDays(-47);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Ash Wednesday for the specified <paramref name="year"/>.
        /// Ash Wednesday is the beginning of Lent and forty-six days before Easter Day.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Ash Wednesday for the specified <paramref name="year"/>.</returns>
        public static DateTime GetAshWednesday(int year) => HolidayHelper.GetEasterDay(year).AddDays(-46);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Palm Sunday for the specified <paramref name="year"/>.
        /// Palm Sunday is the Sunday one week before Easter Day.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Palm Sunday for the specified <paramref name="year"/>.</returns>
        public static DateTime GetPalmSunday(int year) => HolidayHelper.GetEasterDay(year).AddDays(-7);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Maundy Thursday for the specified <paramref name="year"/>.
        /// Maundy Thursday is the Thursday three days before Easter Day.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Maundy Thursday for the specified <paramref name="year"/>.</returns>
        public static DateTime GetMaundyThursday(int year) => HolidayHelper.GetEasterDay(year).AddDays(-3);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Good Friday for the specified <paramref name="year"/>.
        /// Good Friday is the Friday two days before Easter Day.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Good Friday for the specified <paramref name="year"/>.</returns>
        public static DateTime GetGoodFriday(int year) => HolidayHelper.GetEasterDay(year).AddDays(-2);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Ascension Day for the specified <paramref name="year"/>.
        /// Ascension Day is the 40th day of Easter and is thirty-nine days after Easter Day.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Ascension Day for the specified <paramref name="year"/>.</returns>
        public static DateTime GetAscensionDay(int year) => HolidayHelper.GetEasterDay(year).AddDays(39);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Whit Sunday (Pentecost) for the specified <paramref name="year"/>.
        /// Whit Sunday (Pentecost) is seven weeks after Easter Day.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Whit Sunday (Pentecost) for the specified <paramref name="year"/>.</returns>
        public static DateTime GetWhitSunday(int year) => HolidayHelper.GetEasterDay(year).AddDays(49);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Trinity Sunday for the specified <paramref name="year"/>.
        /// Trinity Sunday is one week after Whit Sunday (Pentecost).
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Trinity Sunday for the specified <paramref name="year"/>.</returns>
        public static DateTime GetTrinitySunday(int year) => HolidayHelper.GetEasterDay(year).AddDays(56);

        /// <summary>
        /// Returns a <see cref="DateTime"/> representing Corpus Christi for the specified <paramref name="year"/>.
        /// Corpus Christi is the Thursday after Trinity Sunday.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns><see cref="DateTime"/> representing Corpus Christi for the specified <paramref name="year"/>.</returns>
        public static DateTime GetCorpusChristiFeast(int year) => HolidayHelper.GetEasterDay(year).AddDays(60);

        //public static bool IsEaster(DateTime self)
        //{
        //    int Y = self.Year;
        //    int a = Y % 19;
        //    int b = Y / 100;
        //    int c = Y % 100;
        //    int d = b / 4;
        //    int e = b % 4;
        //    int f = (b + 8) / 25;
        //    int g = (b - f + 1) / 3;
        //    int h = (19 * a + b - d - g + 15) % 30;
        //    int i = c / 4;
        //    int k = c % 4;
        //    int L = (32 + 2 * e + 2 * i - h - k) % 7;
        //    int m = (a + 11 * h + 22 * L) / 451;
        //    int Month = (h + L - 7 * m + 114) / 31;
        //    int Day = ((h + L - 7 * m + 114) % 31) + 1;
        //    DateTime easter = new DateTime(Y, Month, Day);
        //    return self.Date == easter.Date;
        //}

        ///// <remarks>The first sunday of advent is the first sunday at least 4 weeks before christmas</remarks>
        //public static DateTime FirstSundayOfAdvent(int year)
        //{
        //    int weeks = 4;
        //    int correction = 0;
        //    DateTime christmas = GetChristmasDay(year);

        //    if (christmas.DayOfWeek != DayOfWeek.Sunday)
        //    {
        //        weeks--;
        //        correction = ((int)christmas.DayOfWeek - (int)DayOfWeek.Sunday);
        //    }

        //    return christmas.AddDays(-1 * ((weeks * 7) + correction));
        //}

        //public static bool IsChristmas(this DateTime self) => self.Month == 12 && self.Day == 25;

        //public static bool IsChristmasEve(this DateTime self) => self.Month == 12 && self.Day == 24;
    }
}
