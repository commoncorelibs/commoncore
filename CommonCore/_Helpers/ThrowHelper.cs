﻿#region Copyright (c) 2014 Jay Jeckel
// Copyright (c) 2014 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace CommonCore
{
    /// <summary>
    /// A static helper class to wrap boilerplate exception throwing code and
    /// standardize the exception messages. All throw methods are marked with
    /// the <see cref="DebuggerStepThroughAttribute"/> attribute, meaning that
    /// the debugger will mark the line calling the throw method and not the
    /// actual throwing of the exception within the called throw method itself.
    /// 
    /// <para>
    /// !!ATTENTION!!
    /// </para>
    /// <para>
    /// As of v2.0.0-pre.8, the <see cref="ThrowHelper"/> class is considered obsolete.
    /// The new separate <c>VinlandSolutions.CommonCore.Exceptions</c> should
    /// be used instead. Link: https://gitlab.com/commoncorelibs/commoncore-exceptions
    /// </para>
    /// </summary>
    [Obsolete("Use VinlandSolutions.CommonCore.Exceptions package instead.")]
    public static partial class ThrowHelper
    {

        #region IfSelfNull Methods

        /// <summary>
        /// Throws <see cref="NullReferenceException"/> if the given <paramref name="value"/> is <c>null</c>.
        /// This method is primarily intended for extension methods to verify they are being called
        /// from a valid non-null object, similar to non-extension methods.
        /// </summary>
        /// <typeparam name="T">The type of object to validate.</typeparam>
        /// <param name="value">The object to validate.</param>
        /// <exception cref="NullReferenceException">If <paramref name="value"/> is <c>null</c>.</exception>
        //Object reference not set to an instance of an object.
        [DebuggerStepThrough()]
        public static void IfSelfNull<T>(T value) where T : class
        { if (value == null) { throw new NullReferenceException(); } }

        #endregion

        #region IfArgNull Methods

        /// <summary>
        /// Throws <see cref="ArgumentNullException"/> if the given <paramref name="value"/> is <c>null</c>.
        /// </summary>
        /// <typeparam name="T">The type of object to validate.</typeparam>
        /// <param name="value">The object to validate.</param>
        /// <param name="argumentName">The argument name representing the value.</param>
        [DebuggerStepThrough()]
        public static void IfArgNull<T>(T value, string argumentName) where T : class
        { if (value == null) { throw new ArgumentNullException(argumentName, $"Argument '{argumentName}' value must not be null."); } }

        #endregion

        #region IfArgIndexOutOfRange

        /// <summary>
        /// Throws <see cref="ArgumentOutOfRangeException" /> if the <paramref name="index"/> is less than
        /// <c>0</c> or greater than or equal to the <paramref name="span"/> length.
        /// </summary>
        /// <param name="index">The index to validate.</param>
        /// <param name="argumentName">The argument name representing the index.</param>
        /// <param name="span">The span to validate the index against.</param>
        [DebuggerStepThrough()]
        public static void IfArgIndexOutOfRange<T>(int index, string argumentName, Span<T> span)
        {
            if (index < 0 || index >= span.Length)
            { throw new ArgumentOutOfRangeException(argumentName, index, $"Argument '{argumentName}' index '{index}' must be greater than or equal to zero and less than the span length '{span.Length}'."); }
        }

        /// <summary>
        /// Throws <see cref="ArgumentOutOfRangeException" /> if the <paramref name="index"/> is less than
        /// <c>0</c> or greater than or equal to the <paramref name="span"/> length.
        /// </summary>
        /// <param name="index">The index to validate.</param>
        /// <param name="argumentName">The argument name representing the index.</param>
        /// <param name="span">The span to validate the index against.</param>
        [DebuggerStepThrough()]
        public static void IfArgIndexOutOfRange<T>(int index, string argumentName, ReadOnlySpan<T> span)
        {
            if (index < 0 || index >= span.Length)
            { throw new ArgumentOutOfRangeException(argumentName, index, $"Argument '{argumentName}' index '{index}' must be greater than or equal to zero and less than the span length '{span.Length}'."); }
        }

        #endregion

        #region IfArgEnumOutOfRange Methods

        /// <summary>
        /// Throws <see cref="ArgumentOutOfRangeException" /> if the <paramref name="value"/> does not
        /// exist in its enumeration.
        /// </summary>
        /// <param name="value">The enum value to validate.</param>
        /// <param name="argumentName">The argument name representing the enum value.</param>
        [DebuggerStepThrough()]
        public static void IfArgEnumOutOfRange(Enum value, string argumentName)
        {
            var type = value.GetType();
            if (!Enum.IsDefined(type, value))
            {
                var values = Enum.GetValues(type).Cast<int>();
                int min = values.Min();
                int max = values.Max();
                throw new ArgumentOutOfRangeException(argumentName, value, $"Argument '{argumentName}' enum '{type}' value '{value}' must range from '{type}.{type.GetEnumName(min)} == {min}' to '{type}.{type.GetEnumName(max)} == {max}', inclusive.");
            }
        }

        #endregion

        #region IfArgOutOfRange Methods

        /// <summary>
        /// Throws <see cref="ArgumentOutOfRangeException" /> if the given <paramref name="value"/> is not within the defined range.
        /// </summary>
        /// <typeparam name="TValue">The type of the value to validate which must implement <see cref="IComparable{TValue}" />.</typeparam>
        /// <param name="value">The argument value to validate.</param>
        /// <param name="argumentName">The argument name representing the value.</param>
        /// <param name="min">The minimum value of the range.</param>
        /// <param name="max">The maximum value of the range.</param>
        [DebuggerStepThrough()]
        public static void IfArgOutOfRange<TValue>(TValue value, string argumentName, TValue min, TValue max)
            where TValue : IComparable<TValue>
        { if (value.CompareTo(min) < 0 || value.CompareTo(max) > 0) { throw new ArgumentOutOfRangeException(argumentName, value, $"Argument '{argumentName}' value '{value}' must range from '{min}' to '{max}', inclusive."); } }

        /// <summary>
        /// Throws <see cref="ArgumentOutOfRangeException" /> if the given <paramref name="value"/> is not within the defined range.
        /// </summary>
        /// <typeparam name="TValue">The type of the value to validate which must implement <see cref="IComparable{TValue}" />.</typeparam>
        /// <param name="value">The argument value to check.</param>
        /// <param name="argumentName">The argument name representing the value.</param>
        /// <param name="min">The minimum value of the range.</param>
        /// <param name="max">The maximum value of the range.</param>
        [DebuggerStepThrough()]
        public static void IfArgOutOfRangeEqual<TValue>(TValue value, string argumentName, TValue min, TValue max)
            where TValue : IComparable<TValue>
        { if (value.CompareTo(min) <= 0 || value.CompareTo(max) >= 0) { throw new ArgumentOutOfRangeException(argumentName, value, $"Argument '{argumentName}' value '{value}' must range from '{min}' to '{max}', exclusive."); } }

        #endregion

        #region IfArgGreater Methods

        /// <summary>
        /// Throws <see cref="ArgumentOutOfRangeException" /> if the given <paramref name="value"/> is not within the defined range.
        /// </summary>
        /// <typeparam name="TValue">The type of the value to validate which must implement <see cref="IComparable{TValue}" />.</typeparam>
        /// <param name="value">The argument value to check.</param>
        /// <param name="argumentName">The argument name representing the value.</param>
        /// <param name="max">The maximum value of the range.</param>
        [DebuggerStepThrough()]
        public static void IfArgGreater<TValue>(TValue value, string argumentName, TValue max)
            where TValue : IComparable<TValue>
        { if (value.CompareTo(max) > 0) { throw new ArgumentOutOfRangeException(argumentName, value, $"Argument '{argumentName}' value '{value}' must be less than or equal to '{max}'."); } }

        /// <summary>
        /// Throws <see cref="ArgumentOutOfRangeException" /> if the given <paramref name="value"/> is not within the defined range.
        /// </summary>
        /// <typeparam name="TValue">The type of the value to validate which must implement <see cref="IComparable{TValue}" />.</typeparam>
        /// <param name="value">The argument value to check.</param>
        /// <param name="argumentName">The argument name representing the value.</param>
        /// <param name="max">The maximum value of the range.</param>
        [DebuggerStepThrough()]
        public static void IfArgGreaterEqual<TValue>(TValue value, string argumentName, TValue max)
            where TValue : IComparable<TValue>
        { if (value.CompareTo(max) >= 0) { throw new ArgumentOutOfRangeException(argumentName, value, $"Argument '{argumentName}' value '{value}' must be less than '{max}'."); } }

        /// <summary>
        /// Throws <see cref="ArgumentOutOfRangeException" /> if the given <paramref name="value"/> is not within the defined range.
        /// </summary>
        /// <typeparam name="TValue">The type of the value to validate which must implement <see cref="IComparable{TValue}" />.</typeparam>
        /// <param name="value">The argument value to check.</param>
        /// <param name="argumentName">The argument name representing the value.</param>
        /// <param name="max">The maximum value of the range.</param>
        /// <param name="argumentMaxName">The argument name representing the limit.</param>
        [DebuggerStepThrough()]
        public static void IfArgGreater<TValue>(TValue value, string argumentName, TValue max, string argumentMaxName)
            where TValue : IComparable<TValue>
        { if (value.CompareTo(max) > 0) { throw new ArgumentOutOfRangeException(argumentName, value, $"Argument '{argumentName}' value '{value}' must be less than or equal to argument '{argumentMaxName}' value '{max}'."); } }

        /// <summary>
        /// Throws <see cref="ArgumentOutOfRangeException" /> if the given <paramref name="value"/> is not within the defined range.
        /// </summary>
        /// <typeparam name="TValue">The type of the value to validate which must implement <see cref="IComparable{TValue}" />.</typeparam>
        /// <param name="value">The argument value to check.</param>
        /// <param name="argumentName">The argument name representing the value.</param>
        /// <param name="max">The maximum value of the range.</param>
        /// <param name="argumentMaxName">The argument name representing the limit.</param>
        [DebuggerStepThrough()]
        public static void IfArgGreaterEqual<TValue>(TValue value, string argumentName, TValue max, string argumentMaxName)
            where TValue : IComparable<TValue>
        { if (value.CompareTo(max) >= 0) { throw new ArgumentOutOfRangeException(argumentName, value, $"Argument '{argumentName}' value '{value}' must be less than argument '{argumentMaxName}' value '{max}'."); } }

        #endregion

        #region IfArgLesser Methods

        /// <summary>
        /// Throws <see cref="ArgumentOutOfRangeException" /> if the given <paramref name="value"/> is not within the defined range.
        /// </summary>
        /// <typeparam name="TValue">The type of the value to validate which must implement <see cref="IComparable{TValue}" />.</typeparam>
        /// <param name="value">The argument value to check.</param>
        /// <param name="argumentName">The argument name representing the value.</param>
        /// <param name="min">The minimum value of the range.</param>
        [DebuggerStepThrough()]
        public static void IfArgLesser<TValue>(TValue value, string argumentName, TValue min)
            where TValue : IComparable<TValue>
        { if (value.CompareTo(min) < 0) { throw new ArgumentOutOfRangeException(argumentName, value, $"Argument '{argumentName}' value '{value}' must be greater than or equal to '{min}'."); } }

        /// <summary>
        /// Throws <see cref="ArgumentOutOfRangeException" /> if the given <paramref name="value"/> is not within the defined range.
        /// </summary>
        /// <typeparam name="TValue">The type of the value to validate which must implement <see cref="IComparable{TValue}" />.</typeparam>
        /// <param name="value">The argument value to check.</param>
        /// <param name="argumentName">The argument name representing the value.</param>
        /// <param name="min">The minimum value of the range.</param>
        [DebuggerStepThrough()]
        public static void IfArgLesserEqual<TValue>(TValue value, string argumentName, TValue min)
            where TValue : IComparable<TValue>
        { if (value.CompareTo(min) <= 0) { throw new ArgumentOutOfRangeException(argumentName, value, $"Argument '{argumentName}' value '{value}' must be greater than '{min}'."); } }

        /// <summary>
        /// Throws <see cref="ArgumentOutOfRangeException" /> if the given <paramref name="value"/> is not within the defined range
        /// as determined by the second argument.
        /// </summary>
        /// <typeparam name="TValue">The type of the value to validate which must implement <see cref="IComparable{TValue}" />.</typeparam>
        /// <param name="value">The argument value to check.</param>
        /// <param name="argumentName">The argument name representing the value.</param>
        /// <param name="min">The minimum value of the range.</param>
        /// <param name="argumentMinName">The argument name representing the limit.</param>
        [DebuggerStepThrough()]
        public static void IfArgLesser<TValue>(TValue value, string argumentName, TValue min, string argumentMinName)
            where TValue : IComparable<TValue>
        { if (value.CompareTo(min) < 0) { throw new ArgumentOutOfRangeException(argumentName, value, $"Argument '{argumentName}' value '{value}' must be greater than or equal to argument '{argumentMinName}' value '{min}'."); } }

        /// <summary>
        /// Throws <see cref="ArgumentOutOfRangeException" /> if the given <paramref name="value"/> is not within the defined range
        /// as determined by the second argument.
        /// </summary>
        /// <typeparam name="TValue">The type of the value to validate which must implement <see cref="IComparable{TValue}" />.</typeparam>
        /// <param name="value">The argument value to check.</param>
        /// <param name="argumentName">The argument name representing the value.</param>
        /// <param name="min">The minimum value of the range.</param>
        /// <param name="argumentMinName">The argument name representing the limit.</param>
        [DebuggerStepThrough()]
        public static void IfArgLesserEqual<TValue>(TValue value, string argumentName, TValue min, string argumentMinName)
            where TValue : IComparable<TValue>
        { if (value.CompareTo(min) <= 0) { throw new ArgumentOutOfRangeException(argumentName, value, $"Argument '{argumentName}' value '{value}' must be greater than argument '{argumentMinName}' value '{min}'."); } }

        #endregion

    }
}
