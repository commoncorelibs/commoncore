﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace CommonCore
{
    public static partial class ThrowHelper
    {

        #region IfArgNull Methods

        /// <summary>
        /// Throws <see cref="ArgumentException"/> if the specified <paramref name="value"/> is <c>default</c>.
        /// </summary>
        /// <typeparam name="T">The type of struct to validate.</typeparam>
        /// <param name="value">The struct to validate.</param>
        /// <param name="argumentName">The argument name representing the value.</param>
        [DebuggerStepThrough()]
        public static void IfArgDefault<T>(T value, string argumentName) where T : struct
        { if (value.Equals(default(T))) { throw new ArgumentNullException(argumentName, $"Argument '{argumentName}' value must not be default."); } }

        #endregion

    }
}
