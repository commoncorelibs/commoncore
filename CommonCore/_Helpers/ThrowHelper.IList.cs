﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections;
using System.Diagnostics;

namespace CommonCore
{
    public static partial class ThrowHelper
    {
        /// <summary>
        /// Throws <see cref="ArgumentNullException" /> if the specified <paramref name="value"/> is <c>null</c>.
        /// </summary>
        /// <param name="value">The argument value to check.</param>
        /// <param name="argumentName">The argument name representing the value.</param>
        [DebuggerStepThrough]
        public static void IfArgNull(IList value, string argumentName)
        { if (value == null) { throw new ArgumentNullException(argumentName, $"Argument '{argumentName}' value must not be null."); } }

        /// <summary>
        /// Throws <see cref="ArgumentException" /> if the specified <paramref name="value"/> is not <c>null</c> and has a length of zero.
        /// </summary>
        /// <param name="value">The argument value to check.</param>
        /// <param name="argumentName">The argument name representing the value.</param>
        [DebuggerStepThrough]
        public static void IfArgEmpty(IList value, string argumentName)
        { if (value != null && value.Count == 0) { throw new ArgumentException(argumentName, $"Argument '{argumentName}' value must not be empty."); } }

        /// <summary>
        /// Throws <see cref="ArgumentException" /> if the specified <paramref name="value"/> is <c>null</c> or has a length of zero.
        /// </summary>
        /// <param name="value">The argument value to check.</param>
        /// <param name="argumentName">The argument name representing the value.</param>
        [DebuggerStepThrough]
        public static void IfArgNullOrEmpty(IList value, string argumentName)
        { if (value == null || value.Count == 0) { throw new ArgumentException(argumentName, $"Argument '{argumentName}' value must not be null or empty."); } }

        /// <summary>
        /// Throws <see cref="ArgumentOutOfRangeException" /> if the <paramref name="index"/> is less than
        /// <c>0</c> or greater than or equal to the <paramref name="collection"/> count.
        /// </summary>
        /// <param name="index">The index to validate.</param>
        /// <param name="argumentName">The argument name representing the index.</param>
        /// <param name="collection">The collection to validate the index against.</param>
        [DebuggerStepThrough()]
        public static void IfArgIndexOutOfRange(int index, string argumentName, ICollection collection)
        {
            if (index < 0 || index >= collection.Count)
            { throw new ArgumentOutOfRangeException(argumentName, index, $"Argument '{argumentName}' index '{index}' must be greater than or equal to zero and less than the collection count '{collection.Count}'."); }
        }
    }
}
