﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="AObservableObject"/> <c>abstract</c> <c>class</c> implements the
    /// <see cref="INotifyPropertyChanged"/> interface and provides several protected helper
    /// methods for raising the <see cref="PropertyChanged"/> event.
    /// </summary>
    public abstract class AObservableObject : AObject, INotifyPropertyChanged
    {
        /// <summary>
        /// Event raised when an observed property changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Subscribe a handler to or unsubscribe it from the ObservableObject's <see cref="PropertyChanged"/> event.
        /// </summary>
        /// <param name="enabled">True to subscribe or false to unsubscribe.</param>
        /// <param name="handler">The handler to subscribe or unsubscribe.</param>
        public void SubscribePropertyChangedEvent(bool enabled, PropertyChangedEventHandler handler)
        { if (enabled) { this.PropertyChanged += handler; } else { this.PropertyChanged -= handler; } }

        /// <summary>
        /// Raise the <see cref="PropertyChanged"/> event.
        /// </summary>
        /// <param name="name">
        /// Name of the changed property.
        /// If the parameter is excluded, then the name of the calling property will be used.
        /// </param>
        protected virtual void RaisePropertyChanged([CallerMemberName] string name = "")
            => this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

        /// <summary>
        /// Raise the <see cref="PropertyChanged"/> event after setting <paramref name="target"/>
        /// equal to <paramref name="value"/>.
        /// </summary>
        /// <typeparam name="T">Type of the observed property.</typeparam>
        /// <param name="target">Reference to the observed property.</param>
        /// <param name="value">The observed property's new value.</param>
        /// <param name="name">
        /// Name of the changed property.
        /// If the parameter is excluded, then the name of the calling property will be used.
        /// </param>
        protected virtual void RaisePropertyChanged<T>(ref T target, T value, [CallerMemberName] string name = "")
        { target = value; this.RaisePropertyChanged(name); }

        /// <summary>
        /// Raise the <see cref="PropertyChanged"/> event after setting <paramref name="target"/>
        /// equal to <paramref name="value"/>. The event will only be raised if <paramref name="target"/>
        /// and <paramref name="value"/> are not equivalent according to the specified
        /// <see cref="IEqualityComparer{T}"/>.
        /// </summary>
        /// <typeparam name="T">Type of the observed property.</typeparam>
        /// <param name="target">Reference to the observed property.</param>
        /// <param name="value">The observed property's new value.</param>
        /// <param name="comparer">The object used to compare the <paramref name="target"/> and <paramref name="value"/></param>
        /// <param name="name">
        /// Name of the changed property.
        /// If the parameter is excluded, then the name of the calling property will be used.
        /// </param>
        protected virtual void RaiseIfPropertyChanged<T>(ref T target, T value, IEqualityComparer<T> comparer, [CallerMemberName] string name = "")
        { if (!comparer.Equals(target, value)) { target = value; this.RaisePropertyChanged(name); } }

        /// <summary>
        /// Raise the <see cref="PropertyChanged"/> event after setting <paramref name="target"/>
        /// equal to <paramref name="value"/>. The event will only be raised if <paramref name="target"/>
        /// and <paramref name="value"/> are not equivalent according to the default comparer for
        /// <typeparamref name="T"/> type.
        /// </summary>
        /// <typeparam name="T">Type of the observed property.</typeparam>
        /// <param name="target">Reference to the observed property.</param>
        /// <param name="value">The observed property's new value.</param>
        /// <param name="name">
        /// Name of the changed property.
        /// If the parameter is excluded, then the name of the calling property will be used.
        /// </param>
        protected virtual void RaiseIfPropertyChanged<T>(ref T target, T value, [CallerMemberName] string name = "")
            => this.RaiseIfPropertyChanged(ref target, value, EqualityComparer<T>.Default, name);

        /// <summary>
        /// Raise the <see cref="PropertyChanged"/> event.
        /// </summary>
        /// <param name="name">
        /// Name of the changed property.
        /// If the parameter is excluded, then the name of the calling property will be used.
        /// </param>
        [Obsolete("Use RaisePropertyChanged() method instead.", false)]
        protected virtual void OnPropertyChanged([CallerMemberName] string name = "")
            => this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

        /// <summary>
        /// Raise the <see cref="PropertyChanged"/> event after setting <paramref name="target"/>
        /// equal to <paramref name="value"/>. The event will only be raised if <paramref name="target"/>
        /// and <paramref name="value"/> are not equivalent according to the default comparer for
        /// <typeparamref name="T"/> type.
        /// </summary>
        /// <typeparam name="T">Type of the observed property.</typeparam>
        /// <param name="target">Reference to the observed property.</param>
        /// <param name="value">The observed property's new value.</param>
        /// <param name="name">
        /// Name of the changed property.
        /// If the parameter is excluded, then the name of the calling property will be used.
        /// </param>
        [Obsolete("Use RaiseIfPropertyChanged(ref T target, T value) method instead.", false)]
        protected virtual void OnPropertyChanged<T>(ref T target, T value, [CallerMemberName] string name = "")
        { if (!EqualityComparer<T>.Default.Equals(target, value)) { target = value; this.OnPropertyChanged(name); } }
    }
}
