﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Text;

namespace CommonCore
{
    /// <summary>
    /// Static extension class for <see cref="StringBuilder"/> objects.
    /// 
    /// These methods allow transforming <see cref="StringBuilder"/>
    /// content with divergent line ending sequences to a single consistent
    /// form of line ending.
    /// 
    /// These methods return the calling instance in a fluent manner.
    /// </summary>
    public static class StringBuilderExtensions_NormalizeLineEndings
    {

        #region NormalizeLineEndings Methods

        /// <summary>
        /// Normalizes all forms of line ending sequence to the same line ending form.
        /// For example, when called with the default argument, any standard Windows (\r\n)
        /// or malformed/legacy line endings (\n\r or \r) will be converted to the default line ending (\n).
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="ending">Line ending to use for normalization.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>This <see cref="StringBuilder"/> instance.</returns>
        public static StringBuilder NormalizeLineEndings(this StringBuilder self, ELineEnding ending = ELineEnding.N)
        {
            Throw.If.Self.IsNull(self);
            for (int index = 0; index < self.Length; index++)
            {
                char c = self[index];

                ELineEnding oldEnding;
                if (c == '\n') { oldEnding = (self.IsNextChar(index, '\r') ? ELineEnding.NR : ELineEnding.N); }
                else if (c == '\r') { oldEnding = (self.IsNextChar(index, '\n') ? ELineEnding.RN : ELineEnding.R); }
                else { continue; }

                if (ending != oldEnding)
                {
                    var oldText = StringExtensions_NormalizeLineEndings.LineEndings[(int)oldEnding];
                    var newText = StringExtensions_NormalizeLineEndings.LineEndings[(int)ending];
                    self[index] = newText[0];
                    if (oldText.Length < newText.Length) { self.Insert(index + 1, newText[1]); }
                    else if (oldText.Length > newText.Length) { self.Remove(index + 1, 1); }
                    else if (newText.Length == 2) { self[index + 1] = newText[1]; }
                }
                if (ending == ELineEnding.RN || ending == ELineEnding.NR) { index++; }
            }
            return self;
        }

        /// <summary>
        /// Return a new <see cref="string"/> with all forms of line ending sequence normalized to the same
        /// line ending form. Additionally, successive line endings will be merged into a single line ending.
        /// Optionally, line endings separated by only white space can also be merged.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="ending">Line ending to use for normalization.</param>
        /// <param name="includeWhiteSpace">Should whitespace be included when merging two line endings.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A new string with all line endings normalized and merged to the given form.</returns>
        public static StringBuilder NormalizeMergeLineEndings(this StringBuilder self, ELineEnding ending = ELineEnding.N, bool includeWhiteSpace = true)
        {
            Throw.If.Self.IsNull(self);
            self.NormalizeLineEndings(ELineEnding.N);
            for (int index = 0; index < self.Length; index++)
            {
                char c = self[index];
                if (c != '\n') { continue; }
                int count = 0, spaces = 0, current = index + 1;
                while (current < self.Length)
                {
                    if (self.IsChar(current, '\n')) { count += (spaces == 0 ? 1 : spaces + 1); spaces = 0; }
                    else if (includeWhiteSpace && self[current].IsWhiteSpace()) { spaces++; }
                    else { break; }
                    current++;
                }
                if (count > 0) { self.Remove(index + 1, count); }
            }
            if (ending != ELineEnding.N) { self.Replace("\n", StringExtensions_NormalizeLineEndings.LineEndings[(int)ending]); }
            return self;
        }

        #endregion

    }
}
