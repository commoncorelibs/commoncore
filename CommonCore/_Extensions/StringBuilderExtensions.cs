﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.IO;
using System.Text;

namespace CommonCore
{
    /// <summary>
    /// Static extension class for <see cref="StringBuilder"/> objects.
    /// 
    /// The purpose of this class is to bring the full range of standard
    /// and extended functionality to the StringBuilder class.
    /// </summary>
    public static class StringBuilderExtensions
    {

        #region AppendFile Methods

        /// <summary>
        /// Appends the content of the specified file to this <see cref="StringBuilder"/> if the file exists.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="file">The <see cref="FileInfo"/> pointing to the file to append.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="file"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If the content of this <see cref="StringBuilder"/> would exceed <see cref="System.Text.StringBuilder.MaxCapacity"/>.</exception>
        /// <returns>This <see cref="StringBuilder"/> instance.</returns>
        public static StringBuilder AppendFile(this StringBuilder self, FileInfo file)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNull(nameof(file), file);
            if (file.Exists) { self.Append(File.ReadAllText(file.FullName)); }
            return self;
        }

        #endregion

        #region AppendFormatLine Methods

        /// <summary>
        /// Appends the expanded <paramref name="format"/> to this <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="format">A composite format string.</param>
        /// <param name="objs">An array of objects to format.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="format"/> is <c>null</c>.</exception>
        /// <exception cref="FormatException">If <paramref name="format"/> is invalid; or the index of a format item is less than <c>0</c> or greater than or equal to the length of the <paramref name="objs"/> array.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If the content of this <see cref="StringBuilder"/> would exceed <see cref="System.Text.StringBuilder.MaxCapacity"/>.</exception>
        /// <returns>This <see cref="StringBuilder"/> instance.</returns>
        public static StringBuilder AppendFormatLine(this StringBuilder self, string format, params object[] objs)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNull(nameof(format), format);
            return self.AppendLine(string.Format(format, objs));
        }

        #endregion

        #region AppendLine Methods

        /// <summary>
        /// Appends a copy of the <see cref="string"/> conversion of the specified <see cref="object"/> to this <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="obj">The <see cref="object"/> to convert to a <see cref="string"/> and append.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If the content of this <see cref="StringBuilder"/> would exceed <see cref="System.Text.StringBuilder.MaxCapacity"/>.</exception>
        /// <returns>This <see cref="StringBuilder"/> instance.</returns>
        public static StringBuilder AppendLine(this StringBuilder self, object obj)
        {
            Throw.If.Self.IsNull(self);
            return self.AppendLine(obj?.ToString());
        }

        #endregion

        #region CountMatches Methods

        /// <summary>
        /// Reports the number of occurrences of <paramref name="value"/> in this <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The string to be counted.</param>
        /// <param name="maxCount">The maximum number of occurrences to count.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>The number of times <paramref name="value"/> is found in this content.</returns>
        public static int CountMatches(this StringBuilder self, string value, int maxCount = -1, ECharComparison comparison = ECharComparison.CaseSensitive)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsLesser(nameof(maxCount), maxCount, -1);

            if (self.Length == 0 || value == null || value.Length == 0 || value.Length > self.Length || maxCount == 0) { return 0; }
            int count = 0;
            int index = self.IndexOf(value, 0, comparison);
            while (index > -1 && index < self.Length && (maxCount < 0 || count < maxCount))
            {
                count++;
                index += value.Length;
                if (index >= self.Length) { break; }
                index = self.IndexOf(value, index, comparison);
            }
            return count;
        }

        #endregion

        #region CountEndMatches Methods

        /// <summary>
        /// Reports the number of uninterrupted occurrences of <paramref name="value"/> at the end
        /// of this <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The string to be counted.</param>
        /// <param name="maxCount">The maximum number of occurrences to count.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>The number of times <paramref name="value"/> is found at the end of this content.</returns>
        public static int CountEndMatches(this StringBuilder self, string value, int maxCount = -1, ECharComparison comparison = ECharComparison.CaseSensitive)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsLesser(nameof(maxCount), maxCount, -1);

            if (self.Length == 0 || value == null || value.Length == 0 || value.Length > self.Length || maxCount == 0) { return 0; }
            int count = 0;
            while (maxCount < 0 || count < maxCount) { if (self.IsMatch(value, self.Length - ((count + 1) * value.Length))) { count += 1; } else { break; } }
            return count;
        }

        #endregion

        #region CountStartMatches Methods

        /// <summary>
        /// Reports the number of uninterrupted occurrences of <paramref name="value"/> at the beginning
        /// of this <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The string to be counted.</param>
        /// <param name="maxCount">The maximum number of occurrences to count.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>The number of times <paramref name="value"/> is found at the beginning of this content.</returns>
        public static int CountStartMatches(this StringBuilder self, string value, int maxCount = -1, ECharComparison comparison = ECharComparison.CaseSensitive)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsLesser(nameof(maxCount), maxCount, -1);

            if (self.Length == 0 || value == null || value.Length == 0 || value.Length > self.Length || maxCount == 0) { return 0; }
            int count = 0;
            while (maxCount < 0 || count < maxCount) { if (self.IsMatch(value, count * value.Length)) { count += 1; } else { break; } }
            return count;
        }

        #endregion

    }
}
