﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Globalization;
using System.Linq;
using System.Text;

namespace CommonCore
{
    /// <summary>
    /// Static extension class for <see cref="StringBuilder"/> objects.
    /// 
    /// These methods focus on in-place formatting of the <see cref="StringBuilder"/>
    /// content and return the calling instance in a fluent manner.
    /// </summary>
    public static class StringBuilderExtensions_Formatting
    {

        #region Reverse Methods

        /// <summary>
        /// Reverses the order of the available <see cref="char"/> elements.
        /// Reversal begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="startIndex">The starting character position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>This <see cref="StringBuilder"/> instance.</returns>
        public static StringBuilder Reverse(this StringBuilder self, int startIndex = 0, int maxCount = -1)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsLesser(nameof(startIndex), startIndex, 0);
            Throw.If.Arg.IsLesser(nameof(maxCount), maxCount, -1);

            if (self.Length < 2 || startIndex >= self.Length || maxCount == 0 || maxCount == 1) { return self; }
            else if (maxCount == -1) { maxCount = self.Length - startIndex; }

            for (int leftIndex = startIndex; leftIndex < startIndex + maxCount / 2; leftIndex++)
            {
                int rightIndex = (startIndex + maxCount - 1) - (leftIndex - startIndex);
                var left = self[leftIndex];
                var right = self[rightIndex];
                self[leftIndex] = right;
                self[rightIndex] = left;

            }
            return self;
        }

        #endregion

        #region ToLower Methods

        /// <summary>
        /// Converts the available <see cref="char"/> elements to lowercase.
        /// Conversion begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <param name="culture">The <see cref="CultureInfo"/> that supplies culture-specific casing rules or <c>null</c> for default culture.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>This <see cref="StringBuilder"/> instance.</returns>
        public static StringBuilder ToLower(this StringBuilder self, int startIndex = 0, int maxCount = -1, CultureInfo culture = null)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsLesser(nameof(startIndex), startIndex, 0);
            Throw.If.Arg.IsLesser(nameof(maxCount), maxCount, -1);

            if (self.Length == 0 || startIndex >= self.Length || maxCount == 0) { return self; }
            else if (maxCount == -1) { maxCount = self.Length - startIndex; }

            if (culture == null)
            {
                for (int index = startIndex; index < self.Length && index < startIndex + maxCount; index++)
                { self[index] = char.ToLower(self[index]); }
            }
            else
            {
                for (int index = startIndex; index < self.Length && index < startIndex + maxCount; index++)
                { self[index] = char.ToLower(self[index], culture); }
            }
            return self;
        }

        #endregion

        #region ToLowerChar Methods

        /// <summary>
        /// Converts the <see cref="char"/> element at <paramref name="index"/> to lowercase.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="index">The index of the <see cref="char"/> to convert.</param>
        /// <param name="culture">The <see cref="CultureInfo"/> that supplies culture-specific casing rules.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="index"/> is outside the available bounds.</exception>
        /// <returns>This <see cref="StringBuilder"/> instance.</returns>
        public static StringBuilder ToLowerChar(this StringBuilder self, int index, CultureInfo culture = null)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsIndexOutOfRange(nameof(index), index, self);
            self[index] = culture == null ? char.ToLower(self[index]) : char.ToLower(self[index], culture);
            return self;
        }

        #endregion

        #region ToUpper Methods

        /// <summary>
        /// Converts the available <see cref="char"/> elements to uppercase.
        /// Conversion begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <param name="culture">The <see cref="CultureInfo"/> that supplies culture-specific casing rules or <c>null</c> for default culture.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>This <see cref="StringBuilder"/> instance.</returns>
        public static StringBuilder ToUpper(this StringBuilder self, int startIndex = 0, int maxCount = -1, CultureInfo culture = null)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsLesser(nameof(startIndex), startIndex, 0);
            Throw.If.Arg.IsLesser(nameof(maxCount), maxCount, -1);

            if (self.Length == 0 || startIndex >= self.Length || maxCount == 0) { return self; }
            else if (maxCount == -1) { maxCount = self.Length - startIndex; }

            if (culture == null)
            {
                for (int index = startIndex; index < self.Length && index < startIndex + maxCount; index++)
                { self[index] = char.ToUpper(self[index]); }
            }
            else
            {
                for (int index = startIndex; index < self.Length && index < startIndex + maxCount; index++)
                { self[index] = char.ToUpper(self[index], culture); }
            }
            return self;
        }

        #endregion

        #region ToUpperChar Methods

        /// <summary>
        /// Converts the <see cref="char"/> element at <paramref name="index"/> to uppercase.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="index">The index of the <see cref="char"/> to convert.</param>
        /// <param name="culture">The <see cref="CultureInfo"/> that supplies culture-specific casing rules.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="index"/> is outside the available bounds.</exception>
        /// <returns>This <see cref="StringBuilder"/> instance.</returns>
        public static StringBuilder ToUpperChar(this StringBuilder self, int index, CultureInfo culture = null)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsIndexOutOfRange(nameof(index), index, self);
            self[index] = culture == null ? char.ToUpper(self[index]) : char.ToUpper(self[index], culture);
            return self;
        }

        #endregion

        #region Trim Methods

        /// <summary>
        /// Removes all whitespace from the beginning and end of this <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>This <see cref="StringBuilder"/> instance.</returns>
        public static StringBuilder Trim(this StringBuilder self)
        {
            Throw.If.Self.IsNull(self);
            if (self.Length == 0) { return self; }
            TrimStart(self);
            TrimEnd(self);
            return self;
        }

        /// <summary>
        /// Removes all uninterrupted occurrences of <paramref name="trimChar"/> from the
        /// beginning and end of this <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="trimChar">The <see cref="char"/> to trim.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>This <see cref="StringBuilder"/> instance.</returns>
        public static StringBuilder Trim(this StringBuilder self, char trimChar)
        {
            Throw.If.Self.IsNull(self);
            if (self.Length == 0) { return self; }
            TrimStart(self, trimChar);
            TrimEnd(self, trimChar);
            return self;
        }

        /// <summary>
        /// Removes all uninterrupted occurrences of any <paramref name="trimChars"/> elements from the
        /// beginning and end of this <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="trimChars">The <see cref="char"/> elements to trim.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>This <see cref="StringBuilder"/> instance.</returns>
        public static StringBuilder Trim(this StringBuilder self, params char[] trimChars)
        {
            Throw.If.Self.IsNull(self);
            if (self.Length == 0) { return self; }
            TrimStart(self, trimChars);
            TrimEnd(self, trimChars);
            return self;
        }

        /// <summary>
        /// Removes all uninterrupted occurrences of <paramref name="value"/> from the
        /// beginning and end of this <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to trim.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>This <see cref="StringBuilder"/> instance.</returns>
        public static StringBuilder Trim(this StringBuilder self, string value)
        {
            Throw.If.Self.IsNull(self);
            if (self.Length == 0) { return self; }
            TrimStart(self, value);
            TrimEnd(self, value);
            return self;
        }

        #endregion

        #region TrimEnd Methods

        /// <summary>
        /// Removes all whitespace from the end of this <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>This <see cref="StringBuilder"/> instance.</returns>
        public static StringBuilder TrimEnd(this StringBuilder self)
        {
            Throw.If.Self.IsNull(self);
            if (self.Length == 0) { return self; }

            int count = 0;
            for (int index = self.Length - 1; index >= 0; index--) { if (char.IsWhiteSpace(self[index])) { count += 1; } else { break; } }
            if (count > 0) { self.Remove(self.Length - count, count); }

            return self;
        }

        /// <summary>
        /// Removes all uninterrupted occurrences of <paramref name="trimChar"/> from the
        /// end of this <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="trimChar">The <see cref="char"/> to trim.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>This <see cref="StringBuilder"/> instance.</returns>
        public static StringBuilder TrimEnd(this StringBuilder self, char trimChar)
        {
            Throw.If.Self.IsNull(self);
            if (self.Length == 0) { return self; }

            int count = 0;
            for (int index = self.Length - 1; index >= 0; index--) { if (trimChar == self[index]) { count += 1; } else { break; } }
            if (count > 0) { self.Remove(self.Length - count, count); }

            return self;
        }

        /// <summary>
        /// Removes all uninterrupted occurrences of any <paramref name="trimChars"/> elements from the
        /// end of this <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="trimChars">The <see cref="char"/> elements to trim.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>This <see cref="StringBuilder"/> instance.</returns>
        public static StringBuilder TrimEnd(this StringBuilder self, params char[] trimChars)
        {
            Throw.If.Self.IsNull(self);
            if (self.Length == 0 || trimChars == null || trimChars.Length == 0) { return self; }

            int count = 0;
            for (int index = self.Length - 1; index >= 0; index--) { if (trimChars.Contains(self[index])) { count += 1; } else { break; } }
            if (count > 0) { self.Remove(self.Length - count, count); }

            return self;
        }

        /// <summary>
        /// Removes all uninterrupted occurrences of <paramref name="value"/> from the
        /// end of this <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to trim.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>This <see cref="StringBuilder"/> instance.</returns>
        public static StringBuilder TrimEnd(this StringBuilder self, string value)
        {
            Throw.If.Self.IsNull(self);
            if (self.Length == 0 || value == null || value.Length == 0) { return self; }
            int count = self.CountEndMatches(value);
            if (count > 0) { self.Remove(self.Length - (count * value.Length), (count * value.Length)); }
            return self;
        }

        #endregion

        #region TrimStart Methods

        /// <summary>
        /// Removes all whitespace from the beginning of this <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>This <see cref="StringBuilder"/> instance.</returns>
        public static StringBuilder TrimStart(this StringBuilder self)
        {
            Throw.If.Self.IsNull(self);
            if (self.Length == 0) { return self; }

            int count = 0;
            for (int index = 0; index < self.Length; index++)
            { if (char.IsWhiteSpace(self[index])) { count += 1; } else { break; } }
            if (count > 0) { self.Remove(0, count); }

            return self;
        }

        /// <summary>
        /// Removes all uninterrupted occurrences of <paramref name="trimChar"/> from the
        /// beginning of this <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="trimChar">The <see cref="char"/> to trim.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>This <see cref="StringBuilder"/> instance.</returns>
        public static StringBuilder TrimStart(this StringBuilder self, char trimChar)
        {
            Throw.If.Self.IsNull(self);
            if (self.Length == 0) { return self; }

            int count = 0;
            for (int index = 0; index < self.Length; index++)
            { if (trimChar == self[index]) { count += 1; } else { break; } }
            if (count > 0) { self.Remove(0, count); }

            return self;
        }

        /// <summary>
        /// Removes all uninterrupted occurrences of any <paramref name="trimChars"/> elements from the
        /// beginning of this <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="trimChars">The <see cref="char"/> elements to trim.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>This <see cref="StringBuilder"/> instance.</returns>
        public static StringBuilder TrimStart(this StringBuilder self, params char[] trimChars)
        {
            Throw.If.Self.IsNull(self);
            if (self.Length == 0 || trimChars == null || trimChars.Length == 0) { return self; }

            int count = 0;
            for (int index = 0; index < self.Length; index++)
            { if (trimChars.Contains(self[index])) { count += 1; } else { break; } }
            if (count > 0) { self.Remove(0, count); }

            return self;
        }

        /// <summary>
        /// Removes all uninterrupted occurrences of <paramref name="value"/> from the
        /// beginning of this <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to trim.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>This <see cref="StringBuilder"/> instance.</returns>
        public static StringBuilder TrimStart(this StringBuilder self, string value)
        {
            Throw.If.Self.IsNull(self);
            if (self.Length == 0 || value == null || value.Length == 0) { return self; }
            int count = self.CountStartMatches(value);
            if (count > 0) { self.Remove(0, count * value.Length); }
            return self;
        }

        #endregion

    }
}
