﻿#region Copyright (c) 2018 Jay Jeckel
// Copyright (c) 2018 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Globalization;

namespace CommonCore
{
    /// <summary>
    /// Static class providing extension methods for the char class.
    /// 
    /// These extensions are meant to round out the functionality of chars
    /// with a primary focus on exposing static char methods.
    /// </summary>
    public static class CharExtensions
    {
        /// <summary>
        /// Converts the specified <see cref="char"/> object to a double-precision floating point number.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The numeric value of the character if that character represents a number; otherwise, -1.0.</returns>
        public static double GetNumericValue(this char self) => char.GetNumericValue(self);

        /// <summary>
        /// Categorizes a specified <see cref="char"/> object into a group identified by one of the
        /// <see cref="UnicodeCategory"/> values.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>A <see cref="UnicodeCategory"/> value that identifies the group that contains the parameter.</returns>
        public static UnicodeCategory GetUnicodeCategory(this char self) => char.GetUnicodeCategory(self);

        /// <summary>
        /// Indicates whether the specified <see cref="char"/> object is categorized as a control character.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the parameter is a control character; otherwise, <c>false</c>.</returns>
        public static bool IsControl(this char self) => char.IsControl(self);

        /// <summary>
        /// Indicates whether the specified <see cref="char"/> object is categorized as a decimal digit.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the parameter is a decimal digit; otherwise, <c>false</c>.</returns>
        public static bool IsDigit(this char self) => char.IsDigit(self);

        /// <summary>
        /// Indicates whether the specified <see cref="char"/> object is a high surrogate.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the numeric value of the parameter ranges from U+D800 through U+DBFF; otherwise, <c>false</c>.</returns>
        public static bool IsHighSurrogate(this char self) => char.IsHighSurrogate(self);

        /// <summary>
        /// Indicates whether the specified <see cref="char"/> object is categorized as a Unicode letter.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the parameter is a letter; otherwise, <c>false</c>.</returns>
        public static bool IsLetter(this char self) => char.IsLetter(self);

        /// <summary>
        /// Indicates whether the specified <see cref="char"/> object is categorized as a letter or a decimal digit.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the parameter is a letter or a decimal digit; otherwise, <c>false</c>.</returns>
        public static bool IsLetterOrDigit(this char self) => char.IsLetterOrDigit(self);

        /// <summary>
        /// Indicates whether the specified <see cref="char"/> object is categorized as a lowercase letter.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the parameter is a lowercase letter; otherwise, <c>false</c>.</returns>
        public static bool IsLower(this char self) => char.IsLower(self);

        /// <summary>
        /// Indicates whether the specified <see cref="char"/> object is a low surrogate.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the numeric value of the parameter ranges from U+DC00 through U+DFFF; otherwise, <c>false</c>.</returns>
        public static bool IsLowSurrogate(this char self) => char.IsLowSurrogate(self);

        /// <summary>
        /// Indicates whether the specified <see cref="char"/> object is categorized as a number.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the parameter is a number; otherwise, <c>false</c>.</returns>
        public static bool IsNumber(this char self) => char.IsNumber(self);

        /// <summary>
        /// Indicates whether the specified <see cref="char"/> object is categorized as a punctuation mark.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the parameter is a punctuation mark; otherwise, <c>false</c>.</returns>
        public static bool IsPunctuation(this char self) => char.IsPunctuation(self);

        /// <summary>
        /// Indicates whether the specified <see cref="char"/> object is categorized as a separator character.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the parameter is a separator character; otherwise, <c>false</c>.</returns>
        public static bool IsSeparator(this char self) => char.IsSeparator(self);

        /// <summary>
        /// Indicates whether the specified <see cref="char"/> object has a surrogate code point.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the parameter is either a high surrogate or a low surrogate; otherwise, <c>false</c>.</returns>
        public static bool IsSurrogate(this char self) => char.IsSurrogate(self);

        /// <summary>
        /// Indicates whether the specified <see cref="char"/> object is categorized as a symbol character.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the parameter is a symbol character; otherwise, <c>false</c>.</returns>
        public static bool IsSymbol(this char self) => char.IsSymbol(self);

        /// <summary>
        /// Indicates whether the specified <see cref="char"/> object is categorized as an uppercase letter.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the parameter is an uppercase letter; otherwise, <c>false</c>.</returns>
        public static bool IsUpper(this char self) => char.IsUpper(self);

        /// <summary>
        /// Indicates whether the specified <see cref="char"/> object is categorized as white space.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the parameter is white space; otherwise, <c>false</c>.</returns>
        public static bool IsWhiteSpace(this char self) => char.IsWhiteSpace(self);

        /// <summary>
        /// Converts the value of the specified <see cref="char"/> object to its lowercase equivalent.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The lowercase equivalent of the parameter or the unchanged value if the parameter is already lowercase or not alphabetic.</returns>
        public static char ToLower(this char self) => char.ToLower(self);

        /// <summary>
        /// Converts the value of a specified <see cref="char"/> object to its lowercase equivalent using specified culture-specific formatting information.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="culture">An object that supplies culture-specific casing rules.</param>
        /// <returns>The lowercase equivalent of the parameter, modified according to culture, or the unchanged value if the parameter is already lowercase or not alphabetic.</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="culture"/> is <c>null</c>.</exception>
        public static char ToLower(this char self, CultureInfo culture) => char.ToLower(self, culture);

        /// <summary>
        /// Converts the value of the <see cref="char"/> object to its lowercase equivalent using the casing rules of the invariant culture.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The lowercase equivalent of the parameter or the unchanged value if the parameter is already lowercase or not alphabetic.</returns>
        public static char ToLowerInvariant(this char self) => char.ToLowerInvariant(self);

        /// <summary>
        /// Converts the value of the specified <see cref="char"/> object to its uppercase equivalent.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The uppercase equivalent of the parameter or the unchanged value if the parameter is already uppercase or not alphabetic.</returns>
        public static char ToUpper(this char self) => char.ToUpper(self);

        /// <summary>
        /// Converts the value of a specified <see cref="char"/> object to its uppercase equivalent using specified culture-specific formatting information.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="culture">An object that supplies culture-specific casing rules.</param>
        /// <returns>The uppercase equivalent of the parameter, modified according to culture, or the unchanged value if the parameter is already uppercase or not alphabetic.</returns>
        /// <exception cref="ArgumentNullException">If <paramref name="culture"/> is <c>null</c>.</exception>
        public static char ToUpper(this char self, CultureInfo culture) => char.ToUpper(self, culture);

        /// <summary>
        /// Converts the value of the <see cref="char"/> object to its uppercase equivalent using the casing rules of the invariant culture.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The uppercase equivalent of the parameter or the unchanged value if the parameter is already uppercase or not alphabetic.</returns>
        public static char ToUpperInvariant(this char self) => char.ToUpperInvariant(self);
    }
}
