﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.Text;

namespace CommonCore
{
    /// <summary>
    /// Static extension class for <see cref="StringBuilder"/> objects.
    /// 
    /// These methods focus on converting the <see cref="StringBuilder"/> and its content into
    /// various other forms.
    /// </summary>
    public static class StringBuilderExtensions_Converting
    {

        #region Clone Methods

        /// <summary>
        /// Returns a copy of this <see cref="StringBuilder"/> object.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A copy of this <see cref="StringBuilder"/> object.</returns>
        public static StringBuilder Clone(this StringBuilder self)
        {
            Throw.If.Self.IsNull(self);
            var clone = new StringBuilder(self.Capacity, self.MaxCapacity);
            clone.Length = self.Length;
            for (int index = 0; index < self.Length; index++) { clone[index] = self[index]; }
            return clone;
        }

        #endregion

        #region ToCharArray Methods

        /// <summary>
        /// Returns an array of <see cref="char"/> elements representing the content of this <see cref="StringBuilder"/>.
        /// Output begins at the first position and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>Array of <see cref="char"/> elements from this <see cref="StringBuilder"/>.</returns>
        public static char[] ToCharArray(this StringBuilder self)
            => ToCharArray(self, 0, self.Length);

        /// <summary>
        /// Returns an array of <see cref="char"/> elements representing the content of this <see cref="StringBuilder"/>.
        /// Output begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns>Array of <see cref="char"/> elements from this <see cref="StringBuilder"/>.</returns>
        public static char[] ToCharArray(this StringBuilder self, int startIndex)
            => ToCharArray(self, startIndex, self.Length - startIndex);

        /// <summary>
        /// Returns an array of <see cref="char"/> elements representing the content of this <see cref="StringBuilder"/>.
        /// Output begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>Array of <see cref="char"/> elements from this <see cref="StringBuilder"/>.</returns>
        public static char[] ToCharArray(this StringBuilder self, int startIndex, int maxCount)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsLesser(nameof(startIndex), startIndex, 0);
            Throw.If.Arg.IsLesser(nameof(maxCount), maxCount, -1);

            if (self.Length == 0 || startIndex >= self.Length || maxCount == 0) { return new char[0]; }
            else if (maxCount == -1) { maxCount = self.Length - startIndex; }
            else if (maxCount > self.Length - startIndex) { maxCount = self.Length - startIndex; }

            var chars = new char[maxCount];
            self.CopyTo(startIndex, chars, 0, maxCount);
            return chars;
        }

        #endregion

        #region ToCharSequence Methods

        /// <summary>
        /// Returns an <see cref="IEnumerable{T}"/> of <see cref="char"/> elements representing the content of this <see cref="StringBuilder"/>.
        /// Output begins at the first position and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns><see cref="IEnumerable{T}"/> of <see cref="char"/> elements from this <see cref="StringBuilder"/>.</returns>
        public static IEnumerable<char> ToCharSequence(this StringBuilder self)
            => ToCharArray(self, 0, self.Length);

        /// <summary>
        /// Returns an <see cref="IEnumerable{T}"/> of <see cref="char"/> elements representing the content of this <see cref="StringBuilder"/>.
        /// Output begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns><see cref="IEnumerable{T}"/> of <see cref="char"/> elements from this <see cref="StringBuilder"/>.</returns>
        public static IEnumerable<char> ToCharSequence(this StringBuilder self, int startIndex)
            => ToCharArray(self, startIndex, self.Length - startIndex);

        /// <summary>
        /// Returns an <see cref="IEnumerable{T}"/> of <see cref="char"/> elements representing the content of this <see cref="StringBuilder"/>.
        /// Output begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns><see cref="IEnumerable{T}"/> of <see cref="char"/> elements from this <see cref="StringBuilder"/>.</returns>
        public static IEnumerable<char> ToCharSequence(this StringBuilder self, int startIndex, int maxCount)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsLesser(nameof(startIndex), startIndex, 0);
            Throw.If.Arg.IsLesser(nameof(maxCount), maxCount, -1);

            if (self.Length == 0 || startIndex >= self.Length || maxCount == 0) { yield break; }
            else if (maxCount == -1) { maxCount = self.Length - startIndex; }
            else if (maxCount > self.Length - startIndex) { maxCount = self.Length - startIndex; }

            for (int index = startIndex; index < self.Length && index - startIndex < maxCount; index++) { yield return self[index]; }
        }

        #endregion

    }
}
