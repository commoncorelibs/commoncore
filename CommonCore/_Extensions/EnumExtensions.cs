﻿#region Copyright (c) 2013 Jay Jeckel
// Copyright (c) 2013 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.ComponentModel;

namespace CommonCore
{
    /// <summary>
    /// Class to provide Enum extension methods.
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// Get the description text from enum value's DescriptionAttribute, if present.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Description text or empty string if DescriptionAttribute is not present.</returns>
        public static string GetDescription(this Enum self)
        {
            var field = self.GetType().GetField(Enum.GetName(self.GetType(), self));
            // NOTE: This null check shouldn't be necessary as the passed value can't not be a member of its enclosing type.
            //if (field == null) { return string.Empty; }
            var attribute = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute), false) as DescriptionAttribute;
            return (attribute == null ? string.Empty : attribute.Description);
        }

        /// <summary>
        /// Get the name of the enum value as a string.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Name of the enum value as a string.</returns>
        public static string GetName(this Enum self) => Enum.GetName(self.GetType(), self);

        /// <summary>
        /// Determine if the enum value is a valid member of the enumeration.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the target is a valid enum value; otherwise <c>false</c>.</returns>
        public static bool IsDefined(this Enum self) => Enum.IsDefined(self.GetType(), self);

        /// <summary>
        /// Get a value indicating if the enum is decorated with the flags attribute.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the enum is decorated with the flags attribute; otherwise <c>false</c>.</returns>
        public static bool IsFlags(this Enum self) => self.GetType().IsDefined(typeof(FlagsAttribute), false);

        /// <summary>
        /// Determine if the enum value contains any of the given flag values.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="flags">Flag values to compare against the enum value.</param>
        /// <returns><c>true</c> if the enum value contains any of the given flag values; otherwise <c>false</c>.</returns>
        public static bool HasAnyFlags(this Enum self, Enum flags)
        {
            if (!self.GetType().IsEquivalentTo(flags.GetType()))
            { throw new ArgumentException($"Argument type '{flags.GetType()}' does not match internal type '{self.GetType()}'.", "flags"); }
            switch (self.GetTypeCode())
            {
                case TypeCode.Byte: { return (Convert.ToByte(self) & Convert.ToByte(flags)) != 0; }
                case TypeCode.SByte: { return (Convert.ToSByte(self) & Convert.ToSByte(flags)) != 0; }
                case TypeCode.Int16: { return (Convert.ToInt16(self) & Convert.ToInt16(flags)) != 0; }
                case TypeCode.UInt16: { return (Convert.ToUInt16(self) & Convert.ToUInt16(flags)) != 0; }
                case TypeCode.Int32: { return (Convert.ToInt32(self) & Convert.ToInt32(flags)) != 0; }
                case TypeCode.UInt32: { return (Convert.ToUInt32(self) & Convert.ToUInt64(flags)) != 0; }
                case TypeCode.Int64: { return (Convert.ToUInt64(self) & Convert.ToUInt64(flags)) != 0; }
                case TypeCode.UInt64: { return (Convert.ToUInt64(self) & Convert.ToUInt64(flags)) != 0; }
                default: { return false; }
            }
        }

        /// <summary>
        /// Determine if the enum value contains all of the given flag values.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="flags">Flag values to compare against the enum value.</param>
        /// <returns><c>true</c> if the enum value contains all of the given flag values; otherwise <c>false</c>.</returns>
        public static bool HasAllFlags(this Enum self, Enum flags)
        {
            if (!self.GetType().IsEquivalentTo(flags.GetType()))
            { throw new ArgumentException($"Argument type '{flags.GetType()}' does not match internal type '{self.GetType()}'.", "flags"); }
            switch (self.GetTypeCode())
            {
                case TypeCode.Byte: { return (Convert.ToByte(self) & Convert.ToByte(flags)) == Convert.ToByte(flags); }
                case TypeCode.SByte: { return (Convert.ToSByte(self) & Convert.ToSByte(flags)) == Convert.ToSByte(flags); }
                case TypeCode.Int16: { return (Convert.ToInt16(self) & Convert.ToInt16(flags)) == Convert.ToInt16(flags); }
                case TypeCode.UInt16: { return (Convert.ToUInt16(self) & Convert.ToUInt16(flags)) == Convert.ToUInt16(flags); }
                case TypeCode.Int32: { return (Convert.ToInt32(self) & Convert.ToInt32(flags)) == Convert.ToInt32(flags); }
                case TypeCode.UInt32: { return (Convert.ToUInt32(self) & Convert.ToUInt32(flags)) == Convert.ToUInt32(flags); }
                case TypeCode.Int64: { return (Convert.ToInt64(self) & Convert.ToInt64(flags)) == Convert.ToInt64(flags); }
                case TypeCode.UInt64: { return (Convert.ToUInt64(self) & Convert.ToUInt64(flags)) == Convert.ToUInt64(flags); }
                default: { return false; }
            }
        }
    }
}
