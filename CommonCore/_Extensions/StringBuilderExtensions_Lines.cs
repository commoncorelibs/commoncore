﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.Text;

namespace CommonCore
{
    /// <summary>
    /// Static extension class for the <see cref="StringBuilder"/> type.
    /// 
    /// These extensions focus on dealing with a <see cref="StringBuilder"/>
    /// as a collection of lines.
    /// </summary>
    public static class StringBuilderExtensions_Lines
    {

        #region ToLineSequence Methods

        /// <summary>
        /// Returns an enumerable of <see cref="string"/> elements representing the content of
        /// each line in this <see cref="StringBuilder"/> instance.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>An enumerable of <see cref="string"/> elements representing the content of each line in this <see cref="StringBuilder"/> instance.</returns>
        public static IEnumerable<string> ToLineSequence(this StringBuilder self)
        {
            Throw.If.Self.IsNull(self);
            if (self.Length == 0) { yield break; }
            int prev = 0;
            for (int index = 0; index < self.Length; index++)
            {
                char c = self[index];

                if (c == '\n')
                {
                    yield return self.ToString(prev, index - prev);
                    if (self.IsNextChar(index, '\r')) { index++; }
                    prev = index + 1;
                    continue;
                }
                else if (c == '\r')
                {
                    yield return self.ToString(prev, index - prev);
                    if (self.IsNextChar(index, '\n')) { index++; }
                    prev = index + 1;
                    continue;
                }
            }
            yield return self.ToString(prev, self.Length - prev);
        }

        #endregion

        #region RemoveLine Methods

        /// <summary>
        /// Removes the specified line from this <see cref="StringBuilder"/> instance if it exists.
        /// In the case of the last line, its preceding line ending is removed; otherwise the line's
        /// subsequent line ending is removed.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="lineIndex">The index of the line to remove.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="lineIndex"/> is less than <c>0</c>.</exception>
        /// <returns>This <see cref="StringBuilder"/> instance.</returns>
        public static StringBuilder RemoveLine(this StringBuilder self, int lineIndex)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsLesser(nameof(lineIndex), lineIndex, 0);
            if (self.Length == 0) { return self; }

            int prev = 0;
            int lineCount = 0;
            for (int index = 0; index < self.Length; index++)
            {
                char c = self[index];

                if (c == '\n')
                {
                    if (self.IsNextChar(index, '\r')) { index++; }
                    if (lineIndex == lineCount)
                    {
                        self.Remove(prev, index - prev + 1);
                        return self;
                    }
                    lineCount++;
                    prev = index + 1;
                    continue;
                }
                else if (c == '\r')
                {
                    if (self.IsNextChar(index, '\n')) { index++; }
                    if (lineIndex == lineCount)
                    {
                        self.Remove(prev, index - prev + 1);
                        return self;
                    }
                    lineCount++;
                    prev = index + 1;
                    continue;
                }
            }
            if (lineIndex == lineCount && prev <= self.Length)
            {
                if (self.IsPrevChar(prev, '\n'))
                {
                    prev--;
                    if (self.IsPrevChar(prev, '\r'))
                    { prev--; }
                }
                else if (self.IsPrevChar(prev, '\r'))
                {
                    prev--;
                    if (self.IsPrevChar(prev, '\n'))
                    { prev--; }
                }

                if (prev < self.Length)
                { self.Remove(prev, self.Length - prev); }
            }
            return self;
        }

        //public static void RemoveLines(this StringBuilder self, int lineStartIndex, int maxCount)
        //{
        //}

        #endregion

    }
}
