﻿#region Copyright (c) 2014 Jay Jeckel
// Copyright (c) 2014 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;

namespace CommonCore
{
    /// <summary>
    /// Static class providing extension methods for the <see cref="DateTime"/> class.
    /// </summary>
    public static class DateTimeExtensions
    {
        /// <summary>
        /// The zero moment of Unix epoch dating system.
        /// </summary>
        public static readonly DateTime UnixEpochDate1970 = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

        /// <summary>
        /// Determines if the specified <see cref="DateTime"/> is not <see cref="DayOfWeek.Saturday"/> or <see cref="DayOfWeek.Sunday"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if is not a Saturday or Sunday; otherwise <c>false</c>.</returns>
        public static bool IsWeekday(this DateTime self) => self.DayOfWeek != DayOfWeek.Sunday && self.DayOfWeek != DayOfWeek.Saturday;
        // => (self.DayOfWeek & DayOfWeek.Saturday) == 0 && (self.DayOfWeek & DayOfWeek.Sunday) == 0;

        /// <summary>
        /// Determines if the specified <see cref="DateTime"/> is <see cref="DayOfWeek.Saturday"/> or <see cref="DayOfWeek.Sunday"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if is a Saturday or Sunday; otherwise <c>false</c>.</returns>
        public static bool IsWeekend(this DateTime self) => self.DayOfWeek == DayOfWeek.Sunday || self.DayOfWeek == DayOfWeek.Saturday;
        // => (self.DayOfWeek & DayOfWeek.Saturday) != 0 || (self.DayOfWeek & DayOfWeek.Sunday) != 0;

        /// <summary>
        /// Determines if the specified <see cref="DateTime"/> objects have equivalent date aspects.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The other object to be equated with.</param>
        /// <returns><c>true</c> if the dates are equivalent; otherwise <c>false</c>.</returns>
        public static bool EqualDate(this DateTime self, DateTime value) => (self.Date == value.Date);

        /// <summary>
        /// Determines if the specified <see cref="DateTime"/> objects have equivalent time aspects.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The other object to be equated with.</param>
        /// <returns><c>true</c> if the times are equivalent; otherwise <c>false</c>.</returns>
        public static bool EqualTime(this DateTime self, DateTime value) => (self.TimeOfDay == value.TimeOfDay);

        /// <summary>
        /// Returns a new <see cref="DateTime"/> representing the same time the next day.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><see cref="DateTime"/> representing the same time the next day.</returns>
        public static DateTime ToTomorrow(this DateTime self) => self.AddDays(1);

        /// <summary>
        /// Returns a new <see cref="DateTime"/> representing the same time the previous day.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><see cref="DateTime"/> representing the same time the previous day.</returns>
        public static DateTime ToYesterday(this DateTime self) => self.AddDays(-1);

        /// <summary>
        /// Returns a new <see cref="DateTime"/> representing the same date at midnight.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><see cref="DateTime"/> representing the same date at midnight.</returns>
        public static DateTime ToMidnight(this DateTime self) => new DateTime(self.Year, self.Month, self.Day, 0, 0, 0, 0);

        /// <summary>
        /// Returns a new <see cref="DateTime"/> representing the same date at noon.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><see cref="DateTime"/> representing the same date at noon.</returns>
        public static DateTime ToNoon(this DateTime self) => new DateTime(self.Year, self.Month, self.Day, 12, 0, 0, 0);

        /// <summary>
        /// Returns a new <see cref="DateTime"/> representing the same date one millisecond from the next day.
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static DateTime ToLastMoment(this DateTime self) => new DateTime(self.Year, self.Month, self.Day, 23, 59, 59, 999);

        /// <summary>
        /// Converts the specified <see cref="DateTime"/> object into a 64-bit Unix time stamp
        /// representing the number of seconds since January 1st, 1970 (UTC).
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Number of seconds between this time and 1970/01/01 00:00:00.0000 (UTC).</returns>
        public static long ToUnixTimeStamp(this DateTime self) => (long)(self - UnixEpochDate1970).TotalSeconds;

        /// <summary>
        /// Returns the <see cref="PeriodOfDay"/> based on the time aspect of the <see cref="DateTime"/> object.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><see cref="PeriodOfDay"/> based on the time aspect of the <see cref="DateTime"/> object.</returns>
        public static PeriodOfDay GetPeriodOfDay(this DateTime self) => self.Hour < 6 ? PeriodOfDay.Night : self.Hour < 12 ? PeriodOfDay.Morning : self.Hour < 18 ? PeriodOfDay.Afternoon : self.Hour < 22 ? PeriodOfDay.Evening : PeriodOfDay.Night;

        /// <summary>
        /// Converts the current <see cref="DateTime"/> object to its equivalent human-friendly display form.
        /// The date representation progresses as:
        /// Today -->
        /// Yesterday -->
        /// Full Weekday name (dddd) -->
        /// Full Month name and two-digit date (MMMM dd) -->
        /// Finally full Month name, two-digit date, and four-digit year (MMMM dd, yyyy).
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="separator">The string used to separate the date and time components of the output.</param>
        /// <returns><see cref="string"/> representing the current <see cref="DateTime"/> object in human-friendly display form.</returns>
        public static string ToFriendlyString(this DateTime self, string separator = " at ")
        {
            var today = DateTime.Today;
            string date;
            if (self.Date == today) { date = "Today"; }
            else if (self.Date == today.AddDays(-1)) { date = "Yesterday"; }
            else if (self.Date > today.AddDays(-6)) { date = self.ToString("dddd"); }
            else if (self.Year == today.Year) { date = self.ToString("MMMM dd"); }
            else { date = self.ToString("MMMM dd, yyyy"); }
            return $"{date} at {self:t}";
        }
    }
}
