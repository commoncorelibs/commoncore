﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace CommonCore
{
    /// <summary>
    /// Static extension class for <see cref="StringBuilder"/> objects.
    /// 
    /// These methods focus on making comparisons to the <see cref="StringBuilder"/> and its content.
    /// Comparisons are done on the <see cref="char"/> level to avoid any unneeded <see cref="string"/>
    /// allocations. Due to this reliance on <see cref="char"/> comparisons, case-insensitivity can not
    /// be handled by any in-built mechanism. To make case-insensitive comparisons possible, the rule is
    /// that two <see cref="char"/> objects are case-insensitively equal if either their upper cased
    /// or lower cased forms are equivalent.
    /// </summary>
    public static class StringBuilderExtensions_Comparing
    {

        #region Contains(char) Methods

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains <paramref name="value"/>.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="char"/> to seek.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool Contains(this StringBuilder self, char value)
            => IndexOf(self, value, 0, self.Length) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="char"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool Contains(this StringBuilder self, char value, int startIndex)
            => IndexOf(self, value, startIndex, self.Length - startIndex) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="char"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool Contains(this StringBuilder self, char value, int startIndex, int maxCount)
            => IndexOf(self, value, startIndex, maxCount) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains <paramref name="value"/>.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="char"/> to seek.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool Contains(this StringBuilder self, char value, ECharComparison comparison)
            => IndexOf(self, value, 0, self.Length, comparison) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="char"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool Contains(this StringBuilder self, char value, int startIndex, ECharComparison comparison)
            => IndexOf(self, value, startIndex, self.Length - startIndex, comparison) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="char"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool Contains(this StringBuilder self, char value, int startIndex, int maxCount, ECharComparison comparison)
            => IndexOf(self, value, startIndex, maxCount, comparison) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains <paramref name="value"/>.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="char"/> to seek.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool Contains(this StringBuilder self, char value, CultureInfo insensitiveCulture)
            => IndexOf(self, value, 0, self.Length, insensitiveCulture) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="char"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool Contains(this StringBuilder self, char value, int startIndex, CultureInfo insensitiveCulture)
            => IndexOf(self, value, startIndex, self.Length - startIndex, insensitiveCulture) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="char"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool Contains(this StringBuilder self, char value, int startIndex, int maxCount, CultureInfo insensitiveCulture)
            => IndexOf(self, value, startIndex, maxCount, insensitiveCulture) > -1;

        #endregion

        #region Contains(string) Methods

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains <paramref name="value"/>.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to seek.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool Contains(this StringBuilder self, string value)
            => IndexOf(self, value, 0, self.Length) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool Contains(this StringBuilder self, string value, int startIndex)
            => IndexOf(self, value, startIndex, self.Length - startIndex) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool Contains(this StringBuilder self, string value, int startIndex, int maxCount)
            => IndexOf(self, value, startIndex, maxCount) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains <paramref name="value"/>.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to seek.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool Contains(this StringBuilder self, string value, ECharComparison comparison)
            => IndexOf(self, value, 0, self.Length, comparison) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool Contains(this StringBuilder self, string value, int startIndex, ECharComparison comparison)
            => IndexOf(self, value, startIndex, self.Length - startIndex, comparison) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool Contains(this StringBuilder self, string value, int startIndex, int maxCount, ECharComparison comparison)
            => IndexOf(self, value, startIndex, maxCount, comparison) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains <paramref name="value"/>.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to seek.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool Contains(this StringBuilder self, string value, CultureInfo insensitiveCulture)
            => IndexOf(self, value, 0, self.Length, insensitiveCulture) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool Contains(this StringBuilder self, string value, int startIndex, CultureInfo insensitiveCulture)
            => IndexOf(self, value, startIndex, self.Length - startIndex, insensitiveCulture) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool Contains(this StringBuilder self, string value, int startIndex, int maxCount, CultureInfo insensitiveCulture)
            => IndexOf(self, value, startIndex, maxCount, insensitiveCulture) > -1;

        #endregion

        #region ContainsAll(char) Methods

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains all <paramref name="values"/> elements.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns><c>true</c> if all <paramref name="values"/> elements are found; otherwise <c>false</c>.</returns>
        public static bool ContainsAll(this StringBuilder self, IEnumerable<char> values)
            => ContainsAll(self, values, 0, self.Length, null);

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains all <paramref name="values"/> elements.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns><c>true</c> if all <paramref name="values"/> elements are found; otherwise <c>false</c>.</returns>
        public static bool ContainsAll(this StringBuilder self, IEnumerable<char> values, int startIndex)
            => ContainsAll(self, values, startIndex, self.Length - startIndex, null);

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains all <paramref name="values"/> elements.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns><c>true</c> if all <paramref name="values"/> elements are found; otherwise <c>false</c>.</returns>
        public static bool ContainsAll(this StringBuilder self, IEnumerable<char> values, int startIndex, int maxCount)
            => ContainsAll(self, values, startIndex, maxCount, null);

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains all <paramref name="values"/> elements.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns><c>true</c> if all <paramref name="values"/> elements are found; otherwise <c>false</c>.</returns>
        public static bool ContainsAll(this StringBuilder self, IEnumerable<char> values, ECharComparison comparison)
            => ContainsAll(self, values, 0, self.Length, comparison.ToCultureInfo());

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains all <paramref name="values"/> elements.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns><c>true</c> if all <paramref name="values"/> elements are found; otherwise <c>false</c>.</returns>
        public static bool ContainsAll(this StringBuilder self, IEnumerable<char> values, int startIndex, ECharComparison comparison)
            => ContainsAll(self, values, startIndex, self.Length - startIndex, comparison.ToCultureInfo());

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains all <paramref name="values"/> elements.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns><c>true</c> if all <paramref name="values"/> elements are found; otherwise <c>false</c>.</returns>
        public static bool ContainsAll(this StringBuilder self, IEnumerable<char> values, int startIndex, int maxCount, ECharComparison comparison)
            => ContainsAll(self, values, startIndex, maxCount, comparison.ToCultureInfo());

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains all <paramref name="values"/> elements.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns><c>true</c> if all <paramref name="values"/> elements are found; otherwise <c>false</c>.</returns>
        public static bool ContainsAll(this StringBuilder self, IEnumerable<char> values, CultureInfo insensitiveCulture)
            => ContainsAll(self, values, 0, self.Length, insensitiveCulture);

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains all <paramref name="values"/> elements.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns><c>true</c> if all <paramref name="values"/> elements are found; otherwise <c>false</c>.</returns>
        public static bool ContainsAll(this StringBuilder self, IEnumerable<char> values, int startIndex, CultureInfo insensitiveCulture)
            => ContainsAll(self, values, startIndex, self.Length - startIndex, insensitiveCulture);

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains all <paramref name="values"/> elements.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns><c>true</c> if all <paramref name="values"/> elements are found; otherwise <c>false</c>.</returns>
        public static bool ContainsAll(this StringBuilder self, IEnumerable<char> values, int startIndex, int maxCount, CultureInfo insensitiveCulture)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNull(nameof(values), values);
            Throw.If.Arg.IsLesser(nameof(startIndex), startIndex, 0);
            Throw.If.Arg.IsLesser(nameof(maxCount), maxCount, -1);

            if (!values.Any()) { return false; }
            foreach (var value in values) { if (!Contains(self, value, startIndex, maxCount, insensitiveCulture)) { return false; } }
            return true;
        }

        #endregion

        #region ContainsAll(string) Methods

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains all <paramref name="values"/> elements.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns><c>true</c> if all <paramref name="values"/> elements are found; otherwise <c>false</c>.</returns>
        public static bool ContainsAll(this StringBuilder self, IEnumerable<string> values)
            => ContainsAll(self, values, 0, self.Length, null);

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains all <paramref name="values"/> elements.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns><c>true</c> if all <paramref name="values"/> elements are found; otherwise <c>false</c>.</returns>
        public static bool ContainsAll(this StringBuilder self, IEnumerable<string> values, int startIndex)
            => ContainsAll(self, values, startIndex, self.Length - startIndex, null);

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains all <paramref name="values"/> elements.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns><c>true</c> if all <paramref name="values"/> elements are found; otherwise <c>false</c>.</returns>
        public static bool ContainsAll(this StringBuilder self, IEnumerable<string> values, int startIndex, int maxCount)
            => ContainsAll(self, values, startIndex, maxCount, null);

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains all <paramref name="values"/> elements.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns><c>true</c> if all <paramref name="values"/> elements are found; otherwise <c>false</c>.</returns>
        public static bool ContainsAll(this StringBuilder self, IEnumerable<string> values, ECharComparison comparison)
            => ContainsAll(self, values, 0, self.Length, comparison.ToCultureInfo());

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains all <paramref name="values"/> elements.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns><c>true</c> if all <paramref name="values"/> elements are found; otherwise <c>false</c>.</returns>
        public static bool ContainsAll(this StringBuilder self, IEnumerable<string> values, int startIndex, ECharComparison comparison)
            => ContainsAll(self, values, startIndex, self.Length - startIndex, comparison.ToCultureInfo());

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains all <paramref name="values"/> elements.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns><c>true</c> if all <paramref name="values"/> elements are found; otherwise <c>false</c>.</returns>
        public static bool ContainsAll(this StringBuilder self, IEnumerable<string> values, int startIndex, int maxCount, ECharComparison comparison)
            => ContainsAll(self, values, startIndex, maxCount, comparison.ToCultureInfo());

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains all <paramref name="values"/> elements.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns><c>true</c> if all <paramref name="values"/> elements are found; otherwise <c>false</c>.</returns>
        public static bool ContainsAll(this StringBuilder self, IEnumerable<string> values, CultureInfo insensitiveCulture)
            => ContainsAll(self, values, 0, self.Length, insensitiveCulture);

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains all <paramref name="values"/> elements.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns><c>true</c> if all <paramref name="values"/> elements are found; otherwise <c>false</c>.</returns>
        public static bool ContainsAll(this StringBuilder self, IEnumerable<string> values, int startIndex, CultureInfo insensitiveCulture)
            => ContainsAll(self, values, startIndex, self.Length - startIndex, insensitiveCulture);

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains all <paramref name="values"/> elements.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns><c>true</c> if all <paramref name="values"/> elements are found; otherwise <c>false</c>.</returns>
        public static bool ContainsAll(this StringBuilder self, IEnumerable<string> values, int startIndex, int maxCount, CultureInfo insensitiveCulture)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNull(nameof(values), values);
            Throw.If.Arg.IsLesser(nameof(startIndex), startIndex, 0);
            Throw.If.Arg.IsLesser(nameof(maxCount), maxCount, -1);

            if (!values.Any()) { return false; }
            foreach (var value in values) { if (!Contains(self, value, startIndex, maxCount, insensitiveCulture)) { return false; } }
            return true;
        }

        #endregion

        #region ContainsAny(char) Methods

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains any <paramref name="values"/> element.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool ContainsAny(this StringBuilder self, IEnumerable<char> values)
            => IndexOfAny(self, values, 0, self.Length, null) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool ContainsAny(this StringBuilder self, IEnumerable<char> values, int startIndex)
            => IndexOfAny(self, values, startIndex, self.Length - startIndex, null) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool ContainsAny(this StringBuilder self, IEnumerable<char> values, int startIndex, int maxCount)
            => IndexOfAny(self, values, startIndex, maxCount, null) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains any <paramref name="values"/> element.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool ContainsAny(this StringBuilder self, IEnumerable<char> values, ECharComparison comparison)
            => IndexOfAny(self, values, 0, self.Length, comparison) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool ContainsAny(this StringBuilder self, IEnumerable<char> values, int startIndex, ECharComparison comparison)
            => IndexOfAny(self, values, startIndex, self.Length - startIndex, comparison) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool ContainsAny(this StringBuilder self, IEnumerable<char> values, int startIndex, int maxCount, ECharComparison comparison)
            => IndexOfAny(self, values, startIndex, maxCount, comparison) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains any <paramref name="values"/> element.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool ContainsAny(this StringBuilder self, IEnumerable<char> values, CultureInfo insensitiveCulture)
            => IndexOfAny(self, values, 0, self.Length, insensitiveCulture) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool ContainsAny(this StringBuilder self, IEnumerable<char> values, int startIndex, CultureInfo insensitiveCulture)
            => IndexOfAny(self, values, startIndex, self.Length - startIndex, insensitiveCulture) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool ContainsAny(this StringBuilder self, IEnumerable<char> values, int startIndex, int maxCount, CultureInfo insensitiveCulture)
            => IndexOfAny(self, values, startIndex, maxCount, insensitiveCulture) > -1;

        #endregion

        #region ContainsAny(string) Methods

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains any <paramref name="values"/> element.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool ContainsAny(this StringBuilder self, IEnumerable<string> values)
            => IndexOfAny(self, values, 0, self.Length, null) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool ContainsAny(this StringBuilder self, IEnumerable<string> values, int startIndex)
            => IndexOfAny(self, values, startIndex, self.Length - startIndex, null) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool ContainsAny(this StringBuilder self, IEnumerable<string> values, int startIndex, int maxCount)
            => IndexOfAny(self, values, startIndex, maxCount, null) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains any <paramref name="values"/> element.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool ContainsAny(this StringBuilder self, IEnumerable<string> values, ECharComparison comparison)
            => IndexOfAny(self, values, 0, self.Length, comparison) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool ContainsAny(this StringBuilder self, IEnumerable<string> values, int startIndex, ECharComparison comparison)
            => IndexOfAny(self, values, startIndex, self.Length - startIndex, comparison) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool ContainsAny(this StringBuilder self, IEnumerable<string> values, int startIndex, int maxCount, ECharComparison comparison)
            => IndexOfAny(self, values, startIndex, maxCount, comparison) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains any <paramref name="values"/> element.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool ContainsAny(this StringBuilder self, IEnumerable<string> values, CultureInfo insensitiveCulture)
            => IndexOfAny(self, values, 0, self.Length, insensitiveCulture) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool ContainsAny(this StringBuilder self, IEnumerable<string> values, int startIndex, CultureInfo insensitiveCulture)
            => IndexOfAny(self, values, startIndex, self.Length - startIndex, insensitiveCulture) > -1;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool ContainsAny(this StringBuilder self, IEnumerable<string> values, int startIndex, int maxCount, CultureInfo insensitiveCulture)
            => IndexOfAny(self, values, startIndex, maxCount, insensitiveCulture) > -1;

        #endregion

        #region EndsWith Methods

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> ends with <paramref name="value"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to compare.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool EndsWith(this StringBuilder self, string value)
            => EndsWith(self, value, null);

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> ends with <paramref name="value"/>.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to compare.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool EndsWith(this StringBuilder self, string value, ECharComparison comparison)
            => EndsWith(self, value, comparison.ToCultureInfo());

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> ends with <paramref name="value"/>.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to compare.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool EndsWith(this StringBuilder self, string value, CultureInfo insensitiveCulture)
        {
            Throw.If.Self.IsNull(self);

            if (self.Length == 0 || value == null || value.Length == 0 || value.Length > self.Length) { return false; }
            return IsMatch(self, value, self.Length - value.Length, insensitiveCulture);
        }

        #endregion

        #region EndsWithAny Methods

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> ends with any <paramref name="values"/> element.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to compare.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool EndsWithAny(this StringBuilder self, IEnumerable<string> values)
            => EndsWithAny(self, values, null);

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> ends with any <paramref name="values"/> element.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to compare.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool EndsWithAny(this StringBuilder self, IEnumerable<string> values, ECharComparison comparison)
            => EndsWithAny(self, values, comparison.ToCultureInfo());

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> ends with any <paramref name="values"/> element.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to compare.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool EndsWithAny(this StringBuilder self, IEnumerable<string> values, CultureInfo insensitiveCulture)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNull(nameof(values), values);

            if (self.Length == 0) { return false; }
            foreach (var value in values)
            { if (IsMatch(self, value, self.Length - value.Length, insensitiveCulture)) { return true; } }
            return false;
        }

        #endregion

        #region IndexOf(char) Methods

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of <paramref name="value"/>.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="char"/> to seek.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>The index position of <paramref name="value"/> if found; otherwise <c>-1</c>.</returns>
        public static int IndexOf(this StringBuilder self, char value)
            => IndexOf(self, value, 0, self.Length);

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="char"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns>The index position of <paramref name="value"/> if found; otherwise <c>-1</c>.</returns>
        public static int IndexOf(this StringBuilder self, char value, int startIndex)
            => IndexOf(self, value, startIndex, self.Length - startIndex);

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="char"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>The index position of <paramref name="value"/> if found; otherwise <c>-1</c>.</returns>
        public static int IndexOf(this StringBuilder self, char value, int startIndex, int maxCount)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsLesser(nameof(startIndex), startIndex, 0);
            Throw.If.Arg.IsLesser(nameof(maxCount), maxCount, -1);

            if (self.Length == 0 || startIndex >= self.Length || maxCount == 0) { return -1; }
            else if (maxCount == -1) { maxCount = self.Length - startIndex; }

            for (int index = startIndex; index < self.Length && index - startIndex < maxCount; index++)
            { if (value == self[index]) { return index; } }
            return -1;
        }

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of <paramref name="value"/>.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="char"/> to seek.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>The index position of <paramref name="value"/> if found; otherwise <c>-1</c>.</returns>
        public static int IndexOf(this StringBuilder self, char value, ECharComparison comparison)
            => IndexOf(self, value, 0, self.Length, comparison.ToCultureInfo());

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="char"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns>The index position of <paramref name="value"/> if found; otherwise <c>-1</c>.</returns>
        public static int IndexOf(this StringBuilder self, char value, int startIndex, ECharComparison comparison)
            => IndexOf(self, value, startIndex, self.Length - startIndex, comparison.ToCultureInfo());

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="char"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>The index position of <paramref name="value"/> if found; otherwise <c>-1</c>.</returns>
        public static int IndexOf(this StringBuilder self, char value, int startIndex, int maxCount, ECharComparison comparison)
            => IndexOf(self, value, startIndex, maxCount, comparison.ToCultureInfo());

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of <paramref name="value"/>.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="char"/> to seek.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>The index position of <paramref name="value"/> if found; otherwise <c>-1</c>.</returns>
        public static int IndexOf(this StringBuilder self, char value, CultureInfo insensitiveCulture)
            => IndexOf(self, value, 0, self.Length, insensitiveCulture);

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="char"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns>The index position of <paramref name="value"/> if found; otherwise <c>-1</c>.</returns>
        public static int IndexOf(this StringBuilder self, char value, int startIndex, CultureInfo insensitiveCulture)
            => IndexOf(self, value, startIndex, self.Length - startIndex, insensitiveCulture);

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="char"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>The index position of <paramref name="value"/> if found; otherwise <c>-1</c>.</returns>
        public static int IndexOf(this StringBuilder self, char value, int startIndex, int maxCount, CultureInfo insensitiveCulture)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsLesser(nameof(startIndex), startIndex, 0);
            Throw.If.Arg.IsLesser(nameof(maxCount), maxCount, -1);

            if (self.Length == 0 || startIndex >= self.Length || maxCount == 0) { return -1; }
            else if (maxCount == -1) { maxCount = self.Length - startIndex; }

            for (int index = startIndex; index < self.Length && index - startIndex < maxCount; index++)
            { if (CommonHelper.IsCharEqual(value, self[index], insensitiveCulture)) { return index; } }
            return -1;
        }

        #endregion

        #region IndexOf(string) Methods

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of <paramref name="value"/>.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to seek.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>The index position of <paramref name="value"/> if found; otherwise <c>-1</c>.</returns>
        public static int IndexOf(this StringBuilder self, string value)
            => IndexOf(self, value, 0, self.Length, null);

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns>The index position of <paramref name="value"/> if found; otherwise <c>-1</c>.</returns>
        public static int IndexOf(this StringBuilder self, string value, int startIndex)
            => IndexOf(self, value, startIndex, self.Length - startIndex, null);

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>The index position of <paramref name="value"/> if found; otherwise <c>-1</c>.</returns>
        public static int IndexOf(this StringBuilder self, string value, int startIndex, int maxCount)
            => IndexOf(self, value, startIndex, maxCount, null);

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of <paramref name="value"/>.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to seek.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>The index position of <paramref name="value"/> if found; otherwise <c>-1</c>.</returns>
        public static int IndexOf(this StringBuilder self, string value, ECharComparison comparison)
            => IndexOf(self, value, 0, self.Length, comparison.ToCultureInfo());

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns>The index position of <paramref name="value"/> if found; otherwise <c>-1</c>.</returns>
        public static int IndexOf(this StringBuilder self, string value, int startIndex, ECharComparison comparison)
            => IndexOf(self, value, startIndex, self.Length - startIndex, comparison.ToCultureInfo());

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>The index position of <paramref name="value"/> if found; otherwise <c>-1</c>.</returns>
        public static int IndexOf(this StringBuilder self, string value, int startIndex, int maxCount, ECharComparison comparison)
            => IndexOf(self, value, startIndex, maxCount, comparison.ToCultureInfo());

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of <paramref name="value"/>.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to seek.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>The index position of <paramref name="value"/> if found; otherwise <c>-1</c>.</returns>
        public static int IndexOf(this StringBuilder self, string value, CultureInfo insensitiveCulture)
            => IndexOf(self, value, 0, self.Length, insensitiveCulture);

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns>The index position of <paramref name="value"/> if found; otherwise <c>-1</c>.</returns>
        public static int IndexOf(this StringBuilder self, string value, int startIndex, CultureInfo insensitiveCulture)
            => IndexOf(self, value, startIndex, self.Length - startIndex, insensitiveCulture);

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of <paramref name="value"/>.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>The index position of <paramref name="value"/> if found; otherwise <c>-1</c>.</returns>
        public static int IndexOf(this StringBuilder self, string value, int startIndex, int maxCount, CultureInfo insensitiveCulture)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsLesser(nameof(startIndex), startIndex, 0);
            Throw.If.Arg.IsLesser(nameof(maxCount), maxCount, -1);

            if (self.Length == 0 || value == null || value.Length == 0 || value.Length > self.Length || startIndex >= self.Length) { return -1; }
            else if (maxCount == 0) { return -1; }
            else if (maxCount == -1) { maxCount = self.Length - startIndex; }

            for (int index = startIndex; index < self.Length && index - startIndex < maxCount && index <= self.Length - value.Length; index++)
            {
                //bool match = true;
                //for (int sub = 0; sub < value.Length; sub++)
                //{ if (!CommonHelper.IsCharEqual(value[sub], self[index + sub], insensitiveCulture)) { match = false; break; } }
                //if (match) { return index; }
                if (IsMatch(self, value, index, insensitiveCulture)) { return index; }
            }
            return -1;
        }

        #endregion

        #region IndexOfAny(char) Methods

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of any <paramref name="values"/> element.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> to seek.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns>The index position of the first <paramref name="values"/> element found; otherwise <c>-1</c>.</returns>
        public static int IndexOfAny(this StringBuilder self, IEnumerable<char> values)
            => IndexOfAny(self, values, 0, self.Length, null);

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns>The index position of the first <paramref name="values"/> element found; otherwise <c>-1</c>.</returns>
        public static int IndexOfAny(this StringBuilder self, IEnumerable<char> values, int startIndex)
            => IndexOfAny(self, values, startIndex, self.Length - startIndex, null);

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>The index position of the first <paramref name="values"/> element found; otherwise <c>-1</c>.</returns>
        public static int IndexOfAny(this StringBuilder self, IEnumerable<char> values, int startIndex, int maxCount)
            => IndexOfAny(self, values, startIndex, maxCount, null);

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of any <paramref name="values"/> element.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> to seek.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns>The index position of the first <paramref name="values"/> element found; otherwise <c>-1</c>.</returns>
        public static int IndexOfAny(this StringBuilder self, IEnumerable<char> values, ECharComparison comparison)
            => IndexOfAny(self, values, 0, self.Length, comparison.ToCultureInfo());

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns>The index position of the first <paramref name="values"/> element found; otherwise <c>-1</c>.</returns>
        public static int IndexOfAny(this StringBuilder self, IEnumerable<char> values, int startIndex, ECharComparison comparison)
            => IndexOfAny(self, values, startIndex, self.Length - startIndex, comparison.ToCultureInfo());

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>The index position of the first <paramref name="values"/> element found; otherwise <c>-1</c>.</returns>
        public static int IndexOfAny(this StringBuilder self, IEnumerable<char> values, int startIndex, int maxCount, ECharComparison comparison)
            => IndexOfAny(self, values, startIndex, maxCount, comparison.ToCultureInfo());

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of any <paramref name="values"/> element.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> to seek.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns>The index position of the first <paramref name="values"/> element found; otherwise <c>-1</c>.</returns>
        public static int IndexOfAny(this StringBuilder self, IEnumerable<char> values, CultureInfo insensitiveCulture)
            => IndexOfAny(self, values, 0, self.Length, insensitiveCulture);

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns>The index position of the first <paramref name="values"/> element found; otherwise <c>-1</c>.</returns>
        public static int IndexOfAny(this StringBuilder self, IEnumerable<char> values, int startIndex, CultureInfo insensitiveCulture)
            => IndexOfAny(self, values, startIndex, self.Length - startIndex, insensitiveCulture);

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>The index position of the first <paramref name="values"/> element found; otherwise <c>-1</c>.</returns>
        // TODO: Maybe write an article about order invariance for a breadth-first search (what is used) and
        //       a depth-first search. The former is invariant and the latter isn't. See the unit tests for an example.
        public static int IndexOfAny(this StringBuilder self, IEnumerable<char> values, int startIndex, int maxCount, CultureInfo insensitiveCulture)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNull(nameof(values), values);
            Throw.If.Arg.IsLesser(nameof(startIndex), startIndex, 0);
            Throw.If.Arg.IsLesser(nameof(maxCount), maxCount, -1);

            if (self.Length == 0 || startIndex >= self.Length || maxCount == 0) { return -1; }
            else if (maxCount == -1) { maxCount = self.Length - startIndex; }

            for (int index = startIndex; index < self.Length && index - startIndex < maxCount; index++)
            {
                foreach (var value in values)
                { if (CommonHelper.IsCharEqual(value, self[index], insensitiveCulture)) { return index; } }
            }
            return -1;
        }

        #endregion

        #region IndexOfAny(string) Methods

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of any <paramref name="values"/> element.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> to seek.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns>The index position of the first <paramref name="values"/> element found; otherwise <c>-1</c>.</returns>
        public static int IndexOfAny(this StringBuilder self, IEnumerable<string> values)
            => IndexOfAny(self, values, 0, self.Length, null);

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns>The index position of the first <paramref name="values"/> element found; otherwise <c>-1</c>.</returns>
        public static int IndexOfAny(this StringBuilder self, IEnumerable<string> values, int startIndex)
            => IndexOfAny(self, values, startIndex, self.Length - startIndex, null);

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>The index position of the first <paramref name="values"/> element found; otherwise <c>-1</c>.</returns>
        public static int IndexOfAny(this StringBuilder self, IEnumerable<string> values, int startIndex, int maxCount)
            => IndexOfAny(self, values, startIndex, maxCount, null);

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of any <paramref name="values"/> element.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> to seek.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns>The index position of the first <paramref name="values"/> element found; otherwise <c>-1</c>.</returns>
        public static int IndexOfAny(this StringBuilder self, IEnumerable<string> values, ECharComparison comparison)
            => IndexOfAny(self, values, 0, self.Length, comparison.ToCultureInfo());

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns>The index position of the first <paramref name="values"/> element found; otherwise <c>-1</c>.</returns>
        public static int IndexOfAny(this StringBuilder self, IEnumerable<string> values, int startIndex, ECharComparison comparison)
            => IndexOfAny(self, values, startIndex, self.Length - startIndex, comparison.ToCultureInfo());

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>The index position of the first <paramref name="values"/> element found; otherwise <c>-1</c>.</returns>
        public static int IndexOfAny(this StringBuilder self, IEnumerable<string> values, int startIndex, int maxCount, ECharComparison comparison)
            => IndexOfAny(self, values, startIndex, maxCount, comparison.ToCultureInfo());

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of any <paramref name="values"/> element.
        /// Search begins at the first position and can continue through the remaining character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> to seek.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns>The index position of the first <paramref name="values"/> element found; otherwise <c>-1</c>.</returns>
        public static int IndexOfAny(this StringBuilder self, IEnumerable<string> values, CultureInfo insensitiveCulture)
            => IndexOfAny(self, values, 0, self.Length, insensitiveCulture);

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through the remaining character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns>The index position of the first <paramref name="values"/> element found; otherwise <c>-1</c>.</returns>
        public static int IndexOfAny(this StringBuilder self, IEnumerable<string> values, int startIndex, CultureInfo insensitiveCulture)
            => IndexOfAny(self, values, startIndex, self.Length - startIndex, insensitiveCulture);

        /// <summary>
        /// Returns the index in this <see cref="StringBuilder"/> for the first occurrence of any <paramref name="values"/> element.
        /// Search begins at <paramref name="startIndex"/> and can continue through <paramref name="maxCount"/> character positions.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> to seek.</param>
        /// <param name="startIndex">The search starting position.</param>
        /// <param name="maxCount">The number of character positions to examine or <c>-1</c> for unlimited.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>The index position of the first <paramref name="values"/> element found; otherwise <c>-1</c>.</returns>
        public static int IndexOfAny(this StringBuilder self, IEnumerable<string> values, int startIndex, int maxCount, CultureInfo insensitiveCulture)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNull(nameof(values), values);
            Throw.If.Arg.IsLesser(nameof(startIndex), startIndex, 0);
            Throw.If.Arg.IsLesser(nameof(maxCount), maxCount, -1);

            if (self.Length == 0 || startIndex >= self.Length || maxCount == 0) { return -1; }
            else if (maxCount == -1) { maxCount = self.Length - startIndex; }

            for (int index = startIndex; index < self.Length && index - startIndex < maxCount; index++)
            {
                foreach (var value in values)
                { if (IsMatch(self, value, index)) { return index; } }
            }
            return -1;
        }

        #endregion

        #region HasIndex Methods

        /// <summary>
        /// Determines if the specified <paramref name="index"/> is valid for this <see cref="StringBuilder"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="index">Index to validate.</param>
        /// <returns><c>true</c> if the index is greater than <c>-1</c> and less than the string length.</returns>
        public static bool HasIndex(this StringBuilder self, int index)
            => (self != null && index > -1 && index < self.Length);

        #endregion

        #region IsChar Methods

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains a <see cref="char"/> equivalent to
        /// <paramref name="value"/> at the given <paramref name="index"/>. If the <see cref="StringBuilder"/>
        /// is <c>null</c> or <paramref name="index"/> is out of range, then the method returns <c>false</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="index">Index to compare at.</param>
        /// <param name="value">Value to compare to.</param>
        /// <returns><c>true</c> if the <see cref="StringBuilder"/> has a character at the index equivalent to the value; otherwise <c>false</c>.</returns>
        public static bool IsChar(this StringBuilder self, int index, char value)
            => (self == null || index < 0 || index >= self.Length ? false : self[index] == value);

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains a <see cref="char"/> equivalent to
        /// <paramref name="value"/> at the given <paramref name="index"/>. If the <see cref="StringBuilder"/>
        /// is <c>null</c> or <paramref name="index"/> is out of range, then the method returns <c>false</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="index">Index to compare at.</param>
        /// <param name="value">Value to compare to.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <returns><c>true</c> if the <see cref="StringBuilder"/> has a character at the index equivalent to the value; otherwise <c>false</c>.</returns>
        public static bool IsChar(this StringBuilder self, int index, char value, ECharComparison comparison)
            => (self == null || index < 0 || index >= self.Length ? false : CommonHelper.IsCharEqual(self[index], value, comparison));

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains a <see cref="char"/> equivalent to
        /// <paramref name="value"/> at the given <paramref name="index"/>. If the <see cref="StringBuilder"/>
        /// is <c>null</c> or <paramref name="index"/> is out of range, then the method returns <c>false</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="index">Index to compare at.</param>
        /// <param name="value">Value to compare to.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <returns><c>true</c> if the <see cref="StringBuilder"/> has a character at the index equivalent to the value; otherwise <c>false</c>.</returns>
        public static bool IsChar(this StringBuilder self, int index, char value, CultureInfo insensitiveCulture)
            => (self == null || index < 0 || index >= self.Length ? false : CommonHelper.IsCharEqual(self[index], value, insensitiveCulture));

        #endregion

        #region IsPrevChar Methods

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains a <see cref="char"/> equivalent to
        /// <paramref name="prefix"/> at the position before <paramref name="index"/>. If the <see cref="StringBuilder"/>
        /// is <c>null</c> or decremented <paramref name="index"/> is out of range, then the method returns <c>false</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="index">Index to compare at.</param>
        /// <param name="prefix">Value to compare to.</param>
        /// <returns><c>true</c> if the <see cref="StringBuilder"/> has a character at <c>index - 1</c> equivalent to the prefix; otherwise <c>false</c>.</returns>
        public static bool IsPrevChar(this StringBuilder self, int index, char prefix)
            => IsChar(self, index - 1, prefix);

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains a <see cref="char"/> equivalent to
        /// <paramref name="prefix"/> at the position before <paramref name="index"/>. If the <see cref="StringBuilder"/>
        /// is <c>null</c> or decremented <paramref name="index"/> is out of range, then the method returns <c>false</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="index">Index to compare at.</param>
        /// <param name="prefix">Value to compare to.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <returns><c>true</c> if the <see cref="StringBuilder"/> has a character at <c>index - 1</c> equivalent to the prefix; otherwise <c>false</c>.</returns>
        public static bool IsPrevChar(this StringBuilder self, int index, char prefix, ECharComparison comparison)
            => IsChar(self, index - 1, prefix, comparison);

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains a <see cref="char"/> equivalent to
        /// <paramref name="prefix"/> at the position before <paramref name="index"/>. If the <see cref="StringBuilder"/>
        /// is <c>null</c> or decremented <paramref name="index"/> is out of range, then the method returns <c>false</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="index">Index to compare at.</param>
        /// <param name="prefix">Value to compare to.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <returns><c>true</c> if the <see cref="StringBuilder"/> has a character at <c>index - 1</c> equivalent to the prefix; otherwise <c>false</c>.</returns>
        public static bool IsPrevChar(this StringBuilder self, int index, char prefix, CultureInfo insensitiveCulture)
            => IsChar(self, index - 1, prefix, insensitiveCulture);

        #endregion

        #region IsNextChar Methods

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains a <see cref="char"/> equivalent to
        /// <paramref name="suffix"/> at the position after <paramref name="index"/>. If the <see cref="StringBuilder"/>
        /// is <c>null</c> or incremented <paramref name="index"/> is out of range, then the method returns <c>false</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="index">Index to compare at.</param>
        /// <param name="suffix">Value to compare to.</param>
        /// <returns><c>true</c> if the <see cref="StringBuilder"/> has a character at <c>index + 1</c> equivalent to the suffix; otherwise <c>false</c>.</returns>
        public static bool IsNextChar(this StringBuilder self, int index, char suffix)
            => IsChar(self, index + 1, suffix);

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains a <see cref="char"/> equivalent to
        /// <paramref name="suffix"/> at the position after <paramref name="index"/>. If the <see cref="StringBuilder"/>
        /// is <c>null</c> or incremented <paramref name="index"/> is out of range, then the method returns <c>false</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="index">Index to compare at.</param>
        /// <param name="suffix">Value to compare to.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <returns><c>true</c> if the <see cref="StringBuilder"/> has a character at <c>index + 1</c> equivalent to the suffix; otherwise <c>false</c>.</returns>
        public static bool IsNextChar(this StringBuilder self, int index, char suffix, ECharComparison comparison)
            => IsChar(self, index + 1, suffix, comparison);

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains a <see cref="char"/> equivalent to
        /// <paramref name="suffix"/> at the position after <paramref name="index"/>. If the <see cref="StringBuilder"/>
        /// is <c>null</c> or incremented <paramref name="index"/> is out of range, then the method returns <c>false</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="index">Index to compare at.</param>
        /// <param name="suffix">Value to compare to.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <returns><c>true</c> if the <see cref="StringBuilder"/> has a character at <c>index + 1</c> equivalent to the suffix; otherwise <c>false</c>.</returns>
        public static bool IsNextChar(this StringBuilder self, int index, char suffix, CultureInfo insensitiveCulture)
            => IsChar(self, index + 1, suffix, insensitiveCulture);

        #endregion

        #region IsEmpty Methods

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> is <c>null</c> or has a length of <c>0</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the <see cref="StringBuilder"/> is <c>null</c> or has length zero; otherwise <c>false</c>.</returns>
        public static bool IsEmpty(this StringBuilder self)
            => self == null || self.Length == 0;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> is not <c>null</c> and has
        /// a length greater than zero.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the <see cref="StringBuilder"/> is not <c>null</c> and has length greater than zero; otherwise <c>false</c>.</returns>
        public static bool IsNotEmpty(this StringBuilder self)
            => self != null && self.Length > 0;

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> is <c>null</c>, has
        /// a length of zero, or is composed entirely of white space.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the <see cref="StringBuilder"/> is <c>null</c>, has length zero, or is entirely white space; otherwise <c>false</c>.</returns>
        public static bool IsEmptyOrWhitespace(this StringBuilder self)
        {
            if (IsEmpty(self)) { return true; }
            for (int index = 0; index < self.Length; index++) { if (!char.IsWhiteSpace(self[index])) { return false; } }
            return true;
        }

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> is not <c>null</c>, has
        /// a length greater than zero, and is not composed entirely of white space.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the <see cref="StringBuilder"/> is not <c>null</c>, has length greater than zero, and is not entirely white space; otherwise <c>false</c>.</returns>
        public static bool IsNotEmptyOrWhitespace(this StringBuilder self)
        {
            if (IsEmpty(self)) { return false; }
            for (int index = 0; index < self.Length; index++) { if (!char.IsWhiteSpace(self[index])) { return true; } }
            return false;
        }

        #endregion

        #region IsMatch Methods

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains <paramref name="value"/>.
        /// Comparison will begin at <paramref name="startIndex"/> in the specified instance.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The value to compare.</param>
        /// <param name="startIndex">The index in begin comparison against the value.</param>
        /// <returns><c>true</c> if the <see cref="StringBuilder"/> contains the value beginning at the start index; otherwise <c>false</c>.</returns>
        public static bool IsMatch(this StringBuilder self, string value, int startIndex)
        {
            if (IsEmpty(self) || value.IsEmpty()) { return false; }
            else if (value.Length > self.Length) { return false; }
            else if (startIndex < 0 || startIndex > self.Length - value.Length) { return false; }

            for (int index = 0; index < value.Length; index++)
            { if (self[index + startIndex] != value[index]) { return false; } }
            return true;
        }

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains <paramref name="value"/>.
        /// Comparison will begin at <paramref name="startIndex"/> in the specified instance.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The value to compare.</param>
        /// <param name="startIndex">The index in begin comparison against the value.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <returns><c>true</c> if the <see cref="StringBuilder"/> contains the value beginning at the start index; otherwise <c>false</c>.</returns>
        public static bool IsMatch(this StringBuilder self, string value, int startIndex, ECharComparison comparison)
        {
            if (IsEmpty(self) || value.IsEmpty()) { return false; }
            else if (value.Length > self.Length) { return false; }
            else if (startIndex < 0 || startIndex > self.Length - value.Length) { return false; }

            for (int index = 0; index < value.Length; index++)
            { if (!CommonHelper.IsCharEqual(self[index + startIndex], value[index], comparison)) { return false; } }
            return true;
        }

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> contains <paramref name="value"/>.
        /// Comparison will begin at <paramref name="startIndex"/> in the specified instance.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The value to compare.</param>
        /// <param name="startIndex">The index in begin comparison against the value.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <returns><c>true</c> if the <see cref="StringBuilder"/> contains the value beginning at the start index; otherwise <c>false</c>.</returns>
        public static bool IsMatch(this StringBuilder self, string value, int startIndex, CultureInfo insensitiveCulture)
        {
            if (IsEmpty(self) || value.IsEmpty()) { return false; }
            else if (value.Length > self.Length) { return false; }
            else if (startIndex < 0 || startIndex > self.Length - value.Length) { return false; }

            for (int index = 0; index < value.Length; index++)
            { if (!CommonHelper.IsCharEqual(self[index + startIndex], value[index], insensitiveCulture)) { return false; } }
            return true;
        }

        #endregion

        #region StartsWith Methods

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> starts with <paramref name="value"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to compare.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool StartsWith(this StringBuilder self, string value)
            => StartsWith(self, value, null);

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> starts with <paramref name="value"/>.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to compare.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool StartsWith(this StringBuilder self, string value, ECharComparison comparison)
            => StartsWith(self, value, comparison.ToCultureInfo());

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> starts with <paramref name="value"/>.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The <see cref="string"/> to compare.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns><c>true</c> if <paramref name="value"/> is found; otherwise <c>false</c>.</returns>
        public static bool StartsWith(this StringBuilder self, string value, CultureInfo insensitiveCulture)
        {
            Throw.If.Self.IsNull(self);

            if (self.Length == 0 || value == null || value.Length == 0 || value.Length > self.Length) { return false; }
            return IsMatch(self, value, 0, insensitiveCulture);
        }

        #endregion

        #region StartsWithAny Methods

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> starts with any <paramref name="values"/> element.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to compare.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool StartsWithAny(this StringBuilder self, IEnumerable<string> values)
            => StartsWithAny(self, values, null);

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> starts with any <paramref name="values"/> element.
        /// Case-sensitivity of comparisons is determined by the specified <paramref name="comparison"/> value.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to compare.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool StartsWithAny(this StringBuilder self, IEnumerable<string> values, ECharComparison comparison)
            => StartsWithAny(self, values, comparison.ToCultureInfo());

        /// <summary>
        /// Determines if this <see cref="StringBuilder"/> starts with any <paramref name="values"/> element.
        /// Comparisons are case-sensitive if <paramref name="insensitiveCulture"/> is <c>null</c>; otherwise case-insensitive
        /// culture-rules are used.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">The <see cref="IEnumerable{T}"/> of elements to compare.</param>
        /// <param name="insensitiveCulture">The <see cref="CultureInfo"/> specifying culture-specific rules for case-insensitive comparison; <c>null</c> for case-sensitive ordinal comparisons.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentNullException">If <paramref name="values"/> is <c>null</c>.</exception>
        /// <returns><c>true</c> if any <paramref name="values"/> element is found; otherwise <c>false</c>.</returns>
        public static bool StartsWithAny(this StringBuilder self, IEnumerable<string> values, CultureInfo insensitiveCulture)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNull(nameof(values), values);

            if (self.Length == 0) { return false; }
            foreach (var value in values)
            { if (IsMatch(self, value, 0, insensitiveCulture)) { return true; } }
            return false;
        }

        #endregion

    }
}
