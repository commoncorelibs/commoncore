﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="StringExtensions"/> <c>static</c> <c>class</c> provides
    /// extension methods related to <see cref="string"/> objects.
    /// 
    /// These extension methods are focused on rounding out the functionality
    /// of strings. This is accomplished on three fronts: One, offering
    /// instance-level access to static <see cref="string"/> methods by
    /// implementing appropriate wrapper extensions; two, overloading existing
    /// methods with versions that cover a variety of common use cases, such as
    /// case-insensitivity, operating on substrings, and providing char/string
    /// versions for those that only operate on one or the other; and three,
    /// providing higher-level interaction with string contents in a common
    /// and efficient manner.
    /// </summary>
    /// <remarks>
    /// For any string manipulation methods, special concern must be given for garbage
    /// collection. Methods should keep string allocations to an absolute minimum,
    /// preferring to precalculate and perform one allocation as the returned result.
    /// Following this logic, on the lower end, most methods will perform the equivalent
    /// of one new string allocation to generate its output. Many methods will perform
    /// up to three allocations, generically representing a prefix, the content, and a
    /// suffix. On the higher end, the more complex formatting methods may allocate
    /// dozens or theoretically hundreds of strings, but the point of these methods is
    /// to write the most efficient code possible, so it will probably generate less
    /// garbage than any ad hoc reversion rolled by hand.
    /// As of .Net Standard 2.1 and the porting of Common to CommonCore, many of the
    /// more allocation heavy methods have been/are being rewritten to utilize the new
    /// <see cref="string.Create{TState}(int, TState, System.Buffers.SpanAction{char, TState})"/>
    /// method. These changes will bring the effected methods down to the acceptable one
    /// to three allocation range.
    /// Methods that can results in large amounts of string allocations will be marked
    /// appropriately.
    /// </remarks>
    public static class StringExtensions
    {

        //public static IEnumerable<int> GenMatchIndexSequence(this string self, string value, int maxCount = -1)
        //{
        //    if (IsEmpty(self) || IsEmpty(value) || value.Length > self.Length || maxCount == 0) { yield break; }
        //    int count = 0;
        //    int index = self.IndexOf(value, 0);
        //    while (index > -1 && index < self.Length && (maxCount < 0 || count < maxCount))
        //    {
        //        count++;
        //        yield return index;
        //        index += value.Length;
        //        if (index >= self.Length) { break; }
        //        index = self.IndexOf(value, index);
        //    }
        //}

        #region CountMatches Methods

        /// <summary>
        /// Reports the number of occurrences of <paramref name="value"/> in this <see cref="string"/> instance.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The string to be counted.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>The number of times <paramref name="value"/> is found in this string instance.</returns>
        public static int CountMatches(this string self, string value)
            => CountMatches(self, value, -1, StringComparison.CurrentCulture);

        /// <summary>
        /// Reports the number of occurrences of <paramref name="value"/> in this <see cref="string"/> instance.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The string to be counted.</param>
        /// <param name="maxCount">The maximum number of occurrences to count.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>The number of times <paramref name="value"/> is found in this string instance.</returns>
        public static int CountMatches(this string self, string value, int maxCount)
            => CountMatches(self, value, maxCount, StringComparison.CurrentCulture);

        /// <summary>
        /// Reports the number of occurrences of <paramref name="value"/> in this <see cref="string"/> instance.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The string to be counted.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>The number of times <paramref name="value"/> is found in this string instance.</returns>
        public static int CountMatches(this string self, string value, StringComparison comparison)
            => CountMatches(self, value, -1, comparison);

        /// <summary>
        /// Reports the number of occurrences of <paramref name="value"/> in this <see cref="string"/> instance.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The string to be counted.</param>
        /// <param name="maxCount">The maximum number of occurrences to count.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>The number of times <paramref name="value"/> is found in this string instance.</returns>
        public static int CountMatches(this string self, string value, int maxCount, StringComparison comparison)
        {
            Throw.If.Self.IsNull(self);
            if (self.IsEmpty() || value.IsEmpty() || value.Length > self.Length || maxCount == 0) { return 0; }
            int count = 0;
            int index = self.IndexOf(value, 0, comparison);
            while (index > -1 && index < self.Length && (maxCount < 0 || count < maxCount))
            {
                count++;
                index += value.Length;
                if (index >= self.Length) { break; }
                index = self.IndexOf(value, index, comparison);
            }
            return count;
        }

        //public static int CountEndMatches(this string self, string value)
        //{
        //    if (IsEmpty(self) || IsEmpty(value)) { return 0; }
        //    int count = 0;
        //    while (true) { if (IsMatch(self, value, self.Length - ((count + 1) * value.Length))) { count += 1; } else { break; } }
        //    return count;
        //}

        //public static int CountStartMatches(this string self, string value)
        //{
        //    if (IsEmpty(self) || IsEmpty(value)) { return 0; }
        //    int count = 0;
        //    while (true) { if (IsMatch(self, value, count * value.Length)) { count += 1; } else { break; } }
        //    return count;
        //}

        #endregion

        #region Split Methods

        /// <summary>
        /// Splits this <see cref="string"/> instance into substrings based on the specified <paramref name="separators"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="separators">Sequence of strings to use as the delimiters.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>Array of substrings delimited by the specified <paramref name="separators"/>.</returns>
        public static string[] Split(this string self, params string[] separators)
        {
            Throw.If.Self.IsNull(self);
            return self.Split(separators, StringSplitOptions.None);
        }

        #endregion

    }
}
