﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="StringExtensions_NormalizeLineEndings"/> <c>static</c> <c>class</c> provides
    /// extension methods related to <see cref="string"/> objects.
    /// 
    /// These methods allow transforming <see cref="string"/>
    /// content with divergent line ending sequences to a single consistent
    /// form of line ending.
    /// 
    /// These methods return a new <see cref="string"/> instance.
    /// </summary>
    public static class StringExtensions_NormalizeLineEndings
    {
        /// <summary>
        /// Array of line ending <see cref="string"/> objects indexed by <see cref="ELineEnding"/> elements.
        /// <see cref="ELineEnding.N"/> is index <c>0</c> and string object <c>\n</c>;
        /// <see cref="ELineEnding.R"/> is index <c>1</c> and string object <c>\r</c>;
        /// <see cref="ELineEnding.RN"/> is index <c>2</c> and string object <c>\r\n</c>;
        /// <see cref="ELineEnding.NR"/> is index <c>3</c> and string object <c>\n\r</c>.
        /// </summary>
        public static string[] LineEndings { get; } = new[] { "\n", "\r", "\r\n", "\n\r" };

        /// <summary>
        /// Returns a new <see cref="string"/> with all forms of line ending sequence normalized to the same
        /// line ending form. For example, when called with the default argument, any standard Windows (\r\n)
        /// or malformed/legacy line endings (\n\r or \r) will be converted to the default line ending (\n).
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="ending">Line ending to use for normalization.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A new string with all line endings normalized to the given form.</returns>
        public static string NormalizeLineEndings(this string self, ELineEnding ending = ELineEnding.N)
        {
            Throw.If.Self.IsNull(self);
#if NETSTANDARD2_0
            for (int index = 0; index < self.Length; index++)
            {
                char c = self[index];

                ELineEnding oldEnding;
                if (c == '\n') { oldEnding = (self.IsNextChar(index, '\r') ? ELineEnding.NR : ELineEnding.N); }
                else if (c == '\r') { oldEnding = (self.IsNextChar(index, '\n') ? ELineEnding.RN : ELineEnding.R); }
                else { continue; }

                if (ending != oldEnding)
                {
                    self = self.Remove(index, LineEndings[(int) oldEnding].Length);
                    self = self.Insert(index, LineEndings[(int) ending]);
                }
                if (ending == ELineEnding.RN || ending == ELineEnding.NR)
                { index++; }
            }
            return self;
#else
            int countEndings = 0;
            int oldLength = 0;
            for (int index = 0; index < self.Length; index++)
            {
                char c = self[index];
                bool two = false;
                if (c == '\n') { two = self.IsNextChar(index, '\r'); }
                else if (c == '\r') { two = self.IsNextChar(index, '\n'); }
                else { continue; }

                countEndings++;
                oldLength += (two ? 2 : 1);
                if (two) { index++; }
            }

            int width = self.Length - oldLength + (countEndings * LineEndings[(int)ending].Length);
            return string.Create(width, (self, ending),
            (Span<char> chars, (string text, ELineEnding ending) state) =>
            {
                var endingSpan = LineEndings[(int)ending].AsSpan();
                int destIndex = 0;
                for (int index = 0; index < state.text.Length; index++)
                {
                    char c = state.text[index];

                    if (c == '\n')
                    {
                        endingSpan.CopyTo(chars.Slice(destIndex));
                        destIndex += endingSpan.Length;
                        if (state.text.IsNextChar(index, '\r')) { index++; }
                    }
                    else if (c == '\r')
                    {
                        endingSpan.CopyTo(chars.Slice(destIndex));
                        destIndex += endingSpan.Length;
                        if (state.text.IsNextChar(index, '\n')) { index++; }
                    }
                    else
                    {
                        chars[destIndex] = c;
                        destIndex++;
                    }
                }
            });
#endif
        }

        /// <summary>
        /// Returns a new <see cref="string"/> with all forms of line ending sequence normalized to the same
        /// line ending form. Additionally, successive line endings will be merged into a single line ending.
        /// Optionally, line endings separated by only white space can also be merged.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="ending">Line ending to use for normalization.</param>
        /// <param name="mergeWhiteSpace">Should intervening whitespace be stripped when merging two line endings.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A new string with all line endings normalized and merged to the given form.</returns>
        public static string NormalizeMergeLineEndings(this string self, ELineEnding ending = ELineEnding.N, bool mergeWhiteSpace = true)
        {
            Throw.If.Self.IsNull(self);
#if NETSTANDARD2_0
            self = self.NormalizeLineEndings(ELineEnding.N);
            for (int index = 0; index < self.Length; index++)
            {
                char c = self[index];
                if (c != '\n')
                { continue; }
                int count = 0, spaces = 0, current = index + 1;
                while (current < self.Length)
                {
                    if (self.IsChar(current, '\n'))
                    { count += (spaces == 0 ? 1 : spaces + 1); spaces = 0; }
                    else if (mergeWhiteSpace && self[current].IsWhiteSpace())
                    { spaces++; }
                    else
                    { break; }
                    current++;
                }
                if (count > 0)
                { self = self.Remove(index + 1, count); }
            }
            if (ending != ELineEnding.N)
            { self = self.Replace("\n", LineEndings[(int) ending]); }
            return self;

#else
            int countEndings = 0;
            int oldLength = 0;
            for (int index = 0; index < self.Length; index++)
            {
                char c = self[index];
                bool two = false;
                if (c == '\n') { two = self.IsNextChar(index, '\r'); }
                else if (c == '\r') { two = self.IsNextChar(index, '\n'); }
                else { continue; }

                int spaces = 0;
                int current = index + (two ? 2 : 1);
                while (current < self.Length)
                {
                    if (self.IsChar(current, '\n') || self.IsChar(current, '\r')) { spaces++; }
                    else if (mergeWhiteSpace && self[current].IsWhiteSpace()) { spaces++; }
                    else { break; }
                    current++;
                }

                countEndings++;
                oldLength += spaces + (two ? 2 : 1);
                index += spaces;
                if (two) { index++; }
            }

            int width = self.Length - oldLength + (countEndings * LineEndings[(int)ending].Length);
            return string.Create(width, (self, ending, mergeWhiteSpace),
            (Span<char> chars, (string text, ELineEnding ending, bool mergeWhiteSpace) state) =>
            {
                var endingSpan = LineEndings[(int)ending].AsSpan();
                int destIndex = 0;
                for (int index = 0; index < state.text.Length; index++)
                {
                    char c = state.text[index];

                    if (c == '\n')
                    {
                        endingSpan.CopyTo(chars.Slice(destIndex));
                        destIndex += endingSpan.Length;
                        if (state.text.IsNextChar(index, '\r')) { index++; }
                        int spaces = 0;
                        int current = index + 1;
                        while (current < state.text.Length)
                        {
                            if (state.text.IsChar(current, '\n') || state.text.IsChar(current, '\r')) { spaces++; }
                            else if (state.mergeWhiteSpace && state.text[current].IsWhiteSpace()) { spaces++; }
                            else { break; }
                            current++;
                        }
                        if (spaces > 0)
                        {
                            index += spaces;
                            //destIndex += spaces - 1;
                        }
                    }
                    else if (c == '\r')
                    {
                        endingSpan.CopyTo(chars.Slice(destIndex));
                        destIndex += endingSpan.Length;
                        if (state.text.IsNextChar(index, '\n')) { index++; }
                        int spaces = 0;
                        int current = index + 1;
                        while (current < state.text.Length)
                        {
                            if (state.text.IsChar(current, '\n') || state.text.IsChar(current, '\r')) { spaces++; }
                            else if (state.mergeWhiteSpace && state.text[current].IsWhiteSpace()) { spaces++; }
                            else { break; }
                            current++;
                        }
                        if (spaces > 0)
                        {
                            index += spaces;
                            //destIndex += spaces - 1;
                        }
                    }
                    else
                    {
                        chars[destIndex] = c;
                        destIndex++;
                    }
                }
            });
#endif

            //self = self.NormalizeLineEndings(ELineEnding.N);
            //for (int index = 0; index < self.Length; index++)
            //{
            //    char c = self[index];
            //    if (c != '\n') { continue; }

            //    int count = 0, spaces = 0, current = index + 1;
            //    while (current < self.Length)
            //    {
            //        if (self.IsChar(current, '\n')) { count += (spaces == 0 ? 1 : spaces + 1); spaces = 0; }
            //        else if (mergeWhiteSpace && self[current].IsWhiteSpace()) { spaces++; }
            //        else { break; }
            //        current++;
            //    }
            //    if (count > 0) { self = self.Remove(index + 1, count); }
            //}
            //if (ending != ELineEnding.N) { self = self.Replace("\n", LineEndings[(int)ending]); }
            //return self;
        }
    }
}
