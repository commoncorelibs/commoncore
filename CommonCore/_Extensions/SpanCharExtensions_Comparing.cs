﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="SpanCharExtensions_Comparing"/> <c>static</c> <c>class</c> provides
    /// <see cref="Span{Char}" /> and <see cref="ReadOnlySpan{Char}" /> related extension methods.
    /// </summary>
    public static class SpanCharExtensions_Comparing
    {

        #region IsEmpty Methods

        /// <summary>
        /// Determines if this <see cref="ReadOnlySpan{Char}" /> instance has a length of zero.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the string has length zero; otherwise <c>false</c>.</returns>
        public static bool IsEmpty(this ReadOnlySpan<char> self) => self.IsEmpty;

        /// <summary>
        /// Determines if this <see cref="ReadOnlySpan{Char}" /> instance has a length greater than zero.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>
        /// <c>true</c> if the string has length greater than zero; otherwise <c>false</c>.
        /// </returns>
        public static bool IsNotEmpty(this ReadOnlySpan<char> self) => !self.IsEmpty;

        /// <summary>
        /// Determines if this <see cref="ReadOnlySpan{Char}" /> instance has a length of zero or is
        /// composed entirely of white space.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>
        /// <c>true</c> if the string has length zero or is entirely white space; otherwise <c>false</c>.
        /// </returns>
        public static bool IsEmptyOrWhitespace(this ReadOnlySpan<char> self) => self.IsEmpty || self.IsWhiteSpace();

        /// <summary>
        /// Determines if this <see cref="ReadOnlySpan{Char}" /> instance has a length greater than
        /// zero and is not composed entirely of white space.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>
        /// <c>true</c> if the string has length greater than zero and is not entirely white space;
        /// otherwise <c>false</c>.
        /// </returns>
        public static bool IsNotEmptyOrWhitespace(this ReadOnlySpan<char> self) => !self.IsEmpty && !self.IsWhiteSpace();

        #endregion

    }
}
