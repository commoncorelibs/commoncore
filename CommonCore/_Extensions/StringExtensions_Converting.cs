﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Globalization;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="StringExtensions_Converting"/> <c>static</c> <c>class</c> provides
    /// extension methods related to <see cref="string"/> objects.
    /// 
    /// These methods focus on converting the <see cref="string"/> and its content into
    /// various other forms.
    /// </summary>
    public static class StringExtensions_Converting
    {

        #region ToLower Methods

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with the character at <paramref name="index"/>
        /// converted to lowercase.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="index">The zero-based starting character position of a character in this instance.</param>
        /// <returns>A string with the specified character converted to lowercase.</returns>
        public static string ToLowerChar(this string self, int index) => ToLower(self, index, 1);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with the substring starting at <paramref name="startIndex"/>
        /// to the end of the string converted to lowercase.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="startIndex">The zero-based starting character position of a substring in this instance.</param>
        /// <returns>A string with the specified substring converted to lowercase.</returns>
        public static string ToLower(this string self, int startIndex) => ToLower(self, startIndex, self.Length - startIndex);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with the substring starting at <paramref name="startIndex"/>
        /// including <paramref name="count"/> number of character converted to lowercase.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="startIndex">The zero-based starting character position of a substring in this instance.</param>
        /// <param name="count">The number of characters in the substring.</param>
        /// <returns>A string with the specified substring converted to lowercase.</returns>
        public static string ToLower(this string self, int startIndex, int count)
        {
            Throw.If.Self.IsNull(self);
            if (string.IsNullOrWhiteSpace(self)) { return self; }
            int endIndex = startIndex + count;
#if NETSTANDARD2_0
            char[] chars = self.ToCharArray();
            for (int index = startIndex; index < startIndex + count; index++)
            { chars[index] = char.ToLower(chars[index]); }
            return new string(chars);
#else
            return string.Create(self.Length, (self, startIndex, endIndex), (chars, state) =>
            {
                for (int index = 0; index < chars.Length; index++)
                {
                    if (index >= state.startIndex && index < state.endIndex)
                    { chars[index] = char.ToLower(state.self[index]); }
                    else
                    { chars[index] = state.self[index]; }
                }
            });
#endif
        }

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with the character at <paramref name="index"/>
        /// converted to lowercase using the casing rules of the invariant culture.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="index">The zero-based starting character position of a character in this instance.</param>
        /// <returns>A string with the specified character converted to lowercase.</returns>
        public static string ToLowerCharInvariant(this string self, int index) => ToLowerInvariant(self, index, 1);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with the substring starting at <paramref name="startIndex"/>
        /// to the end of the string converted to lowercase using the casing rules of the invariant culture.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="startIndex">The zero-based starting character position of a substring in this instance.</param>
        /// <returns>A string with the specified substring converted to lowercase.</returns>
        public static string ToLowerInvariant(this string self, int startIndex) => ToLowerInvariant(self, startIndex, self.Length - startIndex);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with the substring starting at <paramref name="startIndex"/>
        /// including <paramref name="count"/> number of character converted to lowercase using the casing rules
        /// of the invariant culture.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="startIndex">The zero-based starting character position of a substring in this instance.</param>
        /// <param name="count">The number of characters in the substring.</param>
        /// <returns>A string with the specified substring converted to lowercase.</returns>
        public static string ToLowerInvariant(this string self, int startIndex, int count)
        {
            Throw.If.Self.IsNull(self);
            if (string.IsNullOrWhiteSpace(self)) { return self; }
#if NETSTANDARD2_0
            char[] chars = self.ToCharArray();
            for (int index = startIndex; index < startIndex + count; index++)
            { chars[index] = char.ToLowerInvariant(chars[index]); }
            return new string(chars);
#else
            int endIndex = startIndex + count;
            return string.Create(self.Length, (self, startIndex, endIndex), (chars, state) =>
            {
                for (int index = 0; index < chars.Length; index++)
                {
                    if (index >= state.startIndex && index < state.endIndex)
                    { chars[index] = char.ToLowerInvariant(state.self[index]); }
                    else
                    { chars[index] = state.self[index]; }
                }
            });
#endif
        }

        #endregion

        #region ToOrdinalString

        /// <summary>
        /// Convert the <see cref="byte"/> to its ordinal <see cref="string"/> representation, ie 1st, 2nd, 3rd, etc.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The ordinal representation of the number.</returns>
        public static string ToOrdinalString(this byte self) => ToOrdinalString(self.ToString());

        /// <summary>
        /// Convert the <see cref="sbyte"/> to its ordinal <see cref="string"/> representation, ie 1st, 2nd, 3rd, etc.
        /// Negative numbers receive the same suffixes as the positive counterpart.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The ordinal representation of the number.</returns>
        public static string ToOrdinalString(this sbyte self) => ToOrdinalString(self.ToString());

        /// <summary>
        /// Convert the <see cref="short"/> to its ordinal <see cref="string"/> representation, ie 1st, 2nd, 3rd, etc.
        /// Negative numbers receive the same suffixes as the positive counterpart.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The ordinal representation of the number.</returns>
        public static string ToOrdinalString(this short self) => ToOrdinalString(self.ToString());

        /// <summary>
        /// Convert the <see cref="ushort"/> to its ordinal <see cref="string"/> representation, ie 1st, 2nd, 3rd, etc.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The ordinal representation of the number.</returns>
        public static string ToOrdinalString(this ushort self) => ToOrdinalString(self.ToString());

        /// <summary>
        /// Convert the <see cref="int"/> to its ordinal <see cref="string"/> representation, ie 1st, 2nd, 3rd, etc.
        /// Negative numbers receive the same suffixes as the positive counterpart.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The ordinal representation of the number.</returns>
        public static string ToOrdinalString(this int self) => ToOrdinalString(self.ToString());

        /// <summary>
        /// Convert the <see cref="uint"/> to its ordinal <see cref="string"/> representation, ie 1st, 2nd, 3rd, etc.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The ordinal representation of the number.</returns>
        public static string ToOrdinalString(this uint self) => ToOrdinalString(self.ToString());

        /// <summary>
        /// Convert the <see cref="long"/> to its ordinal <see cref="string"/> representation, ie 1st, 2nd, 3rd, etc.
        /// Negative numbers receive the same suffixes as the positive counterpart.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The ordinal representation of the number.</returns>
        public static string ToOrdinalString(this long self) => ToOrdinalString(self.ToString());

        /// <summary>
        /// Convert the <see cref="ulong"/> to its ordinal <see cref="string"/> representation, ie 1st, 2nd, 3rd, etc.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The ordinal representation of the number.</returns>
        public static string ToOrdinalString(this ulong self) => ToOrdinalString(self.ToString());

        /// <summary>
        /// Helper method to append ordinal suffixes to string representation of integral numbers.
        /// A null or empty string will be returned unmodified. Only the last two characters are
        /// checked to determine the applicable suffix and no further validation is performed. The
        /// calling code should ensure any further validation on the input before calling the method.
        /// </summary>
        /// <param name="number">Instance of extended class.</param>
        /// <returns>The specified string with the applicable suffix appended.</returns>
        public static string ToOrdinalString(string number)
        {
            if (number.IsEmpty()) { return number; }

            string suffix
                = number.Length > 1 && number[^2] == '1' ? "th"
                : number[^1] == '1' ? "st"
                : number[^1] == '2' ? "nd"
                : number[^1] == '3' ? "rd"
                : "th";
#if NETSTANDARD2_0
            return string.Concat(number, suffix);
            //int last = number.Length - 1;
            //if (number[last] == '1') { return number + (number.IsPrevChar(last, '1') ? "th" : "st"); }
            //else if (number[last] == '2') { return number + (number.IsPrevChar(last, '1') ? "th" : "nd"); }
            //else if (number[last] == '3') { return number + (number.IsPrevChar(last, '1') ? "th" : "rd"); }
            //else { return number + "th"; }
#else
            return string.Create(number.Length + 2, (number, suffix), (chars, state) =>
            {
                chars.CopyFrom(state.number);
                chars[^1] = state.suffix[^1];
                chars[^2] = state.suffix[^2];
            });
#endif
        }

        #endregion

        #region ToSignedString

        /// <summary>
        /// Converts the <see cref="sbyte"/> to a signed <see cref="string"/> representation.
        /// Positive numbers are prefixed with <c>+</c>, negative numbers with <c>-</c>,
        /// and zero is left without a prefix.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The signed representation of the number.</returns>
        public static string ToSignedString(this sbyte self) => string.Format("{0:+0;-0;0}", self);

        /// <summary>
        /// Converts the <see cref="short"/> to a signed <see cref="string"/> representation.
        /// Positive numbers are prefixed with <c>+</c>, negative numbers with <c>-</c>,
        /// and zero is left without a prefix.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The signed representation of the number.</returns>
        public static string ToSignedString(this short self) => string.Format("{0:+0;-0;0}", self);

        /// <summary>
        /// Converts the <see cref="int"/> to a signed <see cref="string"/> representation.
        /// Positive numbers are prefixed with <c>+</c>, negative numbers with <c>-</c>,
        /// and zero is left without a prefix.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The signed representation of the number.</returns>
        public static string ToSignedString(this int self) => string.Format("{0:+0;-0;0}", self);

        /// <summary>
        /// Converts the <see cref="long"/> to a signed <see cref="string"/> representation.
        /// Positive numbers are prefixed with <c>+</c>, negative numbers with <c>-</c>,
        /// and zero is left without a prefix.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The signed representation of the number.</returns>
        public static string ToSignedString(this long self) => string.Format("{0:+0;-0;0}", self);

        /// <summary>
        /// Converts the <see cref="float"/> to a signed <see cref="string"/> representation.
        /// Positive numbers are prefixed with <c>+</c>, negative numbers with <c>-</c>,
        /// and zero is left without a prefix.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The signed representation of the number.</returns>
        public static string ToSignedString(this float self) => string.Format("{0:+0;-0;0}", self);

        /// <summary>
        /// Converts the <see cref="double"/> to a signed <see cref="string"/> representation.
        /// Positive numbers are prefixed with <c>+</c>, negative numbers with <c>-</c>,
        /// and zero is left without a prefix.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The signed representation of the number.</returns>
        public static string ToSignedString(this double self) => string.Format("{0:+0;-0;0}", self);

        /// <summary>
        /// Converts the <see cref="decimal"/> to a signed <see cref="string"/> representation.
        /// Positive numbers are prefixed with <c>+</c>, negative numbers with <c>-</c>,
        /// and zero is left without a prefix.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The signed representation of the number.</returns>
        public static string ToSignedString(this decimal self) => string.Format("{0:+0;-0;0}", self);

        #endregion

        #region ToTitleCase

        /// <summary>
        /// Converts the specified <see cref="string"/> to title case (except for words that are entirely
        /// in uppercase, which are considered to be acronyms) using the specified <see cref="CultureInfo"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="culture">Culture to use for title case conversion.</param>
        /// <returns>The specified string converted to title case.</returns>
        public static string ToTitleCase(this string self, CultureInfo culture) => culture.TextInfo.ToTitleCase(self);

        /// <summary>
        /// Converts the specified <see cref="string"/> to title case (except for words that are entirely
        /// in uppercase, which are considered to be acronyms) using <see cref="CultureInfo.CurrentCulture"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The specified string converted to title case.</returns>
        public static string ToTitleCase(this string self) => CultureInfo.CurrentCulture.TextInfo.ToTitleCase(self);

        /// <summary>
        /// Converts the specified <see cref="string"/> to title case (except for words that are entirely
        /// in uppercase, which are considered to be acronyms) using <see cref="CultureInfo.InvariantCulture"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The specified string converted to title case.</returns>
        public static string ToTitleCaseInvariant(this string self) => CultureInfo.InvariantCulture.TextInfo.ToTitleCase(self);

        #endregion

        #region ToUpper Methods

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with the character at <paramref name="index"/>
        /// converted to uppercase.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="index">The zero-based starting character position of a character in this instance.</param>
        /// <returns>A string with the specified character converted to uppercase.</returns>
        public static string ToUpperChar(this string self, int index) => ToUpper(self, index, 1);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with the substring starting at <paramref name="startIndex"/>
        /// to the end of the string converted to uppercase.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="startIndex">The zero-based starting character position of a substring in this instance.</param>
        /// <returns>A string with the specified substring converted to uppercase.</returns>
        public static string ToUpper(this string self, int startIndex) => ToUpper(self, startIndex, self.Length - startIndex);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with the substring starting at <paramref name="startIndex"/>
        /// including <paramref name="count"/> number of character converted to uppercase.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="startIndex">The zero-based starting character position of a substring in this instance.</param>
        /// <param name="count">The number of characters in the substring.</param>
        /// <returns>A string with the specified substring converted to uppercase.</returns>
        public static string ToUpper(this string self, int startIndex, int count)
        {
            Throw.If.Self.IsNull(self);
            if (string.IsNullOrWhiteSpace(self)) { return self; }
#if NETSTANDARD2_0
            char[] chars = self.ToCharArray();
            for (int index = startIndex; index < startIndex + count; index++)
            { chars[index] = char.ToUpper(chars[index]); }
            return new string(chars);
#else
            int endIndex = startIndex + count;
            return string.Create(self.Length, (self, startIndex, endIndex), (chars, state) =>
            {
                for (int index = 0; index < chars.Length; index++)
                {
                    if (index >= state.startIndex && index < state.endIndex)
                    { chars[index] = char.ToUpper(state.self[index]); }
                    else
                    { chars[index] = state.self[index]; }
                }
            });
#endif
        }

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with the character at <paramref name="index"/>
        /// converted to uppercase using the casing rules of the invariant culture.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="index">The zero-based starting character position of a character in this instance.</param>
        /// <returns>A string with the specified character converted to uppercase.</returns>
        public static string ToUpperCharInvariant(this string self, int index) => ToUpperInvariant(self, index, 1);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with the substring starting at <paramref name="startIndex"/>
        /// to the end of the string converted to uppercase using the casing rules of the invariant culture.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="startIndex">The zero-based starting character position of a substring in this instance.</param>
        /// <returns>A string with the specified substring converted to uppercase.</returns>
        public static string ToUpperInvariant(this string self, int startIndex) => ToUpperInvariant(self, startIndex, self.Length - startIndex);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with the substring starting at <paramref name="startIndex"/>
        /// including <paramref name="count"/> number of character converted to uppercase using the casing rules
        /// of the invariant culture.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="startIndex">The zero-based starting character position of a substring in this instance.</param>
        /// <param name="count">The number of characters in the substring.</param>
        /// <returns>A string with the specified substring converted to uppercase.</returns>
        public static string ToUpperInvariant(this string self, int startIndex, int count)
        {
            Throw.If.Self.IsNull(self);
            if (string.IsNullOrWhiteSpace(self)) { return self; }
#if NETSTANDARD2_0
            char[] chars = self.ToCharArray();
            for (int index = startIndex; index < startIndex + count; index++)
            { chars[index] = char.ToUpperInvariant(chars[index]); }
            return new string(chars);
#else
            int endIndex = startIndex + count;
            return string.Create(self.Length, (self, startIndex, endIndex), (chars, state) =>
            {
                for (int index = 0; index < chars.Length; index++)
                {
                    if (index >= state.startIndex && index < state.endIndex)
                    { chars[index] = char.ToUpperInvariant(state.self[index]); }
                    else
                    { chars[index] = state.self[index]; }
                }
            });
#endif
        }

        #endregion

    }
}
