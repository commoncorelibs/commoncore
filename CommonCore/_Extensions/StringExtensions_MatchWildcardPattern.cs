﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
//
// The code in this module was created by someone else and has, most likely, been modified for
// inclusion in this library. At a minimum, the code has been modified to follow the library's 
// standards and conventions.
//
// Original Source: http://hasullivan.com/2016/04/13/fast-wildcard-matching-in-c-sharp/
// The original licensing information is included below. If the license differs from that of this library,
// then the work included in this file is considered to be dual licensed under both the above and below.
//
// MIT License
//
// Copyright(c) [2016]
// [H.A. Sullivan]
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
#endregion

using System.Collections.Generic;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="StringExtensions_MatchWildcardPattern"/> <c>static</c> <c>class</c> provides
    /// extension methods related to <see cref="string"/> objects.
    /// 
    /// These methods allow basic wildcard matching against <see cref="string"/>
    /// content.
    /// </summary>
    public static class StringExtensions_MatchWildcardPattern
    {
        /// <summary>
        /// Determines if this <see cref="string"/> instance matches the <paramref name="pattern"/>.
        /// The pattern allows two filter characters, the question mark (?) and the star (*).
        /// The ? matches one and only one character. The * matches zero or more characters.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="pattern">A standard wildcard filter string.</param>
        /// <returns><c>true</c> if the string matches the pattern; otherwise <c>false</c>.</returns>
        /// <remarks>
        /// Many changes were made to the original form of the method, but two changes stand out.
        /// First, the target string and the pattern string are used directly for char comparisons,
        /// instead of through allocated char arrays. Second, the code from match cases 2 and 3 have
        /// been combined to avoid over duplication. Lastly, execution flow and variable scopes have
        /// received some clean up.
        /// </remarks>
        public static bool IsMatchWildcardPattern(this string self, string pattern)
        {
            if (pattern.IsEmpty()) { return false; }
            if (self == null) { self = string.Empty; }

            // Set which case will be used: 0 = no wildcards, 1 = only ?, 2 = only *, 3 = both ? and *
            byte matchCase = 0;
            if (pattern.IndexOf('?') > -1) { matchCase += 1; }
            if (pattern.IndexOf('*') > -1) { matchCase += 2; }

            if (matchCase == 0) { return self == pattern; }
            else if(matchCase == 1)
            {
                if (self.Length != pattern.Length) { return false; }
                for (int i = 0; i < self.Length; i++)
                { if (pattern[i] != '?' && pattern[i] != self[i]) { return false; } }
                return true;
            }
            else
            {
                // Search for matches until first *
                int headIndex = 0;
                for (int index = 0; index < pattern.Length; index++)
                {
                    if (pattern[index] == '*') { headIndex = index; break; }
                    else { if (pattern[index] != self[index] && (matchCase != 3 || pattern[index] != '?')) { return false; } }
                }

                // Search Tail for matches until first *
                int tailIndex = 0;
                for (int index = 0; index < pattern.Length; index++)
                {
                    int patternIndex = pattern.Length - 1 - index;
                    if (pattern[patternIndex] == '*') { tailIndex = index; break; }
                    else { if (pattern[patternIndex] != self[self.Length - 1 - index] && (matchCase != 3 || pattern[patternIndex] != '?')) { return false; } }
                }

                // Create a reverse word and pattern for searching in reverse.
                // The reversed word and pattern do not include already checked chars.
                var revWord = new char[self.Length - headIndex - tailIndex];
                for (int index = 0; index < revWord.Length; index++)
                { revWord[index] = self[self.Length - (index + 1) - tailIndex]; }

                var revPattern = new char[pattern.Length - headIndex - tailIndex];
                for (int index = 0; index < revPattern.Length; index++)
                { revPattern[index] = pattern[pattern.Length - (index + 1) - tailIndex]; }

                // Cut up the pattern into separate sub-patterns, exclude * as they are not longer needed
                var revPatterns = new List<char[]>();
                for (int index = 0, patternIndex = 0; index < revPattern.Length; index++)
                {
                    if (revPattern[index] == '*')
                    {
                        if (index - patternIndex > 0)
                        {
                            var subPattern = new char[index - patternIndex];
                            for (int subIndex = 0; subIndex < subPattern.Length; subIndex++)
                            {
                                subPattern[subIndex] = revPattern[patternIndex + subIndex];
                            }
                            revPatterns.Add(subPattern);
                        }
                        patternIndex = index + 1;
                    }
                }

                // Search for the patterns
                for (int index = 0, revIndex = 0; index < revPatterns.Count; index++)
                {
                    for (int subIndex = 0; subIndex < revPatterns[index].Length; subIndex++)
                    {
                        if ((revPatterns[index].Length - 1 - subIndex) > (revWord.Length - 1 - revIndex)) { return false; }

                        if (revPatterns[index][subIndex] != revWord[revIndex + subIndex] && (matchCase != 3 || revPatterns[index][subIndex] != '?'))
                        {
                            revIndex += 1;
                            subIndex = -1;
                        }
                        else
                        {
                            if (subIndex == revPatterns[index].Length - 1)
                            {
                                revIndex += revPatterns[index].Length;
                            }
                        }
                    }
                }
                return true;
            }
        }
    }
}
