﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="StringExtensions_Comparing"/> <c>static</c> <c>class</c> provides
    /// extension methods related to <see cref="string"/> objects.
    /// 
    /// These methods focus on making comparisons to the <see cref="string"/> and its content.
    /// Comparisons are done on the <see cref="char"/> level to avoid any unneeded <see cref="string"/>
    /// allocations. Due to this reliance on <see cref="char"/> comparisons, case-insensitivity can not
    /// be handled by any in-built mechanism. To make case-insensitive comparisons possible, the rule is
    /// that two <see cref="char"/> objects are case-insensitively equal if either their upper cased
    /// or lower cased forms are equivalent.
    /// </summary>
    public static class StringExtensions_Comparing
    {

        #region EndsWith Methods

#if NETSTANDARD2_0
        /// <summary>
        /// Determines if this <see cref="string"/> instance ends with the specified <paramref name="value"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The character to compare.</param>
        /// <returns><c>true</c> if the string ends with the specified <paramref name="value"/>; otherwise <c>false</c>.</returns>
        public static bool EndsWith(this string self, char value)
        {
            Throw.If.Self.IsNull(self);
            return self.Length > 0 && self[^1] == value;
        }
#endif

        #endregion

        #region EndsWithAny Methods

        /// <summary>
        /// Determines if this <see cref="string"/> instance ends with one of the specified <paramref name="values"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">An array of strings to compare.</param>
        /// <returns><c>true</c> if the string ends with one of the specified values; otherwise <c>false</c>.</returns>
        public static bool EndsWithAny(this string self, params string[] values)
        { foreach (var value in values) { if (self.EndsWith(value)) { return true; } } return false; }

        /// <summary>
        /// Determines if this <see cref="string"/> instance ends with one of the specified <paramref name="values"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">An enumerable of strings to compare.</param>
        /// <returns><c>true</c> if the string ends with one of the specified values; otherwise <c>false</c>.</returns>
        public static bool EndsWithAny(this string self, IEnumerable<string> values)
        { foreach (var value in values) { if (self.EndsWith(value)) { return true; } } return false; }

        #endregion

        #region HasIndex Methods

        /// <summary>
        /// Determines if the specified <paramref name="index"/> is valid for this <see cref="string"/> instance.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="index">Index to validate.</param>
        /// <returns><c>true</c> if the index is greater than <c>-1</c> and less than the string length.</returns>
        public static bool HasIndex(this string self, int index)
            => (self != null && index > -1 && index < self.Length);

        #endregion

        #region Equals Methods

        /// <summary>
        /// Determines if this <see cref="string"/> instance equates to <paramref name="value"/>
        /// according to a <see cref="StringComparison.InvariantCultureIgnoreCase"/> comparison.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">Value to compare with the instance.</param>
        /// <returns><c>true</c> if the strings are equivalent according to a case-insensitive invariant culture comparison; otherwise <c>false</c>.</returns>
        /// <remarks>
        /// First we check for reference equality, which will catch the case where self and value
        /// are both null. If they are equivalent references, then equals is conditionally called
        /// from self; otherwise false is returned.
        /// </remarks>
        public static bool EqualsIgnoreCase(this string self, string value)
            => object.ReferenceEquals(self, value) || (self?.Equals(value, StringComparison.InvariantCultureIgnoreCase) ?? false);

        #endregion

        #region IsChar Methods

        /// <summary>
        /// Determines if this <see cref="string"/> instance contains a <see cref="char"/> equivalent to
        /// <paramref name="value"/> at the specified <paramref name="index"/>. If the <see cref="string"/>
        /// is <c>null</c> or <paramref name="index"/> is out of range, then the method returns <c>false</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="index">Index to compare at.</param>
        /// <param name="value">Value to compare to.</param>
        /// <returns><c>true</c> if the string has a character at the index equivalent to the value; otherwise <c>false</c>.</returns>
        public static bool IsChar(this string self, int index, char value)
            => (self == null || index < 0 || index >= self.Length ? false : self[index] == value);

        /// <summary>
        /// Determines if this <see cref="string"/> instance contains a <see cref="char"/> equivalent to
        /// <paramref name="prefix"/> at the position before <paramref name="index"/>. If the <see cref="string"/>
        /// is <c>null</c> or decremented <paramref name="index"/> is out of range, then the method returns <c>false</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="index">Index to compare at.</param>
        /// <param name="prefix">Value to compare to.</param>
        /// <returns><c>true</c> if the string has a character at <c>index - 1</c> equivalent to the prefix; otherwise <c>false</c>.</returns>
        public static bool IsPrevChar(this string self, int index, char prefix)
            => IsChar(self, index - 1, prefix);

        /// <summary>
        /// Determines if this <see cref="string"/> instance contains a <see cref="char"/> equivalent to
        /// <paramref name="suffix"/> at the position after <paramref name="index"/>. If the <see cref="string"/>
        /// is <c>null</c> or incremented <paramref name="index"/> is out of range, then the method returns <c>false</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="index">Index to compare at.</param>
        /// <param name="suffix">Value to compare to.</param>
        /// <returns><c>true</c> if the string has a character at <c>index + 1</c> equivalent to the suffix; otherwise <c>false</c>.</returns>
        public static bool IsNextChar(this string self, int index, char suffix)
            => IsChar(self, index + 1, suffix);

        #endregion

        #region IsEmpty Methods

        /// <summary>
        /// Determines if this <see cref="string"/> instance is <c>null</c> or has
        /// a length of zero.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the string is <c>null</c> or has length zero; otherwise <c>false</c>.</returns>
        public static bool IsEmpty(this string self) => self == null || self.Length == 0;

        /// <summary>
        /// Determines if this <see cref="string"/> instance is not <c>null</c> and has
        /// a length greater than zero.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the string is not <c>null</c> and has length greater than zero; otherwise <c>false</c>.</returns>
        public static bool IsNotEmpty(this string self) => self != null && self.Length > 0;

        /// <summary>
        /// Determines if this <see cref="string"/> instance is <c>null</c>, has
        /// a length of zero, or is composed entirely of white space.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the string is <c>null</c>, has length zero, or is entirely white space; otherwise <c>false</c>.</returns>
        public static bool IsEmptyOrWhitespace(this string self) => string.IsNullOrWhiteSpace(self);

        /// <summary>
        /// Determines if this <see cref="string"/> instance is not <c>null</c>, has
        /// a length greater than zero, and is not composed entirely of white space.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the string is not <c>null</c>, has length greater than zero, and is not entirely white space; otherwise <c>false</c>.</returns>
        public static bool IsNotEmptyOrWhitespace(this string self) => !string.IsNullOrWhiteSpace(self);

        #endregion

        #region IsMatch Methods

        /// <inheritdoc cref="IsMatch(string, ReadOnlySpan{char}, int)"/>
        public static bool IsMatch(this string self, string value, int startIndex)
            => self.IsMatch(value.AsSpan(), startIndex);

        /// <inheritdoc cref="IsMatch(string, ReadOnlySpan{char}, int)"/>
        public static bool IsMatch(this string self, char[] value, int startIndex)
            => self.IsMatch(value.AsSpan(), startIndex);

        /// <inheritdoc cref="IsMatch(string, ReadOnlySpan{char}, int)"/>
        public static bool IsMatch(this string self, Span<char> value, int startIndex)
            => self.IsMatch((ReadOnlySpan<char>)value, startIndex);

        /// <summary>
        /// Determines if this <see cref="string"/> instance contains <paramref name="value"/>.
        /// Comparison will begin at <paramref name="startIndex"/> in the specified string.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The value to compare.</param>
        /// <param name="startIndex">The index in begin comparison against the value.</param>
        /// <returns><c>true</c> if the string contains the value beginning at the start index; otherwise <c>false</c>.</returns>
        public static bool IsMatch(this string self, ReadOnlySpan<char> value, int startIndex)
        {
            if (IsEmpty(self) || value.IsEmpty()) { return false; }
            else if (value.Length > self.Length) { return false; }
            else if (startIndex < 0 || startIndex > self.Length - value.Length) { return false; }

            for (int index = 0; index < value.Length; index++)
            { if (self[index + startIndex] != value[index]) { return false; } }
            return true;
        }

        #endregion

        #region StartsWithAny Methods

        /// <summary>
        /// Determines if this <see cref="string"/> instance starts with one of the specified <paramref name="values"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">An array of strings to compare.</param>
        /// <returns><c>true</c> if the string starts with one of the specified values; otherwise <c>false</c>.</returns>
        public static bool StartsWithAny(this string self, params string[] values)
        { foreach (var value in values) { if (self.StartsWith(value)) { return true; } } return false; }

        /// <summary>
        /// Determines if this <see cref="string"/> instance starts with one of the specified <paramref name="values"/>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">An enumerable of strings to compare.</param>
        /// <returns><c>true</c> if the string starts with one of the specified values; otherwise <c>false</c>.</returns>
        public static bool StartsWithAny(this string self, IEnumerable<string> values)
        { foreach (var value in values) { if (self.StartsWith(value)) { return true; } } return false; }

        #endregion

    }
}
