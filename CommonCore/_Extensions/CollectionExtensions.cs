﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CommonCore
{
    /// <summary>
    /// Static extension class for collection types, such as <see cref="IEnumerable"/>,
    /// <see cref="ICollection"/>, <see cref="IList"/>, etc, and their generic forms.
    /// </summary>
    /// <remarks>
    /// Following the principle of least knowledge, extensions should only extend the
    /// lowest interface to achieve their goal. At a minimum, that will be <see cref="IEnumerable"/>
    /// and probably <see cref="IEnumerable{T}"/>. When a solid count of elements allows
    /// more efficient code, the next lowest interface and its generic, <see cref="ICollection"/>
    /// and <see cref="ICollection{T}"/>, would be appropriate. At the next level, it can
    /// be helpful to have direct index access to elements, which would call for <see cref="IList"/>
    /// and <see cref="IList{T}"/>. Generic array types should generally not be extended
    /// directly, instead falling back to the closest above interface.
    /// </remarks>
    public static class CollectionExtensions
    {

        #region Contains Methods

        /// <summary>
        /// Determines if this <see cref="IEnumerable"/> instance contains an element
        /// equating to <paramref name="value"/> according to a
        /// <see cref="StringComparison.InvariantCultureIgnoreCase"/> comparison.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The string to compare against the sequence elements.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns><c>true</c> if the sequence contains an element equivalent to value according to a case-insensitive invariant culture comparison; otherwise <c>false</c>.</returns>
        public static bool ContainsIgnoreCase(this IEnumerable<string> self, string value)
        {
            Throw.If.Self.IsNull(self);
            foreach (var element in self) { if (element.EqualsIgnoreCase(value)) { return true; } }
            return false;
        }

        #endregion

        #region IsEmpty Methods

        /// <summary>
        /// Determines if this <see cref="ICollection"/> instance is <c>null</c> or has a count of zero.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the sequence is <c>null</c> or has count zero; otherwise <c>false</c>.</returns>
        public static bool IsEmpty(this ICollection self) => (self == null || self.Count == 0);

        /// <summary>
        /// Determines if this <see cref="IEnumerable"/> instance is <c>null</c> or has no elements.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the sequence is <c>null</c> or has no elements; otherwise <c>false</c>.</returns>
        public static bool IsEmpty(this IEnumerable self)
        {
            if (self == null) { return true; }
            foreach (var _ in self) { return false; }
            return true;
        }

        /// <summary>
        /// Determines if this <see cref="ICollection"/> instance is not <c>null</c> and has a count greater than zero.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the sequence is not <c>null</c> and has count greater than zero; otherwise <c>false</c>.</returns>
        public static bool IsNotEmpty(this ICollection self) => (self != null && self.Count > 0);

        /// <summary>
        /// Determines if this <see cref="IEnumerable"/> instance is not <c>null</c> and has at least one element.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the sequence is not <c>null</c> and has at least one element; otherwise <c>false</c>.</returns>
        public static bool IsNotEmpty(this IEnumerable self)
        {
            if (self == null) { return false; }
            foreach (var _ in self) { return true; }
            return false;
        }

        #endregion

        #region Join Methods

        /// <summary>
        /// Concatenates this <see cref="IEnumerable"/> to a <see cref="string"/> using the
        /// specified <paramref name="separator"/>.
        /// Example 1: <c>{ }</c> results in <c>""</c>.
        /// Example 2: <c>{ "a" }</c> results in <c>"a"</c>.
        /// Example 3: <c>{ "a", "b" }</c> results in <c>"a, b"</c>.
        /// Example 4: <c>{ "a", "b", "c" }</c> results in <c>"a, b, c"</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="separator">The string to use as a separator when the sequence has two or more items.</param>
        /// <param name="converter">Optional delegate to transform an element to a string.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A string that consists of the sequence elements delimited by the separator.</returns>
        public static string Join(this IEnumerable self, string separator, Func<object, string> converter = null)
            => Join(self.Cast<object>(), separator, converter);

        /// <summary>
        /// Concatenates this <see cref="IEnumerable"/> to a <see cref="string"/> using the
        /// specified <paramref name="separator"/>.
        /// Example 1: <c>{ }</c> results in <c>""</c>.
        /// Example 2: <c>{ "a" }</c> results in <c>"a"</c>.
        /// Example 3: <c>{ "a", "b" }</c> results in <c>"a, b"</c>.
        /// Example 4: <c>{ "a", "b", "c" }</c> results in <c>"a, b, c"</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="separator">The string to use as a separator when the sequence has two or more items.</param>
        /// <param name="startIndex">The first element of the sequence to use.</param>
        /// <param name="converter">Optional delegate to transform an element to a string.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns>A string that consists of the sequence elements delimited by the separator.</returns>
        public static string Join(this IEnumerable self, string separator, int startIndex, Func<object, string> converter = null)
            => Join(self.Cast<object>(), separator, startIndex, converter);

        /// <summary>
        /// Concatenates this <see cref="IEnumerable"/> to a <see cref="string"/> using the
        /// specified <paramref name="separator"/>.
        /// Example 1: <c>{ }</c> results in <c>""</c>.
        /// Example 2: <c>{ "a" }</c> results in <c>"a"</c>.
        /// Example 3: <c>{ "a", "b" }</c> results in <c>"a, b"</c>.
        /// Example 4: <c>{ "a", "b", "c" }</c> results in <c>"a, b, c"</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="separator">The string to use as a separator when the sequence has two or more items.</param>
        /// <param name="startIndex">The first element of the sequence to use.</param>
        /// <param name="maxCount">The maximum number of elements of the sequence to use.</param>
        /// <param name="converter">Optional delegate to transform an element to a string.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>A string that consists of the sequence elements delimited by the separator.</returns>
        public static string Join(this IEnumerable self, string separator, int startIndex, int maxCount, Func<object, string> converter = null)
            => Join(self.Cast<object>(), separator, startIndex, maxCount, converter);

        /// <summary>
        /// Concatenates this <see cref="IEnumerable{TItem}"/> to a <see cref="string"/> using the
        /// specified <paramref name="separator"/>.
        /// Example 1: <c>{ }</c> results in <c>""</c>.
        /// Example 2: <c>{ "a" }</c> results in <c>"a"</c>.
        /// Example 3: <c>{ "a", "b" }</c> results in <c>"a, b"</c>.
        /// Example 4: <c>{ "a", "b", "c" }</c> results in <c>"a, b, c"</c>.
        /// </summary>
        /// <typeparam name="TItem">Type of items in the sequence.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="separator">The string to use as a separator when the sequence has two or more items.</param>
        /// <param name="converter">Optional delegate to transform an element to a string.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A string that consists of the sequence elements delimited by the separator.</returns>
        public static string Join<TItem>(this IEnumerable<TItem> self, string separator, Func<TItem, string> converter = null)
        {
            Throw.If.Self.IsNull(self);
            if (converter == null) { return string.Join(separator, self); }
            else { return string.Join(separator, self.Select(item => converter(item))); }
        }

        /// <summary>
        /// Concatenates this <see cref="IEnumerable{TItem}"/> to a <see cref="string"/> using the
        /// specified <paramref name="separator"/>.
        /// Example 1: <c>{ }</c> results in <c>""</c>.
        /// Example 2: <c>{ "a" }</c> results in <c>"a"</c>.
        /// Example 3: <c>{ "a", "b" }</c> results in <c>"a, b"</c>.
        /// Example 4: <c>{ "a", "b", "c" }</c> results in <c>"a, b, c"</c>.
        /// </summary>
        /// <typeparam name="TItem">Type of items in the sequence.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="separator">The string to use as a separator when the sequence has two or more items.</param>
        /// <param name="startIndex">The first element of the sequence to use.</param>
        /// <param name="converter">Optional delegate to transform an element to a string.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>.</exception>
        /// <returns>A string that consists of the sequence elements delimited by the separator.</returns>
        public static string Join<TItem>(this IEnumerable<TItem> self, string separator, int startIndex, Func<TItem, string> converter = null)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsLesser(nameof(startIndex), startIndex, 0);

            var enumerable = self;
            if (startIndex > 0) { enumerable = enumerable.Skip(startIndex); }
            if (converter == null) { return string.Join(separator, enumerable); }
            else { return string.Join(separator, enumerable.Select(item => converter(item))); }
        }

        /// <summary>
        /// Concatenates this <see cref="IEnumerable{TItem}"/> to a <see cref="string"/> using the
        /// specified <paramref name="separator"/>.
        /// Example 1: <c>{ }</c> results in <c>""</c>.
        /// Example 2: <c>{ "a" }</c> results in <c>"a"</c>.
        /// Example 3: <c>{ "a", "b" }</c> results in <c>"a, b"</c>.
        /// Example 4: <c>{ "a", "b", "c" }</c> results in <c>"a, b, c"</c>.
        /// </summary>
        /// <typeparam name="TItem">Type of items in the sequence.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="separator">The string to use as a separator when the sequence has two or more items.</param>
        /// <param name="startIndex">The first element of the sequence to use.</param>
        /// <param name="maxCount">The maximum number of elements of the sequence to use.</param>
        /// <param name="converter">Optional delegate to transform an element to a string.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>A string that consists of the sequence elements delimited by the separator.</returns>
        public static string Join<TItem>(this IEnumerable<TItem> self, string separator, int startIndex, int maxCount, Func<TItem, string> converter = null)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsLesser(nameof(startIndex), startIndex, 0);
            Throw.If.Arg.IsLesser(nameof(maxCount), maxCount, -1);
            if (maxCount == 0) { return string.Empty; }

            var enumerable = self;
            if (startIndex > 0) { enumerable = enumerable.Skip(startIndex); }
            if (maxCount > 0) { enumerable = enumerable.Take(maxCount); }
            if (converter == null) { return string.Join(separator, enumerable); }
            else { return string.Join(separator, enumerable.Select(item => converter(item))); }
        }

        #endregion

        #region Subset Methods

        /// <summary>
        /// Returns a subset of this <see cref="IEnumerable"/> confined to the specified
        /// <paramref name="startIndex"/> and <paramref name="maxCount"/> values.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="startIndex">The element start the subset.</param>
        /// <param name="maxCount">The maximum number of elements in the subset or <c>-1</c> for all available elements.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>A subset of this enumerable confined to the specified <paramref name="startIndex"/> and <paramref name="maxCount"/> values.</returns>
        public static IEnumerable Subset(this IEnumerable self, int startIndex, int maxCount = -1)
            => Subset(self.Cast<object>(), startIndex, maxCount);

        /// <summary>
        /// Returns a subset of this <see cref="IEnumerable{TItem}"/> confined to the specified
        /// <paramref name="startIndex"/> and <paramref name="maxCount"/> values.
        /// </summary>
        /// <typeparam name="TItem">Type of items in the sequence.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="startIndex">The element start the subset.</param>
        /// <param name="maxCount">The maximum number of elements in the subset or <c>-1</c> for all available elements.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="startIndex"/> is less than <c>0</c>; or <paramref name="maxCount"/> is less than <c>-1</c>.</exception>
        /// <returns>A subset of this enumerable confined to the specified <paramref name="startIndex"/> and <paramref name="maxCount"/> values.</returns>
        public static IEnumerable<TItem> Subset<TItem>(this IEnumerable<TItem> self, int startIndex, int maxCount = -1)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsLesser(nameof(startIndex), startIndex, 0);
            Throw.If.Arg.IsLesser(nameof(maxCount), maxCount, -1);
            if (maxCount == 0) { return Enumerable.Empty<TItem>(); }

            var enumerable = self;
            if (startIndex > 0) { enumerable = enumerable.Skip(startIndex); }
            if (maxCount > 0) { enumerable = enumerable.Take(maxCount); }
            return enumerable;
        }

        #endregion

        #region ToListString Methods

        /// <summary>
        /// Converts this <see cref="IList{TItem}"/> to a human-readable <see cref="string"/> list using the
        /// specified <paramref name="delimiter"/> and <paramref name="conjunction"/>.
        /// Example 1: <c>{ "a" }</c> results in <c>"a"</c>.
        /// Example 2: <c>{ "a", "b" }</c> results in <c>"a and b"</c>.
        /// Example 3: <c>{ "a", "b", "c" }</c> results in <c>"a, b, and c"</c>.
        /// </summary>
        /// <typeparam name="TItem">Type of items in the enumerable.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="conjunction">The conjunction used before the last item in lists of two or more items.</param>
        /// <param name="delimiter">The delimiter used after each item other than the last in lists of three or more items.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A human-readable string composed of the specified enumerable items, conjunction, and delimiter.</returns>
        public static string ToListString<TItem>(this IList<TItem> self, string conjunction = "and", string delimiter = ", ")
        {
            Throw.If.Self.IsNull(self);
            if (self.Count == 0) { return string.Empty; }
            else if (self.Count == 1) { return self[0].ToString(); }
            else if (self.Count == 2) { return $"{self[0]} {conjunction} {self[1]}"; }
            else { return $"{string.Join(delimiter, self.Take(self.Count - 1))}{delimiter}{conjunction} {self[^1]}"; }
        }

        /// <summary>
        /// Converts this <see cref="ICollection{TItem}"/> to a human-readable <see cref="string"/> list using the
        /// specified <paramref name="delimiter"/> and <paramref name="conjunction"/>.
        /// Example 1: <c>{ "a" }</c> results in <c>"a"</c>.
        /// Example 2: <c>{ "a", "b" }</c> results in <c>"a and b"</c>.
        /// Example 3: <c>{ "a", "b", "c" }</c> results in <c>"a, b, and c"</c>.
        /// </summary>
        /// <typeparam name="TItem">Type of items in the enumerable.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="conjunction">The conjunction used before the last item in lists of two or more items.</param>
        /// <param name="delimiter">The delimiter used after each item other than the last in lists of three or more items.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A human-readable string composed of the specified enumerable items, conjunction, and delimiter.</returns>
        public static string ToListString<TItem>(this ICollection<TItem> self, string conjunction = "and", string delimiter = ", ")
        {
            Throw.If.Self.IsNull(self);
            if (self.Count == 0) { return string.Empty; }
            else if (self.Count == 1) { return self.Single().ToString(); }
            else if (self.Count == 2) { return $"{self.First()} {conjunction} {self.Last()}"; }
            else { return $"{string.Join(delimiter, self.Take(self.Count - 1))}{delimiter}{conjunction} {self.Last()}"; }
        }

        /// <summary>
        /// Converts this <see cref="IEnumerable{TItem}"/> to a human-readable <see cref="string"/> list using the
        /// specified <paramref name="delimiter"/> and <paramref name="conjunction"/>.
        /// Example 1: <c>{ "a" }</c> results in <c>"a"</c>.
        /// Example 2: <c>{ "a", "b" }</c> results in <c>"a and b"</c>.
        /// Example 3: <c>{ "a", "b", "c" }</c> results in <c>"a, b, and c"</c>.
        /// </summary>
        /// <typeparam name="TItem">Type of items in the enumerable.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="conjunction">The conjunction used before the last item in lists of two or more items.</param>
        /// <param name="delimiter">The delimiter used after each item other than the last in lists of three or more items.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A human-readable string composed of the specified enumerable items, conjunction, and delimiter.</returns>
        public static string ToListString<TItem>(this IEnumerable<TItem> self, string conjunction = "and", string delimiter = ", ")
        {
            Throw.If.Self.IsNull(self);
            int count = self.Take(3).Count();
            if (count == 0) { return string.Empty; }
            else if (count == 1) { return self.Single().ToString(); }
            else if (count == 2) { return $"{self.First()} {conjunction} {self.Last()}"; }
            else { return $"{string.Join(delimiter, self.Take(count - 1))}{delimiter}{conjunction} {self.Last()}"; }
        }

        #endregion

    }
}
