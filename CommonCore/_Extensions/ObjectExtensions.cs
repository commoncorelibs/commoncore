﻿#region Copyright (c) 2014 Jay Jeckel
// Copyright (c) 2014 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Diagnostics;

namespace CommonCore
{
    /// <summary>
    /// Class to provide <see cref="object"/> extension methods.
    /// </summary>
    public static class ObjectExtensions
    {
        /// <summary>
        /// Determine if this <see cref="object"/> instance is equal to <c>null</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the object is equal to <c>null</c>.</returns>
        [DebuggerStepThrough()]
        public static bool IsNull(this object self) => object.ReferenceEquals(self, null);

        /// <summary>
        /// Determine if this <see cref="object"/> instance is not equal to <c>null</c>.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the object is not equal to <c>null</c>.</returns>
        [DebuggerStepThrough()]
        public static bool IsNotNull(this object self) => !object.ReferenceEquals(self, null);

        /// <summary>
        /// Determine if this <see cref="object"/> instance is of the type <c>T</c>.
        /// </summary>
        /// <typeparam name="T">Type to compare.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the object is of the type <c>T</c>.</returns>
        [DebuggerStepThrough()]
        public static bool Is<T>(this object self) where T : class => self is T;

        /// <summary>
        /// Determine if this <see cref="object"/> instance is not of the type <c>T</c>.
        /// </summary>
        /// <typeparam name="T">Type to compare.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <returns><c>true</c> if the object is not of the type <c>T</c>.</returns>
        [DebuggerStepThrough()]
        public static bool IsNot<T>(this object self) where T : class => !(self is T);

        /// <summary>
        /// Return this <see cref="object"/> instance boxed as the given type.
        /// </summary>
        /// <typeparam name="T">Type to box the object as.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>Object boxed as type <c>T</c>.</returns>
        [DebuggerStepThrough()]
        public static T As<T>(this object self) where T : class => self as T;

        /// <summary>
        /// Return this <see cref="object"/> instance cast as the given type.
        /// </summary>
        /// <typeparam name="T">Type to cast the object as.</typeparam>
        /// <param name="self">Instance of extended class.</param>
        /// <exception cref="System.InvalidCastException">If <typeparamref name="T"/> is not a valid cast for this object.</exception>
        /// <returns>Object cast as type <c>T</c>.</returns>
        [DebuggerStepThrough()]
        public static T CastAs<T>(this object self) where T : class => (T)self;
    }
}
