﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.Text;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="StringExtensions_Formatting"/> <c>static</c> <c>class</c> provides
    /// extension methods related to <see cref="string"/> objects.
    /// 
    /// These methods focus on formatting of the <see cref="string"/>
    /// content and return a new <see cref="string"/> object.
    /// </summary>
    public static class StringExtensions_Formatting
    {

        #region AsFormat Methods

        /// <summary>
        /// Treat the specified <see cref="string"/> as a composite format string and
        /// replace the format items with the string representation of the corresponding
        /// object in the <paramref name="args"/> array.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="args">An object array that contains zero or more objects to format.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="args"/> is <c>null</c>.</exception>
        /// <exception cref="FormatException">
        /// If the format is invalid or the index of a format item is outside the bounds of
        /// the <paramref name="args"/> array.
        /// </exception>
        /// <returns>
        /// A copy of string in which the format items have been replaced by the string representation
        /// of the corresponding objects in args.
        /// </returns>
        public static string AsFormat(this string self, params object[] args) => string.Format(self, args);

        #endregion

        #region Align Methods

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance aligned to the left and padded
        /// with spaces on the right. The string will be unchanged if it is greater than or equal
        /// to <paramref name="width"/>. A <c>null</c> or empty instance will result in a string
        /// composed entirely of spaces.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="width">The width of the aligned string.</param>
        /// <returns>A left aligned string of width length.</returns>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="width"/> is less than zero.</exception>
        [Obsolete("Use the built-in string.PadRight() instead.", false)]
        public static string AlignLeft(this string self, int width)
        {
            Throw.If.Arg.IsLesser(nameof(width), width, 0);
            if (self == null || self.Length == 0) { return new string(' ', width); }
            else if (self.Length >= width) { return self; }
            else
            {
#if NETSTANDARD2_0
                return self + new string(' ', width - self.Length);
#else
                return string.Create(width, self,
                (chars, state) =>
                {
                    state.AsSpan().CopyTo(chars.Slice(0, state.Length));
                    for (int index = state.Length; index < chars.Length; index++)
                    { chars[index] = ' '; }
                });
#endif
            }
        }

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance aligned to the right and padded
        /// with spaces on the left. The string will be unchanged if it is greater than or equal
        /// to <paramref name="width"/>. A <c>null</c> or empty instance will result in a string
        /// composed entirely of spaces.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="width">The width of the aligned string.</param>
        /// <returns>A right aligned string of width length.</returns>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="width"/> is less than zero.</exception>
        [Obsolete("Use the built-in string.PadLeft() instead.", false)]
        public static string AlignRight(this string self, int width)
        {
            Throw.If.Arg.IsLesser(nameof(width), width, 0);
            if (self == null || self.Length == 0) { return new string(' ', width); }
            else if (self.Length >= width) { return self; }
            else
            {
#if NETSTANDARD2_0
                return new string(' ', width - self.Length) + self;
#else
                return string.Create(width, self,
                (chars, state) =>
                {
                    int pad = chars.Length - state.Length;
                    for (int index = 0; index < pad; index++)
                    { chars[index] = ' '; }
                    state.AsSpan().CopyTo(chars.Slice(pad, state.Length));
                });
#endif
            }
        }

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance aligned to the center and padded
        /// with spaces on both sides. The string will be unchanged if it is greater than or equal
        /// to <paramref name="width"/>. A <c>null</c> or empty instance will result in a string
        /// composed entirely of spaces. If the difference between the instance length and the width
        /// is odd, then the extra space will be included to the right of the aligned text.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="width">The width of the aligned string.</param>
        /// <returns>A center aligned string of width length.</returns>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="width"/> is less than zero.</exception>
        //public static string AlignCenter(this string self, int width)
        //{
        //    Throw.If.Arg.IsLesser(nameof(width), width, 0);
        //    if (self == null || self.Length == 0) { return new string(' ', width); }
        //    else if (self.Length >= width) { return self; }

        //    int pad = width - self.Length;
        //    bool even = (pad % 2 == 0);
        //    pad /= 2;
        //    return new string(' ', pad) + self + new string(' ', (even ? pad : pad + 1));
        //}
        public static string AlignCenter(this string self, int width)
        {
            Throw.If.Arg.IsLesser(nameof(width), width, 0);
            if (self == null || self.Length == 0) { return new string(' ', width); }
            else if (self.Length >= width) { return self; }
            else
            {
#if NETSTANDARD2_0
                int pad = width - self.Length;
                bool even = (pad % 2 == 0);
                pad /= 2;
                return new string(' ', pad) + self + new string(' ', (even ? pad : pad + 1));
#else
                return string.Create(width, self,
                (chars, state) =>
                {
                    int pad = width - self.Length;
                    bool even = (pad % 2 == 0);
                    pad /= 2;
                    for (int index = 0; index < pad; index++)
                    { chars[index] = ' '; }
                    state.AsSpan().CopyTo(chars.Slice(pad, state.Length));
                    for (int index = pad + state.Length; index < chars.Length; index++)
                    { chars[index] = ' '; }
                });
#endif
            }
        }

#endregion

#region Remove Methods

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all occurrences of <paramref name="value"/> removed.
        /// 
        /// Warning, this method performs string allocations per operation. If profiling shows these
        /// allocations as a bottleneck, then consider using the <see cref="StringBuilder"/> instead.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The string to be removed.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A copy of the string with all occurrences of <paramref name="value"/> removed.</returns>
        public static string Remove(this string self, string value)
        {
            Throw.If.Self.IsNull(self);
            return self.IsEmpty() || value.IsEmpty() ? self : self.Replace(value, string.Empty);
        }

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all occurrences of <paramref name="value"/> removed
        /// using the specified <paramref name="comparison"/> to determine case-sensitivity.
        /// 
        /// Warning, this method performs string allocations per operation. If profiling shows these
        /// allocations as a bottleneck, then consider using the <see cref="StringBuilder"/> instead.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The string to be removed.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A copy of the string with all occurrences of <paramref name="value"/> removed.</returns>
        public static string Remove(this string self, string value, StringComparison comparison)
        {
            Throw.If.Self.IsNull(self);
            return self.IsEmpty() || value.IsEmpty() ? self : self.Replace(value, string.Empty, comparison);
        }

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all occurrences of <paramref name="value"/> removed
        /// up to a maximum number of <paramref name="maxCount"/> times.
        /// 
        /// Warning, this method performs string allocations per operation. If profiling shows these
        /// allocations as a bottleneck, then consider using the <see cref="StringBuilder"/> instead.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The string to be removed.</param>
        /// <param name="maxCount">The maximum number of replacements to perform.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A copy of the string with zero or more occurrences of <paramref name="value"/> removed.</returns>
        public static string Remove(this string self, string value, int maxCount)
        {
            Throw.If.Self.IsNull(self);
            return self.IsEmpty() || value.IsEmpty() || maxCount == 0 ? self : Replace(self, value, string.Empty, maxCount);
        }

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all occurrences of <paramref name="value"/> removed
        /// up to a maximum number of <paramref name="maxCount"/> times
        /// using the specified <paramref name="comparison"/> to determine case-sensitivity.
        /// 
        /// Warning, this method performs string allocations per operation. If profiling shows these
        /// allocations as a bottleneck, then consider using the <see cref="StringBuilder"/> instead.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The string to be removed.</param>
        /// <param name="maxCount">The maximum number of replacements to perform.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A copy of the string with zero or more occurrences of <paramref name="value"/> removed.</returns>
        public static string Remove(this string self, string value, int maxCount, StringComparison comparison)
        {
            Throw.If.Self.IsNull(self);
            return self.IsEmpty() || value.IsEmpty() || maxCount == 0 ? self : Replace(self, value, string.Empty, maxCount, comparison);
        }

#endregion

#region RemoveAll Methods

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all occurrences of <paramref name="values"/> removed.
        /// 
        /// Warning, this method performs string allocations per operation. If profiling shows these
        /// allocations as a bottleneck, then consider using the <see cref="StringBuilder"/> instead.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">Array of strings to be removed.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A copy of the string with all occurrences of <paramref name="values"/> removed.</returns>
        public static string RemoveAll(this string self, params string[] values)
        {
            Throw.If.Self.IsNull(self);
            return self.IsEmpty() || values == null || values.Length == 0 ? self : ReplaceAll(self, string.Empty, values);
        }

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all occurrences of <paramref name="values"/> removed
        /// using the specified <paramref name="comparison"/> to determine case-sensitivity.
        /// 
        /// Warning, this method performs string allocations per operation. If profiling shows these
        /// allocations as a bottleneck, then consider using the <see cref="StringBuilder"/> instead.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <param name="values">Array of strings to be removed.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A copy of the string with all occurrences of <paramref name="values"/> removed.</returns>
        public static string RemoveAll(this string self, StringComparison comparison, params string[] values)
        {
            Throw.If.Self.IsNull(self);
            return self.IsEmpty() || values == null || values.Length == 0 ? self : ReplaceAll(self, string.Empty, comparison, values);
        }

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all occurrences of <paramref name="values"/> removed.
        /// 
        /// Warning, this method performs string allocations per operation. If profiling shows these
        /// allocations as a bottleneck, then consider using the <see cref="StringBuilder"/> instead.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">Array of strings to be removed.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A copy of the string with all occurrences of <paramref name="values"/> removed.</returns>
        public static string RemoveAll(this string self, IEnumerable<string> values)
        {
            Throw.If.Self.IsNull(self);
            return self.IsEmpty() || values == null ? self : ReplaceAll(self, string.Empty, values);
        }

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all occurrences of <paramref name="values"/> removed
        /// using the specified <paramref name="comparison"/> to determine case-sensitivity.
        /// 
        /// Warning, this method performs string allocations per operation. If profiling shows these
        /// allocations as a bottleneck, then consider using the <see cref="StringBuilder"/> instead.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">Array of strings to be removed.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A copy of the string with all occurrences of <paramref name="values"/> removed.</returns>
        public static string RemoveAll(this string self, IEnumerable<string> values, StringComparison comparison)
        {
            Throw.If.Self.IsNull(self);
            return self.IsEmpty() || values == null ? self : ReplaceAll(self, string.Empty, values, comparison);
        }

        #endregion

        #region Replace Methods

#if NETSTANDARD2_0
        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all occurrences of
        /// <paramref name="oldValue"/> replaced by <paramref name="newValue"/>
        /// using the specified <paramref name="comparison"/> to determine case-sensitivity.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="oldValue">The string to be replaced.</param>
        /// <param name="newValue">The string to be the replacement.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A copy of the string with all occurrences of <paramref name="oldValue"/> replaced by <paramref name="newValue"/>.</returns>
        public static string Replace(this string self, string oldValue, string newValue, StringComparison comparison)
            => Replace(self, oldValue, newValue, -1, comparison);
#endif

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all occurrences of
        /// <paramref name="oldValue"/> replaced by <paramref name="newValue"/>
        /// up to a maximum number of <paramref name="maxCount"/> times.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="oldValue">The string to be replaced.</param>
        /// <param name="newValue">The string to be the replacement.</param>
        /// <param name="maxCount">The maximum number of replacements to perform.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A copy of the string with all occurrences of <paramref name="oldValue"/> replaced by <paramref name="newValue"/>.</returns>
        public static string Replace(this string self, string oldValue, string newValue, int maxCount)
            => Replace(self, oldValue, newValue, maxCount, StringComparison.CurrentCulture);

        internal struct StateContext
        {
            public StateContext(string source, string oldValue, string newValue, int maxCount)
            {
                this.Source = source;
                this.OldValue = oldValue;
                this.NewValue = newValue;
                this.MaxCount = maxCount;
            }

            public string Source { get; }

            public string OldValue { get; }

            public string NewValue { get; }

            public int MaxCount { get; }
        }

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all occurrences of
        /// <paramref name="oldValue"/> replaced by <paramref name="newValue"/>
        /// up to a maximum number of <paramref name="maxCount"/> times
        /// using the specified <paramref name="comparison"/> to determine case-sensitivity.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="oldValue">The string to be replaced.</param>
        /// <param name="newValue">The string to be the replacement.</param>
        /// <param name="maxCount">The maximum number of replacements to perform.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A copy of the string with all occurrences of <paramref name="oldValue"/> replaced by <paramref name="newValue"/>.</returns>
        public static string Replace(this string self, string oldValue, string newValue, int maxCount, StringComparison comparison)
        {
            Throw.If.Self.IsNull(self);
            if (self.IsEmpty() || oldValue.IsEmpty() || oldValue.Length > self.Length || maxCount == 0) { return self; }
            newValue ??= string.Empty;

            int matchCount = self.CountMatches(oldValue, maxCount, comparison);
            if (matchCount == 0) { return self; }

            int width = self.Length - (matchCount * oldValue.Length) + (matchCount * newValue.Length);
            if (width == 0) { return string.Empty; }

#if NETSTANDARD2_0
            int index = 0;
            int num = 0;
            while ((index = self.IndexOf(oldValue, index, comparison)) > -1 && (maxCount < 0 || num < maxCount))
            {
                self = self.Remove(index, oldValue.Length);
                if (newValue.IsNotEmpty())
                {
                    self = self.Insert(index, newValue);
                    index += newValue.Length;
                }
                num += 1;
            }
            return self;
#else
            return string.Create(width, new StateContext(self, oldValue, newValue, maxCount),
            (Span<char> chars, StateContext state) =>
            {
                int indexSourse = 0, indexDest = 0, count = 0, length = 0;
                var replacement = state.NewValue.AsSpan();

                int indexMatch = state.Source.IndexOf(state.OldValue, indexSourse, comparison);
                while (indexMatch != -1 && (state.MaxCount < 0 || count < state.MaxCount))
                {
                    count++;
                    length = indexMatch - indexSourse;
                    state.Source.AsSpan().Slice(indexSourse, length).CopyTo(chars.Slice(indexDest, length));
                    indexDest += length;
                    if (replacement.Length > 0)
                    {
                        replacement.CopyTo(chars.Slice(indexDest, replacement.Length));
                        indexDest += replacement.Length;
                    }
                    indexSourse = indexMatch + state.OldValue.Length;
                    indexMatch = state.Source.IndexOf(state.OldValue, indexSourse, comparison);
                }
                if (indexSourse < state.Source.Length - 1)
                {
                    indexMatch = state.Source.Length;
                    length = indexMatch - indexSourse;
                    state.Source.AsSpan().Slice(indexSourse, length).CopyTo(chars.Slice(indexDest, length));
                }
            });
#endif
        }
        // IMPORTANT: Keep below original code to compare and profile against the new code.
        //public static string Replace(this string self, string oldValue, string newValue, int count, StringComparison comparison)
        //{
        //    Throw.If.Self.IsNull(self);
        //    if (self.IsEmpty() || IsEmpty(oldValue) || count == 0) { return self; }
        //    int index = 0;
        //    int num = 0;
        //    while ((index = self.IndexOf(oldValue, index, comparison)) > -1 && (count < 0 || num < count))
        //    {
        //        self = self.Remove(index, oldValue.Length);
        //        if (IsNotEmpty(newValue))
        //        {
        //            self = self.Insert(index, newValue);
        //            index += newValue.Length;
        //        }
        //        num += 1;
        //    }
        //    return self;
        //}
        // IMPORTANT: Keep above original code to compare and profile against the new code.

        #endregion

        #region ReplaceAll Methods

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all occurrences of
        /// <paramref name="oldValues"/> replaced by <paramref name="newValue"/>.
        /// 
        /// Notice, this method performs up to one string allocation per <paramref name="oldValues"/> element.
        /// If profiling shows these allocations as a bottleneck, then consider using the <see cref="StringBuilder"/> instead.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="newValue">The string to be the replacement.</param>
        /// <param name="oldValues">Array of strings to be replaced.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A copy of the string with all occurrences of <paramref name="oldValues"/> replaced by <paramref name="newValue"/>.</returns>
        public static string ReplaceAll(this string self, string newValue, params string[] oldValues)
            => ReplaceAll(self, newValue, oldValues, StringComparison.CurrentCulture);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all occurrences of
        /// <paramref name="oldValues"/> replaced by <paramref name="newValue"/>
        /// using the specified <paramref name="comparison"/> to determine case-sensitivity.
        /// 
        /// Notice, this method performs up to one string allocation per <paramref name="oldValues"/> element.
        /// If profiling shows these allocations as a bottleneck, then consider using the <see cref="StringBuilder"/> instead.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="newValue">The string to be the replacement.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <param name="oldValues">Array of strings to be replaced.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A copy of the string with all occurrences of <paramref name="oldValues"/> replaced by <paramref name="newValue"/>.</returns>
        public static string ReplaceAll(this string self, string newValue, StringComparison comparison, params string[] oldValues)
            => ReplaceAll(self, newValue, oldValues, comparison);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all occurrences of
        /// <paramref name="oldValues"/> replaced by <paramref name="newValue"/>.
        /// 
        /// Notice, this method performs up to one string allocation per <paramref name="oldValues"/> element.
        /// If profiling shows these allocations as a bottleneck, then consider using the <see cref="StringBuilder"/> instead.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="newValue">The string to be the replacement.</param>
        /// <param name="oldValues">Array of strings to be replaced.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A copy of the string with all occurrences of <paramref name="oldValues"/> replaced by <paramref name="newValue"/>.</returns>
        public static string ReplaceAll(this string self, string newValue, IEnumerable<string> oldValues)
            => ReplaceAll(self, newValue, oldValues, StringComparison.CurrentCulture);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all occurrences of
        /// <paramref name="oldValues"/> replaced by <paramref name="newValue"/>
        /// using the specified <paramref name="comparison"/> to determine case-sensitivity.
        /// 
        /// Notice, this method performs up to one string allocation per <paramref name="oldValues"/> element.
        /// If profiling shows these allocations as a bottleneck, then consider using the <see cref="StringBuilder"/> instead.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="newValue">The string to be the replacement.</param>
        /// <param name="oldValues">Array of strings to be replaced.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A copy of the string with all occurrences of <paramref name="oldValues"/> replaced by <paramref name="newValue"/>.</returns>
        public static string ReplaceAll(this string self, string newValue, IEnumerable<string> oldValues, StringComparison comparison)
        {
            Throw.If.Self.IsNull(self);
            if (self.IsEmpty() || oldValues == null) { return self; }
            foreach (var oldValue in oldValues)
            { if (oldValue.IsNotEmpty() && oldValue.Length <= self.Length) { self = self.Replace(oldValue, newValue, comparison); } }
            return self;
        }

#endregion

#region ReplaceAny Methods

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all occurrences of the keys in
        /// <paramref name="values"/> replaced by their respective values.
        /// 
        /// Notice, this method performs up to one string allocation per <paramref name="values"/> element.
        /// If profiling shows these allocations as a bottleneck, then consider using the <see cref="StringBuilder"/> instead.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">Dictionary of keys to be replaced by their respective values.</param>
        /// <returns>A copy of the string with all occurrences of the dictionary keys replaced by their respective values.</returns>
        public static string ReplaceAny(this string self, Dictionary<string, string> values)
            => ReplaceAny(self, values?.Keys, values?.Values, StringComparison.CurrentCulture);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all occurrences of the keys in
        /// <paramref name="values"/> replaced by their respective values
        /// using the specified <paramref name="comparison"/> to determine case-sensitivity.
        /// 
        /// Notice, this method performs up to one string allocation per <paramref name="values"/> element.
        /// If profiling shows these allocations as a bottleneck, then consider using the <see cref="StringBuilder"/> instead.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">Dictionary of keys to be replaced by their respective values.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <returns>A copy of the string with all occurrences of the dictionary keys replaced by their respective values.</returns>
        public static string ReplaceAny(this string self, Dictionary<string, string> values, StringComparison comparison)
            => ReplaceAny(self, values?.Keys, values?.Values, comparison);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all occurrences of the items in
        /// <paramref name="oldValues"/> replaced by their respective items in <paramref name="newValues"/>.
        /// 
        /// The two enumerables will be traversed together, replacing each old value with the new value.
        /// Replacement will terminate as soon as either sequence stops yielding values.
        /// 
        /// Notice, this method performs up to one string allocation per <paramref name="oldValues"/> element.
        /// If profiling shows these allocations as a bottleneck, then consider using the <see cref="StringBuilder"/> instead.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="oldValues">Sequence of strings to be replaced.</param>
        /// <param name="newValues">Sequence of strings to be the replacements.</param>
        /// <returns>A copy of the string with all occurrences of the strings in <paramref name="oldValues"/> replaced by the respective item in <paramref name="newValues"/>.</returns>
        public static string ReplaceAny(this string self, IEnumerable<string> oldValues, IEnumerable<string> newValues)
            => ReplaceAny(self, oldValues, newValues, StringComparison.CurrentCulture);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all occurrences of the items in
        /// <paramref name="oldValues"/> replaced by their respective items in <paramref name="newValues"/>
        /// using the specified <paramref name="comparison"/> to determine case-sensitivity.
        /// 
        /// The two enumerables will be traversed together, replacing each old value with the new value.
        /// Replacement will terminate as soon as either sequence stops yielding values.
        /// 
        /// Notice, this method performs up to one string allocation per <paramref name="oldValues"/> element.
        /// If profiling shows these allocations as a bottleneck, then consider using the <see cref="StringBuilder"/> instead.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="oldValues">Sequence of strings to be replaced.</param>
        /// <param name="newValues">Sequence of strings to be the replacements.</param>
        /// <param name="comparison">Enumeration value that specifies the rules for case-sensitivity.</param>
        /// <returns>A copy of the string with all occurrences of the strings in <paramref name="oldValues"/> replaced by the respective item in <paramref name="newValues"/>.</returns>
        public static string ReplaceAny(this string self, IEnumerable<string> oldValues, IEnumerable<string> newValues, StringComparison comparison)
        {
            Throw.If.Self.IsNull(self);
            if (self.IsEmpty() || oldValues == null || newValues == null) { return self; }
            using (IEnumerator<string> olds = oldValues.GetEnumerator())
            using (IEnumerator<string> news = newValues.GetEnumerator())
            {
                while (olds.MoveNext() && news.MoveNext())
                { if (olds.Current.IsNotEmpty() && olds.Current.Length <= self.Length) { self = self.Replace(olds.Current, news.Current, comparison); } }
            }
            return self;
        }

#endregion

#region Trim Methods

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with <paramref name="count"/>
        /// number of characters trimmed from the start and end of the string. Any count greater
        /// than necessary to trim all characters will result in an empty string.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="count">The number of characters to trim from the start and end.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="count"/> is less than zero.</exception>
        /// <returns>A copy of the string with <paramref name="count"/> number of characters trimmed from the start and end.</returns>
        public static string Trim(this string self, int count)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsLesser(nameof(count), count, 0);
            if (count == 0) { return self; }
            else if (count * 2 >= self.Length) { return string.Empty; }
            else { return self.Substring(count, self.Length - (count * 2)); }
        }

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with <paramref name="startCount"/>
        /// and <paramref name="endCount"/> number of characters trimmed from the start and end, respectively,
        /// of the string. Any combination of counts greater than necessary to trim all characters will result
        /// in an empty string.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="startCount">The number of characters to trim from the start.</param>
        /// <param name="endCount">The number of characters to trim from the end.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If either <paramref name="startCount"/> or <paramref name="endCount"/> are less than zero.</exception>
        /// <returns>A copy of the string with <paramref name="startCount"/> and <paramref name="endCount"/> number of characters trimmed from the start and end.</returns>
        public static string Trim(this string self, int startCount, int endCount)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsLesser(nameof(startCount), startCount, 0);
            Throw.If.Arg.IsLesser(nameof(endCount), endCount, 0);
            if (startCount == 0 && endCount == 0) { return self; }
            else if (startCount + endCount >= self.Length) { return string.Empty; }
            else { return self.Substring(startCount, self.Length - (startCount + endCount)); }
        }

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all successive occurrences of
        /// <paramref name="value"/> trimmed from the start and end of the string. Note, both the start
        /// and end pattern are applied independently,
        /// meaning that any character that matches its pattern, regardless of modifications the other pattern
        /// could make, will be removed. This behavior differs from that of calling
        /// <c>self.TrimStart(<paramref name="value"/>).TrimEnd(<paramref name="value"/>)</c> or
        /// <c>self.TrimEnd(<paramref name="value"/>).TrimStart(<paramref name="value"/>)</c> where
        /// the first call's trimming could remove characters that would have been matched by the second call's
        /// pattern.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The string to trim from the start and end.</param>
        /// <returns>A copy of the string with all successive occurrences of <paramref name="value"/> trimmed from the start and end.</returns>
        public static string Trim(this string self, string value)
            => Trim(self, value, value, -1, -1);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with up to <paramref name="maxCount"/>
        /// successive occurrences of <paramref name="value"/> trimmed from the start and end of the
        /// string. If <paramref name="maxCount"/> is equal to zero, then no trimming will be done;
        /// if less than zero, then all occurrences will be trimmed.
        /// Note, both the start and end pattern, as well as the count, are applied independently,
        /// meaning that any character that matches its pattern, regardless of modifications the other pattern
        /// could make, will be removed. This behavior differs from that of calling
        /// <c>self.TrimStart(<paramref name="value"/>).TrimEnd(<paramref name="value"/>)</c> or
        /// <c>self.TrimEnd(<paramref name="value"/>).TrimStart(<paramref name="value"/>)</c> where
        /// the first call's trimming could remove characters that would have been matched by the second call's
        /// pattern.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The string to trim from the start and end.</param>
        /// <param name="maxCount">The maximum number of occurrences to trim.</param>
        /// <returns>A copy of the string with zero or more successive occurrences of <paramref name="value"/> trimmed from the start and end.</returns>
        public static string Trim(this string self, string value, int maxCount)
            => Trim(self, value, value, maxCount, maxCount);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all successive occurrences of
        /// <paramref name="startValue"/> and <paramref name="endValue"/> trimmed from the start and end,
        /// respectively, of the string. Note, both the start and end patterns are applied independently,
        /// meaning that any character that matches its pattern, regardless of modifications the other pattern
        /// could make, will be removed. This behavior differs from that of calling
        /// <c>self.TrimStart(<paramref name="startValue"/>).TrimEnd(<paramref name="endValue"/>)</c> or
        /// <c>self.TrimEnd(<paramref name="endValue"/>).TrimStart(<paramref name="startValue"/>)</c> where
        /// the first call's trimming could remove characters that would have been matched by the second call's
        /// pattern.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="startValue">The string to trim from the start.</param>
        /// <param name="endValue">The string to trim from the end.</param>
        /// <returns>A copy of the string with all successive occurrences of <paramref name="startValue"/> and <paramref name="endValue"/> trimmed from the start and end.</returns>
        public static string Trim(this string self, string startValue, string endValue)
            => Trim(self, startValue, endValue, -1, -1);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with up to <paramref name="maxStartCount"/>
        /// and <paramref name="maxEndCount"/> successive occurrences of
        /// <paramref name="startValue"/> and <paramref name="endValue"/> trimmed from the start and end,
        /// respectively, of the string. If <paramref name="maxStartCount"/> or <paramref name="maxEndCount"/>
        /// are equal to zero, then no trimming will be done on that side; if less than zero, then all
        /// occurrences will be trimmed. Note, both the start and end patterns are applied independently,
        /// meaning that any character that matches its pattern, regardless of modifications the other pattern
        /// could make, will be removed. This behavior differs from that of calling
        /// <c>self.TrimStart(<paramref name="startValue"/>).TrimEnd(<paramref name="endValue"/>)</c> or
        /// <c>self.TrimEnd(<paramref name="endValue"/>).TrimStart(<paramref name="startValue"/>)</c> where
        /// the first call's trimming could remove characters that would have been matched by the second call's
        /// pattern.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="startValue">The string to trim from the start.</param>
        /// <param name="endValue">The string to trim from the end.</param>
        /// <param name="maxStartCount">The maximum number of occurrences to trim.</param>
        /// <param name="maxEndCount">The maximum number of occurrences to trim.</param>
        /// <returns>A copy of the string with zero or more successive occurrences of <paramref name="startValue"/> and <paramref name="endValue"/> trimmed from the start and end.</returns>
        public static string Trim(this string self, string startValue, string endValue, int maxStartCount, int maxEndCount)
        {
            Throw.If.Self.IsNull(self);
            if (self.IsEmpty() || ((startValue.IsEmpty() || maxStartCount == 0) && (endValue.IsEmpty() || maxEndCount == 0))) { return self; }

            int startCount = 0;
            if (!startValue.IsEmpty() && maxStartCount != 0)
            {
                while (maxStartCount < 0 || startCount < maxStartCount)
                {
                    if (!self.IsMatch(startValue, startCount * startValue.Length)) { break; }
                    startCount += 1;
                }
            }

            int endCount = 0;
            if (!endValue.IsEmpty() && maxEndCount != 0)
            {
                while (maxEndCount < 0 || endCount < maxEndCount)
                {
                    if (!self.IsMatch(endValue, self.Length - ((endCount + 1) * endValue.Length))) { break; }
                    endCount += 1;
                }
            }

            if (startCount == 0 && endCount == 0) { return self; }

            int startLength = startCount * startValue.Length;
            int endLength = endCount * endValue.Length;
            if (startLength + endLength >= self.Length) { return string.Empty; }

            return self.Substring(startLength, self.Length - (startLength + endLength));
        }

#endregion

#region TrimAll Methods

        //public static string TrimAll(this string self, params string[] values) => Trim(self, -1, values);

        //public static string TrimAll(this string self, int maxCount, params string[] values) { }

        //public static string TrimAll(this string self, IEnumerable<string> values) => Trim(self, values, -1);

        //public static string TrimAll(this string self, IEnumerable<string> values, int maxCount) { }

#endregion

#region TrimEnd Methods

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with <paramref name="count"/>
        /// number of characters trimmed from the end of the string. Any count greater than
        /// or equal to the string length results in an empty string.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="count">The number of characters to trim.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="count"/> is less than zero.</exception>
        /// <returns>A copy of the string with count number of characters trimmed from the end.</returns>
        public static string TrimEnd(this string self, int count)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsLesser(nameof(count), count, 0);
            if (count == 0) { return self; }
            else if (count >= self.Length) { return string.Empty; }
            else { return self.Substring(0, self.Length - count); }
        }
        
        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all successive occurrences of
        /// <paramref name="value"/> trimmed from the end of the string.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The string to trim.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A copy of the string with all successive occurrences of <paramref name="value"/> trimmed from the end.</returns>
        public static string TrimEnd(this string self, string value)
            => TrimEnd(self, value, -1);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with up to <paramref name="maxCount"/>
        /// successive occurrences of <paramref name="value"/> trimmed from the end of the string. If
        /// <paramref name="maxCount"/> equal to zero, then no trimming will be done; if less than zero,
        /// then all occurrences will be trimmed.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The string to trim.</param>
        /// <param name="maxCount">The maximum number of occurrences to trim.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A copy of the string with zero or more successive occurrences of <paramref name="value"/> trimmed from the end.</returns>
        public static string TrimEnd(this string self, string value, int maxCount)
        {
            Throw.If.Self.IsNull(self);
            if (self.IsEmpty() || value.IsEmpty() || maxCount == 0) { return self; }

            int count = 0;
            while (maxCount < 0 || count < maxCount)
            {
                if (!self.IsMatch(value, self.Length - ((count + 1) * value.Length))) { break; }
                count += 1;
            }
            return (count == 0 ? self : self.Remove(self.Length - (count * value.Length), (count * value.Length)));
        }
        
        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all successive occurrences of
        /// all <paramref name="values"/> items trimmed from the end of the string. The end of the
        /// string will be checked against each value in turn, removing the first matched value. This process
        /// is then repeated until no matching value is found.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">Array of strings to trim.</param>
        /// <returns>A copy of the string with all matching <paramref name="values"/> trimmed from the end.</returns>
        public static string TrimEnd(this string self, params string[] values)
            => TrimEnd(self, -1, values);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with up to <paramref name="maxCount"/>
        /// successive occurrences of all <paramref name="values"/> items trimmed from the end of the string.
        /// The end of the string will be checked against each value in turn, removing the first matched value.
        /// This process is then repeated until no matching value is found. If <paramref name="maxCount"/> equal
        /// to zero, then no trimming will be done; if less than zero, then all occurrences will be trimmed.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">Array of strings to trim.</param>
        /// <param name="maxCount">The maximum number of occurrences to trim.</param>
        /// <returns>A copy of the string with zero or more matching <paramref name="values"/> trimmed from the end.</returns>
        public static string TrimEnd(this string self, int maxCount, params string[] values)
        {
            Throw.If.Self.IsNull(self);
            if (self.IsEmpty() || values == null || values.Length == 0 || maxCount == 0) { return self; }

            int count = 0;
            int length = 0;
            while (maxCount < 0 || count < maxCount)
            {
                bool found = false;
                foreach (var value in values)
                {
                    if (self.IsMatch(value, self.Length - length - value.Length))
                    {
                        count += 1;
                        length += value.Length;
                        found = true;
                        break;
                    }
                }
                if (!found || length >= self.Length) { break; }
            }
            return (length == 0 ? self : length >= self.Length ? string.Empty : self.Substring(0, self.Length - length));
        }
        
        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all successive occurrences of
        /// all <paramref name="values"/> items trimmed from the end of the string. The end of the
        /// string will be checked against each value in turn, removing the first matched value. This process
        /// is then repeated until no matching value is found.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">Enumerable of strings to trim.</param>
        /// <returns>A copy of the string with all matching <paramref name="values"/> trimmed from the end.</returns>
        public static string TrimEnd(this string self, IEnumerable<string> values)
            => TrimEnd(self, values, -1);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with up to <paramref name="maxCount"/>
        /// successive occurrences of all <paramref name="values"/> items trimmed from the end of the string.
        /// The end of the string will be checked against each value in turn, removing the first matched value.
        /// This process is then repeated until no matching value is found. If <paramref name="maxCount"/> equal
        /// to zero, then no trimming will be done; if less than zero, then all occurrences will be trimmed.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">Enumerable of strings to trim.</param>
        /// <param name="maxCount">The maximum number of occurrences to trim.</param>
        /// <returns>A copy of the string with zero or more matching <paramref name="values"/> trimmed from the end.</returns>
        public static string TrimEnd(this string self, IEnumerable<string> values, int maxCount)
        {
            Throw.If.Self.IsNull(self);
            if (self.IsEmpty() || values == null || maxCount == 0) { return self; }

            int count = 0;
            int length = 0;
            while (maxCount < 0 || count < maxCount)
            {
                bool found = false;
                foreach (var value in values)
                {
                    if (self.IsMatch(value, self.Length - length - value.Length))
                    {
                        count += 1;
                        length += value.Length;
                        found = true;
                        break;
                    }
                }
                if (!found || length >= self.Length) { break; }
            }
            return (length == 0 ? self : length >= self.Length ? string.Empty : self.Substring(0, self.Length - length));
        }

#endregion

#region TrimStart Methods

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with <paramref name="count"/>
        /// number of characters trimmed from the start of the string. Any count greater than
        /// or equal to the string length results in an empty string.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="count">The number of characters to trim.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="count"/> is less than zero.</exception>
        /// <returns>A copy of the string with count number of characters trimmed from the start.</returns>
        public static string TrimStart(this string self, int count)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsLesser(nameof(count), count, 0);
            if (count == 0) { return self; }
            else if (count >= self.Length) { return string.Empty; }
            else { return self.Substring(count); }
        }

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all successive occurrences of
        /// <paramref name="value"/> trimmed from the start of the string.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The string to trim.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A copy of the string with all successive occurrences of <paramref name="value"/> trimmed from the start.</returns>
        public static string TrimStart(this string self, string value)
            => TrimStart(self, value, -1);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with up to <paramref name="maxCount"/>
        /// successive occurrences of <paramref name="value"/> trimmed from the start of the string. If
        /// <paramref name="maxCount"/> equal to zero, then no trimming will be done; if less than zero,
        /// then all occurrences will be trimmed.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="value">The string to trim.</param>
        /// <param name="maxCount">The maximum number of occurrences to trim.</param>
        /// <exception cref="NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <returns>A copy of the string with zero or more successive occurrences of <paramref name="value"/> trimmed from the start.</returns>
        public static string TrimStart(this string self, string value, int maxCount)
        {
            Throw.If.Self.IsNull(self);
            if (self.IsEmpty() || value.IsEmpty() || maxCount == 0) { return self; }

            int count = 0;
            while (maxCount < 0 || count < maxCount)
            {
                if (!self.IsMatch(value, count * value.Length)) { break; }
                count += 1;
            }
            return (count == 0 ? self : self.Remove(0, count * value.Length));
        }

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all successive occurrences of
        /// all <paramref name="values"/> items trimmed from the start of the string. The start of the
        /// string will be checked against each value in turn, removing the first matched value. This process
        /// is then repeated until no matching value is found.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">Array of strings to trim.</param>
        /// <returns>A copy of the string with all matching <paramref name="values"/> trimmed from the start.</returns>
        public static string TrimStart(this string self, params string[] values)
            => TrimStart(self, -1, values);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with up to <paramref name="maxCount"/>
        /// successive occurrences of all <paramref name="values"/> items trimmed from the start of the string.
        /// The start of the string will be checked against each value in turn, removing the first matched value.
        /// This process is then repeated until no matching value is found. If <paramref name="maxCount"/> equal
        /// to zero, then no trimming will be done; if less than zero, then all occurrences will be trimmed.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">Array of strings to trim.</param>
        /// <param name="maxCount">The maximum number of occurrences to trim.</param>
        /// <returns>A copy of the string with zero or more matching <paramref name="values"/> trimmed from the start.</returns>
        public static string TrimStart(this string self, int maxCount, params string[] values)
        {
            Throw.If.Self.IsNull(self);
            if (self.IsEmpty() || values == null || values.Length == 0 || maxCount == 0) { return self; }

            int count = 0;
            int length = 0;
            while (maxCount < 0 || count < maxCount)
            {
                bool found = false;
                foreach (var value in values)
                {
                    if (self.IsMatch(value, length))
                    {
                        count += 1;
                        length += value.Length;
                        found = true;
                        break;
                    }
                }
                if (!found || length >= self.Length) { break; }
            }
            return (length == 0 ? self : length >= self.Length ? string.Empty : self.Substring(length));
        }

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with all successive occurrences of
        /// all <paramref name="values"/> items trimmed from the start of the string. The start of the
        /// string will be checked against each value in turn, removing the first matched value. This process
        /// is then repeated until no matching value is found.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">Enumerable of strings to trim.</param>
        /// <returns>A copy of the string with all matching <paramref name="values"/> trimmed from the start.</returns>
        public static string TrimStart(this string self, IEnumerable<string> values)
            => TrimStart(self, values, -1);

        /// <summary>
        /// Returns a copy of this <see cref="string"/> instance with up to <paramref name="maxCount"/>
        /// successive occurrences of all <paramref name="values"/> items trimmed from the start of the string.
        /// The start of the string will be checked against each value in turn, removing the first matched value.
        /// This process is then repeated until no matching value is found. If <paramref name="maxCount"/> equal
        /// to zero, then no trimming will be done; if less than zero, then all occurrences will be trimmed.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="values">Enumerable of strings to trim.</param>
        /// <param name="maxCount">The maximum number of occurrences to trim.</param>
        /// <returns>A copy of the string with zero or more matching <paramref name="values"/> trimmed from the start.</returns>
        public static string TrimStart(this string self, IEnumerable<string> values, int maxCount)
        {
            Throw.If.Self.IsNull(self);
            if (self.IsEmpty() || values == null || maxCount == 0) { return self; }

            int count = 0;
            int length = 0;
            while (maxCount < 0 || count < maxCount)
            {
                bool found = false;
                foreach (var value in values)
                {
                    if (self.IsMatch(value, length))
                    {
                        count += 1;
                        length += value.Length;
                        found = true;
                        break;
                    }
                }
                if (!found || length >= self.Length) { break; }
            }
            return (length == 0 ? self : length >= self.Length ? string.Empty : self.Substring(length));
        }

#endregion

    }
}
