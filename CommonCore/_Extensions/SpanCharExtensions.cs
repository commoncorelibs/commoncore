﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;

namespace CommonCore
{
    /// <summary>
    /// The <see cref="SpanCharExtensions" /> <c>static</c> <c>class</c> provides
    /// <see cref="Span{Char}" /> and <see cref="ReadOnlySpan{Char}" /> related extension methods.
    /// </summary>
    public static class SpanCharExtensions
    {
        /// <summary>
        /// Copies the contents of the specified <paramref name="source" /> into this <see cref="Span{T}" /> instance.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="source">The span to copy items from.</param>
        public static void CopyFrom(this Span<char> self, Span<char> source)
        {
            for (int index = 0; index < self.Length && index < source.Length; index++)
            { self[index] = source[index]; }
        }

        /// <summary>
        /// Copies the contents of the specified <paramref name="source" /> into this <see cref="Span{T}" /> instance.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="source">The span to copy items from.</param>
        public static void CopyFrom(this Span<char> self, ReadOnlySpan<char> source)
        {
            for (int index = 0; index < self.Length && index < source.Length; index++)
            { self[index] = source[index]; }
        }

        /// <summary>
        /// Copies the specified <paramref name="count" /> of characters from the specified
        /// <paramref name="source" /> to this instance according to the specified
        /// <paramref name="destinationIndex" /> and <paramref name="sourceIndex" /> positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="destinationIndex">
        /// The index in this instance at which at which the copy operation begins.
        /// </param>
        /// <param name="source">The sequence of characters to be copied to this instance.</param>
        /// <param name="sourceIndex">
        /// The index of the first character in the specified <paramref name="source" /> to copy.
        /// </param>
        /// <param name="count">The number of characters in this instance to copy to destination.</param>
        public static void CopyFrom(this Span<char> self, int destinationIndex, Span<char> source, int sourceIndex, int count)
        {
            Throw.If.Arg.IsIndexOutOfRange(nameof(destinationIndex), destinationIndex, self);
            Throw.If.Arg.IsIndexOutOfRange(nameof(sourceIndex), sourceIndex, source);
            for (int index = 0; index < count; index++)
            {
                int idest = index + destinationIndex;
                if (idest >= self.Length) { break; }
                int isource = index + sourceIndex;
                if (isource >= source.Length) { break; }
                self[idest] = source[isource];
            }
        }

        /// <summary>
        /// Copies the specified <paramref name="count" /> of characters from the specified
        /// <paramref name="source" /> to this instance according to the specified
        /// <paramref name="destinationIndex" /> and <paramref name="sourceIndex" /> positions.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="destinationIndex">
        /// The index in this instance at which at which the copy operation begins.
        /// </param>
        /// <param name="source">The sequence of characters to be copied to this instance.</param>
        /// <param name="sourceIndex">
        /// The index of the first character in the specified <paramref name="source" /> to copy.
        /// </param>
        /// <param name="count">The number of characters in this instance to copy to destination.</param>
        public static void CopyFrom(this Span<char> self, int destinationIndex, ReadOnlySpan<char> source, int sourceIndex, int count)
        {
            Throw.If.Arg.IsIndexOutOfRange(nameof(destinationIndex), destinationIndex, self);
            Throw.If.Arg.IsIndexOutOfRange(nameof(sourceIndex), sourceIndex, source);
            for (int index = 0; index < count; index++)
            {
                int idest = index + destinationIndex;
                if (idest >= self.Length) { break; }
                int isource = index + sourceIndex;
                if (isource >= source.Length) { break; }
                self[idest] = source[isource];
            }
        }


        //
        // Summary:
        //     Copies a specified number of characters from a specified position in this instance
        //     to a specified position in an array of Unicode characters.
        //
        // Parameters:
        //   sourceIndex:
        //     The index of the first character in this instance to copy.
        //
        //   destination:
        //     An array of Unicode characters to which characters in this instance are copied.
        //
        //   destinationIndex:
        //     The index in destination at which the copy operation begins.
        //
        //   count:
        //     The number of characters in this instance to copy to destination.
        //
        // Exceptions:
        //   T:System.ArgumentNullException:
        //     destination is null.
        //
        //   T:System.ArgumentOutOfRangeException:
        //     sourceIndex, destinationIndex, or count is negative -or- sourceIndex does not
        //     identify a position in the current instance. -or- destinationIndex does not identify
        //     a valid index in the destination array. -or- count is greater than the length
        //     of the substring from sourceIndex to the end of this instance -or- count is greater
        //     than the length of the subarray from destinationIndex to the end of the destination
        //     array.
        //public void CopyTo(int sourceIndex, char[] destination, int destinationIndex, int count);
    }
}
