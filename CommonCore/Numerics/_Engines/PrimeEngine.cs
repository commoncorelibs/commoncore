﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Numerics;
using System.Text;
using CommonCore;

namespace CommonCore.Numerics
{
    // 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97
    // Possible Useful Info:
    // https://primes.utm.edu/nthprime/index.php
    // https://stackoverflow.com/a/9704912/6739316
    // https://bitbucket.org/dafis/javaprimes/src/default/
    /// <summary>
    /// The <see cref="PrimeEngine" /> <c>class</c> provides a state-driven means of generating
    /// the unbounded sequence of triangular values, ie 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41,
    /// etc. This engine DOES NOT support the ability to store the state of the engine and resume generation from that
    /// arbitrary point. Ideally this feature will be implemented in the future.
    /// </summary>
    /// <seealso href="https://en.wikipedia.org/wiki/Prime_number">Wikipedia Prime</seealso>
    /// <seealso href="https://oeis.org/A000040">OEIS Prime</seealso>
    public partial class PrimeEngine : ISequenceEngine<PrimeEngine, long>
    {
        private static readonly int[] _Steps = new[] { 2, 4, 2, 4, 6, 2, 6, 4 };
        /// <summary>
        /// Determines if the specified <paramref name="value"/> is a prime number.
        /// </summary>
        /// <param name="value">The value to be checked.</param>
        /// <returns><c>true</c> if <paramref name="value"/> is a prime number; otherwise <c>false</c>.</returns>
        public static bool IsPrime(long value)
        {
            // Negatives, 0, and 1 are not prime.
            if (value < 2) { return false; }
            // 2, 3, 5, and 7 are prime,
            // but their multiples are not prime.
            else if (value % 2 == 0) { return value == 2; }
            else if (value % 3 == 0) { return value == 3; }
            else if (value % 5 == 0) { return value == 5; }
            else if (value % 7 == 0) { return value == 7; }
            // TODO: Investigate if adding more of these clauses improves performance.
            // Remember to update the steps if the starting point changes.
            //else if (value % 11 == 0) { return value == 11; }
            //else if (value % 13 == 0) { return value == 13; }
            else
            {
                // Only need to check factors up to the square root of the value.
                long root = (long)Math.Sqrt(value);
                // Check only odd factors that aren't already covered based on the steps pattern.
                int step = 0;
                for (long n = 11; n <= root; step = (step++ % 8), n += _Steps[step])
                { if (value % n == 0) { return false; } }
                return true;
            }
        }

        /// <summary>
        /// Calculates an approximate value for the prime at the specified <paramref name="index"/>.
        /// </summary>
        /// <param name="index">The zero-based index of the prime value.</param>
        /// <returns>An approximate value for the prime at the specified <paramref name="index"/>.</returns>
        public static BigInteger CalApproximatePrime(long index)
        {
            // Math Source: https://github.com/pinkbegemot/Robustly-Prime-Numbers/blob/9c8fe7d91519914af575b573f8f0d8186d840a16/RPNgenerator.cs#L187
            // The math assumes one-based indexing. Silly mathemagicians.
            double nth = (double)index + 1;
            double prime;
            if (index < 0) { prime = 0; }
            else if (index == 0) { return 2; }
            else if (index == 1) { return 3; }
            else if (index == 2) { return 5; }
            else if (index == 3) { return 7; }
            else if (index == 4) { return 11; }
            else if (index < 7021) { prime = nth * Math.Log(nth) + nth * Math.Log(Math.Log(nth)); }
            else { prime = nth * Math.Log(nth) + nth * (Math.Log(Math.Log(nth)) - 0.9385); }
            return (BigInteger)prime;
        }

        /// <summary>
        /// Calculates a <see cref="BitArray" />, using the sieve of Eratosthenes, suitable for
        /// sieving prime values less than or equal to the specified <paramref name="maxValue" />.
        /// The returned <see cref="BitArray" /> has a maximum size of <see cref="int.MaxValue" />,
        /// so the value of <paramref name="maxValue" /> must be less than
        /// <see cref="int.MaxValue" /> as the underlying implementation initializes the array to
        /// <c><paramref name="maxValue" /> + 1</c>.
        /// </summary>
        /// <param name="maxValue">The maximum value from which to sieve lesser valued prime values.</param>
        /// <returns>
        /// A <see cref="BitArray" />, using the sieve of Eratosthenes, suitable for sieving prime values.
        /// </returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// If <paramref name="maxValue" /> is equal to <see cref="int.MaxValue" />.
        /// </exception>
        /// <seealso href="https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes">
        /// Wikipedia Sieve of Eratosthenes
        /// </seealso>
        /// <seealso href="https://oeis.org/wiki/Sieve_of_Eratosthenes">OEIS Wiki Sieve of Eratosthenes</seealso>
        /// <seealso href="https://primes.utm.edu/glossary/page.php?sort=SieveOfEratosthenes">
        /// UTM Sieve of Eratosthenes
        /// </seealso>
        public static BitArray SieveOfEratosthenes(int maxValue)
        // Possible improvements, especially raising the range to uint: https://stackoverflow.com/a/18139477/6739316
        {
            // TODO: This check is nonsense. The int will never be greater than max int.
            Throw.If.Arg.IsGreaterEqual(nameof(maxValue), maxValue, int.MaxValue);
            var bits = new BitArray(maxValue + 1, true);
            bits[0] = bits[1] = false;
            for (long i = 0; i * i <= maxValue; i++)
            { if (bits[(int)i]) { for (long j = i * i; j <= maxValue; j += i) { bits[(int)j] = false; } } }
            return bits;
        }

        /// <summary>
        /// Calculates a <see cref="BitArray" />, using the sieve of Eratosthenes, suitable for
        /// sieving prime values less than or equal to the specified <paramref name="maxValue" />.
        /// </summary>
        /// <param name="maxValue">The maximum value from which to sieve lesser valued prime values.</param>
        /// <returns>
        /// A <see cref="BitArray" />, using the sieve of Sundaram, suitable for sieving prime values.
        /// </returns>
        /// <seealso href="https://en.wikipedia.org/wiki/Sieve_of_Sundaram">
        /// Wikipedia Sieve of Sundaram
        /// </seealso>
        public static BitArray SieveOfSundaram(int maxValue)
        {
            maxValue /= 2;
            BitArray bits = new BitArray(maxValue + 1, true);
            for (long i = 1; 3 * i + 1 < maxValue; i++)
            { for (long j = 1; i + j + 2 * i * j <= maxValue; j++) { bits[(int)(i + j + 2 * i * j)] = false; } }
            return bits;
        }

        // https://en.wikipedia.org/wiki/Sieve_of_Atkin
        // https://cr.yp.to/primegen.html
        // https://stackoverflow.com/questions/1569127/c-implementation-of-the-sieve-of-atkin
        //public static ? SieveOfAtkin(?);

        /// <summary>
        /// Generates the bounded sequence of prime values, ie 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, etc,
        /// up to the inclusive bounds of <see cref="int.MaxValue"/> using the sieve of Eratosthenes. This sequence is
        /// bounded by the specified <paramref name="limitValueIndex"/>. Do to underlying implementation restrictions,
        /// that results in the sequence having a maximum upper bound of generating <c>105,097,565</c> prime values, with
        /// prime index <c>105,097,564</c> having a value of <c>2,147,483,647</c> which is equal to <see cref="int.MaxValue"/>.
        /// </summary>
        /// <param name="limitValueIndex">The zero-based index of the prime value used to limit the sequence.</param>
        /// <returns>The bounded sequence of prime values.</returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// If <paramref name="limitValueIndex" /> is greater than or equal to <c>105,097,564</c>.
        /// </exception>
        /// <seealso href="https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes">
        /// Wikipedia Sieve of Eratosthenes
        /// </seealso>
        /// <seealso href="https://oeis.org/wiki/Sieve_of_Eratosthenes">OEIS Wiki Sieve of Eratosthenes</seealso>
        /// <seealso href="https://primes.utm.edu/glossary/page.php?sort=SieveOfEratosthenes">
        /// UTM Sieve of Eratosthenes
        /// </seealso>
        public static IEnumerable<long> PrimeEratosthenesSieveSequence(int limitValueIndex)
        {
            // The prime of index N=105_097_565 is equal to int.MaxValue.
            Throw.If.Arg.IsGreaterEqual(nameof(limitValueIndex), limitValueIndex, 105_097_565);
            int maxValue = (int)CalApproximatePrime(limitValueIndex);
            var bits = PrimeEngine.SieveOfEratosthenes(maxValue);
            for (long index = 0; index <= maxValue; index++)
            { if (bits[(int)index]) { yield return index; } }
        }

        /// <summary>
        /// Generates the bounded sequence of prime values, ie 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, etc,
        /// up to the inclusive bounds of <see cref="int.MaxValue"/> using the sieve of Sundaram. This sequence is
        /// bounded by the specified <paramref name="limitValueIndex"/>. Do to underlying implementation restrictions,
        /// that results in the sequence having a maximum upper bound of generating <c>105,097,565</c> prime values, with
        /// prime index <c>105,097,564</c> having a value of <c>2,147,483,647</c> which is equal to <see cref="int.MaxValue"/>.
        /// </summary>
        /// <param name="limitValueIndex">The zero-based index of the prime value used to limit the sequence.</param>
        /// <returns>The bounded sequence of prime values.</returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// If <paramref name="limitValueIndex" /> is greater than or equal to <c>105,097,564</c>.
        /// </exception>
        /// <seealso href="https://en.wikipedia.org/wiki/Sieve_of_Sundaram">
        /// Wikipedia Sieve of Sundaram
        /// </seealso>
        public static IEnumerable<long> PrimeSundaramSieveSequence(int limitValueIndex)
        {
            // The prime of index N=105_097_565 is equal to int.MaxValue.
            Throw.If.Arg.IsGreaterEqual(nameof(limitValueIndex), limitValueIndex, 105_097_565);
            int maxValue = (int)CalApproximatePrime(limitValueIndex);
            var bits = PrimeEngine.SieveOfSundaram(maxValue);
            yield return 2;
            long value = 3;
            for (long index = 1; value <= maxValue; index++, value = 2 * index + 1)
            { if (bits[(int)index]) { yield return value; } }
        }

        /// <summary>
        /// Generates the bounded sequence of prime values, ie 2, 3, 5, 7, 11, 13, 17, 19, 23, 29,
        /// 31, 37, 41, etc, up to the inclusive bounds of <see cref="int.MaxValue" />. This is an
        /// extremely naive implementation and, after yielding <c>2</c>, simply iterates through all
        /// the odd numbers checking each one for primality, yielding the value if it is prime.
        /// Currently the sequence has a maximum upper bound of generating <c>105,097,565</c> prime
        /// values, with prime index <c>105,097,564</c> having a value of <c>2,147,483,647</c> which
        /// is equal to <see cref="int.MaxValue" />.
        /// </summary>
        /// <returns>The bounded sequence of prime values.</returns>
        public static IEnumerable<long> PrimeNaiveSequence()
        {
            yield return 2;
            for (long value = 3; value < int.MaxValue; value += 2)
            { if (PrimeEngine.IsPrime(value)) { yield return value; } }
        }








        /// <summary>
        /// Initializes a <see cref="PrimeEngine" /> instance suitable for generating the bounded
        /// sequence of prime values, ie 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, etc,
        /// up to the inclusive bounds of <see cref="int.MaxValue"/>.
        /// </summary>
        /// <param name="sieveType">The value specifying which type of sieve to use.</param>
        /// <param name="limitValueIndex">The zero-based index of the prime value used to limit the sequence.</param>
        /// <exception cref="ArgumentOutOfRangeException">
        /// If <paramref name="sieveType"/> is not a valid <see cref="EPrimeSieveType"/> value;
        /// or <paramref name="limitValueIndex" /> is greater than or equal to <c>105,097,564</c>.
        /// </exception>
        public PrimeEngine(EPrimeSieveType sieveType = EPrimeSieveType.Naive, int limitValueIndex = -1)
        {
            Throw.If.Arg.IsEnumOutOfRange(nameof(sieveType), sieveType);
            // The prime of index 105_097_565 is equal to int.MaxValue.
            Throw.If.Arg.IsGreaterEqual(nameof(limitValueIndex), limitValueIndex, 105_097_565);
            this.SieveType = sieveType;
            this.LimitValueIndex = limitValueIndex;
            if (this.SieveType == EPrimeSieveType.Sundaram)
            {
                this.LimitValue = ((int)CalApproximatePrime(this.LimitValueIndex)).Min(int.MaxValue);
                this.SieveBits = PrimeEngine.SieveOfSundaram(this.LimitValue);
            }
            else if (this.SieveType == EPrimeSieveType.Eratosthenes)
            {
                this.LimitValue = ((int)CalApproximatePrime(this.LimitValueIndex)).Min(int.MaxValue);
                this.SieveBits = PrimeEngine.SieveOfEratosthenes(this.LimitValue);
            }
            else
            {
                this.LimitValueIndex = -1;
                this.LimitValue = -1;
                this.SieveBits = null;
            }
        }

        /// <summary>
        /// Gets data about <see cref="PrimeEngine" /> state stamps.
        /// Stamps contain <c>2</c> sections and are of the format 'index:value'.
        /// </summary>
        public (string format, int count) StampDetails { get; } = (null, 0);

        /// <summary>
        /// Gets the type of algorithm used to generate the prime sequence.
        /// </summary>
        public EPrimeSieveType SieveType { get; } = EPrimeSieveType.Naive;

        /// <summary>
        /// Gets the index used by various sieve algorithms to limit the generation of the sequence.
        /// </summary>
        public int LimitValueIndex { get; } = -1;

        /// <summary>
        /// Gets the value used by various sieve algorithms to limit the generation of the sequence.
        /// </summary>
        public int LimitValue { get; } = -1;

        /// <summary>
        /// Gets the array of bits used by various sieve algorithms.
        /// </summary>
        public BitArray SieveBits { get; } = null;

        /// <inheritdoc/>
        public long Index { get; protected set; } = 0;

        /// <inheritdoc/>
        public long Value { get; protected set; } = 2;

        /// <inheritdoc/>
        public PrimeEngine Reset()
        {
            this.Index = 0;
            this.Value = 2;
            return this;
        }

        /// <inheritdoc/>
        /// <exception cref="NotSupportedException">This method is not supported by the sequence engine.</exception>
        public PrimeEngine LoadStamp(string stamp)
            => throw new NotSupportedException("This sequence engine does not support arbitrary state reinitialization.");

        /// <inheritdoc/>
        /// <exception cref="NotSupportedException">This method is not supported by the sequence engine.</exception>
        public string ToStamp()
            => throw new NotSupportedException("This sequence engine does not support arbitrary state reinitialization.");

        /// <summary>
        /// Returns an enumerator that iterates through the bounded sequence of prime values, ie 2,
        /// 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, etc, using the sieve defined by <see cref="SieveType" />.
        /// </summary>
        /// <returns>An enumerator that iterates through the unbounded sequence of prime values.</returns>
        public IEnumerator<long> GetEnumerator()
        {
            if (this.SieveType == EPrimeSieveType.Sundaram)
            {
                this.Value = 2;
                yield return 2;
                this.Index++;
                int value = 3;
                for (int index = 1; value <= this.LimitValue; index++, value = 2 * index + 1)
                {
                    if (!this.SieveBits[index]) { continue; }
                    this.Value = value;
                    yield return value;
                    this.Index++;
                }
            }
            else if (this.SieveType == EPrimeSieveType.Eratosthenes)
            {
                for (int index = 0; index <= this.LimitValue; index++)
                {
                    if (!this.SieveBits[index]) { continue; }
                    this.Value = index;
                    yield return index;
                    this.Index++;
                }
            }
            else
            {
                if (this.Index == 0)
                {
                    yield return this.Value;
                    this.Index++;
                    this.Value = 3;
                }
                for (long value = this.Value; value < int.MaxValue; value += 2)
                {
                    if (!PrimeEngine.IsPrime(value)) { continue; }
                    this.Value = value;
                    yield return this.Value;
                    this.Index++;
                }
            }
        }

        /// <inheritdoc cref="IEnumerable.GetEnumerator"/>
        [ExcludeFromCodeCoverage]
        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
    }
}
