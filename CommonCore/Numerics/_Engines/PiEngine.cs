﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Numerics;

namespace CommonCore.Numerics
{
    /// <summary>
    /// The <see cref="PiEngine" /> <c>class</c> provides a state-driven means of generating the
    /// unbounded sequence representing the factional digits of PI, ie 1, 4, 1, 5, 9, 2, 6, 5, 3, 5,
    /// etc, with the ability to store the state of the engine and resume generation from that
    /// arbitrary point. The <see cref="PiEngine" /> uses the spigot method, as
    /// <see href="https://www.cs.ox.ac.uk/jeremy.gibbons/publications/spigot.pdf">documented by
    /// Jeremy Gibbons</see>, for generating digits of base-10 <see href="https://en.wikipedia.org/wiki/Pi">PI</see>.
    /// </summary>
    /// <remarks>
    /// <para>
    /// In addition to tracking the digit and state data for the current position of PI, the engine
    /// also tracks the index of the current digit. This provides the only hard-coded limit to digit
    /// generation; the index is stored as a <see cref="long" />, ie 64bit Integer, with a maximum
    /// value of 9,223,372,036,854,775,807, that is slightly more than 9.223 quintrillion positions.
    /// It is unlikely that limit will be reached before an out-of-memory exception is thrown, but
    /// might as well future-proof to 9 quintrillion instead of only 2 (<see cref="int" />) or 4 (<see cref="uint" />) billion.
    /// </para>
    /// <para>
    /// The memory footprint of the algorithm, the computation time of each step, and the size of each
    /// successive state stamp grows at a fairly constant rate. Examples of this increase can be seen
    /// in the following table. Listed are the index of the digit, the character length of the state
    /// stamp, the human-readable byte size of the state stamp, and the human-readable time needed to
    /// calculate the sequence from index zero.
    /// </para>
    /// <list type="table">
    /// <listheader>
    /// <term>Index</term>
    /// <description>Stamp Length</description> |
    /// <description>Stamp Size</description> |
    /// <description>Sequence Time</description>
    /// </listheader>
    /// <item>
    /// <term>0</term>
    /// <description>23</description> |
    /// <description>46.00 B</description> |
    /// <description>0.013 s</description>
    /// </item>
    /// <item>
    /// <term>10</term>
    /// <description>178</description> |
    /// <description>356.00 B</description> |
    /// <description>0.000 s</description>
    /// </item>
    /// <item>
    /// <term>100</term>
    /// <description>2425</description> |
    /// <description>4.74 KB</description> |
    /// <description>0.002 s</description>
    /// </item>
    /// <item>
    /// <term>1000</term>
    /// <description>33778</description> |
    /// <description>65.97 KB</description> |
    /// <description>0.110 s</description>
    /// </item>
    /// <item>
    /// <term>10000</term>
    /// <description>437306</description> |
    /// <description>854.11 KB</description> |
    /// <description>10.254 s</description>
    /// </item>
    /// <item>
    /// <term>20000</term>
    /// <description>934671</description> |
    /// <description>1.78 MB</description> |
    /// <description>43.757 s</description>
    /// </item>
    /// <item>
    /// <term>30000</term>
    /// <description>1454563</description> |
    /// <description>2.77 MB</description> |
    /// <description>1.735 m</description>
    /// </item>
    /// <item>
    /// <term>40000</term>
    /// <description>1989208</description> |
    /// <description>3.79 MB</description> |
    /// <description>2.628 m</description>
    /// </item>
    /// <item>
    /// <term>50000</term>
    /// <description>2534915</description> |
    /// <description>4.83 MB</description> |
    /// <description>4.889 m</description>
    /// </item>
    /// <item>
    /// <term>60000</term>
    /// <description>3089144</description> |
    /// <description>5.89 MB</description> |
    /// <description>7.252 m</description>
    /// </item>
    /// <item>
    /// <term>70000</term>
    /// <description>3650700</description> |
    /// <description>6.96 MB</description> |
    /// <description>9.957 m</description>
    /// </item>
    /// <item>
    /// <term>80000</term>
    /// <description>4218463</description> |
    /// <description>8.05 MB</description> |
    /// <description>13.585 m</description>
    /// </item>
    /// <item>
    /// <term>90000</term>
    /// <description>4791631</description> |
    /// <description>9.14 MB</description> |
    /// <description>16.631 m</description>
    /// </item>
    /// <item>
    /// <term>100000</term>
    /// <description>5369688</description> |
    /// <description>10.24 MB</description> |
    /// <description>20.275 m</description>
    /// </item>
    /// </list>
    /// </remarks>
    /// <seealso href="https://en.wikipedia.org/wiki/Pi">Wikipedia PI</seealso>
    /// <seealso href="https://oeis.org/A000796">OEIS PI</seealso>
    /// <seealso href="https://www.cs.ox.ac.uk/jeremy.gibbons/publications/spigot.pdf">PI Spigot PDF</seealso>
    public class PiEngine : ISequenceEngine<PiEngine, byte>
    {
        /// <summary>
        /// Gets <c>0</c> as a <see cref="BigInteger"/> for use in generating digits of PI.
        /// </summary>
        protected static BigInteger Big0 { get; } = BigInteger.Zero;

        /// <summary>
        /// Gets <c>1</c> as a <see cref="BigInteger"/> for use in generating digits of PI.
        /// </summary>
        protected static BigInteger Big1 { get; } = BigInteger.One;

        /// <summary>
        /// Gets <c>2</c> as a <see cref="BigInteger"/> for use in generating digits of PI.
        /// </summary>
        protected static BigInteger Big2 { get; } = new BigInteger(2);

        /// <summary>
        /// Gets <c>3</c> as a <see cref="BigInteger"/> for use in generating digits of PI.
        /// </summary>
        protected static BigInteger Big3 { get; } = new BigInteger(3);

        /// <summary>
        /// Gets <c>4</c> as a <see cref="BigInteger"/> for use in generating digits of PI.
        /// </summary>
        protected static BigInteger Big4 { get; } = new BigInteger(4);

        /// <summary>
        /// Gets <c>7</c> as a <see cref="BigInteger"/> for use in generating digits of PI.
        /// </summary>
        protected static BigInteger Big7 { get; } = new BigInteger(7);

        /// <summary>
        /// Initializes a <see cref="PiEngine" /> instance suitable for generating the unbounded
        /// sequence representing the factional digits of PI.
        /// </summary>
        public PiEngine() { }

        /// <summary>
        /// Gets data about <see cref="TriangularEngine" /> state stamps.
        /// Stamps contain <c>8</c> sections and are of the format 'base:index:value:k:l:q:r:t'.
        /// </summary>
        public (string format, int count) StampDetails { get; } = ("{0}:{1}:{2}:{3:R}:{4:R}:{5:R}:{6:R}:{7:R}", 8);

        /// <summary>
        /// Gets the numeric system base used to generate the sequence.
        /// </summary>
        public byte Base { get; protected set; } = 10;

        /// <inheritdoc/>
        public long Index { get; protected set; } = -1;

        /// <inheritdoc/>
        public byte Value { get; protected set; } = 3;

        /// <summary>
        /// Gets the current k-state in the sequence.
        /// </summary>
        public BigInteger K { get; protected set; } = 1;

        /// <summary>
        /// Gets the current l-state in the sequence.
        /// </summary>
        public BigInteger L { get; protected set; } = 3;

        /// <summary>
        /// Gets the current q-state in the sequence.
        /// </summary>
        public BigInteger Q { get; protected set; } = 1;

        /// <summary>
        /// Gets the current r-state in the sequence.
        /// </summary>
        public BigInteger R { get; protected set; } = 0;

        /// <summary>
        /// Gets the current t-state in the sequence.
        /// </summary>
        public BigInteger T { get; protected set; } = 1;

        /// <summary>
        /// Gets the current digit as a <see cref="char"/>.
        /// </summary>
        public char DigitChar => (char)(this.Value + '0');

        /// <inheritdoc/>
        public PiEngine Reset() => this.LoadStamp(10, -1, 3, Big1, Big3, Big1, Big0, Big1);

        /// <inheritdoc/>
        public PiEngine LoadStamp(string stamp)
        {
            Throw.If.Arg.IsNullOrEmpty(nameof(stamp), stamp);
            var sections = stamp.Split(':');
            if (sections.Length != this.StampDetails.count)
            { throw new FormatException($"Argument '{nameof(stamp)}' must contain exactly '{this.StampDetails.count}' colon ':' delimited sections."); }

            // Parse all the values before setting them so we avoid
            // leaving the engine in a partially changed state.
            int sectionIndex = 0;
            var @base = byte.Parse(sections[sectionIndex++]);
            var index = long.Parse(sections[sectionIndex++]);
            var digit = byte.Parse(sections[sectionIndex++]);
            var k = BigInteger.Parse(sections[sectionIndex++]);
            var l = BigInteger.Parse(sections[sectionIndex++]);
            var q = BigInteger.Parse(sections[sectionIndex++]);
            var r = BigInteger.Parse(sections[sectionIndex++]);
            var t = BigInteger.Parse(sections[sectionIndex++]);
            return this.LoadStamp(@base, index, digit, k, l, q, r, t);
        }

        /// <summary>
        /// Sets the engine state based on the specified parameters.
        /// </summary>
        /// <param name="base">The state base value.</param>
        /// <param name="index">The state index value.</param>
        /// <param name="digit">The state digit value.</param>
        /// <param name="k">The k-state value.</param>
        /// <param name="l">The l-state value.</param>
        /// <param name="q">The q-state value.</param>
        /// <param name="r">The r-state value.</param>
        /// <param name="t">The t-state value.</param>
        /// <returns>A fluent reference to <c>this</c> <see cref="PiEngine"/> instance.</returns>
        public PiEngine LoadStamp(byte @base, long index, byte digit, BigInteger k, BigInteger l, BigInteger q, BigInteger r, BigInteger t)
        {
            this.Base = @base;
            this.Value = digit;
            this.Index = index < -1 ? -1 : index;
            this.K = k;
            this.L = l;
            this.Q = q;
            this.R = r;
            this.T = t;
            return this;
        }

        /// <inheritdoc/>
        public string ToStamp()
            => string.Format(this.StampDetails.format, this.Base, this.Index, this.Value, this.K, this.L, this.Q, this.R, this.T);

        /// <summary>
        /// Returns an enumerator that iterates through the unbounded sequence representing the
        /// factional digits of PI, ie 1, 4, 1, 5, 9, 2, 6, 5, 3, 5, etc. This enumerable is
        /// infinite and the calling code is responsible for ending the sequence, such as through
        /// the <see cref="Enumerable.Take" /> method.
        /// </summary>
        /// <returns>
        /// An enumerator that iterates through the unbounded sequence representing the factional
        /// digits of PI.
        /// </returns>
        public IEnumerator<byte> GetEnumerator()
        {
            BigInteger digit = this.Value;
            BigInteger nr = this.Base * (this.R - this.T * digit);
            digit = this.Base * (Big3 * this.Q + this.R) / this.T - this.Base * digit;
            this.Q *= this.Base;
            this.R = nr;

            while (true)
            {
                BigInteger tn = this.T * digit;
                if (Big4 * this.Q + this.R - this.T < tn)
                {
                    this.Index++;
                    this.Value = (byte)(int)digit;
                    yield return this.Value;
                    nr = this.Base * (this.R - tn);
                    digit = this.Base * (Big3 * this.Q + this.R) / this.T - this.Base * digit;
                    this.Q *= this.Base;
                }
                else
                {
                    this.T *= this.L;
                    nr = (Big2 * this.Q + this.R) * this.L;
                    digit = (this.Q * (Big7 * this.K) + Big2 + this.R * this.L) / this.T;
                    this.Q *= this.K;
                    this.L += Big2;
                    ++this.K;
                }
                this.R = nr;
            }
        }

        /// <inheritdoc cref="IEnumerable.GetEnumerator"/>
        [ExcludeFromCodeCoverage]
        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
    }
}
