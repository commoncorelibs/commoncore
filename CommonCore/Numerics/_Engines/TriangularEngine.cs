﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Numerics;

namespace CommonCore.Numerics
{
    // 0, 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 66, 78, 91, 105, 120, 136, 153, 171, 190
    /// <summary>
    /// The <see cref="TriangularEngine" /> <c>class</c> provides a state-driven means of generating
    /// the unbounded sequence of triangular values, ie 0, 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 66,
    /// 78, etc, with the ability to store the state of the engine and resume generation from that
    /// arbitrary point.
    /// <para>
    /// A triangular value is calculated by summing all positive integers less than or equal to its
    /// index. For example, the triangular value for index <c>2</c> is <c>2 + 1 + 0</c> with a
    /// result of <c>3</c>, index <c>3</c> is <c>3 + 2 + 1 + 0</c> equaling <c>6</c>, index <c>4</c>
    /// is <c>4 + 3 + 2 + 1 + 0</c> which equals <c>10</c>, and index <c>10</c> is <c>10 + 9 + 8 + 7
    /// + 6 + 5 + 4 + 3 + 2 + 1 + 0</c> summing as <c>55</c>.
    /// </para>
    /// <para>
    /// Unlike most infinite sequences, the triangular sequence has a formula for calculating the
    /// value of any arbitrary element of the sequence. See
    /// <see cref="TriangularEngine.CalValue(BigInteger)" /> for more information.
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// In addition to tracking the value, the engine also tracks the index of the current value.
    /// This provides the only hard-coded limit to generation; the index is stored as a
    /// <see cref="long" />, ie 64bit Integer, with a maximum value of 9,223,372,036,854,775,807,
    /// that is slightly more than 9.223 quintrillion values. With this particular sequence, it is
    /// theoretically possible this limit could be reached given a non-trival amount of time. That
    /// said, a limit of 9 quintrillion is preferable to only 2 (<see cref="int" />) or 4
    /// (<see cref="uint" />) billion.
    /// </para>
    /// <para>
    /// The size of triangular values grow large over time, though not as drastically as other
    /// infinite sequences; however, due to the index limit of <c>9,223,372,036,854,775,807</c>, no
    /// value larger than the max value of <see cref="float" /> can be generated.
    /// </para>
    /// <para>
    /// Computation times grow over time as one would expect. Finding values up to index
    /// <c>1_000_000</c> takes almost no time, while values near index <c>10_000_000</c> can be
    /// calculated in a few seconds and those near <c>1_000_000_000</c> can take over a minute. This
    /// is be expected when dealing with such high indexes, such as index <c>10_000_000_000</c>
    /// which took over twenty minutes to calculate, but only contains 20 digits and weighs in at a
    /// mere 9 bytes in size.
    /// </para>
    /// </remarks>
    /// <seealso href="https://en.wikipedia.org/wiki/Triangular_number">Wikipedia Triangular</seealso>
    /// <seealso href="https://oeis.org/A000217">OEIS Triangular</seealso>
    public class TriangularEngine : ISequenceEngine<TriangularEngine, BigInteger>
    {
        /// <summary>
        /// Calculates the triangular sum of the specified <paramref name="index"/>.
        /// The sum is calculated in-place and does not require iterating through the sequence.
        /// </summary>
        /// <param name="index">The index to sum.</param>
        /// <returns>The triangular sum of the specified <paramref name="index"/>.</returns>
        public static BigInteger CalValue(BigInteger index) => (index == 0 ? 0 : (index * (index + 1)) / 2);

        /// <summary>
        /// Initializes a <see cref="TriangularEngine" /> instance suitable for generating the unbounded
        /// sequence of triangular sum values, ie 0, 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 66, 78, etc.
        /// </summary>
        public TriangularEngine() { }

        /// <summary>
        /// Gets data about <see cref="TriangularEngine" /> state stamps.
        /// Stamps contain <c>2</c> sections and are of the format 'index:value'.
        /// </summary>
        public (string format, int count) StampDetails { get; } = ("{0}:{1:R}", 2);

        /// <inheritdoc/>
        public long Index { get; protected set; } = 0;

        /// <inheritdoc/>
        public BigInteger Value { get; protected set; } = BigInteger.Zero;

        /// <inheritdoc/>
        public TriangularEngine Reset() => this.LoadStamp(0, BigInteger.Zero);

        /// <inheritdoc/>
        public TriangularEngine LoadStamp(string stamp)
        {
            Throw.If.Arg.IsNullOrEmpty(nameof(stamp), stamp);
            var sections = stamp.Split(':');
            if (sections.Length != this.StampDetails.count)
            { throw new FormatException($"Argument '{nameof(stamp)}' must contain exactly '{this.StampDetails.count}' colon ':' delimited sections."); }

            // Parse all the values before setting them so we avoid
            // leaving the engine in a partially changed state.
            int sectionIndex = 0;
            var index = long.Parse(sections[sectionIndex++]);
            var value = BigInteger.Parse(sections[sectionIndex++]);
            return this.LoadStamp(index, value);
        }

        /// <summary>
        /// Sets the engine state based on the specified parameters.
        /// </summary>
        /// <param name="index">The state index value.</param>
        /// <param name="sum">The state triangular sum value.</param>
        /// <returns>A fluent reference to <c>this</c> <see cref="TriangularEngine"/> instance.</returns>
        public TriangularEngine LoadStamp(long index, BigInteger sum)
        {
            this.Index = index < 0 ? 0 : index;
            this.Value = sum;
            return this;
        }

        /// <inheritdoc/>
        public string ToStamp()
            => string.Format(this.StampDetails.format, this.Index, this.Value);

        /// <summary>
        /// Returns an enumerator that iterates through the unbounded sequence of triangular sum values,
        /// ie 0, 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 66, 78, etc. This enumerable is infinite and the
        /// calling code is responsible for ending the sequence, such as through the
        /// <see cref="Enumerable.Take" /> method.
        /// </summary>
        /// <returns>
        /// An enumerator that iterates through the unbounded sequence of triangular sum values.
        /// </returns>
        public IEnumerator<BigInteger> GetEnumerator()
        {
            while (true)
            {
                yield return this.Value;
                this.Index++;
                this.Value += this.Index;
            }
        }

        /// <inheritdoc cref="IEnumerable.GetEnumerator"/>
        [ExcludeFromCodeCoverage]
        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
    }
}
