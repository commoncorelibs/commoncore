﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Numerics;

namespace CommonCore.Numerics
{
    /// <summary>
    /// The <see cref="FactorialEngine" /> <c>class</c> provides a state-driven means of generating
    /// the unbounded sequence of factorial values, ie 1, 1, 2, 6, 24, 120, 720, 5040, 40320, etc,
    /// with the ability to store the state of the engine and resume generation from that arbitrary point.
    /// <para>
    /// A factorial is calculated by multiplying a positive integer by all non-zero positive
    /// integers that are less than or equal to it. The factorial value of <c>0</c> is defined as
    /// <c>1</c>. For example, factorial of <c>4</c> would be <c>4 * 3 * 2 * 1</c> with a result of <c>24</c>.
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// In addition to tracking the factorial, the engine also tracks the index of the current
    /// factorial. This provides the only hard-coded limit to generation; the index is stored as a
    /// <see cref="long" />, ie 64bit Integer, with a maximum value of 9,223,372,036,854,775,807,
    /// that is slightly more than 9.223 quintrillion values. It is unlikely that limit will be
    /// reached before an out-of-memory exception is thrown, but might as well future-proof to 9
    /// quintrillion instead of only 2 ( <see cref="int" />) or 4 ( <see cref="uint" />) billion.
    /// </para>
    /// <para>
    /// The size of factorials grows drastically, surpassing the size of <see cref="int" /> and
    /// <see cref="uint" /> at <c>13!</c>, <see cref="long" /> and <see cref="ulong" /> at
    /// <c>21!</c>, <see cref="decimal" /> at <c>28!</c>, <see cref="float" /> at <c>35!</c>, and
    /// finally <see cref="double" /> at <c>171!</c>. However, <see cref="BigInteger" /> has no
    /// defined maximum value and can support arbitrarily large factorial values dependent only on
    /// the available memory resources.
    /// </para>
    /// <para>
    /// Computation times are as one would expect. Finding values up to <c>1_000!</c> takes almost
    /// no time, while values near <c>100_000!</c> can be calculated in a few seconds and those near
    /// <c>400_000!</c> can take over a minute. This is be expected when dealing with such
    /// ridiculously huge values, such as <c>500_000!</c> whose value is composed of over
    /// two-and-a-half million digits (2,632,342 to be exact), took nearly two minutes to calculate,
    /// and weighs in at just over a megabyte in size.
    /// </para>
    /// </remarks>
    /// <seealso href="https://en.wikipedia.org/wiki/Factorial">Wikipedia Factorial</seealso>
    /// <seealso href="https://oeis.org/A000142">OEIS Factorial</seealso>
    public class FactorialEngine : ISequenceEngine<FactorialEngine, BigInteger>
    {
        /// <summary>
        /// Initializes a <see cref="FactorialEngine" /> instance suitable for generating the unbounded
        /// sequence of factorial values, ie 1, 1, 2, 6, 24, 120, 720, 5040, 40320, etc.
        /// </summary>
        public FactorialEngine() { }

        /// <summary>
        /// Gets data about <see cref="FactorialEngine" /> state stamps.
        /// Stamps contain <c>2</c> sections and are of the format 'index:value'.
        /// </summary>
        public (string format, int count) StampDetails { get; } = ("{0}:{1:R}", 2);

        /// <inheritdoc/>
        public long Index { get; protected set; } = 0;

        /// <inheritdoc/>
        public BigInteger Value { get; protected set; } = BigInteger.One;

        /// <inheritdoc/>
        public FactorialEngine Reset() => this.LoadStamp(0, BigInteger.One);

        /// <inheritdoc/>
        public FactorialEngine LoadStamp(string stamp)
        {
            Throw.If.Arg.IsNullOrEmpty(nameof(stamp), stamp);
            var sections = stamp.Split(':');
            if (sections.Length != this.StampDetails.count)
            { throw new FormatException($"Argument '{nameof(stamp)}' must contain exactly '{this.StampDetails.count}' colon ':' delimited sections."); }

            // Parse all the values before setting them so we avoid
            // leaving the engine in a partially changed state.
            int sectionIndex = 0;
            var index = long.Parse(sections[sectionIndex++]);
            var factorial = BigInteger.Parse(sections[sectionIndex++]);
            return this.LoadStamp(index, factorial);
        }

        /// <summary>
        /// Sets the engine state based on the specified parameters.
        /// </summary>
        /// <param name="index">The state index value.</param>
        /// <param name="factorial">The state factorial value.</param>
        /// <returns>A fluent reference to <c>this</c> <see cref="FactorialEngine"/> instance.</returns>
        public FactorialEngine LoadStamp(long index, BigInteger factorial)
        {
            this.Index = index < 0 ? 0 : index;
            this.Value = factorial;
            return this;
        }

        /// <inheritdoc/>
        public string ToStamp()
            => string.Format(this.StampDetails.format, this.Index, this.Value);

        /// <summary>
        /// Returns an enumerator that iterates through the unbounded sequence of factorial values,
        /// ie 1, 1, 2, 6, 24, 120, 720, 5040, 40320, etc. This enumerable is infinite and the
        /// calling code is responsible for ending the sequence, such as through the
        /// <see cref="Enumerable.Take" /> method.
        /// </summary>
        /// <returns>
        /// An enumerator that iterates through the unbounded sequence of factorial values.
        /// </returns>
        public IEnumerator<BigInteger> GetEnumerator()
        {
            while (true)
            {
                yield return this.Value;
                this.Index++;
                this.Value *= this.Index;
            }
        }

        /// <inheritdoc cref="IEnumerable.GetEnumerator"/>
        [ExcludeFromCodeCoverage]
        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
    }
}
