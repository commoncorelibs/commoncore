﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Numerics;

namespace CommonCore.Numerics
{
    /// <summary>
    /// The <see cref="FibonacciEngine" /> <c>class</c> provides a state-driven means of generating
    /// the unbounded sequence of fibonacci values, ie 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144,
    /// etc, with the ability to store the state of the engine and resume generation from that
    /// arbitrary point.
    /// <para>
    /// A fibonacci value is calculated by summing the values of the two preceding items in the
    /// sequence. The first two values of the fibonacci sequence are defined as <c>0</c> and
    /// <c>1</c>. For example, the fibonacci value for index <c>2</c> is <c>1 + 0</c> with a result
    /// of <c>1</c>, index <c>3</c> is <c>1 + 1</c> equaling <c>2</c>, and index <c>4</c> is <c>2 +
    /// 1</c> equaling <c>3</c>.
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// In addition to tracking the value, the engine also tracks the index of the current value.
    /// This provides the only hard-coded limit to generation; the index is stored as a
    /// <see cref="long" />, ie 64bit Integer, with a maximum value of 9,223,372,036,854,775,807,
    /// that is slightly more than 9.223 quintrillion values. It is unlikely that limit will be
    /// reached before an out-of-memory exception is thrown, but might as well future-proof to 9
    /// quintrillion instead of only 2 ( <see cref="int" />) or 4 ( <see cref="uint" />) billion.
    /// </para>
    /// <para>
    /// The size of fibonacci values grow drastically, surpassing the size of <see cref="int" /> at
    /// index <c>47</c>, <see cref="uint" /> at index <c>48</c>, <see cref="long" /> at index
    /// <c>93</c>, <see cref="ulong" /> at index <c>94</c>, <see cref="decimal" /> at index
    /// <c>140</c>, <see cref="float" /> at index <c>187</c>, and finally <see cref="double" /> at
    /// index <c>1477</c>. However, <see cref="BigInteger" /> has no defined maximum value and can
    /// support arbitrarily large values dependent only on the available memory resources.
    /// </para>
    /// <para>
    /// Computation times grow over time as one would expect. Finding values up to index
    /// <c>1_000</c> takes almost no time, while values near index <c>300_000</c> can be calculated
    /// in a few seconds and those near <c>2_000_000</c> can take over a minute. This is be expected
    /// when dealing with such ridiculously huge values, such as index <c>5_000_000</c> whose value
    /// is composed of just over a million digits (1,044,938 to be exact), took nearly ten minutes
    /// to calculate, and weighs in at just under half a megabyte in size.
    /// </para>
    /// </remarks>
    /// <seealso href="https://en.wikipedia.org/wiki/Fibonacci_number">Wikipedia Fibonacci</seealso>
    /// <seealso href="https://oeis.org/A000045">OEIS Fibonacci</seealso>
    public class FibonacciEngine : ISequenceEngine<FibonacciEngine, BigInteger>
    {
        /// <summary>
        /// Initializes a <see cref="FibonacciEngine" /> instance suitable for generating the unbounded
        /// sequence of fibonacci values, ie 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, etc.
        /// </summary>
        public FibonacciEngine() { }

        /// <summary>
        /// Gets data about <see cref="FibonacciEngine" /> state stamps.
        /// Stamps contain <c>3</c> sections and are of the format 'index:value:previous'.
        /// </summary>
        public (string format, int count) StampDetails { get; } = ("{0}:{1:R}:{2:R}", 3);

        /// <inheritdoc/>
        public long Index { get; protected set; } = 0;

        /// <inheritdoc/>
        public BigInteger Value { get; protected set; } = BigInteger.Zero;

        /// <summary>
        /// Gets the previous value in the sequence.
        /// </summary>
        public BigInteger Previous { get; protected set; } = BigInteger.Zero;

        /// <inheritdoc/>
        public FibonacciEngine Reset() => this.LoadStamp(0, BigInteger.Zero, BigInteger.Zero);

        /// <inheritdoc/>
        public FibonacciEngine LoadStamp(string stamp)
        {
            Throw.If.Arg.IsNullOrEmpty(nameof(stamp), stamp);
            var sections = stamp.Split(':');
            if (sections.Length != this.StampDetails.count)
            { throw new FormatException($"Argument '{nameof(stamp)}' must contain exactly '{this.StampDetails.count}' colon ':' delimited sections."); }

            // Parse all the values before setting them so we avoid
            // leaving the engine in a partially changed state.
            int sectionIndex = 0;
            var index = long.Parse(sections[sectionIndex++]);
            var value = BigInteger.Parse(sections[sectionIndex++]);
            var previous = BigInteger.Parse(sections[sectionIndex++]);
            return this.LoadStamp(index, value, previous);
        }

        /// <summary>
        /// Sets the engine state based on the specified parameters.
        /// </summary>
        /// <param name="index">The state index value.</param>
        /// <param name="value">The state fibonacci value.</param>
        /// <param name="previous">The state previous fibonacci value.</param>
        /// <returns>A fluent reference to <c>this</c> <see cref="FibonacciEngine"/> instance.</returns>
        public FibonacciEngine LoadStamp(long index, BigInteger value, BigInteger previous)
        {
            this.Index = index < 0 ? 0 : index;
            this.Value = value;
            this.Previous = previous;
            return this;
        }

        /// <inheritdoc/>
        public string ToStamp()
            => string.Format(this.StampDetails.format, this.Index, this.Value, this.Previous);

        /// <summary>
        /// Returns an enumerator that iterates through the unbounded sequence of fibonacci values,
        /// ie 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, etc. This enumerable is infinite and the
        /// calling code is responsible for ending the sequence, such as through the
        /// <see cref="Enumerable.Take" /> method.
        /// </summary>
        /// <returns>
        /// An enumerator that iterates through the unbounded sequence of fibonacci values.
        /// </returns>
        public IEnumerator<BigInteger> GetEnumerator()
        {
            if (this.Index != 0) { yield return this.Value; }
            else
            {
                this.Value = BigInteger.Zero;
                this.Previous = BigInteger.Zero;
                yield return this.Value;
                this.Index++;
                this.Value = BigInteger.One;
                this.Previous = BigInteger.Zero;
                yield return this.Value;
            }

            while (true)
            {
                this.Index++;
                var temp = this.Value;
                this.Value += this.Previous;
                this.Previous = temp;
                yield return this.Value;
            }
        }

        /// <inheritdoc cref="IEnumerable.GetEnumerator"/>
        [ExcludeFromCodeCoverage]
        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
    }
}
