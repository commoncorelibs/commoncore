﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace CommonCore.Numerics
{
    /// <summary>
    /// The <see cref="ISequence{TValue}" /><c>generic</c><c>interface</c> represents a
    /// <see cref="IEnumerable{T}" /> indexed sequence of <typeparamref name="TValue" /> typed
    /// <see cref="IComparable{T}" /> values.
    /// </summary>
    /// <typeparam name="TValue">The type of the sequence values.</typeparam>
    public interface ISequence<TValue> : IEnumerable<TValue>
        where TValue : IComparable<TValue>
    {
        /// <summary>
        /// Gets the current index in the sequence.
        /// </summary>
        long Index { get; }

        /// <summary>
        /// Gets the current in the sequence.
        /// </summary>
        TValue Value { get; }

        ///// <inheritdoc cref="IEnumerable.GetEnumerator"/>
        //[ExcludeFromCodeCoverage]
        //IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
    }
}
