﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Numerics;

namespace CommonCore.Numerics
{
    // Possible improvements for sqrt methods:
    // https://stackoverflow.com/questions/3432412/calculate-square-root-of-a-biginteger-system-numerics-biginteger
    /// <summary>
    /// The <see cref="BigIntegerExtensions"/> <c>static</c> <c>class</c> provides
    /// extension methods related to <see cref="System.Numerics.BigInteger"/> objects.
    /// </summary>
    public static class BigIntegerExtensions
    {
        /// <summary>
        /// Calculates the count of digits in this <see cref="BigInteger" />.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>The count of digits in this <see cref="BigInteger" />.</returns>
        public static long GetDigitCount(this BigInteger self)
            //=> self.IsZero ? 1 : (long)Math.Floor(BigInteger.Log10(self) + 1);
            => self.IsZero ? 1 : (long)Math.Floor(BigInteger.Log10(self > BigInteger.Zero ? self : -self)) + 1;

        /// <summary>
        /// Determines if the specified <paramref name="root" /> represents an integer square root
        /// of this <see cref="BigInteger" />.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="root">The value to process.</param>
        /// <returns>
        /// <c>true</c> if the specified <paramref name="root" /> represents an integer square root
        /// of this <see cref="BigInteger" />; otherwise <c>false</c>.
        /// </returns>
        public static bool HasSqrt(this BigInteger self, BigInteger root)
        {
            if (self <= BigInteger.Zero) { return false; }
            var min = root * root;
            return (self >= min && self <= min + root + root);
        }

        /// <summary>
        /// Calculates the integer square root of this <see cref="BigInteger" /> if it is a positive
        /// number greater than or equal to one; otherwise return zero.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <returns>
        /// The integer square root of this <see cref="BigInteger" /> if it is a positive number
        /// greater than or equal to one; otherwise return zero.
        /// </returns>
        public static BigInteger Sqrt(this BigInteger self)
        {
            if (self <= BigInteger.Zero) { return BigInteger.Zero; }
            int bits = (int)Math.Ceiling(BigInteger.Log(self, 2));
            var root = BigInteger.One << (bits >> 1);
            while (!self.HasSqrt(root)) { root = (root + (self / root)) >> 1; }
            return root;
        }
    }
}
