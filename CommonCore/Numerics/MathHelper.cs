﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace CommonCore.Numerics
{
    /// <summary>
    /// The <see cref="MathHelper"/> <c>static</c> <c>class</c> provides methods
    /// and members related to mathematics.
    /// </summary>
    public static partial class MathHelper
    {
        /// <summary>
        /// Represents the common value. "In mathematics, two quantities are in the golden ratio if
        /// their ratio is the same as the ratio of their sum to the larger of the two quantities."
        /// -- Wikipedia
        /// </summary>
        /// <seealso href="https://en.wikipedia.org/wiki/Golden_ratio">Wikipedia Golden Ratio</seealso>
        public const double GoldenRatio = 1.618033988749894;

        /// <summary>
        /// Represents the conjugate of the <see cref="GoldenRatio"/>.
        /// </summary>
        /// <seealso href="https://en.wikipedia.org/wiki/Golden_ratio#Golden_ratio_conjugate">Wikipedia Golden Ratio Conjugate</seealso>
        public const double GoldenRatioConjugate = 0.618033988749894;

        /// <summary>
        /// Represents the ratio of the circumference of a circle to its diameter, specified by the
        /// constant, π.
        /// </summary>
        /// <seealso href="https://en.wikipedia.org/wiki/Pi">Wikipedia </seealso>
        public const double PI = 3.141592653589793;

        /// <summary>
        /// Represents the natural logarithmic base, specified by the constant, e.
        /// </summary>
        /// <seealso href="https://en.wikipedia.org/wiki/E_(mathematical_constant)">Wikipedia </seealso>
        public const double E = 2.718281828459045;

        /// <summary>
        /// Represents the irrational number square root 2.
        /// </summary>
        /// <seealso href="https://en.wikipedia.org/wiki/Square_root_of_2">Wikipedia Sqrt 2</seealso>
        /// <seealso href="https://oeis.org/A002193">OEIS Sqrt 2</seealso>
        public const double Sqrt2 = 1.414213562373095;

        /// <summary>
        /// Represents the irrational number square root 3.
        /// </summary>
        /// <seealso href="https://en.wikipedia.org/wiki/Square_root_of_3">Wikipedia Sqrt 3</seealso>
        /// <seealso href="https://oeis.org/A002194">OEIS Sqrt 3</seealso>
        public const double Sqrt3 = 1.732050807568877;

        /// <summary>
        /// Represents the irrational number square root 5.
        /// </summary>
        /// <seealso href="https://en.wikipedia.org/wiki/Square_root_of_5">Wikipedia Sqrt 5</seealso>
        /// <seealso href="https://oeis.org/A040002">OEIS Sqrt 5</seealso>
        public const double Sqrt5 = 2.236067977499789;

        /// <summary>
        /// Calculates the <see cref="double"/> ratio between the specified <paramref name="left"/>
        /// and <paramref name="right"/> values. Result will be <c>0</c> if either value is <c>0</c>.
        /// </summary>
        /// <param name="left">The left value.</param>
        /// <param name="right">The right value.</param>
        /// <returns>The <see cref="double"/> ratio between the specified <paramref name="left"/>
        /// and <paramref name="right"/> values; <c>0</c> if either value is <c>0</c>.</returns>
        public static double CalRatio(double left, double right)
            => (left == 0 || right == 0 ? 0 : left / right);

        /// <summary>
        /// Calculates the <see cref="double"/> percentage between the specified <paramref name="left"/>
        /// and <paramref name="right"/> values. Result will be <c>0</c> if either value is <c>0</c>.
        /// This value is functionally equivalent to ratio of the values multiplied by <c>100</c>.
        /// </summary>
        /// <param name="left">The left value.</param>
        /// <param name="right">The right value.</param>
        /// <returns>The <see cref="double"/> percentage between the specified <paramref name="left"/>
        /// and <paramref name="right"/> values; <c>0</c> if either value is <c>0</c>.</returns>
        public static double CalPercent(double left, double right)
            => (left == 0 || right == 0 ? 0 : (left / right) * 100.0);

        /// <inheritdoc cref="TriangularEngine.CalValue(BigInteger)"/>
        public static BigInteger CalTriangularSum(BigInteger index)
            => TriangularEngine.CalValue(index);

        /// <inheritdoc cref="PrimeEngine.IsPrime(long)"/>
        public static bool IsPrime(long value)
            => PrimeEngine.IsPrime(value);

        /// <inheritdoc cref="PrimeEngine.CalApproximatePrime(long)"/>
        public static BigInteger CalApproximatePrime(long nth)
            => PrimeEngine.CalApproximatePrime(nth);





        /// <summary>
        /// Generates the unbounded sequence of factorial values, ie 1, 1, 2, 6, 24, 120, 720, 5040,
        /// 40320, etc. This enumerable is infinite and the calling code is responsible for ending
        /// the sequence, such as through the <see cref="Enumerable.Take" /> method. For
        /// more information, see <see cref="FactorialEngine" />.
        /// </summary>
        /// <returns>The unbounded sequence of factorial values.</returns>
        /// <seealso href="https://en.wikipedia.org/wiki/Factorial">Wikipedia Factorial</seealso>
        /// <seealso href="https://oeis.org/A000142">OEIS Factorial</seealso>
        public static IEnumerable<BigInteger> FactorialSequence() => new FactorialEngine();

        /// <summary>
        /// Generates the unbounded sequence of fibonacci values, ie 0, 1, 1, 2, 3, 5, 8, 13, 21,
        /// 34, 55, 89, 144, etc. This enumerable is infinite and the calling code is responsible
        /// for ending the sequence, such as through the <see cref="Enumerable.Take" /> method. For
        /// more information, see <see cref="FibonacciEngine" />.
        /// </summary>
        /// <returns>The unbounded sequence of fibonacci values.</returns>
        /// <seealso href="https://en.wikipedia.org/wiki/Fibonacci_number">Wikipedia Fibonacci</seealso>
        /// <seealso href="https://oeis.org/A000045">OEIS Fibonacci</seealso>
        public static IEnumerable<BigInteger> FibonacciSequence() => new FibonacciEngine();

        /// <summary>
        /// Generates the unbounded sequence representing the factional digits of PI, ie 1, 4, 1, 5,
        /// 9, 2, 6, 5, 3, 5, etc. This enumerable is infinite and the calling code is responsible
        /// for ending the sequence, such as through the <see cref="Enumerable.Take" /> method. For
        /// more information, see <see cref="PiEngine" />.
        /// </summary>
        /// <returns>The unbounded sequence representing the factional digits of PI.</returns>
        /// <seealso href="https://en.wikipedia.org/wiki/Pi">Wikipedia PI</seealso>
        /// <seealso href="https://oeis.org/A000796">OEIS PI</seealso>
        public static IEnumerable<byte> PiSequence() => new PiEngine();

        /// <summary>
        /// Generates the unbounded sequence of triangular values, ie 0, 1, 3, 6, 10, 15, 21, 28,
        /// 36, 45, 55, 66, 78, etc. This enumerable is infinite and the calling code is responsible
        /// for ending the sequence, such as through the <see cref="Enumerable.Take" /> method. For
        /// more information, see <see cref="TriangularEngine" />.
        /// </summary>
        /// <returns>The unbounded sequence representing the factional digits of PI.</returns>
        /// <seealso href="https://en.wikipedia.org/wiki/Triangular_number">Wikipedia Triangular</seealso>
        /// <seealso href="https://oeis.org/A000217">OEIS Triangular</seealso>
        public static IEnumerable<BigInteger> TriangularSequence() => new TriangularEngine();

        /// <summary>
        /// Generates the bounded sequence of prime values, ie 2, 3, 5, 7, 11, 13, 17, 19, 23, 29,
        /// 31, 37, 41, etc, up to the inclusive bounds of <see cref="int.MaxValue" /> using the
        /// specified <paramref name="sieveType"/> and, if the sieve allows, <paramref name="limitValueIndex"/>.
        /// </summary>
        /// <param name="sieveType">The value specifying which type of sieve to use.</param>
        /// <param name="limitValueIndex">The zero-based index of the prime value used to limit the sequence.</param>
        /// <returns>The bounded sequence of prime values.</returns>
        /// <seealso href="https://en.wikipedia.org/wiki/Prime_number">Wikipedia Prime</seealso>
        /// <seealso href="https://oeis.org/A000040">OEIS Prime</seealso>
        public static IEnumerable<long> PrimeSequence(EPrimeSieveType sieveType, int limitValueIndex = -1)
            => new PrimeEngine(sieveType, limitValueIndex);
    }
}
