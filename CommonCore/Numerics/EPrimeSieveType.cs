﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

namespace CommonCore.Numerics
{
    /// <summary>
    /// The <see cref="EPrimeSieveType"/> <c>enum</c> specifies which sieve to use for generating the prime number sequence.
    /// See <see cref="PrimeEngine"/> for more information.
    /// </summary>
    public enum EPrimeSieveType
    {
        /// <summary>
        /// Indicates the naive sieve. See <see cref="PrimeEngine.PrimeNaiveSequence"/> for more information.
        /// </summary>
        Naive,

        /// <summary>
        /// Indicates the sieve of Sundaram. See <see cref="PrimeEngine.PrimeSundaramSieveSequence(int)"/> for more information.
        /// </summary>
        Sundaram,

        /// <summary>
        /// Indicates the sieve of Eratosthenes. See <see cref="PrimeEngine.PrimeEratosthenesSieveSequence(int)"/> for more information.
        /// </summary>
        Eratosthenes
    }
}
