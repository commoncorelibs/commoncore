﻿#region Copyright (c) 2020 Jay Jeckel
// Copyright (c) 2020 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;

namespace CommonCore.Numerics
{
    /// <summary>
    /// The <see cref="ISequenceEngine{TEngine, TValue}" /><c>generic</c><c>interface</c> represents
    /// a mechanism for generating <see cref="ISequence{TValue}" /> values with the ability to
    /// arbitrarily store and restore the state of the engine.
    /// </summary>
    /// <typeparam name="TEngine">The type of the engine implementing the interface.</typeparam>
    /// <typeparam name="TValue">The type of the sequence values.</typeparam>
    public interface ISequenceEngine<TEngine, TValue> : ISequence<TValue>
        where TValue : IComparable<TValue>
    {
        /// <summary>
        /// Gets data about the engine-specific state stamps.
        /// </summary>
        (string format, int count) StampDetails { get; }

        /// <summary>
        /// Resets the engine to the initial state.
        /// </summary>
        /// <returns>A fluent reference to <c>this</c> <typeparamref name="TEngine"/> instance.</returns>
        TEngine Reset();

        /// <summary>
        /// Sets the engine state based on the specified <paramref name="stamp" />.
        /// See <see cref="StampDetails" /> for engine-specific format information.
        /// </summary>
        /// <param name="stamp">A <see cref="string" /> representing an engine state.</param>
        /// <returns>A fluent reference to <c>this</c><typeparamref name="TEngine" /> instance.</returns>
        /// <exception cref="ArgumentException">
        /// If <paramref name="stamp" /> is <c>null</c> or <c>empty</c>.
        /// </exception>
        /// <exception cref="FormatException">
        /// If <paramref name="stamp" /> does not contain the number of sections as defined by the
        /// <see cref="StampDetails" />; or a section can not be parsed as its underlying type.
        /// </exception>
        TEngine LoadStamp(string stamp);

        /// <summary>
        /// Converts the current state to a <see cref="string" /> representation suitable for
        /// resuming the generation from the current index. See <see cref="StampDetails" /> for
        /// engine-specific format information.
        /// </summary>
        /// <returns>A <see cref="string" /> representation of the current engine state.</returns>
        string ToStamp();
    }
}
