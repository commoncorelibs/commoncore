﻿#region Copyright (c) 2014 Jay Jeckel
// Copyright (c) 2014 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.IO;

namespace CommonCore.IO
{
    /// <summary>
    /// Static helper class for <see cref="DirectoryInfo"/>, <see cref="FileInfo"/>,
    /// and other related operations.
    /// </summary>
    public static class FileHelper
    {
        /// <summary>
        /// Gets a <see cref="DirectoryInfo"/> for the current user's temporary folder, with trailing backslash.
        /// </summary>
        public static DirectoryInfo TempDirectory { get; } = new DirectoryInfo(Path.GetTempPath());

        /// <summary>
        /// Change the file name element of the given path, leaving the directory and extension unmodified.
        /// </summary>
        /// <param name="path">Path of file for name change.</param>
        /// <param name="name">New file name.</param>
        /// <exception cref="System.ArgumentException">If <paramref name="path"/> or <paramref name="name"/> is <c>null</c> or <c>empty</c>.</exception>
        /// <returns>Path with name changed.</returns>
        public static string ChangeName(string path, string name)
        {
            Throw.If.Arg.IsNullOrEmpty(nameof(path), path);
            Throw.If.Arg.IsNullOrEmpty(nameof(name), name);
            return Path.Combine(Path.GetDirectoryName(path), Path.ChangeExtension(name, Path.GetExtension(path)));
        }

        /// <summary>
        /// Creates the specified directory if it does not already exist.
        /// </summary>
        /// <param name="path">Directory path to ensure exists.</param>
        /// <exception cref="System.ArgumentException">If <paramref name="path"/> is <c>null</c> or <c>empty</c>.</exception>
        public static void EnsureDirectoryExist(string path)
        {
            Throw.If.Arg.IsNullOrEmpty(nameof(path), path);
            if (!Directory.Exists(path)) { Directory.CreateDirectory(path); }
        }

        /// <summary>
        /// Creates any specified path if it does not already exist.
        /// </summary>
        /// <param name="paths">Directory paths to ensure exist.</param>
        /// <exception cref="System.ArgumentException">If <paramref name="paths"/> is <c>null</c> or <c>empty</c>.</exception>
        public static void EnsureDirectoryExist(params string[] paths)
        {
            Throw.If.Arg.IsNullOrEmpty(nameof(paths), paths);
            foreach (string item in paths)
            {
                if (string.IsNullOrEmpty(item)) { continue; }
                if (!Directory.Exists(item)) { Directory.CreateDirectory(item); }
            }
        }

        /// <summary>
        /// Creates the specified directory if it does not already exist.
        /// </summary>
        /// <param name="path">Directory path to ensure exists.</param>
        /// <exception cref="System.ArgumentNullException">If <paramref name="path"/> is <c>null</c>.</exception>
        public static void EnsureDirectoryExist(DirectoryInfo path)
        {
            Throw.If.Arg.IsNull(nameof(path), path);
            if (!path.Exists) { Directory.CreateDirectory(path.FullName); }
        }

        /// <summary>
        /// Creates any specified path if it does not already exist.
        /// </summary>
        /// <param name="paths">Directory paths to ensure exist.</param>
        /// <exception cref="System.ArgumentException">If <paramref name="paths"/> is <c>null</c> or <c>empty</c>.</exception>
        public static void EnsureDirectoryExist(params DirectoryInfo[] paths)
        {
            Throw.If.Arg.IsNullOrEmpty(nameof(paths), paths);
            foreach (DirectoryInfo item in paths)
            {
                if (item is null) { continue; }
                if (!item.Exists) { Directory.CreateDirectory(item.FullName); }
            }
        }

        /// <summary>
        /// Stores a two-dimensional array of file size suffixes in both full and abbreviated forms.
        /// </summary>
        public static readonly string[,] FileSizeTerms = { { "B", "Bytes" }, { "KB", "Kilobytes" }, { "MB", "Megabytes" }, { "GB", "Gigabytes" }, { "TB", "Terabytes" }, { "PB", "Petabytes" }, { "EB", "Exabytes" }, };

        /// <summary>
        /// Converts file size, in bytes, to a human readable formatted string. Proper base-2
        /// is used to determine formatted size and suffix, ie kilobyte (KB) equals 1024 bytes,
        /// not 1000 bytes.
        /// The following suffixes are supported: B (bytes), KB (kilobytes), MB (megabytes),
        /// GB (gigabytes), TB (terabytes), PB (petabytes), and EB (exabytes).
        /// </summary>
        /// <param name="size">Size of file in bytes.</param>
        /// <param name="abbreviate">Use the abbreviated form of the size suffix, ie KB, else use
        /// the full suffix name, ie Kilobyte.</param>
        /// <param name="format">The <see cref="string.Format(string, object)"/> suitable format used to build the output.</param>
        /// <exception cref="System.ArgumentException">If <paramref name="format"/> is <c>null</c> or <c>empty</c>.</exception>
        /// <returns>String formated file size.</returns>
        // Original Source: https://www.somacon.com/p576.php
        // TODO: Investigate adding remark with original source link. Ensure works with docs.
        // TODO: Support the silly 1000 byte kilo standard.
        // TODO: Validate the code doesn't suffer from these SI pitfalls when above todo is done:
        //       https://programming.guide/worlds-most-copied-so-snippet.html
        //       https://programming.guide/java/formatting-byte-size-to-human-readable-format.html
        public static string FormatFileSize(long size, bool abbreviate = true, string format = "{0:#,##0.##} {1}")
        {
            Throw.If.Arg.IsNullOrEmpty(nameof(format), format);
            bool neg = size < 0;
            if (neg) { size = -size; }

            int index;
            double value;
            if (size >= 0x1000000000000000) { index = 6; value = (double)(size >> 50) / 1024D; }
            else if (size >= 0x0004000000000000) { index = 5; value = (double)(size >> 40) / 1024D; }
            else if (size >= 0x0000010000000000) { index = 4; value = (double)(size >> 30) / 1024D; }
            else if (size >= 0x0000000040000000) { index = 3; value = (double)(size >> 20) / 1024D; }
            else if (size >= 0x0000000000100000) { index = 2; value = (double)(size >> 10) / 1024D; }
            else if (size >= 0x0000000000000400) { index = 1; value = (double)(size) / 1024D; }
            else { index = 0; value = size; }

            if (neg) { value = -value; }
            return string.Format(format, value, FileSizeTerms[index, abbreviate ? 0 : 1]);
            //ThrowHelper.IfArgNullOrEmpty(format, nameof(format));
            //if (size < 0)
            //{
            //    format = "-" + format;
            //    size = -size;
            //}

            //if (size >= 0x1000000000000000) { return ((double)(size >> 50) / 1024D).ToString(format) + (abbreviate ? " EB" : " Exabytes"); }
            //else if (size >= 0x0004000000000000) { return ((double)(size >> 40) / 1024D).ToString(format) + (abbreviate ? " PB" : " Petabytes"); }
            //else if (size >= 0x0000010000000000) { return ((double)(size >> 30) / 1024D).ToString(format) + (abbreviate ? " TB" : " Terabytes"); }
            //else if (size >= 0x0000000040000000) { return ((double)(size >> 20) / 1024D).ToString(format) + (abbreviate ? " GB" : " Gigabytes"); }
            //else if (size >= 0x0000000000100000) { return ((double)(size >> 10) / 1024D).ToString(format) + (abbreviate ? " MB" : " Megabytes"); }
            //else if (size >= 0x0000000000000400) { return ((double)(size) / 1024D).ToString(format) + (abbreviate ? " KB" : " Kilobytes"); }
            //else { return size.ToString(format) + (abbreviate ? " B" : " Bytes"); }
        }

        /// <summary>
        /// Gets a <see cref="FileInfo"/> for a temporary file with the specified <paramref name="extension"/>.
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        public static FileInfo GenTempFilePath(string extension)
            => new FileInfo(Path.ChangeExtension(Path.Combine(FileHelper.TempDirectory.FullName, Guid.NewGuid().ToString()), extension));// { Attributes = FileAttributes.Temporary };
    }
}
