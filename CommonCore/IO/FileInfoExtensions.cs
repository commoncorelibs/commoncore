﻿#region Copyright (c) 2014 Jay Jeckel
// Copyright (c) 2014 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace CommonCore.IO
{
    /// <summary>
    /// Static extension class for <see cref="FileInfo"/> related operations.
    /// </summary>
    public static class FileInfoExtensions
    {
        /// <summary>
        /// Throws <see cref="FileNotFoundException"/> if the specified <see cref="FileInfo"/>
        /// path does not exist.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <exception cref="System.NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="System.IO.FileNotFoundException">If this file path is not found..</exception>
        [DebuggerStepThrough]
        public static void ThrowIfNotFound(this FileInfo self)
        {
            Throw.If.Self.IsNull(self);
            if (!self.Exists) { throw new FileNotFoundException("Unable to locate FileInfo source path: " + self.FullName, self.FullName); }
        }

        /// <summary>
        /// Converts file size, in bytes, to a human readable formatted string. Proper base 2
        /// is used to determine formatted size and suffix, ie kilobyte (KB) equals 1024 bytes,
        /// not 1000 bytes.
        /// The following suffixes are supported: B (bytes), KB (kilobytes), MB (megabytes),
        /// GB (gigabytes), TB (terabytes), PB (petabytes), and EB (exabytes).
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="abbreviate">Use the abbreviated form of the size suffix, ie KB, else use
        /// the full suffix name, ie Kilobyte.</param>
        /// <param name="format">The <see cref="string.Format(string, object)"/> suitable format used to build the output.</param>
        /// <returns>String formated file size.</returns>
        /// <exception cref="System.NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="System.ArgumentException">If <paramref name="format"/> is <c>null</c> or <c>empty</c>.</exception>
        ///// <exception cref="System.IO.FileNotFoundException">Thrown if associated file path is not found.</exception>
        [DebuggerStepThrough]
        public static string GetFormattedSize(this FileInfo self, bool abbreviate = true, string format = "{0:#,##0.##} {1}")
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNullOrEmpty(nameof(format), format);
            //FileInfoExtensions.ThrowIfFound(self);
            return FileHelper.FormatFileSize(self.Length, abbreviate, format);
        }

        /// <summary>
        /// Opens a binary file, reads the contents of the file into a byte array, and then closes the file.
        /// </summary>
        /// <param name="self">The file to read.</param>
        /// <returns>Contents of the read file.</returns>
        public static byte[] ReadAllBytes(this FileInfo self)
        {
            Throw.If.Self.IsNull(self);
            FileInfoExtensions.ThrowIfNotFound(self);
            return File.ReadAllBytes(self.FullName);
        }

        /// <summary>
        /// Opens a text file, reads all lines of the file, and then closes the file.
        /// </summary>
        /// <param name="self">The file to read.</param>
        /// <returns>Contents of the read file.</returns>
        public static string[] ReadAllLines(this FileInfo self)
        {
            Throw.If.Self.IsNull(self);
            FileInfoExtensions.ThrowIfNotFound(self);
            return File.ReadAllLines(self.FullName);
        }

        /// <summary>
        /// Opens a text file, reads all text of the file, and then closes the file.
        /// </summary>
        /// <param name="self">The file to read.</param>
        /// <returns>Contents of the read file.</returns>
        public static string ReadAllText(this FileInfo self)
        {
            Throw.If.Self.IsNull(self);
            FileInfoExtensions.ThrowIfNotFound(self);
            return File.ReadAllText(self.FullName);
        }

        /// <summary>
        /// Enumerate the lines of the file.
        /// </summary>
        /// <param name="self">The file to read.</param>
        /// <returns>Contents of the read file.</returns>
        public static IEnumerable<string> ReadLines(this FileInfo self)
        {
            Throw.If.Self.IsNull(self);
            FileInfoExtensions.ThrowIfNotFound(self);
            return File.ReadLines(self.FullName);
        }

        /// <inheritdoc cref="File.WriteAllBytes(string, byte[])" />
        public static void WriteAllBytes(this FileInfo self, byte[] bytes)
        {
            Throw.If.Self.IsNull(self);
            FileInfoExtensions.ThrowIfNotFound(self);
            File.WriteAllBytes(self.FullName, bytes);
        }

        /// <inheritdoc cref="File.WriteAllLines(string, string[])" />
        public static void WriteAllLines(this FileInfo self, string[] contents)
        {
            Throw.If.Self.IsNull(self);
            FileInfoExtensions.ThrowIfNotFound(self);
            File.WriteAllLines(self.FullName, contents);
        }

        /// <inheritdoc cref="File.WriteAllLines(string, string[], Encoding)" />
        public static void WriteAllLines(this FileInfo self, string[] contents, Encoding encoding)
        {
            Throw.If.Self.IsNull(self);
            FileInfoExtensions.ThrowIfNotFound(self);
            File.WriteAllLines(self.FullName, contents, encoding);
        }

        /// <inheritdoc cref="File.WriteAllLines(string, IEnumerable{string})" />
        public static void WriteAllLines(this FileInfo self, IEnumerable<string> contents)
        {
            Throw.If.Self.IsNull(self);
            FileInfoExtensions.ThrowIfNotFound(self);
            File.WriteAllLines(self.FullName, contents);
        }

        /// <inheritdoc cref="File.WriteAllLines(string, IEnumerable{string}, Encoding)" />
        public static void WriteAllLines(this FileInfo self, IEnumerable<string> contents, Encoding encoding)
        {
            Throw.If.Self.IsNull(self);
            FileInfoExtensions.ThrowIfNotFound(self);
            File.WriteAllLines(self.FullName, contents, encoding);
        }

        /// <inheritdoc cref="File.WriteAllText(string, string)" />
        public static void WriteAllText(this FileInfo self, string contents)
        {
            Throw.If.Self.IsNull(self);
            FileInfoExtensions.ThrowIfNotFound(self);
            File.WriteAllText(self.FullName, contents);
        }

        /// <inheritdoc cref="File.WriteAllText(string, string, Encoding)" />
        public static void WriteAllText(this FileInfo self, string contents, Encoding encoding)
        {
            Throw.If.Self.IsNull(self);
            FileInfoExtensions.ThrowIfNotFound(self);
            File.WriteAllText(self.FullName, contents, encoding);
        }

        /// <summary>
        /// Rename the associated file, including the file's name and extension.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="nameextension">New file name and extension.</param>
        /// <exception cref="System.NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="System.ArgumentException">If <paramref name="nameextension"/> is <c>null</c> or <c>empty</c>.</exception>
        /// <exception cref="System.IO.FileNotFoundException">Thrown if associated file path is not found.</exception>
        [DebuggerStepThrough]
        public static void Rename(this FileInfo self, string nameextension)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNullOrEmpty(nameof(nameextension), nameextension);
            FileInfoExtensions.ThrowIfNotFound(self);
            self.MoveTo(Path.Combine(self.DirectoryName, nameextension));
        }

        /// <summary>
        /// Rename the associated file, including the file's name and extension.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="name">New file name.</param>
        /// <param name="extension">New file extension.</param>
        /// <exception cref="System.NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="System.ArgumentException">If <paramref name="name"/> is <c>null</c> or <c>empty</c>.</exception>
        /// <exception cref="System.IO.FileNotFoundException">Thrown if associated file path is not found.</exception>
        [DebuggerStepThrough]
        public static void Rename(this FileInfo self, string name, string extension)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNullOrEmpty(nameof(name), name);
            FileInfoExtensions.ThrowIfNotFound(self);
            self.MoveTo(Path.Combine(self.DirectoryName, Path.ChangeExtension(name, extension)));
        }

        /// <summary>
        /// Rename the associated file by changing the name of the file without changing the file extension.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="name">New name for the file.</param>
        /// <exception cref="System.NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="System.ArgumentException">If <paramref name="name"/> is <c>null</c> or <c>empty</c>.</exception>
        /// <exception cref="System.IO.FileNotFoundException">Thrown if associated file path is not found.</exception>
        [DebuggerStepThrough]
        public static void RenameName(this FileInfo self, string name)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNullOrEmpty(nameof(name), name);
            FileInfoExtensions.ThrowIfNotFound(self);
            self.MoveTo(FileHelper.ChangeName(self.FullName, name));
        }

        /// <summary>
        /// Rename the associated file by changing the extension of the file without changing the file name.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="extension">New extension for the file.</param>
        /// <exception cref="System.NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="System.IO.FileNotFoundException">Thrown if associated file path is not found.</exception>
        [DebuggerStepThrough]
        public static void RenameExtension(this FileInfo self, string extension)
        {
            Throw.If.Self.IsNull(self);
            FileInfoExtensions.ThrowIfNotFound(self);
            self.MoveTo(Path.ChangeExtension(self.FullName, extension));
        }

        //public static Image GetAssociatedIcon(this FileInfo self)
        //{
        //    Throw.If.Self.IsNull(self);
        //    FileInfoExtensions.ThrowIfFound(self);
        //    try { return Icon.ExtractAssociatedIcon(self.FullName).ToBitmap(); }
        //    catch (Exception) { return null; }
        //}
    }
}
