﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CommonCore.IO
{
    /// <summary>
    /// The <see cref="ExtensionFilter"/> struct represents a collection of file extensions related to
    /// one category of data -- such as image, video, text, etc -- formatted as a component filter
    /// <see cref="string"/> appropriate for the filter property of file open and save dialogs.
    /// </summary>
    // TODO: Maybe add basic groups: audio, document, ect. Check Shortcutter/Helper.cs for a start.
    public struct ExtensionFilter : IEnumerable, IEnumerable<string>, IEquatable<ExtensionFilter>
    {
        /// <summary>
        /// Gets the <see cref="ExtensionFilter"/> representing an empty filter.
        /// </summary>
        public static ExtensionFilter Empty { get; } = new ExtensionFilter();

        /// <summary>
        /// Gets the <see cref="ExtensionFilter"/> representing all files, ie <c>*.*</c>.
        /// </summary>
        public static ExtensionFilter All { get; } = new ExtensionFilter("All Files", "*");

        //public static ExtensionFilterGroup ImageExtensionFilterGroup { get; }
        //    = new ExtensionFilterGroup("Image", "ai", "bmp", "gif", "ico", "jpeg", "jpg", "png", "ps", "psd", "svg", "tif", "tiff");

        //public static ExtensionFilterGroup JsonExtensionFilterGroup { get; }
        //    = new ExtensionFilterGroup("Json", "json");

        /// <summary>
        /// Determines whether two specified <see cref="ExtensionFilter"/> objects represent the same value.
        /// </summary>
        /// <param name="left">The first <see cref="ExtensionFilter"/> to compare.</param>
        /// <param name="right">The second <see cref="ExtensionFilter"/> to compare.</param>
        /// <returns><c>true</c> if both <see cref="ExtensionFilter"/> objects represent the same value; otherwise <c>false</c>.</returns>
        public static bool Equals(ExtensionFilter left, ExtensionFilter right)
            => string.Equals(left.Filter, right.Filter);

        /// <summary>
        /// Concatenates the <see cref="ExtensionFilter"/> filter components into a compound filter <see cref="string"/>.
        /// Empty filter components will be excluded from the finally compound filter.
        /// </summary>
        /// <param name="filters">Sequence of <see cref="ExtensionFilter"/> elements to concatenate.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="filters"/> is <c>null</c>.</exception>
        /// <returns>A compound filter string composed of the specified filter components.</returns>
        public static string Concat(params ExtensionFilter[] filters)
            => ExtensionFilter.Concat((IEnumerable<ExtensionFilter>)filters);

        /// <summary>
        /// Concatenates the <see cref="ExtensionFilter"/> filter components into a compound filter <see cref="string"/>.
        /// Empty filter components will be excluded from the finally compound filter.
        /// </summary>
        /// <param name="filters">Sequence of <see cref="ExtensionFilter"/> elements to concatenate.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="filters"/> is <c>null</c>.</exception>
        /// <returns>A compound filter string composed of the specified filter components.</returns>
        public static string Concat(IEnumerable<ExtensionFilter> filters)
        {
            Throw.If.Arg.IsNull(nameof(filters), filters);
            return string.Join("|", filters.Where(filter => !string.IsNullOrEmpty(filter.Filter)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtensionFilter"/> class.
        /// Extensions should not include a dot prefixed.
        /// </summary>
        /// <param name="title">The title of the filter group.</param>
        /// <param name="extensions">The extensions of the filter group.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="extensions"/> is <c>null</c>.</exception>
        public ExtensionFilter(string title, params string[] extensions)
            : this(title, (IEnumerable<string>)extensions) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtensionFilter"/> class.
        /// Extensions should not include a dot prefixed.
        /// </summary>
        /// <param name="title">The title of the filter group.</param>
        /// <param name="extensions">The extensions of the filter group.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="title"/> or <paramref name="extensions"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentEmptyException">If <paramref name="title"/> is equal to <see cref="string.Empty"/>.</exception>
        public ExtensionFilter(string title, IEnumerable<string> extensions)
        {
            Throw.If.Arg.IsNullOrEmpty(nameof(title), title);
            Throw.If.Arg.IsNull(nameof(extensions), extensions);
            this.Title = title;
            this.Extensions = extensions.ToList().AsReadOnly();
            if (!extensions.Any()) { this.Filter = string.Empty; }
            else
            {
                var pattern = string.Join(";*.", extensions);
                this.Filter = $"{title} (*.{pattern})|*.{pattern}";
            }

        }

        /// <summary>
        /// Gets the title of the filter component.
        /// </summary>
        public string Title { get; }

        /// <summary>
        /// Gets the extensions of the filter component.
        /// Extensions should not include a dot prefixed.
        /// </summary>
        public IReadOnlyList<string> Extensions { get; }

        /// <summary>
        /// Gets the initialized filter component.
        /// Format: &lt;Title Text&gt; (*.&lt;ext0&gt;;*.&lt;ext1&gt;;...;*.&lt;extN&gt;)|*.&lt;ext0&gt;;*.&lt;ext1&gt;;...;*.&lt;extN&gt;
        /// </summary>
        public string Filter { get; }

        /// <summary>
        /// Returns a <see cref="string"/> that represents the filter component.
        /// </summary>
        /// <returns>A <see cref="string"/> that represents the filter component.</returns>
        public override string ToString()
            => this.Filter;

        /// <summary>
        /// Returns the hash code for this <see cref="ExtensionFilter"/>.
        /// </summary>
        /// <returns>A 32-bit signed integer hash code.</returns>
        public override int GetHashCode()
            => this.Filter?.GetHashCode() ?? 0;

        /// <summary>
        /// Indicates whether this instance and a specified object are equal.
        /// </summary>
        /// <param name="obj">The object to compare with this instance.</param>
        /// <returns><c>true</c> if <paramref name="obj"/> and this instance are the same type and represent the same value; otherwise <c>false</c>.</returns>
        public override bool Equals(object obj)
            => (obj is ExtensionFilter ? this.Equals((ExtensionFilter)obj) : false);

        /// <summary>
        /// Determines whether this instance and another specified <see cref="ExtensionFilter"/> object represent the same value.
        /// </summary>
        /// <param name="other">The <see cref="ExtensionFilter"/> to compare with this instance.</param>
        /// <returns><c>true</c> if <paramref name="other"/> and this instance represent the same value; otherwise <c>false</c>.</returns>
        public bool Equals(ExtensionFilter other)
            => ExtensionFilter.Equals(this, other);

        /// <summary>
        /// Returns an enumerator that iterates through the filter extensions.
        /// </summary>
        /// <returns>An enumerator that can be used to iterate through the collection.</returns>
        public IEnumerator<string> GetEnumerator()
            => ((IEnumerable<string>)this.Extensions).GetEnumerator();

        /// <summary>
        /// Returns an enumerator that iterates through the filter extensions.
        /// </summary>
        /// <returns>An enumerator that can be used to iterate through the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
            => this.GetEnumerator();

        /// <summary>
        /// Determines whether two specified <see cref="ExtensionFilter"/> objects represent the same value.
        /// </summary>
        /// <param name="left">The first <see cref="ExtensionFilter"/> to compare.</param>
        /// <param name="right">The second <see cref="ExtensionFilter"/> to compare.</param>
        /// <returns><c>true</c> if <paramref name="left"/> represents the same value as <paramref name="right"/>; otherwise <c>false</c>.</returns>
        public static bool operator ==(ExtensionFilter left, ExtensionFilter right) => ExtensionFilter.Equals(left, right);

        /// <summary>
        /// Determines whether two specified <see cref="ExtensionFilter"/> objects represent different values.
        /// </summary>
        /// <param name="left">The first <see cref="ExtensionFilter"/> to compare.</param>
        /// <param name="right">The second <see cref="ExtensionFilter"/> to compare.</param>
        /// <returns><c>true</c> if <paramref name="left"/> represents a different value than <paramref name="right"/>; otherwise <c>false</c>.</returns>
        public static bool operator !=(ExtensionFilter left, ExtensionFilter right) => !ExtensionFilter.Equals(left, right);



























        //[Obsolete("!!DEPRECATED!! Use ImageExtensionFilterGroup.ToFilterAndAll() instead.", false)]
        //public static string ImageExtensionFilter { get; } = string.Join("|", new[] { ImageExtensionFilterGroup, ExtensionFilterComponent.All });

        //[Obsolete("!!DEPRECATED!! Use JsonExtensionFilterGroup.ToFilterAndAll() instead.", false)]
        //public static string JsonExtensionFilter { get; } = string.Join("|", new[] { JsonExtensionFilterGroup, ExtensionFilterComponent.All });



        ///// <summary>
        ///// Builds a filter string for use with file open and save dialogs.
        ///// Do NOT prefix the extensions with a dot.
        ///// </summary>
        ///// <param name="title"></param>
        ///// <param name="extensions"></param>
        //[Obsolete("!!DEPRECATED!! Use class instances instead of these static methods.", false)]
        //public static string BuildExtensionFilter(string title, params string[] extensions)
        //{
        //    //var all = "All Files (*.*)|*.*";
        //    if (extensions == null || extensions.Length == 0)
        //    { return BuildExtensionFilter(title); }
        //    else if (extensions.Length == 1)
        //    { return BuildExtensionFilter(title, extensions[0]); }
        //    else
        //    {
        //        var group = $"{title} Files (*.{string.Join(";*.", extensions)})|*.{string.Join(";*.", extensions)}";
        //        var main = extensions.Join("|", ext => $"{ext.ToUpper()} Files (*.{ext})|*.{ext}");
        //        return $"{group}|{main}|{BuildExtensionFilter()}";
        //    }
        //}

        //[Obsolete("!!DEPRECATED!! Use class instances instead of these static methods.", false)]
        //private static string BuildExtensionFilter(string title, string extension) => $"{(title.IsEmpty() ? extension.ToUpper() : title)} Files (*.{extension})|*.{extension}";

        //[Obsolete("!!DEPRECATED!! Use class instances instead of these static methods.", false)]
        //private static string BuildExtensionFilter(string title) => $"{(title.IsEmpty() ? "All" : title)} Files (*.*)|*.*";

        //[Obsolete("!!DEPRECATED!! Use class instances instead of these static methods.", false)]
        //private static string BuildExtensionFilter() => "All Files (*.*)|*.*";
    }
}
