﻿#region Copyright (c) 2014 Jay Jeckel
// Copyright (c) 2014 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using static System.Net.WebRequestMethods;

namespace CommonCore.IO
{
    /// <summary>
    /// Static extension class for <see cref="DirectoryInfo"/> related operations.
    /// </summary>
    public static class DirectoryInfoExtensions
    {
        /// <summary>
        /// Throws <see cref="DirectoryNotFoundException"/> if the specified <see cref="DirectoryInfo"/>
        /// path does not exist.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <exception cref="System.NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="System.IO.DirectoryNotFoundException">If this directory path is not found.</exception>
        [DebuggerStepThrough]
        public static void ThrowIfNotFound(this DirectoryInfo self)
        {
            Throw.If.Self.IsNull(self);
            if (!self.Exists) { throw new DirectoryNotFoundException("Unable to locate DirectoryInfo source path: " + self.FullName); }
        }

        /// <summary>
        /// Calculate the number of matching directories under the specified directory.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="recursive">Include sub-directories.</param>
        /// <param name="filter">Wildcard string to filter directory names.</param>
        /// <returns>Number of matching directories under the specified directory.</returns>
        /// <exception cref="System.ArgumentNullException">Thrown if path or filter arguments are null or empty.</exception>
        public static int CalculateDirectoryCount(this DirectoryInfo self, bool recursive, string filter = null)
        {
            Throw.If.Self.IsNull(self);
            if (!self.Exists) { return 0; }
            return self.EnumerateDirectories(string.IsNullOrEmpty(filter) ? "*" : filter, recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly).Count();
        }

        /// <summary>
        /// Calculate the number of matching files under the specified directory.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="recursive">Include sub-directories.</param>
        /// <param name="filter">Wildcard string to filter file names.</param>
        /// <returns>Number of matching files under the specified directory.</returns>
        /// <exception cref="System.ArgumentNullException">Thrown if path or filter arguments are null or empty.</exception>
        public static int CalculateFileCount(this DirectoryInfo self, bool recursive, string filter = null)
        {
            Throw.If.Self.IsNull(self);
            if (!self.Exists) { return 0; }
            return self.EnumerateFiles(string.IsNullOrEmpty(filter) ? "*" : filter, recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly).Count();
        }

        /// <summary>
        /// Copies matching files to the specified <paramref name="target"/> directory.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="target">Destination directory for copied files.</param>
        /// <param name="filter">Wildcard string used to filter file names.</param>
        /// <param name="recursive">Recursively include files in sub-directories; otherwise only files in the this directory.</param>
        /// <exception cref="System.NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="System.ArgumentNullException">If <paramref name="target"/> is <c>null</c>.</exception>
        /// <exception cref="System.ArgumentException">If <paramref name="filter"/> contains one or more invalid characters defined by the <see cref="System.IO.Path.GetInvalidPathChars"/> method.</exception>
        /// <exception cref="System.IO.DirectoryNotFoundException">If <paramref name="target"/> path is not found.</exception>
        //     searchPattern contains one or more invalid characters defined by the System.IO.Path.GetInvalidPathChars
        //     method.
        [DebuggerStepThrough]
        public static void CopyFiles(this DirectoryInfo self, DirectoryInfo target, string filter = "*.*", bool recursive = true)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNull(nameof(target), target);
            ThrowIfNotFound(self);

            int len = self.FullName.Length;
            if (self.FullName.EndsWith('/') || self.FullName.EndsWith('\\')) { len += 1; }

            foreach (FileInfo file in self.EnumerateFiles(string.IsNullOrEmpty(filter) ? "*" : filter, (recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)))
            {
                string loc = file.FullName.TrimStart(len);
                string path = Path.Combine(target.FullName, loc);
                Directory.CreateDirectory(Path.GetDirectoryName(path));
                file.CopyTo(path, true);
            }
        }

        /// <summary>
        /// Creates the directory if it does not exist.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <exception cref="System.NullReferenceException">If this instance is <c>null</c>.</exception>
        [DebuggerStepThrough]
        public static void EnsureExists(this DirectoryInfo self)
        {
            Throw.If.Self.IsNull(self);
            if (!self.Exists) { Directory.CreateDirectory(self.FullName); }
        }

        /// <summary>
        /// Gets a <see cref="FileInfo"/> in this directory for the specified full file name.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="file">Full name of the file, including name and extension.</param>
        /// <exception cref="System.NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="System.ArgumentException">If <paramref name="file"/> is <c>null</c> or <c>empty</c>.</exception>
        /// <returns><see cref="FileInfo"/> for the specified full file name.</returns>
        [DebuggerStepThrough]
        public static FileInfo GetFile(this DirectoryInfo self, string file)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNullOrEmpty(nameof(file), file);
            return new FileInfo(Path.Combine(self.FullName, file));
        }

        /// <summary>
        /// Gets a <see cref="FileInfo"/> in this directory for the specified file name and extension.
        /// Note that <see cref="Path.ChangeExtension(string, string)"/> is used to join the name and
        /// extension. This means that any existing extension part of <paramref name="name"/> will be
        /// replaced by specified <paramref name="extension"/>.
        /// If <paramref name="extension"/> is <c>empty</c>, then the dot delimiter will be appended.
        /// Specifying a <c>null</c> or <c>empty</c> string for <paramref name="extension"/> will both
        /// strip any existing extension from <paramref name="name"/>. However, an <c>empty</c> string
        /// will then append the extension dot delimiter.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="name">Name of the file.</param>
        /// <param name="extension">Extension of the file.</param>
        /// <exception cref="System.NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="System.ArgumentException">If <paramref name="name"/> is <c>null</c> or <c>empty</c>.</exception>
        /// <returns><see cref="FileInfo"/> for the specified file name and extension.</returns>
        [DebuggerStepThrough]
        public static FileInfo GetFile(this DirectoryInfo self, string name, string extension)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNullOrEmpty(nameof(name), name);
            return new FileInfo(Path.Combine(self.FullName, Path.ChangeExtension(name, extension)));
        }

        /// <summary>
        /// Gets a <see cref="DirectoryInfo"/> for the sub-directory at the specified sub-path.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="subpath">Sub-path of the sub-directory to retrieve.</param>
        /// <exception cref="System.NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="System.ArgumentException">If <paramref name="subpath"/> is <c>null</c> or <c>empty</c>.</exception>
        /// <returns><see cref="DirectoryInfo"/> for the specified sub-directory.</returns>
        [DebuggerStepThrough]
        public static DirectoryInfo GetSubDirectory(this DirectoryInfo self, string subpath)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNullOrEmpty(nameof(subpath), subpath);
            return new DirectoryInfo(Path.Combine(self.FullName, subpath));
        }

        /// <summary>
        /// Gets a <see cref="DirectoryInfo"/> for the sub-directory at the specified sub-path.
        /// </summary>
        /// <param name="self">Instance of extended class.</param>
        /// <param name="subpaths">Sub-paths of the sub-directory to retrieve.</param>
        /// <exception cref="System.NullReferenceException">If this instance is <c>null</c>.</exception>
        /// <exception cref="System.ArgumentException">If <paramref name="subpaths"/> is <c>null</c> or <c>empty</c>.</exception>
        /// <returns><see cref="DirectoryInfo"/> for the specified sub-directory.</returns>
        [DebuggerStepThrough]
        public static DirectoryInfo GetSubDirectory(this DirectoryInfo self, params string[] subpaths)
        {
            Throw.If.Self.IsNull(self);
            Throw.If.Arg.IsNullOrEmpty(nameof(subpaths), subpaths);
            return new DirectoryInfo(Path.Combine(self.FullName, Path.Combine(subpaths)));
        }

        ///// <summary>
        ///// Evaluate a relative path using the source info's path as the root directory.
        ///// The current working directory is set to the root directory before and restored
        ///// after the given relative path is evaluated.
        ///// </summary>
        ///// <param name="self">Instance of extended class.</param>
        ///// <param name="relative">Relative path to be evaluated.</param>
        ///// <returns>Directory info class containing the evaluated relative path.</returns>
        ///// <exception cref="System.NullReferenceException">If this instance is <c>null</c>.</exception>
        ///// <exception cref="System.ArgumentException">If <paramref name="relative"/> is <c>null</c> or <c>empty</c>.</exception>
        //public static DirectoryInfo EvaluateRelative(this DirectoryInfo self, string relative)
        //{
        //    Throw.If.Self.IsNull(self);
        //    Throw.If.Arg.IsNullOrEmpty(nameof(relative), relative);
        //    string restore = System.IO.Directory.GetCurrentDirectory();
        //    System.IO.Directory.SetCurrentDirectory(self.FullName);
        //    DirectoryInfo dir = new DirectoryInfo(Path.GetFullPath(relative));
        //    System.IO.Directory.SetCurrentDirectory(restore);
        //    return dir;
        //}

        //public static void Rename(this DirectoryInfo self, string name)
        //{
        //    Throw.If.Self.IsNull(self);
        //    Throw.If.Arg.IsNullOrEmpty(nameof(name), name);
        //    ThrowIfDirectoryNotFound(self);
        //    if (self.FullName != Path.Combine(self.Parent.FullName, name))
        //    { self.MoveTo(Path.Combine(self.Parent.FullName, name)); }
        //}

        //public static Image GetAssociatedIcon(this DirectoryInfo self)//, FolderIconSize size = FolderIconSize.Small, FolderIconType folderType = FolderIconType.Closed)
        //{
        //    Throw.If.Self.IsNull(self);
        //    ThrowIfDirectoryNotFound(self);
        //    //return InternalHelper.GetFolderIcon(self.FullName, size, folderType).ToBitmap();
        //    return InternalHelper.GetFolderIcon(self.FullName).ToBitmap();
        //}
    }
}
