﻿#region Copyright (c) 2017 Jay Jeckel
// Copyright (c) 2017 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CommonCore.IO
{
    /// <summary>
    /// The <see cref="ExtensionFilterCollection"/> class represents a collection of <see cref="ExtensionFilter"/>
    /// elements and providing the ability to format the filter components into a compound filter
    /// <see cref="string"/> appropriate for the filter property of file open and file save dialogs.
    /// </summary>
    public class ExtensionFilterCollection : Collection<ExtensionFilter>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExtensionFilterCollection"/> class.
        /// </summary>
        public ExtensionFilterCollection() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtensionFilterCollection"/> class.
        /// </summary>
        /// <param name="filters">Sequence of initial <see cref="ExtensionFilter"/> elements.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="filters"/> is <c>null</c>.</exception>
        public ExtensionFilterCollection(IList<ExtensionFilter> filters) : base(filters) { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtensionFilterCollection"/> class.
        /// </summary>
        /// <param name="filters">Sequence of initial <see cref="ExtensionFilter"/> elements.</param>
        /// <exception cref="ArgumentNullException">If <paramref name="filters"/> is <c>null</c>.</exception>
        public ExtensionFilterCollection(IEnumerable<ExtensionFilter> filters) : base()
        {
            Throw.If.Arg.IsNull(nameof(filters), filters);
            foreach (var filter in filters) { this.Add(filter); }
        }

        /// <summary>
        /// Returns a <see cref="string"/> that represents the compound filter composed of the contained
        /// <see cref="ExtensionFilter"/> components.
        /// </summary>
        /// <returns>A compound filter string composed of the contained filter components.</returns>
        public string ToFilter() => ExtensionFilter.Concat(this);

        /// <summary>
        /// Returns a <see cref="string"/> that represents the compound filter composed of the contained
        /// <see cref="ExtensionFilter"/> components.
        /// </summary>
        /// <returns>A compound filter string composed of the contained filter components.</returns>
        public override string ToString() => this.ToFilter();
    }
}
