﻿using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests
{
    [ExcludeFromCodeCoverage]
    public class Test_ECharComparison
    {
        [Theory]
        [InlineData((ECharComparison)(-1), null)]
        [InlineData(ECharComparison.CaseSensitive, null)]
        [InlineData(ECharComparison.CurrentCultureIgnoreCase, "current")]
        [InlineData(ECharComparison.InvariantCultureIgnoreCase, "invariant")]
        public void ToCultureInfo(ECharComparison comparison, string culture)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            comparison.ToCultureInfo().Should().BeEquivalentTo(info);
        }
    }
}
