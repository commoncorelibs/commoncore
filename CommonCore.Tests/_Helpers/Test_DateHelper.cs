﻿using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests._Helpers
{
    [ExcludeFromCodeCoverage]
    public class Test_DateHelper
    {
        [Theory]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Sunday, 1)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Monday, 2)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Tuesday, 3)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Wednesday, 4)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Thursday, 5)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Friday, 6)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Saturday, 7)]
        public void FindFirstWeekday(int year, MonthOfYear month, DayOfWeek weekday, int expectedDate)
        {
            var result = DateHelper.FindFirstWeekday(year, month, weekday);
            result.Day.Should().Be(expectedDate);
        }

        [Theory]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Sunday, 8)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Monday, 9)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Tuesday, 10)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Wednesday, 11)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Thursday, 12)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Friday, 13)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Saturday, 14)]
        public void FindSecondWeekday(int year, MonthOfYear month, DayOfWeek weekday, int expectedDate)
        {
            var result = DateHelper.FindSecondWeekday(year, month, weekday);
            result.Day.Should().Be(expectedDate);
        }

        [Theory]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Sunday, 15)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Monday, 16)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Tuesday, 17)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Wednesday, 18)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Thursday, 19)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Friday, 20)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Saturday, 21)]
        public void FindThirdWeekday(int year, MonthOfYear month, DayOfWeek weekday, int expectedDate)
        {
            var result = DateHelper.FindThirdWeekday(year, month, weekday);
            result.Day.Should().Be(expectedDate);
        }

        [Theory]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Sunday, 22)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Monday, 23)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Tuesday, 24)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Wednesday, 25)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Thursday, 26)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Friday, 27)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Saturday, 28)]
        public void FindFourthWeekday(int year, MonthOfYear month, DayOfWeek weekday, int expectedDate)
        {
            var result = DateHelper.FindFourthWeekday(year, month, weekday);
            result.Day.Should().Be(expectedDate);
        }

        [Theory]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Sunday, 29)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Monday, 30)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Tuesday, 24)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Wednesday, 25)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Thursday, 26)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Friday, 27)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Saturday, 28)]
        public void FindLastWeekday(int year, MonthOfYear month, DayOfWeek weekday, int expectedDate)
        {
            var result = DateHelper.FindLastWeekday(year, month, weekday);
            result.Day.Should().Be(expectedDate);
        }

        [Theory]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Sunday, 1, 1)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Monday, 1, 2)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Tuesday, 1, 3)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Wednesday, 1, 4)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Thursday, 1, 5)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Friday, 1, 6)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Saturday, 1, 7)]

        [InlineData(2019, MonthOfYear.September, DayOfWeek.Sunday, 2, 8)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Monday, 2, 9)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Tuesday, 2, 10)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Wednesday, 2, 11)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Thursday, 2, 12)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Friday, 2, 13)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Saturday, 2, 14)]

        [InlineData(2019, MonthOfYear.September, DayOfWeek.Sunday, 3, 15)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Monday, 3, 16)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Tuesday, 3, 17)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Wednesday, 3, 18)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Thursday, 3, 19)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Friday, 3, 20)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Saturday, 3, 21)]

        [InlineData(2019, MonthOfYear.September, DayOfWeek.Sunday, 4, 22)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Monday, 4, 23)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Tuesday, 4, 24)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Wednesday, 4, 25)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Thursday, 4, 26)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Friday, 4, 27)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Saturday, 4, 28)]

        [InlineData(2019, MonthOfYear.September, DayOfWeek.Sunday, 5, 29)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Monday, 5, 30)]
        //[InlineData(2019, MonthOfYear.September, DayOfWeek.Tuesday, 5, 31)]
        //[InlineData(2019, MonthOfYear.September, DayOfWeek.Wednesday, 5, 32)]
        //[InlineData(2019, MonthOfYear.September, DayOfWeek.Thursday, 5, 33)]
        //[InlineData(2019, MonthOfYear.September, DayOfWeek.Friday, 5, 34)]
        //[InlineData(2019, MonthOfYear.September, DayOfWeek.Saturday, 5, 35)]
        public void FindNthWeekday(int year, MonthOfYear month, DayOfWeek weekday, int occurrence, int expectedDate)
        {
            var result = DateHelper.FindNthWeekday(year, month, weekday, occurrence);
            result.Day.Should().Be(expectedDate);
        }

        [Theory]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Tuesday, 5)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Wednesday, 5)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Thursday, 5)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Friday, 5)]
        [InlineData(2019, MonthOfYear.September, DayOfWeek.Saturday, 5)]
        public void FindNthWeekday_Throw(int year, MonthOfYear month, DayOfWeek weekday, int occurrence)
        {
            Action act = () => DateHelper.FindNthWeekday(year, month, weekday, occurrence);
            act.Should().Throw<ArgumentOutOfRangeException>().Where(exp => exp.ParamName == nameof(occurrence));
        }
    }
}
