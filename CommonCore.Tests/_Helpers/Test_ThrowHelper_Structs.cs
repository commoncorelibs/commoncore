﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
// <auto-generated />
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests._Helpers
{
    [ExcludeFromCodeCoverage, Obsolete]
    public class Test_ThrowHelper_Structs
    {
        private struct TestStruct
        {
            public static TestStruct None { get; } = default;

            public TestStruct(int value) => this.Value = value;
            public int Value { get; }
        }

        [Fact]
        public void IfArgDefault()
        {
            TestStruct subject;
            Action act;

            subject = default;
            act = () => ThrowHelper.IfArgDefault(subject, "test");
            act.Should().Throw<ArgumentException>();

            subject = TestStruct.None;
            act = () => ThrowHelper.IfArgDefault(subject, "test");
            act.Should().Throw<ArgumentException>();

            subject = new TestStruct();
            act = () => ThrowHelper.IfArgDefault(subject, "test");
            act.Should().Throw<ArgumentException>();

            subject = new TestStruct(42);
            act = () => ThrowHelper.IfArgDefault(subject, "test");
            act.Should().NotThrow();
        }
    }
}
