﻿using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests._Helpers
{
    [ExcludeFromCodeCoverage]
    public class Test_CommonHelper
    {

        #region IsCharEqual Methods

        [Theory]
        [InlineData('a', 'x', ECharComparison.CurrentCultureIgnoreCase, false)]
        [InlineData('a', 'A', ECharComparison.CurrentCultureIgnoreCase, true)]
        [InlineData('a', 'a', ECharComparison.CurrentCultureIgnoreCase, true)]
        [InlineData(';', ':', ECharComparison.CurrentCultureIgnoreCase, false)]
        [InlineData(';', ';', ECharComparison.CurrentCultureIgnoreCase, true)]

        [InlineData('a', 'x', ECharComparison.InvariantCultureIgnoreCase, false)]
        [InlineData('a', 'A', ECharComparison.InvariantCultureIgnoreCase, true)]
        [InlineData('a', 'a', ECharComparison.InvariantCultureIgnoreCase, true)]
        [InlineData(';', ':', ECharComparison.InvariantCultureIgnoreCase, false)]
        [InlineData(';', ';', ECharComparison.InvariantCultureIgnoreCase, true)]

        [InlineData('a', 'x', ECharComparison.CaseSensitive, false)]
        [InlineData('a', 'A', ECharComparison.CaseSensitive, false)]
        [InlineData('a', 'a', ECharComparison.CaseSensitive, true)]
        [InlineData(';', ':', ECharComparison.CaseSensitive, false)]
        [InlineData(';', ';', ECharComparison.CaseSensitive, true)]

        [InlineData('a', 'x', (ECharComparison)(-1), false)]
        [InlineData('a', 'A', (ECharComparison)(-1), false)]
        [InlineData('a', 'a', (ECharComparison)(-1), true)]
        [InlineData(';', ':', (ECharComparison)(-1), false)]
        [InlineData(';', ';', (ECharComparison)(-1), true)]
        public void IsCharEqual1(char a, char b, ECharComparison comparison, bool expected)
            => CommonHelper.IsCharEqual(a, b, comparison).Should().Be(expected);

        [Theory]
        [InlineData('a', 'x', "current", false)]
        [InlineData('a', 'a', "current", true)]
        [InlineData('a', 'A', "current", true)]
        [InlineData(';', ':', "current", false)]
        [InlineData(';', ';', "current", true)]

        [InlineData('a', 'x', "invariant", false)]
        [InlineData('a', 'a', "invariant", true)]
        [InlineData('a', 'A', "invariant", true)]
        [InlineData(';', ':', "invariant", false)]
        [InlineData(';', ';', "invariant", true)]

        [InlineData('a', 'x', "en-US", false)]
        [InlineData('a', 'a', "en-US", true)]
        [InlineData('a', 'A', "en-US", true)]
        [InlineData(';', ':', "en-US", false)]
        [InlineData(';', ';', "en-US", true)]

        [InlineData('a', 'x', "fr-FR", false)]
        [InlineData('a', 'a', "fr-FR", true)]
        [InlineData('a', 'A', "fr-FR", true)]
        [InlineData(';', ':', "fr-FR", false)]
        [InlineData(';', ';', "fr-FR", true)]
        public void IsCharEqual2(char a, char b, string culture, bool expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            CommonHelper.IsCharEqual(a, b, info).Should().Be(expected);
        }

        #endregion

    }
}
