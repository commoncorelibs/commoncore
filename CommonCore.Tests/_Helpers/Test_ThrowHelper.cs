﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
// <auto-generated />
#endregion

using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests._Helpers
{
    [ExcludeFromCodeCoverage, Obsolete]
    public class Test_ThrowHelper
    {

        #region IfSelfNull Tests

        [Fact]
        public void IfSelfNull()
        {
            object subject;
            Action act;

            subject = null;
            act = () => ThrowHelper.IfSelfNull(subject);
            act.Should().Throw<NullReferenceException>();

            subject = new object();
            act = () => ThrowHelper.IfSelfNull(subject);
            act.Should().NotThrow();
        }

        #endregion

        #region IfArgNull Tests

        [Fact]
        public void IfArgNull()
        {
            object subject;
            Action act;

            subject = null;
            act = () => ThrowHelper.IfArgNull(subject, "test");
            act.Should().Throw<ArgumentNullException>();

            subject = new object();
            act = () => ThrowHelper.IfArgNull(subject, "test");
            act.Should().NotThrow();
        }

        #endregion

        #region IfArgIndexOutOfRange Tests

        [Theory]
        [InlineData(-1, new[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }, true)]
        [InlineData(0, new[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }, false)]
        [InlineData(9, new[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }, false)]
        [InlineData(10, new[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }, true)]
        public void IfArgIndexOutOfRange_Collection(int index, ICollection collection, bool throws)
        {
            Action act = () => ThrowHelper.IfArgIndexOutOfRange(index, "test", collection);
            if (throws)
            { act.Should().Throw<ArgumentOutOfRangeException>(); }
            else
            { act.Should().NotThrow(); }
        }

        [Theory]
        [InlineData(-1, "0123456789", true)]
        [InlineData(0, "0123456789", false)]
        [InlineData(9, "0123456789", false)]
        [InlineData(10, "0123456789", true)]
        public void IfArgIndexOutOfRange_StringBuilder(int index, string content, bool throws)
        {
            Action act = () => ThrowHelper.IfArgIndexOutOfRange(index, "test", new StringBuilder(content));
            if (throws)
            { act.Should().Throw<ArgumentOutOfRangeException>(); }
            else
            { act.Should().NotThrow(); }
        }

        #endregion

        #region IfArgEnumOutOfRange Tests

        public enum ETestOutOfRange
        {
            None = -1,
            A = 1,
            B = 2,
            C = 4
        }

        [Theory]
        [InlineData((ETestOutOfRange)(-2), true)]
        [InlineData((ETestOutOfRange)(-1), false)]
        [InlineData((ETestOutOfRange)0, true)]
        [InlineData((ETestOutOfRange)1, false)]
        [InlineData((ETestOutOfRange)2, false)]
        [InlineData((ETestOutOfRange)3, true)]
        [InlineData((ETestOutOfRange)4, false)]
        [InlineData((ETestOutOfRange)5, true)]
        public void IfArgEnumOutOfRange(Enum value, bool throws)
        {
            Action act = () => ThrowHelper.IfArgEnumOutOfRange(value, "test");
            if (throws)
            { act.Should().Throw<ArgumentOutOfRangeException>(); }
            else
            { act.Should().NotThrow(); }
        }

        #endregion

        #region IfArgOutOfRange Tests

        [Theory]
        [InlineData(1, 0, 10, false)]
        [InlineData(9, 0, 10, false)]
        [InlineData(0, 0, 10, false)]
        [InlineData(10, 0, 10, false)]
        [InlineData(-1, 0, 10, true)]
        [InlineData(11, 0, 10, true)]
        public void IfArgOutOfRange(int value, int min, int max, bool throws)
        {
            Action act = () => ThrowHelper.IfArgOutOfRange<int>(value, "test", min, max);
            if (throws) { act.Should().Throw<ArgumentOutOfRangeException>(); }
            else { act.Should().NotThrow(); }
        }

        [Theory]
        [InlineData(1, 0, 10, false)]
        [InlineData(9, 0, 10, false)]
        [InlineData(0, 0, 10, true)]
        [InlineData(10, 0, 10, true)]
        [InlineData(-1, 0, 10, true)]
        [InlineData(11, 0, 10, true)]
        public void IfArgOutOfRangeEqual(int value, int min, int max, bool throws)
        {
            Action act = () => ThrowHelper.IfArgOutOfRangeEqual<int>(value, "test", min, max);
            if (throws) { act.Should().Throw<ArgumentOutOfRangeException>(); }
            else { act.Should().NotThrow(); }
        }

        #endregion

        #region IfArgGreater Tests

        [Theory]
        [InlineData(0, 1, false)]
        [InlineData(1, 1, false)]
        [InlineData(2, 1, true)]
        public void IfArgGreater(int value, int max, bool throws)
        {
            Action act;
            act = () => ThrowHelper.IfArgGreater<int>(value, "value", max);
            if (throws) { act.Should().Throw<ArgumentOutOfRangeException>(); }
            else { act.Should().NotThrow(); }
            act = () => ThrowHelper.IfArgGreater<int>(value, "value", max, "limit");
            if (throws) { act.Should().Throw<ArgumentOutOfRangeException>(); }
            else { act.Should().NotThrow(); }
        }

        [Theory]
        [InlineData(0, 1, false)]
        [InlineData(1, 1, true)]
        [InlineData(2, 1, true)]
        public void IfArgGreaterEqual(int value, int max, bool throws)
        {
            Action act;
            act = () => ThrowHelper.IfArgGreaterEqual<int>(value, "value", max);
            if (throws) { act.Should().Throw<ArgumentOutOfRangeException>(); }
            else { act.Should().NotThrow(); }
            act = () => ThrowHelper.IfArgGreaterEqual<int>(value, "value", max, "limit");
            if (throws) { act.Should().Throw<ArgumentOutOfRangeException>(); }
            else { act.Should().NotThrow(); }
        }

        #endregion

        #region IfArgLesser Tests

        [Theory]
        [InlineData(0, 1, true)]
        [InlineData(1, 1, false)]
        [InlineData(2, 1, false)]
        public void IfArgLesser(int value, int min, bool throws)
        {
            Action act;
            act = () => ThrowHelper.IfArgLesser<int>(value, "value", min);
            if (throws) { act.Should().Throw<ArgumentOutOfRangeException>(); }
            else { act.Should().NotThrow(); }
            act = () => ThrowHelper.IfArgLesser<int>(value, "value", min, "limit");
            if (throws) { act.Should().Throw<ArgumentOutOfRangeException>(); }
            else { act.Should().NotThrow(); }
        }

        [Theory]
        [InlineData(0, 1, true)]
        [InlineData(1, 1, true)]
        [InlineData(2, 1, false)]
        public void IfArgLesserEqual(int value, int min, bool throws)
        {
            Action act;
            act = () => ThrowHelper.IfArgLesserEqual<int>(value, "value", min);
            if (throws) { act.Should().Throw<ArgumentOutOfRangeException>(); }
            else { act.Should().NotThrow(); }
            act = () => ThrowHelper.IfArgLesserEqual<int>(value, "value", min, "limit");
            if (throws) { act.Should().Throw<ArgumentOutOfRangeException>(); }
            else { act.Should().NotThrow(); }
        }

        #endregion

    }
}
