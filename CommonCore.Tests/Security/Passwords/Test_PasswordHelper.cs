﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security;
using CommonCore.Security;
using CommonCore.Security.Passwords;
using FluentAssertions;
using Xunit;
using System.Diagnostics.CodeAnalysis;

namespace CommonCore.Tests.Security.Passwords
{
    [ExcludeFromCodeCoverage]
    public class Test_PasswordHelper
    {
        [Fact]
        public void Hashes()
        {
            string text = "password";
            using var password = new SecureString().AppendChars(text.ToCharArray(), true);
            var hash = PasswordHelper.GenerateHash(password);
            PasswordHelper.CompareHash(password, hash.ToString()).Should().BeTrue();
        }

        [Fact]
        public void Analysis()
        {
            string text = "password";
            using var password = new SecureString().AppendChars(text.ToCharArray(), true);
            var analysis = PasswordHelper.GenerateAnalysis(password);
            int score = PasswordHelper.CalculateScore(password);
            score.Should().Be(analysis.Score);
        }
    }
}
