﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Security;
using CommonCore.Security;
using CommonCore.Security.Passwords.Hashing;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests.Security.Passwords.Hashing
{
    [ExcludeFromCodeCoverage]
    public class Test_PasswordHasher
    {
        [Theory]
        [InlineData("password")]
        [InlineData("different text")]
        [InlineData("unicode\u263Atest")]
        public void GenerateHash(string inputText)
        {
            using var secret = new SecureString().AppendChars(inputText.ToCharArray(), true);
            Pbkdf2Hash hash = PasswordHasher.GenerateHash(secret);
            hash.IsNone().Should().BeFalse();
        }

        [Theory]
        [InlineData("password")]
        [InlineData("different text")]
        [InlineData("unicode\u263Atest")]
        public void CompareHash(string inputText)
        {
            Action act;
            using var inputPassword = new SecureString().AppendChars(inputText.ToCharArray(), true);
            string inputHash = PasswordHasher.GenerateHash(inputPassword).ToString();

            // 1. Correct password works.
            string rightText = inputText;
            using (var rightPassword = new SecureString().AppendChars(rightText.ToCharArray(), true))
            { PasswordHasher.CompareHash(rightPassword, inputHash).Should().BeTrue(); }

            // 2. Wrong password fails.
            string wrongText = "wrong_" + inputText;
            using (var wrongPassword = new SecureString().AppendChars(wrongText.ToCharArray(), true))
            { PasswordHasher.CompareHash(wrongPassword, inputHash).Should().BeFalse(); }

            // 3. Truncated hash fails with exception (for correct password).
            int minLength = inputHash.LastIndexOf(':');
            for (int length = inputHash.Length - 1; length > minLength; length--)
            {
                act = () => PasswordHasher.CompareHash(inputPassword, inputHash.Substring(0, length));
                act.Should().Throw<FormatException>();
            }

            // 4. Hashing the same password twice produces two different hashes.
            PasswordHasher.GenerateHash(inputPassword).Should().NotBe(inputHash);

            // 5. Replacing sha1: with sha256: raises an exception
            act = () => PasswordHasher.CompareHash(inputPassword, inputHash.Replace("sha1:", "sha256:"));
            act.Should().Throw<FormatException>();

            // 6. Appending :a to the hash raises an exception.
            act = () => PasswordHasher.CompareHash(inputPassword, inputHash + ":a");
            act.Should().Throw<FormatException>();

            // 7. Removing everything after (and including) the last : raises an exception.
            act = () => PasswordHasher.CompareHash(inputPassword, inputHash.Substring(0, minLength));
            act.Should().Throw<FormatException>();
        }
    }
}
