﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Security;
using CommonCore.Security;
using CommonCore.Security.Passwords.Hashing;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests.Security.Passwords.Hashing
{
    [ExcludeFromCodeCoverage]
    public class Test_Pbkdf2Hash
    {
        [Fact]
        public void IsNone()
        {
            Pbkdf2Hash hash;

            int typeHashCode = default(Pbkdf2Hash).GetHashCode();

            hash = default;
            hash.IsNone().Should().BeTrue();
            (hash == default).Should().BeTrue();
            (hash != default).Should().BeFalse();
            hash.GetHashCode().Should().Be(typeHashCode);

            hash = Pbkdf2Hash.None;
            hash.IsNone().Should().BeTrue();
            (hash == default).Should().BeTrue();
            (hash != default).Should().BeFalse();
            hash.GetHashCode().Should().Be(typeHashCode);

            hash = new Pbkdf2Hash();
            hash.IsNone().Should().BeTrue();
            (hash == default).Should().BeTrue();
            (hash != default).Should().BeFalse();
            hash.GetHashCode().Should().Be(typeHashCode);

            string text = "password";
            using var password = new SecureString().AppendChars(text.ToCharArray(), true);
            hash = PasswordHasher.GenerateHash(password);
            hash.IsNone().Should().BeFalse();
            (hash == default).Should().BeFalse();
            (hash != default).Should().BeTrue();
        }

        [Theory]
        [InlineData("", true)]
        [InlineData("sha1:64000:18:v741vZLHthpjnKAUZhOoQRT67oopJ14E", true)]
        [InlineData("sha1:64000:18:v741vZLHthpjnKAUZhOoQRT67oopJ14E:NxVUAo1coj+Wtihhju1bqlmp:a", true)]
        [InlineData("sha1:64000:18:v741vZLHthpjnKAUZhOoQRT67oopJ14E:NxVUAo1coj+Wtihhju1bqlmp", false)]
        public void ParseString(string hash, bool throws)
        {
            Action act = () => Pbkdf2Hash.ParseString(hash);
            if (throws) { act.Should().Throw<FormatException>(); }
            else { act.Should().NotThrow(); }
        }

        [Theory]
        [InlineData("", true)]
        [InlineData("sha256", true)]
        [InlineData("sha1", false)]
        public void ParseSectionAlgorithm(string section, bool throws)
        {
            Action act = () => Pbkdf2Hash.ParseSectionAlgorithm(section);
            if (throws) { act.Should().Throw<FormatException>(); }
            else { act.Should().NotThrow(); }
        }

        [Theory]
        [InlineData("", true)]
        [InlineData("a", true)]
        [InlineData("-1", true)]
        [InlineData("0", true)]
        [InlineData("1", false)]
        public void ParseSectionIterations(string section, bool throws)
        {
            Action act = () => Pbkdf2Hash.ParseSectionIterations(section);
            if (throws) { act.Should().Throw<FormatException>(); }
            else { act.Should().NotThrow(); }
        }

        [Theory]
        [InlineData("", true)]
        [InlineData("a", true)]
        [InlineData("-1", true)]
        [InlineData("0", false)]
        public void ParseSectionIntegrity(string section, bool throws)
        {
            Action act = () => Pbkdf2Hash.ParseSectionIntegrity(section);
            if (throws) { act.Should().Throw<FormatException>(); }
            else { act.Should().NotThrow(); }
        }

        [Theory]
        [InlineData("", true)]
        [InlineData("!", true)]
        [InlineData("a", true)]
        [InlineData("t5Pd9Wk0uTtVKNL5LCkAuNZ2D733f2ug", false)]
        public void ParseSectionSalt(string section, bool throws)
        {
            Action act = () => Pbkdf2Hash.ParseSectionSalt(section, 24);
            if (throws) { act.Should().Throw<FormatException>(); }
            else { act.Should().NotThrow(); }
        }

        [Theory]
        [InlineData("", true)]
        [InlineData("!", true)]
        [InlineData("a", true)]
        [InlineData("4QyrwY4A+GNqCQUevSUNQwCm", false)]
        public void ParseSectionPayload(string section, bool throws)
        {
            Action act = () => Pbkdf2Hash.ParseSectionPayload(section, 18);
            if (throws) { act.Should().Throw<FormatException>(); }
            else { act.Should().NotThrow(); }
        }
    }
}
