﻿using System;
using System.Collections.Generic;
using System.Security;
using System.Text;
using CommonCore.Security;
using CommonCore.Security.Passwords.Analysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests.Security.Passwords.Analysis
{
    public class Test_PasswordAnalyzer
    {
        [Theory]
        [InlineData("some_password_text")]
        [InlineData("other text")]
        [InlineData("password")]
        [InlineData("unicode\u263Atest")]
        public void GenerateAnalysis(string input)
        {
            using var password = new SecureString().AppendChars(input.ToCharArray(), true);
            var result = PasswordAnalyzer.DefaultAnalyzer.GenerateAnalysis(password);
            result.Score.Should().BeInRange(0, 100);
        }
    }
}
