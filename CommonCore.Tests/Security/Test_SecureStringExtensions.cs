﻿using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security;
using System.Text;
using CommonCore.Security;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests.Security
{
    [ExcludeFromCodeCoverage]
    public class Test_SecureStringExtensions
    {
        [Fact]
        public void AsByteSequence()
        {
            string text = "password";
            using var password = new SecureString().AppendChars(text.ToCharArray(), true);
            Enumerable.SequenceEqual<byte>(Encoding.Unicode.GetBytes(text), password.AsByteSequence())
                .Should().BeTrue();
        }

        [Fact]
        public void AsCharSequence()
        {
            string text = "password";
            using var password = new SecureString().AppendChars(text.ToCharArray(), true);
            Enumerable.SequenceEqual<char>(text, password.AsCharSequence())
                .Should().BeTrue();
        }
    }
}
