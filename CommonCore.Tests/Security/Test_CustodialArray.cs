﻿using System;
using CommonCore.Security;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests.Security
{
    public class Test_CustodialArray
    {
        [Fact]
        public void CleanAfterUsing()
        {
            byte[] empty = { 0, 0, 0, 0, 0 };
            byte[] content = { 1, 2, 3, 4, 5 };
            byte[] stored = null;
            using (var source = new CustodialArray<byte>(content.Length))
            {
                source.Length.Should().Be(content.Length);
                source.Content.Should().BeEquivalentTo(empty);
                //Array.Copy(content, 0, source.Content, 0, 5);
                for (int index = 0; index < source.Length; index++)
                { source[index] = content[index]; }
                for (int index = 0; index < source.Length; index++)
                { source[index].Should().Be(content[index]); }
                source.Content.Should().BeEquivalentTo(content);
                stored = source.Content;
            }
            stored.Should().NotBeNull().And.BeEquivalentTo(empty);
        }
    }
}
