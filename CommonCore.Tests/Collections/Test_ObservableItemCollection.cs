﻿using System.Collections.Specialized;
using System.Linq;
using CommonCore.Collections;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests.Collections
{
    public class Test_ObservableItemCollection
    {
        private class TestClass : AObservableObject
        {
            public bool ValueProp
            {
                get => this._valueProp;
                set => this.RaiseIfPropertyChanged(ref this._valueProp, value);
            }
            private bool _valueProp = false;
        }

        [Fact]
        public void Constructors()
        {
            ObservableItemCollection<TestClass> subject;
            subject = new ObservableItemCollection<TestClass>();
            subject.Should().HaveCount(0);

            var items = new[] { new TestClass(), new TestClass() };
            subject = new ObservableItemCollection<TestClass>(items.ToList());
            subject.Should().HaveCount(items.Length);
            subject = new ObservableItemCollection<TestClass>(items);
            subject.Should().HaveCount(items.Length);
        }

        [Fact]
        public void ClearItems()
        {
            var items = new[] { new TestClass(), new TestClass() };
            var subject = new ObservableItemCollection<TestClass>(items);
            subject.Should().HaveCount(items.Length);
            subject.Clear();
            subject.Should().HaveCount(0);
        }

        [Fact]
        public void OnCollectionChanged()
        {
            var item = new TestClass();
            var subject = new ObservableItemCollection<TestClass>();
            using (var monitor = subject.Monitor())
            {
                subject.Should().HaveCount(0);
                subject.Add(item);
                subject.Should().HaveCount(1);
                monitor.Should().Raise("CollectionChanged").WithSender(subject)
                    .WithArgs<NotifyCollectionChangedEventArgs>(args => args.Action == NotifyCollectionChangedAction.Add);
            }
            using (var monitor = subject.Monitor())
            {
                subject.Should().HaveCount(1);
                subject.Remove(item);
                subject.Should().HaveCount(0);
                monitor.Should().Raise("CollectionChanged").WithSender(subject)
                    .WithArgs<NotifyCollectionChangedEventArgs>(args => args.Action == NotifyCollectionChangedAction.Remove);
            }
        }

        [Fact]
        public void OnItemPropertyChanged()
        {
            var item = new TestClass();
            var subject = new ObservableItemCollection<TestClass>();
            item.ValueProp = true;
            subject.Add(item);
            item.ValueProp = false;
            subject.Remove(item);
            using (var monitor = subject.Monitor())
            {
                subject.Add(item);
                item.ValueProp = true;
                monitor.Should().Raise("ItemPropertyChanged").WithSender(subject)
                    .WithArgs<ItemPropertyChangedEventArgs>(args => args.PropertyName == nameof(item.ValueProp));
            }
            using (var monitor = subject.Monitor())
            {
                subject.Remove(item);
                item.ValueProp = false;
                monitor.Should().NotRaise("ItemPropertyChanged");
            }
        }
    }
}
