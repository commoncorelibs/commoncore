﻿using CommonCore.Collections;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests.Collections
{
    public class Test_ItemPropertyChangedEventArgs
    {
        [Theory]
        [InlineData(-1, null)]
        [InlineData(0, "SomeProperty")]
        [InlineData(10, "Count")]
        public void Test(int index, string name)
        {
            var subject = new ItemPropertyChangedEventArgs(index, name);
            subject.ItemIndex.Should().Be(index);
            subject.PropertyName.Should().Be(name);
        }
    }
}
