using System;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests._Extensions
{
    [ExcludeFromCodeCoverage]
    public class Test_ObjectExtensions
    {
        [Fact]
        public void IsNull()
        {
            ((object)null).IsNull().Should().BeTrue();
            ((object)new object()).IsNull().Should().BeFalse();
        }

        [Fact]
        public void IsNotNull()
        {
            ((object)null).IsNotNull().Should().BeFalse();
            ((object)new object()).IsNotNull().Should().BeTrue();
        }

        [Fact]
        public void Is()
        {
            var obj = new object();
            var str = new string('-', 10);
            obj.Is<string>().Should().BeFalse();
            str.Is<string>().Should().BeTrue();
        }

        [Fact]
        public void IsNot()
        {
            var obj = new object();
            var str = new string('-', 10);
            obj.IsNot<string>().Should().BeTrue();
            str.IsNot<string>().Should().BeFalse();
        }

        [Fact]
        public void As()
        {
            string str = new string('-', 10);

            object obj = null;
            Action act;

            act = () => obj = str.As<object>();
            act.Should().NotThrow();
            obj.Should().NotBeNull();

            act = () => obj = str.As<StringBuilder>();
            act.Should().NotThrow();
            obj.Should().BeNull();
        }

        [Fact]
        public void Cast()
        {
            string str = new string('-', 10);

            object obj = null;
            Action act;

            act = () => obj = str.CastAs<object>();
            act.Should().NotThrow();
            obj.Should().NotBeNull();

            act = () => obj = str.CastAs<StringBuilder>();
            act.Should().Throw<InvalidCastException>();
        }
    }
}
