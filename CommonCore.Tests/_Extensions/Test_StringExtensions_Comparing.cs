﻿using System.Collections.Generic;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests._Extensions
{
    public class Test_StringExtensions_Comparing
    {
        [Theory]
        [InlineData(null, 0, false)]
        [InlineData("", 0, false)]
        [InlineData("test", -1, false)]
        [InlineData("test", 4, false)]
        [InlineData("test", 0, true)]
        [InlineData("test", 3, true)]
        public void HasIndex(string subject, int index, bool expected)
            => subject.HasIndex(index).Should().Be(expected);

        public const string SharedString = "test";

        [Theory]
        [InlineData(null, null, true)]
        [InlineData(SharedString, SharedString, true)]
        [InlineData("a", "a", true)]
        [InlineData("a", "A", true)]
        [InlineData("soMe tExt", "SomE TexT", true)]
        [InlineData("text value", "other value", false)]
        [InlineData("test", null, false)]
        [InlineData(null, "test", false)]
        public void EqualsIgnoreCase(string subject, string other, bool expected)
        {
            subject.EqualsIgnoreCase(other).Should().Be(expected);
            other.EqualsIgnoreCase(subject).Should().Be(expected);
        }

        [Theory]
        [InlineData("abcdefg", 1, 'b', true)]
        [InlineData("abcdefg", 1, 'B', false)]
        [InlineData(null, 0, '\0', false)]
        [InlineData("abcdefg", -1, '\0', false)]
        [InlineData("abcdefg", 7, '\0', false)]
        public void IsChar(string subject, int index, char value, bool expected)
            => subject.IsChar(index, value).Should().Be(expected);

        [Theory]
        [InlineData("abcdefg", 1, 'a', true)]
        [InlineData("abcdefg", 1, 'A', false)]
        [InlineData(null, 0, '\0', false)]
        [InlineData("abcdefg", -1, '\0', false)]
        [InlineData("abcdefg", 7, '\0', false)]
        public void IsPrevChar(string subject, int index, char value, bool expected)
            => subject.IsPrevChar(index, value).Should().Be(expected);

        [Theory]
        [InlineData("abcdefg", 1, 'c', true)]
        [InlineData("abcdefg", 1, 'C', false)]
        [InlineData(null, 0, '\0', false)]
        [InlineData("abcdefg", -1, '\0', false)]
        [InlineData("abcdefg", 7, '\0', false)]
        public void IsNextChar(string subject, int index, char value, bool expected)
            => subject.IsNextChar(index, value).Should().Be(expected);

        [Theory]
        [InlineData(null, true)]
        [InlineData("", true)]
        [InlineData("text", false)]
        public void IsEmpty(string subject, bool expected)
        {
            subject.IsEmpty().Should().Be(expected);
            subject.IsNotEmpty().Should().Be(!expected);
        }

        [Theory]
        [InlineData(null, true)]
        [InlineData("", true)]
        [InlineData("    ", true)]
        [InlineData("    \t    ", true)]
        [InlineData("\t\t", true)]
        [InlineData("\t    \t", true)]
        [InlineData("text", false)]
        [InlineData("    text", false)]
        [InlineData("text    ", false)]
        [InlineData("    text    ", false)]
        public void IsEmptyOrWhitespace(string subject, bool expected)
        {
            subject.IsEmptyOrWhitespace().Should().Be(expected);
            subject.IsNotEmptyOrWhitespace().Should().Be(!expected);
        }

        [Theory]
        [InlineData(null, null, -1, false)]
        [InlineData(null, "", -1, false)]
        [InlineData("", null, -1, false)]
        [InlineData("", "", -1, false)]
        [InlineData("a", "aa", -1, false)]
        [InlineData("abcdef", "aa", -1, false)]
        [InlineData("abcdef", "aa", 5, false)]
        [InlineData("abcdef", "aa", 1, false)]
        [InlineData("abcdef", "bc", 1, true)]
        public void IsMatch(string subject, string value, int index, bool expected)
            => subject.IsMatch(value, index).Should().Be(expected);

        [Fact]
        public void StartsWithAny()
        {
            string text = "aaabbbccc";

            text.StartsWithAny("bbb", "ccc", "a").Should().BeTrue();
            text.StartsWithAny("bbb", "ccc", "aa").Should().BeTrue();
            text.StartsWithAny("bbb", "ccc", "aaa").Should().BeTrue();
            text.StartsWithAny("bbb", "ccc", "aaaa").Should().BeFalse();

            text.StartsWithAny((IEnumerable<string>)new[] { "bbb", "ccc", "a" }).Should().BeTrue();
            text.StartsWithAny((IEnumerable<string>)new[] { "bbb", "ccc", "aa" }).Should().BeTrue();
            text.StartsWithAny((IEnumerable<string>)new[] { "bbb", "ccc", "aaa" }).Should().BeTrue();
            text.StartsWithAny((IEnumerable<string>)new[] { "bbb", "ccc", "aaaa" }).Should().BeFalse();
        }

        [Fact]
        public void EndsWithAny()
        {
            string text = "aaabbbccc";

            text.EndsWithAny("aaa", "bbb", "c").Should().BeTrue();
            text.EndsWithAny("aaa", "bbb", "cc").Should().BeTrue();
            text.EndsWithAny("aaa", "bbb", "ccc").Should().BeTrue();
            text.EndsWithAny("aaa", "bbb", "cccc").Should().BeFalse();

            text.EndsWithAny((IEnumerable<string>)new[] { "aaa", "bbb", "c" }).Should().BeTrue();
            text.EndsWithAny((IEnumerable<string>)new[] { "aaa", "bbb", "cc" }).Should().BeTrue();
            text.EndsWithAny((IEnumerable<string>)new[] { "aaa", "bbb", "ccc" }).Should().BeTrue();
            text.EndsWithAny((IEnumerable<string>)new[] { "aaa", "bbb", "cccc" }).Should().BeFalse();
        }
    }
}
