﻿using System;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests._Extensions
{
    public class Test_StringExtensions_NormalizeLineEndings
    {
        [Theory]
        [InlineData(ELineEnding.N, "ab", "ab")]
        [InlineData(ELineEnding.N, "a\n\nb", "a\n\nb")]
        [InlineData(ELineEnding.N, "a\r\nb\n \r   \n\nc\n\rd", "a\nb\n \n   \n\nc\nd")]
        [InlineData(ELineEnding.R, "a\r\nb\n \r   \n\nc\n\rd", "a\rb\r \r   \r\rc\rd")]
        [InlineData(ELineEnding.RN, "a\r\nb\n \r   \n\nc\n\rd", "a\r\nb\r\n \r\n   \r\n\r\nc\r\nd")]
        [InlineData(ELineEnding.NR, "a\r\nb\n \r   \n\nc\n\rd", "a\n\rb\n\r \n\r   \n\r\n\rc\n\rd")]
        public void NormalizeLineEndings(ELineEnding ending, string subject, string expected)
            => subject.NormalizeLineEndings(ending).Should().Be(expected);

        [Theory]
        [InlineData(ELineEnding.N, "ab", "ab")]
        [InlineData(ELineEnding.N, "a\n\nb", "a\nb")]
        [InlineData(ELineEnding.N, "a\n\n \n\nb", "a\nb")]
        [InlineData(ELineEnding.N, "a\r\nb\n \r   \n\nc\n\rd", "a\nb\nc\nd")]
        [InlineData(ELineEnding.R, "a\r\nb\n \r   \n\nc\n\rd", "a\rb\rc\rd")]
        [InlineData(ELineEnding.RN, "a\r\nb\n \r   \n\nc\n\rd", "a\r\nb\r\nc\r\nd")]
        [InlineData(ELineEnding.NR, "a\r\nb\n \r   \n\nc\n\rd", "a\n\rb\n\rc\n\rd")]
        public void NormalizeMergeLineEndings_True(ELineEnding ending, string subject, string expected)
            => subject.NormalizeMergeLineEndings(ending, true).Should().Be(expected);

        [Theory]
        [InlineData(ELineEnding.N, "ab", "ab")]
        [InlineData(ELineEnding.N, "a\n\nb", "a\nb")]
        [InlineData(ELineEnding.N, "a\n\n \n\nb", "a\n \nb")]
        [InlineData(ELineEnding.N, "a\r\nb\n \r   \n\nc\n\rd", "a\nb\n \n   \nc\nd")]
        [InlineData(ELineEnding.R, "a\r\nb\n \r   \n\nc\n\rd", "a\rb\r \r   \rc\rd")]
        [InlineData(ELineEnding.RN, "a\r\nb\n \r   \n\nc\n\rd", "a\r\nb\r\n \r\n   \r\nc\r\nd")]
        [InlineData(ELineEnding.NR, "a\r\nb\n \r   \n\nc\n\rd", "a\n\rb\n\r \n\r   \n\rc\n\rd")]
        public void NormalizeMergeLineEndings_False(ELineEnding ending, string subject, string expected)
            => subject.NormalizeMergeLineEndings(ending, false).Should().Be(expected);
    }
}
