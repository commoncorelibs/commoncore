﻿using System.Text;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests._Extensions
{
    public class Test_StringBuilderExtensions_Converting
    {

        #region Clone Tests

        [Theory]
        [InlineData("")]
        [InlineData("Some text.")]
        public void Clone(string subject)
        {
            var source = new StringBuilder(subject);
            var clone = source.Clone();
            clone.Length.Should().Be(source.Length);
            clone.Capacity.Should().Be(source.Capacity);
            clone.MaxCapacity.Should().Be(source.MaxCapacity);
            clone.ToString().Should().Be(source.ToString());
        }

        #endregion

        #region ToCharArray Tests

        [Theory]
        [InlineData("", new char[0])]
        [InlineData("abcdefgh", new[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' })]
        public void ToCharArray0(string subject, char[] expected)
            => new StringBuilder(subject).ToCharArray().Should().BeEquivalentTo(expected);

        [Theory]
        [InlineData("", 0, new char[0])]
        [InlineData("abcdefgh", 0, new[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' })]
        [InlineData("abcdefgh", 1, new[] { 'b', 'c', 'd', 'e', 'f', 'g', 'h' })]
        [InlineData("abcdefgh", 7, new[] { 'h' })]
        [InlineData("abcdefgh", 8, new char[0])]
        public void ToCharArray1(string subject, int startIndex, char[] expected)
            => new StringBuilder(subject).ToCharArray(startIndex).Should().BeEquivalentTo(expected);

        [Theory]
        [InlineData("", 0, -1, new char[0])]
        [InlineData("abcdefgh", 0, 0, new char[0])]
        [InlineData("abcdefgh", 0, -1, new[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' })]
        [InlineData("abcdefgh", 1, -1, new[] { 'b', 'c', 'd', 'e', 'f', 'g', 'h' })]
        [InlineData("abcdefgh", 7, -1, new[] { 'h' })]
        [InlineData("abcdefgh", 8, -1, new char[0])]
        [InlineData("abcdefgh", 1, 6, new[] { 'b', 'c', 'd', 'e', 'f', 'g' })]
        [InlineData("abcdefgh", 1, 7, new[] { 'b', 'c', 'd', 'e', 'f', 'g', 'h' })]
        [InlineData("abcdefgh", 1, 8, new[] { 'b', 'c', 'd', 'e', 'f', 'g', 'h' })]
        public void ToCharArray2(string subject, int startIndex, int maxCount, char[] expected)
            => new StringBuilder(subject).ToCharArray(startIndex, maxCount).Should().BeEquivalentTo(expected);

        #endregion

        #region ToCharSequence Tests

        [Theory]
        [InlineData("", new char[0])]
        [InlineData("abcdefgh", new[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' })]
        public void ToCharSequence0(string subject, char[] expected)
            => new StringBuilder(subject).ToCharSequence().Should().BeEquivalentTo(expected);

        [Theory]
        [InlineData("", 0, new char[0])]
        [InlineData("abcdefgh", 0, new[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' })]
        [InlineData("abcdefgh", 1, new[] { 'b', 'c', 'd', 'e', 'f', 'g', 'h' })]
        [InlineData("abcdefgh", 7, new[] { 'h' })]
        [InlineData("abcdefgh", 8, new char[0])]
        public void ToCharSequence1(string subject, int startIndex, char[] expected)
            => new StringBuilder(subject).ToCharSequence(startIndex).Should().BeEquivalentTo(expected);

        [Theory]
        [InlineData("", 0, -1, new char[0])]
        [InlineData("abcdefgh", 0, 0, new char[0])]
        [InlineData("abcdefgh", 0, -1, new[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' })]
        [InlineData("abcdefgh", 1, -1, new[] { 'b', 'c', 'd', 'e', 'f', 'g', 'h' })]
        [InlineData("abcdefgh", 7, -1, new[] { 'h' })]
        [InlineData("abcdefgh", 8, -1, new char[0])]
        [InlineData("abcdefgh", 1, 6, new[] { 'b', 'c', 'd', 'e', 'f', 'g' })]
        [InlineData("abcdefgh", 1, 7, new[] { 'b', 'c', 'd', 'e', 'f', 'g', 'h' })]
        [InlineData("abcdefgh", 1, 8, new[] { 'b', 'c', 'd', 'e', 'f', 'g', 'h' })]
        public void ToCharSequence2(string subject, int startIndex, int maxCount, char[] expected)
            => new StringBuilder(subject).ToCharSequence(startIndex, maxCount).Should().BeEquivalentTo(expected);

        #endregion

    }
}
