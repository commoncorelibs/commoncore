﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Text;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests._Extensions
{
    [ExcludeFromCodeCoverage]
    public class Test_StringBuilderExtensions_Formatting
    {

        #region Reverse Methods

        [Theory]
        [InlineData("", 0, -1, "")]
        [InlineData("a", 0, -1, "a")]
        [InlineData("abcdefgh", 0, -1, "hgfedcba")]
        [InlineData("abcdefgh", 1, 6, "agfedcbh")]
        public void Reverse(string subject, int startIndex, int maxCount, string expected)
            => new StringBuilder(subject).Reverse(startIndex, maxCount).ToString().Should().Be(expected);

        #endregion

        #region ToLower Tests

        [Theory]
        [InlineData("", 0, -1, "")]
        [InlineData("ABCDEFGH", -1, -1, null, true)]
        [InlineData("ABCDEFGH", 0, -2, null, true)]

        [InlineData("ABCDEFGH", 0, -1, "abcdefgh")]
        [InlineData("ABCDEFGH", 1, -1, "Abcdefgh")]
        [InlineData("ABCDEFGH", 7, -1, "ABCDEFGh")]
        [InlineData("ABCDEFGH", 8, -1, "ABCDEFGH")]

        [InlineData("ABCDEFGH", 0, 9, "abcdefgh")]
        [InlineData("ABCDEFGH", 0, 8, "abcdefgh")]
        [InlineData("ABCDEFGH", 0, 7, "abcdefgH")]
        [InlineData("ABCDEFGH", 1, 6, "AbcdefgH")]
        public void ToLower(string subject, int startIndex, int maxCount, string expected, bool throws = false)
        {
            if (!throws)
            {
                new StringBuilder(subject).ToLower(startIndex, maxCount, null).ToString().Should().Be(expected);
                new StringBuilder(subject).ToLower(startIndex, maxCount, CultureInfo.InvariantCulture).ToString().Should().Be(expected);
            }
            else
            {
                Action act;
                act = () => new StringBuilder(subject).ToLower(startIndex, maxCount, null);
                act.Should().Throw<Exception>();
                act = () => new StringBuilder(subject).ToLower(startIndex, maxCount, CultureInfo.InvariantCulture);
                act.Should().Throw<Exception>();
            }
        }

        #endregion

        #region ToLowerChar Tests

        [Theory]
        [InlineData(null, 0, null, true)]
        [InlineData("", 0, null, true)]
        [InlineData("ABCDEFGH", -1, null, true)]
        [InlineData("ABCDEFGH", 8, null, true)]
        [InlineData("ABCDEFGH", 0, "aBCDEFGH")]
        [InlineData("ABCDEFGH", 7, "ABCDEFGh")]
        public void ToLowerChar(string subject, int index, string expected, bool throws = false)
        {
            if (!throws)
            {
                new StringBuilder(subject).ToLowerChar(index, null).ToString().Should().Be(expected);
                new StringBuilder(subject).ToLowerChar(index, CultureInfo.InvariantCulture).ToString().Should().Be(expected);
            }
            else
            {
                Action act;
                act = () => new StringBuilder(subject).ToLowerChar(index, null);
                act.Should().Throw<Exception>();
                act = () => new StringBuilder(subject).ToLowerChar(index, CultureInfo.InvariantCulture);
                act.Should().Throw<Exception>();
            }
        }

        #endregion

        #region ToUpper Tests

        [Theory]
        [InlineData("", 0, -1, "")]
        [InlineData("abcdefgh", -1, -1, null, true)]
        [InlineData("abcdefgh", 0, -2, null, true)]

        [InlineData("abcdefgh", 0, -1, "ABCDEFGH")]
        [InlineData("abcdefgh", 1, -1, "aBCDEFGH")]
        [InlineData("abcdefgh", 7, -1, "abcdefgH")]
        [InlineData("abcdefgh", 8, -1, "abcdefgh")]

        [InlineData("abcdefgh", 0, 9, "ABCDEFGH")]
        [InlineData("abcdefgh", 0, 8, "ABCDEFGH")]
        [InlineData("abcdefgh", 0, 7, "ABCDEFGh")]
        [InlineData("abcdefgh", 1, 6, "aBCDEFGh")]
        public void ToUpper(string subject, int startIndex, int maxCount, string expected, bool throws = false)
        {
            if (!throws)
            {
                new StringBuilder(subject).ToUpper(startIndex, maxCount, null).ToString().Should().Be(expected);
                new StringBuilder(subject).ToUpper(startIndex, maxCount, CultureInfo.InvariantCulture).ToString().Should().Be(expected);
            }
            else
            {
                Action act;
                act = () => new StringBuilder(subject).ToUpper(startIndex, maxCount, null);
                act.Should().Throw<Exception>();
                act = () => new StringBuilder(subject).ToUpper(startIndex, maxCount, CultureInfo.InvariantCulture);
                act.Should().Throw<Exception>();
            }
        }

        #endregion

        #region ToUpperChar Tests

        [Theory]
        [InlineData(null, 0, null, true)]
        [InlineData("", 0, null, true)]
        [InlineData("abcdefgh", -1, null, true)]
        [InlineData("abcdefgh", 8, null, true)]
        [InlineData("abcdefgh", 0, "Abcdefgh")]
        [InlineData("abcdefgh", 7, "abcdefgH")]
        public void ToUpperChar(string subject, int index, string expected, bool throws = false)
        {
            if (!throws)
            {
                new StringBuilder(subject).ToUpperChar(index, null).ToString().Should().Be(expected);
                new StringBuilder(subject).ToUpperChar(index, CultureInfo.InvariantCulture).ToString().Should().Be(expected);
            }
            else
            {
                Action act;
                act = () => new StringBuilder(subject).ToUpperChar(index, null);
                act.Should().Throw<Exception>();
                act = () => new StringBuilder(subject).ToUpperChar(index, CultureInfo.InvariantCulture);
                act.Should().Throw<Exception>();
            }
        }

        #endregion

        #region Trim Tests

        [Theory]
        [InlineData("", "")]
        [InlineData("a  a", "a  a")]
        [InlineData("  \t  a  a  \t  ", "a  a")]
        public void Trim0(string subject, string expected)
            => new StringBuilder(subject).Trim().ToString().Should().Be(expected);

        [Theory]
        [InlineData("", ' ', "")]
        [InlineData("a  a", ' ', "a  a")]
        [InlineData("  \t  a  a  \t  ", ' ', "\t  a  a  \t")]
        public void Trim_Char(string subject, char trimChar, string expected)
            => new StringBuilder(subject).Trim(trimChar).ToString().Should().Be(expected);

        [Theory]
        [InlineData("", new char[0], "")]
        [InlineData("a  a", new[] { ' ', '\t' }, "a  a")]
        [InlineData("  \t  a  a  \t  ", new[] { ' ', '\t' }, "a  a")]
        public void Trim_Params_Char(string subject, char[] trimChars, string expected)
            => new StringBuilder(subject).Trim(trimChars).ToString().Should().Be(expected);

        [Theory]
        [InlineData("", "", "")]
        [InlineData("a  a", " ", "a  a")]
        [InlineData("  \t  a  a  \t  ", " ", "\t  a  a  \t")]
        public void Trim_String(string subject, string value, string expected)
            => new StringBuilder(subject).Trim(value).ToString().Should().Be(expected);

        #endregion

        #region TrimEnd Tests

        [Theory]
        [InlineData("", "")]
        [InlineData("a  a", "a  a")]
        [InlineData("  \t  a  a  \t  ", "  \t  a  a")]
        public void TrimEnd0(string subject, string expected)
            => new StringBuilder(subject).TrimEnd().ToString().Should().Be(expected);

        [Theory]
        [InlineData("", ' ', "")]
        [InlineData("a  a", ' ', "a  a")]
        [InlineData("  \t  a  a  \t  ", ' ', "  \t  a  a  \t")]
        public void TrimEnd_Char(string subject, char trimChar, string expected)
            => new StringBuilder(subject).TrimEnd(trimChar).ToString().Should().Be(expected);

        [Theory]
        [InlineData("", new char[0], "")]
        [InlineData("a  a", new[] { ' ', '\t' }, "a  a")]
        [InlineData("  \t  a  a  \t  ", new[] { ' ', '\t' }, "  \t  a  a")]
        public void TrimEnd_Params_Char(string subject, char[] trimChars, string expected)
            => new StringBuilder(subject).TrimEnd(trimChars).ToString().Should().Be(expected);

        [Theory]
        [InlineData("", "", "")]
        [InlineData("a  a", " ", "a  a")]
        [InlineData("  \t  a  a  \t  ", " ", "  \t  a  a  \t")]
        public void TrimEnd_String(string subject, string value, string expected)
            => new StringBuilder(subject).TrimEnd(value).ToString().Should().Be(expected);

        #endregion

        #region TrimStart Tests

        [Theory]
        [InlineData("", "")]
        [InlineData("a  a", "a  a")]
        [InlineData("  \t  a  a  \t  ", "a  a  \t  ")]
        public void TrimStart0(string subject, string expected)
            => new StringBuilder(subject).TrimStart().ToString().Should().Be(expected);

        [Theory]
        [InlineData("", ' ', "")]
        [InlineData("a  a", ' ', "a  a")]
        [InlineData("  \t  a  a  \t  ", ' ', "\t  a  a  \t  ")]
        public void TrimStart_Char(string subject, char trimChar, string expected)
            => new StringBuilder(subject).TrimStart(trimChar).ToString().Should().Be(expected);

        [Theory]
        [InlineData("", new char[0], "")]
        [InlineData("a  a", new[] { ' ', '\t' }, "a  a")]
        [InlineData("  \t  a  a  \t  ", new[] { ' ', '\t' }, "a  a  \t  ")]
        public void TrimStart_Params_Char(string subject, char[] trimChars, string expected)
            => new StringBuilder(subject).TrimStart(trimChars).ToString().Should().Be(expected);

        [Theory]
        [InlineData("", "", "")]
        [InlineData("a  a", " ", "a  a")]
        [InlineData("  \t  a  a  \t  ", " ", "\t  a  a  \t  ")]
        public void TrimStart_String(string subject, string value, string expected)
            => new StringBuilder(subject).TrimStart(value).ToString().Should().Be(expected);

        #endregion

    }
}
