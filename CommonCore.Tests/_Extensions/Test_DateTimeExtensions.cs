﻿using System;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests._Extensions
{
    public class Test_DateTimeExtensions
    {
        [Theory]
        [InlineData(2019, 10, 5, 12, 50, 50, 2019, 10, 5, 12, 50, 50, true)]
        [InlineData(2019, 10, 5, 12, 50, 50, 2019, 10, 5, 8, 50, 50, true)]
        [InlineData(2019, 10, 5, 12, 50, 50, 2019, 10, 4, 12, 50, 50, false)]
        [InlineData(2019, 10, 5, 12, 50, 50, 2019, 10, 4, 8, 50, 50, false)]
        public void EqualDate(int yearA, int monthA, int dateA, int hourA, int minuteA, int secondA, int yearB, int monthB, int dateB, int hourB, int minuteB, int secondB, bool expected)
        {
            var a = new DateTime(yearA, monthA, dateA, hourA, minuteA, secondA);
            var b = new DateTime(yearB, monthB, dateB, hourB, minuteB, secondB);
            a.EqualDate(b).Should().Be(expected);
        }

        [Theory]
        [InlineData(2019, 10, 5, 12, 50, 50, 2019, 10, 5, 12, 50, 50, true)]
        [InlineData(2019, 10, 5, 12, 50, 50, 2019, 10, 5, 8, 50, 50, false)]
        [InlineData(2019, 10, 5, 12, 50, 50, 2019, 10, 4, 12, 50, 50, true)]
        [InlineData(2019, 10, 5, 12, 50, 50, 2019, 10, 4, 8, 50, 50, false)]
        public void EqualTime(int yearA, int monthA, int dateA, int hourA, int minuteA, int secondA, int yearB, int monthB, int dateB, int hourB, int minuteB, int secondB, bool expected)
        {
            var a = new DateTime(yearA, monthA, dateA, hourA, minuteA, secondA);
            var b = new DateTime(yearB, monthB, dateB, hourB, minuteB, secondB);
            a.EqualTime(b).Should().Be(expected);
        }

        [Theory]
        [InlineData(00, PeriodOfDay.Night)]
        [InlineData(01, PeriodOfDay.Night)]
        [InlineData(02, PeriodOfDay.Night)]
        [InlineData(03, PeriodOfDay.Night)]
        [InlineData(04, PeriodOfDay.Night)]
        [InlineData(05, PeriodOfDay.Night)]
        [InlineData(06, PeriodOfDay.Morning)]
        [InlineData(07, PeriodOfDay.Morning)]
        [InlineData(08, PeriodOfDay.Morning)]
        [InlineData(09, PeriodOfDay.Morning)]
        [InlineData(10, PeriodOfDay.Morning)]
        [InlineData(11, PeriodOfDay.Morning)]
        [InlineData(12, PeriodOfDay.Afternoon)]
        [InlineData(13, PeriodOfDay.Afternoon)]
        [InlineData(14, PeriodOfDay.Afternoon)]
        [InlineData(15, PeriodOfDay.Afternoon)]
        [InlineData(16, PeriodOfDay.Afternoon)]
        [InlineData(17, PeriodOfDay.Afternoon)]
        [InlineData(18, PeriodOfDay.Evening)]
        [InlineData(19, PeriodOfDay.Evening)]
        [InlineData(20, PeriodOfDay.Evening)]
        [InlineData(21, PeriodOfDay.Evening)]
        [InlineData(22, PeriodOfDay.Night)]
        [InlineData(23, PeriodOfDay.Night)]
        public void GetPeriodOfDay(int hour, PeriodOfDay expected) => new DateTime(2019, 09, 21, hour, 0, 0).GetPeriodOfDay().Should().Be(expected);

        [Theory]
        [InlineData(2019, 09, 1, false)]
        [InlineData(2019, 09, 2, true)]
        [InlineData(2019, 09, 3, true)]
        [InlineData(2019, 09, 4, true)]
        [InlineData(2019, 09, 5, true)]
        [InlineData(2019, 09, 6, true)]
        [InlineData(2019, 09, 7, false)]
        public void IsWeekday(int year, int month, int date, bool expected) => new DateTime(year, month, date).IsWeekday().Should().Be(expected);

        [Theory]
        [InlineData(2019, 09, 1, true)]
        [InlineData(2019, 09, 2, false)]
        [InlineData(2019, 09, 3, false)]
        [InlineData(2019, 09, 4, false)]
        [InlineData(2019, 09, 5, false)]
        [InlineData(2019, 09, 6, false)]
        [InlineData(2019, 09, 7, true)]
        public void IsWeekend(int year, int month, int date, bool expected) => new DateTime(year, month, date).IsWeekend().Should().Be(expected);

        [Fact]
        public void ToFriendlyString()
        {
            DateTime value;

            value = DateTime.Now;
            value.ToFriendlyString().Should().StartWith("Today at ");

            value = DateTime.Now.AddDays(-1);
            value.ToFriendlyString().Should().StartWith("Yesterday at ");

            value = DateTime.Now.AddDays(-4);
            value.ToFriendlyString().Should().StartWith($"{value:dddd} at ");

            // This test will fail if run in the first week of the year, because
            // this format is only given if the date is more than 6 days ago
            // and in the same year.
            value = DateTime.Now.AddDays(-8);
            value.ToFriendlyString().Should().StartWith($"{value:MMMM dd} at ");

            value = DateTime.Now.AddYears(-1);
            value.ToFriendlyString().Should().StartWith($"{value:MMMM dd, yyyy} at ");
        }

        [Fact]
        public void ToLastMoment()
        {
            var now = DateTime.Now;
            var value = now.ToLastMoment();
            value.Year.Should().Be(now.Year);
            value.Month.Should().Be(now.Month);
            value.Day.Should().Be(now.Day);
            value.Hour.Should().Be(23);
            value.Minute.Should().Be(59);
            value.Second.Should().Be(59);
            value.Millisecond.Should().Be(999);
        }

        [Fact]
        public void ToMidnight()
        {
            var now = DateTime.Now;
            var value = now.ToMidnight();
            value.Year.Should().Be(now.Year);
            value.Month.Should().Be(now.Month);
            value.Day.Should().Be(now.Day);
            value.Hour.Should().Be(0);
            value.Minute.Should().Be(0);
            value.Second.Should().Be(0);
            value.Millisecond.Should().Be(0);
        }

        [Fact]
        public void ToNoon()
        {
            var now = DateTime.Now;
            var value = now.ToNoon();
            value.Year.Should().Be(now.Year);
            value.Month.Should().Be(now.Month);
            value.Day.Should().Be(now.Day);
            value.Hour.Should().Be(12);
            value.Minute.Should().Be(0);
            value.Second.Should().Be(0);
            value.Millisecond.Should().Be(0);
        }

        [Fact]
        public void ToTomorrow()
        {
            var now = DateTime.Now.ToUniversalTime();
            now.ToTomorrow().Should().Be(now.AddDays(1));
        }

        [Fact]
        public void ToYesterday()
        {
            var now = DateTime.Now;
            now.ToYesterday().Should().Be(now.AddDays(-1));
        }

        [Theory]
        [InlineData(2050, 06, 15, 2538864000L)]
        [InlineData(2019, 09, 27, 1569542400L)]
        [InlineData(1999, 01, 12, 916099200L)]
        [InlineData(1986, 03, 07, 510537600L)]
        [InlineData(1969, 12, 25, -604800L)]
        public void ToUnixTimeStamp(int year, int month, int date, long expected) => new DateTime(year, month, date).ToUnixTimeStamp().Should().Be(expected);
    }
}
