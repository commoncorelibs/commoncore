﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests._Extensions
{
    public class Test_StringExtensions_Converting
    {

        #region ToLower Tests

        [Theory]
        [InlineData("", -1, "")]
        [InlineData("aBCd", 1, "abCd")]
        [InlineData("aBCd", 2, "aBcd")]
        public void ToLowerChar(string subject, int index, string expected)
            => subject.ToLowerChar(index).Should().Be(expected);

        [Theory]
        [InlineData("", -1, "")]
        [InlineData("aBCdeF", 1, "abcdef")]
        [InlineData("aBCdeF", 2, "aBcdef")]
        public void ToLower(string subject, int start, string expected)
            => subject.ToLower(start).Should().Be(expected);

        [Theory]
        [InlineData("", -1, 0, "")]
        [InlineData("aBCDEfG", 1, 2, "abcDEfG")]
        [InlineData("aBCDEfG", 2, 3, "aBcdefG")]
        public void ToLower_Length(string subject, int start, int length, string expected)
            => subject.ToLower(start, length).Should().Be(expected);

        [Theory]
        [InlineData("", -1, "")]
        [InlineData("aBCd", 1, "abCd")]
        [InlineData("aBCd", 2, "aBcd")]
        public void ToLowerCharInvariant(string subject, int index, string expected)
            => subject.ToLowerCharInvariant(index).Should().Be(expected);

        [Theory]
        [InlineData("", -1, "")]
        [InlineData("aBCdeF", 1, "abcdef")]
        [InlineData("aBCdeF", 2, "aBcdef")]
        public void ToLowerInvariant(string subject, int start, string expected)
            => subject.ToLowerInvariant(start).Should().Be(expected);

        [Theory]
        [InlineData("", -1, 0, "")]
        [InlineData("aBCDEfG", 1, 2, "abcDEfG")]
        [InlineData("aBCDEfG", 2, 3, "aBcdefG")]
        public void ToLowerInvariant_Length(string subject, int start, int length, string expected)
            => subject.ToLowerInvariant(start, length).Should().Be(expected);

        #endregion

        #region ToOrdinalString Tests

        [Theory]
        [InlineData(null, null)]
        [InlineData("", "")]
        [InlineData("abc", "abcth")]
        [InlineData("0", "0th")]
        [InlineData("1", "1st")]
        [InlineData("2", "2nd")]
        [InlineData("3", "3rd")]
        [InlineData("4", "4th")]
        [InlineData("5", "5th")]
        [InlineData("6", "6th")]
        [InlineData("7", "7th")]
        [InlineData("8", "8th")]
        [InlineData("9", "9th")]
        [InlineData("10", "10th")]
        [InlineData("11", "11th")]
        [InlineData("12", "12th")]
        [InlineData("13", "13th")]
        [InlineData("14", "14th")]
        [InlineData("15", "15th")]
        [InlineData("16", "16th")]
        [InlineData("17", "17th")]
        [InlineData("18", "18th")]
        [InlineData("19", "19th")]
        [InlineData("20", "20th")]
        [InlineData("21", "21st")]
        [InlineData("22", "22nd")]
        [InlineData("23", "23rd")]
        [InlineData("24", "24th")]
        public void ToOrdinalString(string subject, string expected)
            => StringExtensions_Converting.ToOrdinalString(subject).Should().Be(expected);

        [Theory]
        [InlineData(0, "0th")]
        [InlineData(1, "1st")]
        [InlineData(2, "2nd")]
        [InlineData(3, "3rd")]
        [InlineData(4, "4th")]
        [InlineData(5, "5th")]
        [InlineData(6, "6th")]
        [InlineData(7, "7th")]
        [InlineData(8, "8th")]
        [InlineData(9, "9th")]
        [InlineData(10, "10th")]
        [InlineData(11, "11th")]
        [InlineData(12, "12th")]
        [InlineData(13, "13th")]
        [InlineData(14, "14th")]
        [InlineData(15, "15th")]
        [InlineData(16, "16th")]
        [InlineData(17, "17th")]
        [InlineData(18, "18th")]
        [InlineData(19, "19th")]
        [InlineData(20, "20th")]
        [InlineData(21, "21st")]
        [InlineData(22, "22nd")]
        [InlineData(23, "23rd")]
        [InlineData(24, "24th")]
        public void ToOrdinalString_Positive(int subject, string expected)
        {
            ((byte)subject).ToOrdinalString().Should().Be(expected);
            ((sbyte)subject).ToOrdinalString().Should().Be(expected);
            ((short)subject).ToOrdinalString().Should().Be(expected);
            ((ushort)subject).ToOrdinalString().Should().Be(expected);
            ((int)subject).ToOrdinalString().Should().Be(expected);
            ((uint)subject).ToOrdinalString().Should().Be(expected);
            ((long)subject).ToOrdinalString().Should().Be(expected);
            ((ulong)subject).ToOrdinalString().Should().Be(expected);
        }

        [Theory]
        [InlineData(0, "0th")]
        [InlineData(-1, "-1st")]
        [InlineData(-2, "-2nd")]
        [InlineData(-3, "-3rd")]
        [InlineData(-4, "-4th")]
        [InlineData(-5, "-5th")]
        [InlineData(-6, "-6th")]
        [InlineData(-7, "-7th")]
        [InlineData(-8, "-8th")]
        [InlineData(-9, "-9th")]
        [InlineData(-10, "-10th")]
        [InlineData(-11, "-11th")]
        [InlineData(-12, "-12th")]
        [InlineData(-13, "-13th")]
        [InlineData(-14, "-14th")]
        [InlineData(-15, "-15th")]
        [InlineData(-16, "-16th")]
        [InlineData(-17, "-17th")]
        [InlineData(-18, "-18th")]
        [InlineData(-19, "-19th")]
        [InlineData(-20, "-20th")]
        [InlineData(-21, "-21st")]
        [InlineData(-22, "-22nd")]
        [InlineData(-23, "-23rd")]
        [InlineData(-24, "-24th")]
        public void ToOrdinalString_Negative(int subject, string expected)
        {
            ((sbyte)subject).ToOrdinalString().Should().Be(expected);
            ((short)subject).ToOrdinalString().Should().Be(expected);
            ((int)subject).ToOrdinalString().Should().Be(expected);
            ((long)subject).ToOrdinalString().Should().Be(expected);
        }

        #endregion

        #region ToSignedString Tests

        [Theory]
        [InlineData(-1, "-1")]
        [InlineData(0, "0")]
        [InlineData(1, "+1")]
        public void ToSignedString(int subject, string expected)
        {
            ((sbyte)subject).ToSignedString().Should().Be(expected);
            ((short)subject).ToSignedString().Should().Be(expected);
            ((int)subject).ToSignedString().Should().Be(expected);
            ((long)subject).ToSignedString().Should().Be(expected);
            ((float)subject).ToSignedString().Should().Be(expected);
            ((double)subject).ToSignedString().Should().Be(expected);
            ((decimal)subject).ToSignedString().Should().Be(expected);
        }

        #endregion

        #region ToTitleCase Tests

        [Theory]
        [InlineData("movie title", "Movie Title")]
        [InlineData("movie ACRONYM title", "Movie ACRONYM Title")]
        // TODO: Implement actual testing with different cultures.
        public void ToTitleCase(string subject, string expected)
        {
            subject.ToTitleCase().Should().Be(expected);
            subject.ToTitleCaseInvariant().Should().Be(expected);
            subject.ToTitleCase(CultureInfo.CurrentCulture).Should().Be(expected);
            subject.ToTitleCase(CultureInfo.InvariantCulture).Should().Be(expected);
        }

        #endregion

        #region ToUpper Tests

        [Theory]
        [InlineData("", -1, "")]
        [InlineData("abcd", 1, "aBcd")]
        [InlineData("abcd", 2, "abCd")]
        public void ToUpperChar(string subject, int index, string expected)
            => subject.ToUpperChar(index).Should().Be(expected);

        [Theory]
        [InlineData("", -1, "")]
        [InlineData("abcdef", 1, "aBCDEF")]
        [InlineData("abcdef", 2, "abCDEF")]
        public void ToUpper(string subject, int start, string expected)
            => subject.ToUpper(start).Should().Be(expected);

        [Theory]
        [InlineData("", -1, 0, "")]
        [InlineData("abcdef", 1, 2, "aBCdef")]
        [InlineData("abcdef", 2, 3, "abCDEf")]
        public void ToUpper_Length(string subject, int start, int length, string expected)
            => subject.ToUpper(start, length).Should().Be(expected);

        [Theory]
        [InlineData("", -1, "")]
        [InlineData("abcd", 1, "aBcd")]
        [InlineData("abcd", 2, "abCd")]
        public void ToUpperCharInvariant(string subject, int index, string expected)
            => subject.ToUpperCharInvariant(index).Should().Be(expected);

        [Theory]
        [InlineData("", -1, "")]
        [InlineData("abcdef", 1, "aBCDEF")]
        [InlineData("abcdef", 2, "abCDEF")]
        public void ToUpperInvariant(string subject, int start, string expected)
            => subject.ToUpperInvariant(start).Should().Be(expected);

        [Theory]
        [InlineData("", -1, 0, "")]
        [InlineData("abcdef", 1, 2, "aBCdef")]
        [InlineData("abcdef", 2, 3, "abCDEf")]
        public void ToUpperInvariant_Length(string subject, int start, int length, string expected)
            => subject.ToUpperInvariant(start, length).Should().Be(expected);

        #endregion

    }
}
