﻿using System;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests._Extensions
{
    public class Test_NumericExtensions
    {
        [Theory]
        [InlineData(0, 0)]
        [InlineData(1, 1)]
        [InlineData(-1, 1)]
        public void Abs(int value, int expected)
        {
            ((decimal)value).Abs().Should().Be((decimal)expected);
            ((double)value).Abs().Should().Be((double)expected);
            ((float)value).Abs().Should().Be((float)expected);
            ((int)value).Abs().Should().Be((int)expected);
            ((long)value).Abs().Should().Be((long)expected);
            ((sbyte)value).Abs().Should().Be((sbyte)expected);
            ((short)value).Abs().Should().Be((short)expected);
        }

        [Theory]
        [InlineData(0.1, 1.0)]
        [InlineData(0.5, 1.0)]
        [InlineData(0.9, 1.0)]
        [InlineData(-0.1, 0.0)]
        [InlineData(-0.5, 0.0)]
        [InlineData(-0.9, 0.0)]
        public void Ceiling(double value, double expected)
        {
            ((decimal)value).Ceiling().Should().Be((decimal)expected);
            ((double)value).Ceiling().Should().Be((double)expected);
            ((float)value).Ceiling().Should().Be((float)expected);
        }

        [Theory]
        [InlineData(2000, 1500, 2500, 2000)]
        [InlineData(1000, 1500, 2500, 1500)]
        [InlineData(3000, 1500, 2500, 2500)]
        public void Clamp_DateTime(int yearValue, int yearMin, int yearMax, int yearExpected)
        {
            var value = new DateTime(yearValue, 1, 1);
            var min = new DateTime(yearMin, 1, 1);
            var max = new DateTime(yearMax, 1, 1);
            var expected = new DateTime(yearExpected, 1, 1);

            value.Clamp(min, max).Should().Be(expected);
        }

        [Theory]
        [InlineData(10, 5, 15, 10)]
        [InlineData(0, 5, 15, 5)]
        [InlineData(20, 5, 15, 15)]
        public void Clamp_TimeSpan(int hourValue, int hourMin, int hourMax, int hourExpected)
        {
            var value = new TimeSpan(hourValue, 0, 0);
            var min = new TimeSpan(hourMin, 0, 0);
            var max = new TimeSpan(hourMax, 0, 0);
            var expected = new TimeSpan(hourExpected, 0, 0);

            value.Clamp(min, max).Should().Be(expected);
        }

        [Theory]
        [InlineData(10, 5, 15, 10)]
        [InlineData(0, 5, 15, 5)]
        [InlineData(20, 5, 15, 15)]
        public void Clamp(int value, int min, int max, int expected)
        {
            ((byte)value).Clamp((byte)min, (byte)max).Should().Be((byte)expected);
            ((decimal)value).Clamp((decimal)min, (decimal)max).Should().Be((decimal)expected);
            ((double)value).Clamp((double)min, (double)max).Should().Be((double)expected);
            ((float)value).Clamp((float)min, (float)max).Should().Be((float)expected);
            ((int)value).Clamp((int)min, (int)max).Should().Be((int)expected);
            ((long)value).Clamp((long)min, (long)max).Should().Be((long)expected);
            ((sbyte)value).Clamp((sbyte)min, (sbyte)max).Should().Be((sbyte)expected);
            ((short)value).Clamp((short)min, (short)max).Should().Be((short)expected);
            ((uint)value).Clamp((uint)min, (uint)max).Should().Be((uint)expected);
            ((ulong)value).Clamp((ulong)min, (ulong)max).Should().Be((ulong)expected);
            ((ushort)value).Clamp((ushort)min, (ushort)max).Should().Be((ushort)expected);
        }

        [Theory]
        [InlineData(0.1, 0.0)]
        [InlineData(0.5, 0.0)]
        [InlineData(0.9, 0.0)]
        [InlineData(-0.1, -1.0)]
        [InlineData(-0.5, -1.0)]
        [InlineData(-0.9, -1.0)]
        public void Floor(double value, double expected)
        {
            ((decimal)value).Floor().Should().Be((decimal)expected);
            ((double)value).Floor().Should().Be((double)expected);
            ((float)value).Floor().Should().Be((float)expected);
        }

        [Theory]
        [InlineData(2000, 1500, 2000)]
        [InlineData(1500, 2000, 2000)]
        public void Max_DateTime(int yearValue, int yearOther, int yearExpected)
        {
            var value = new DateTime(yearValue, 1, 1);
            var other = new DateTime(yearOther, 1, 1);
            var expected = new DateTime(yearExpected, 1, 1);

            value.Max(other).Should().Be(expected);
        }

        [Theory]
        [InlineData(20, 15, 20)]
        [InlineData(15, 20, 20)]
        public void Max_TimeSpan(int hourValue, int hourOther, int hourExpected)
        {
            var value = new TimeSpan(hourValue, 0, 0);
            var other = new TimeSpan(hourOther, 0, 0);
            var expected = new TimeSpan(hourExpected, 0, 0);

            value.Max(other).Should().Be(expected);
        }

        [Theory]
        [InlineData(20, 15, 20)]
        [InlineData(15, 20, 20)]
        public void Max(int value, int other, int expected)
        {
            ((byte)value).Max((byte)other).Should().Be((byte)expected);
            ((decimal)value).Max((decimal)other).Should().Be((decimal)expected);
            ((double)value).Max((double)other).Should().Be((double)expected);
            ((float)value).Max((float)other).Should().Be((float)expected);
            ((int)value).Max((int)other).Should().Be((int)expected);
            ((long)value).Max((long)other).Should().Be((long)expected);
            ((sbyte)value).Max((sbyte)other).Should().Be((sbyte)expected);
            ((short)value).Max((short)other).Should().Be((short)expected);
            ((uint)value).Max((uint)other).Should().Be((uint)expected);
            ((ulong)value).Max((ulong)other).Should().Be((ulong)expected);
            ((ushort)value).Max((ushort)other).Should().Be((ushort)expected);
        }

        [Theory]
        [InlineData(2000, 1500, 1500)]
        [InlineData(1500, 2000, 1500)]
        public void Min_DateTime(int yearValue, int yearOther, int yearExpected)
        {
            var value = new DateTime(yearValue, 1, 1);
            var other = new DateTime(yearOther, 1, 1);
            var expected = new DateTime(yearExpected, 1, 1);

            value.Min(other).Should().Be(expected);
        }

        [Theory]
        [InlineData(20, 15, 15)]
        [InlineData(15, 20, 15)]
        public void Min_TimeSpan(int hourValue, int hourOther, int hourExpected)
        {
            var value = new TimeSpan(hourValue, 0, 0);
            var other = new TimeSpan(hourOther, 0, 0);
            var expected = new TimeSpan(hourExpected, 0, 0);

            value.Min(other).Should().Be(expected);
        }

        [Theory]
        [InlineData(20, 15, 15)]
        [InlineData(15, 20, 15)]
        public void Min(int value, int other, int expected)
        {
            ((byte)value).Min((byte)other).Should().Be((byte)expected);
            ((decimal)value).Min((decimal)other).Should().Be((decimal)expected);
            ((double)value).Min((double)other).Should().Be((double)expected);
            ((float)value).Min((float)other).Should().Be((float)expected);
            ((int)value).Min((int)other).Should().Be((int)expected);
            ((long)value).Min((long)other).Should().Be((long)expected);
            ((sbyte)value).Min((sbyte)other).Should().Be((sbyte)expected);
            ((short)value).Min((short)other).Should().Be((short)expected);
            ((uint)value).Min((uint)other).Should().Be((uint)expected);
            ((ulong)value).Min((ulong)other).Should().Be((ulong)expected);
            ((ushort)value).Min((ushort)other).Should().Be((ushort)expected);
        }

        [Theory]
        [InlineData(-10, -1)]
        [InlineData(0, 0)]
        [InlineData(10, 1)]
        public void Sign(int value, int expected)
        {
            ((decimal)value).Sign().Should().Be(expected);
            ((double)value).Sign().Should().Be(expected);
            ((float)value).Sign().Should().Be(expected);
            ((int)value).Sign().Should().Be(expected);
            ((long)value).Sign().Should().Be(expected);
            ((sbyte)value).Sign().Should().Be(expected);
            ((short)value).Sign().Should().Be(expected);
        }
    }
}
