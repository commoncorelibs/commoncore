﻿using System.Text;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests._Extensions
{
    public class Test_StringBuilderExtensions_Lines
    {

        #region ToLineSequence Tests

        [Theory]
        [InlineData("", new string[0])]
        [InlineData("abc\ndef\nghi", new[] { "abc", "def", "ghi" })]
        [InlineData("abc\r\ndef\r\nghi", new[] { "abc", "def", "ghi" })]
        [InlineData("abc\rdef\rghi", new[] { "abc", "def", "ghi" })]
        [InlineData("abc\n\rdef\n\rghi", new[] { "abc", "def", "ghi" })]
        public void ToLineSequence(string subject, string[] expected)
            => new StringBuilder(subject).ToLineSequence().Should().HaveCount(expected.Length).And.BeEquivalentTo(expected);

        #endregion

        #region RemoveLine Tests

        [Theory]
        [InlineData("", 0, "")]
        [InlineData("abc", 0, "")]
        [InlineData("abc\n", 0, "")]
        [InlineData("\nabc", 0, "abc")]
        [InlineData("abc\n", 1, "abc")]
        [InlineData("\nabc", 1, "")]

        [InlineData("abc\ndef\nghi", 0, "def\nghi")]
        [InlineData("abc\r\ndef\r\nghi", 0, "def\r\nghi")]
        [InlineData("abc\rdef\rghi", 0, "def\rghi")]
        [InlineData("abc\n\rdef\n\rghi", 0, "def\n\rghi")]

        [InlineData("abc\ndef\nghi", 1, "abc\nghi")]
        [InlineData("abc\r\ndef\r\nghi", 1, "abc\r\nghi")]
        [InlineData("abc\rdef\rghi", 1, "abc\rghi")]
        [InlineData("abc\n\rdef\n\rghi", 1, "abc\n\rghi")]

        [InlineData("abc\ndef\nghi", 2, "abc\ndef")]
        [InlineData("abc\r\ndef\r\nghi", 2, "abc\r\ndef")]
        [InlineData("abc\rdef\rghi", 2, "abc\rdef")]
        [InlineData("abc\n\rdef\n\rghi", 2, "abc\n\rdef")]
        public void RemoveLine(string subject, int index, string expected)
            => new StringBuilder(subject).RemoveLine(index).ToString().Replace("\r", "\\r").Replace("\n", "\\n").Should().Be(expected.Replace("\r", "\\r").Replace("\n", "\\n"));

        #endregion

    }
}
