﻿using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests._Extensions
{
    [ExcludeFromCodeCoverage]
    public class Test_EnumExtensions
    {
        public enum EDummy : int
        {
            [Description("The Describe enum description.")]
            Described = 0,

            NotDescribed = 1
        }

        [Flags] public enum EByteFlags : byte { None = 0, A = 1, B = 2, C = 4, D = 8, E = 16 }

        [Flags] public enum ESByteFlags : sbyte { None = 0, A = 1, B = 2, C = 4, D = 8, E = 16 }

        [Flags] public enum EShortFlags : short { None = 0, A = 1, B = 2, C = 4, D = 8, E = 16 }

        [Flags] public enum EUShortFlags : ushort { None = 0, A = 1, B = 2, C = 4, D = 8, E = 16 }

        [Flags] public enum EIntFlags : int { None = 0, A = 1, B = 2, C = 4, D = 8, E = 16 }

        [Flags] public enum EUIntFlags : uint { None = 0, A = 1, B = 2, C = 4, D = 8, E = 16 }

        [Flags] public enum ELongFlags : long { None = 0, A = 1, B = 2, C = 4, D = 8, E = 16 }

        [Flags] public enum EULongFlags : ulong { None = 0, A = 1, B = 2, C = 4, D = 8, E = 16 }

        [Flags] public enum EFlags : int { None = 0, A = 1, B = 2, C = 4, D = 8, E = 16 }

        public enum ENotFlags : int { None = 0, A = 1, B = 2, C = 4, D = 8, E = 16 }

        [Fact]
        public void GetDescription()
        {
            EDummy.Described.GetDescription().Should().NotBe(string.Empty);
            EDummy.NotDescribed.GetDescription().Should().Be(string.Empty);
        }

        [Theory]
        [InlineData(EDummy.Described)]
        [InlineData(EDummy.NotDescribed)]
        [InlineData(EFlags.None)]
        [InlineData(EFlags.A)]
        public void GetName(Enum value) => value.GetName().Should().Be(value.ToString());

        [Theory]
        [InlineData(true, EDummy.Described, EDummy.Described)]
        [InlineData(false, EDummy.Described, EDummy.NotDescribed)]

        [InlineData(true, EFlags.None, EFlags.None)]

        [InlineData(true, EFlags.A, EFlags.A)]
        [InlineData(true, EFlags.A | EFlags.B, EFlags.A)]
        [InlineData(true, EFlags.A | EFlags.B, EFlags.A | EFlags.B)]
        [InlineData(false, EFlags.A, EFlags.B)]
        [InlineData(false, EFlags.A | EFlags.B, EFlags.A | EFlags.B | EFlags.C)]

        [InlineData(true, EByteFlags.A, EByteFlags.A)]
        [InlineData(true, EByteFlags.A | EByteFlags.B, EByteFlags.A)]
        [InlineData(true, EByteFlags.A | EByteFlags.B, EByteFlags.A | EByteFlags.B)]
        [InlineData(false, EByteFlags.A, EByteFlags.B)]
        [InlineData(false, EByteFlags.A | EByteFlags.B, EByteFlags.A | EByteFlags.B | EByteFlags.C)]

        [InlineData(true, ESByteFlags.A, ESByteFlags.A)]
        [InlineData(true, ESByteFlags.A | ESByteFlags.B, ESByteFlags.A)]
        [InlineData(true, ESByteFlags.A | ESByteFlags.B, ESByteFlags.A | ESByteFlags.B)]
        [InlineData(false, ESByteFlags.A, ESByteFlags.B)]
        [InlineData(false, ESByteFlags.A | ESByteFlags.B, ESByteFlags.A | ESByteFlags.B | ESByteFlags.C)]

        [InlineData(true, EShortFlags.A, EShortFlags.A)]
        [InlineData(true, EShortFlags.A | EShortFlags.B, EShortFlags.A)]
        [InlineData(true, EShortFlags.A | EShortFlags.B, EShortFlags.A | EShortFlags.B)]
        [InlineData(false, EShortFlags.A, EShortFlags.B)]
        [InlineData(false, EShortFlags.A | EShortFlags.B, EShortFlags.A | EShortFlags.B | EShortFlags.C)]

        [InlineData(true, EUShortFlags.A, EUShortFlags.A)]
        [InlineData(true, EUShortFlags.A | EUShortFlags.B, EUShortFlags.A)]
        [InlineData(true, EUShortFlags.A | EUShortFlags.B, EUShortFlags.A | EUShortFlags.B)]
        [InlineData(false, EUShortFlags.A, EUShortFlags.B)]
        [InlineData(false, EUShortFlags.A | EUShortFlags.B, EUShortFlags.A | EUShortFlags.B | EUShortFlags.C)]

        [InlineData(true, EIntFlags.A, EIntFlags.A)]
        [InlineData(true, EIntFlags.A | EIntFlags.B, EIntFlags.A)]
        [InlineData(true, EIntFlags.A | EIntFlags.B, EIntFlags.A | EIntFlags.B)]
        [InlineData(false, EIntFlags.A, EIntFlags.B)]
        [InlineData(false, EIntFlags.A | EIntFlags.B, EIntFlags.A | EIntFlags.B | EIntFlags.C)]

        [InlineData(true, EUIntFlags.A, EUIntFlags.A)]
        [InlineData(true, EUIntFlags.A | EUIntFlags.B, EUIntFlags.A)]
        [InlineData(true, EUIntFlags.A | EUIntFlags.B, EUIntFlags.A | EUIntFlags.B)]
        [InlineData(false, EUIntFlags.A, EUIntFlags.B)]
        [InlineData(false, EUIntFlags.A | EUIntFlags.B, EUIntFlags.A | EUIntFlags.B | EUIntFlags.C)]

        [InlineData(true, ELongFlags.A, ELongFlags.A)]
        [InlineData(true, ELongFlags.A | ELongFlags.B, ELongFlags.A)]
        [InlineData(true, ELongFlags.A | ELongFlags.B, ELongFlags.A | ELongFlags.B)]
        [InlineData(false, ELongFlags.A, ELongFlags.B)]
        [InlineData(false, ELongFlags.A | ELongFlags.B, ELongFlags.A | ELongFlags.B | ELongFlags.C)]

        [InlineData(true, EULongFlags.A, EULongFlags.A)]
        [InlineData(true, EULongFlags.A | EULongFlags.B, EULongFlags.A)]
        [InlineData(true, EULongFlags.A | EULongFlags.B, EULongFlags.A | EULongFlags.B)]
        [InlineData(false, EULongFlags.A, EULongFlags.B)]
        [InlineData(false, EULongFlags.A | EULongFlags.B, EULongFlags.A | EULongFlags.B | EULongFlags.C)]
        public void HasAllFlags(bool expected, Enum value, Enum flags) => value.HasAllFlags(flags).Should().Be(expected);

        [Fact]
        public void HasAllFlags_Throw()
        {
            Action act = () => EFlags.None.HasAllFlags(ENotFlags.None);
            act.Should().Throw<ArgumentException>();
        }

        [Theory]
        [InlineData(false, EDummy.Described, EDummy.NotDescribed)]

        // A value of zero can't have flags, should probably look into them being true for consistency with HasAllFlags.
        [InlineData(false, EDummy.Described, EDummy.Described)]
        [InlineData(false, EFlags.None, EFlags.None)]

        [InlineData(true, EFlags.A, EFlags.A)]
        [InlineData(true, EFlags.A, EFlags.A | EFlags.B)]
        [InlineData(true, EFlags.A | EFlags.B, EFlags.A | EFlags.B)]
        [InlineData(false, EFlags.A, EFlags.B)]
        [InlineData(false, EFlags.A, EFlags.B | EFlags.C)]

        [InlineData(true, EByteFlags.A, EByteFlags.A)]
        [InlineData(true, EByteFlags.A, EByteFlags.A | EByteFlags.B)]
        [InlineData(true, EByteFlags.A | EByteFlags.B, EByteFlags.A | EByteFlags.B)]
        [InlineData(false, EByteFlags.A, EByteFlags.B)]
        [InlineData(false, EByteFlags.A, EByteFlags.B | EByteFlags.C)]

        [InlineData(true, ESByteFlags.A, ESByteFlags.A)]
        [InlineData(true, ESByteFlags.A, ESByteFlags.A | ESByteFlags.B)]
        [InlineData(true, ESByteFlags.A | ESByteFlags.B, ESByteFlags.A | ESByteFlags.B)]
        [InlineData(false, ESByteFlags.A, ESByteFlags.B)]
        [InlineData(false, ESByteFlags.A, ESByteFlags.B | ESByteFlags.C)]

        [InlineData(true, EShortFlags.A, EShortFlags.A)]
        [InlineData(true, EShortFlags.A, EShortFlags.A | EShortFlags.B)]
        [InlineData(true, EShortFlags.A | EShortFlags.B, EShortFlags.A | EShortFlags.B)]
        [InlineData(false, EShortFlags.A, EShortFlags.B)]
        [InlineData(false, EShortFlags.A, EShortFlags.B | EShortFlags.C)]

        [InlineData(true, EUShortFlags.A, EUShortFlags.A)]
        [InlineData(true, EUShortFlags.A, EUShortFlags.A | EUShortFlags.B)]
        [InlineData(true, EUShortFlags.A | EUShortFlags.B, EUShortFlags.A | EUShortFlags.B)]
        [InlineData(false, EUShortFlags.A, EUShortFlags.B)]
        [InlineData(false, EUShortFlags.A, EUShortFlags.B | EUShortFlags.C)]

        [InlineData(true, EIntFlags.A, EIntFlags.A)]
        [InlineData(true, EIntFlags.A, EIntFlags.A | EIntFlags.B)]
        [InlineData(true, EIntFlags.A | EIntFlags.B, EIntFlags.A | EIntFlags.B)]
        [InlineData(false, EIntFlags.A, EIntFlags.B)]
        [InlineData(false, EIntFlags.A, EIntFlags.B | EIntFlags.C)]

        [InlineData(true, EUIntFlags.A, EUIntFlags.A)]
        [InlineData(true, EUIntFlags.A, EUIntFlags.A | EUIntFlags.B)]
        [InlineData(true, EUIntFlags.A | EUIntFlags.B, EUIntFlags.A | EUIntFlags.B)]
        [InlineData(false, EUIntFlags.A, EUIntFlags.B)]
        [InlineData(false, EUIntFlags.A, EUIntFlags.B | EUIntFlags.C)]

        [InlineData(true, ELongFlags.A, ELongFlags.A)]
        [InlineData(true, ELongFlags.A, ELongFlags.A | ELongFlags.B)]
        [InlineData(true, ELongFlags.A | ELongFlags.B, ELongFlags.A | ELongFlags.B)]
        [InlineData(false, ELongFlags.A, ELongFlags.B)]
        [InlineData(false, ELongFlags.A, ELongFlags.B | ELongFlags.C)]

        [InlineData(true, EULongFlags.A, EULongFlags.A)]
        [InlineData(true, EULongFlags.A, EULongFlags.A | EULongFlags.B)]
        [InlineData(true, EULongFlags.A | EULongFlags.B, EULongFlags.A | EULongFlags.B)]
        [InlineData(false, EULongFlags.A, EULongFlags.B)]
        [InlineData(false, EULongFlags.A, EULongFlags.B | EULongFlags.C)]
        public void HasAnyFlags(bool expected, Enum value, Enum flags) => value.HasAnyFlags(flags).Should().Be(expected);

        [Fact]
        public void HasAnyFlags_Throw()
        {
            Action act = () => EFlags.None.HasAnyFlags(ENotFlags.None);
            act.Should().Throw<ArgumentException>();
        }

        [Theory]
        [InlineData(true, EDummy.Described)]
        [InlineData(false, (EDummy)64)]
        public void IsDefined(bool expected, Enum value) => value.IsDefined().Should().Be(expected);

        [Theory]
        [InlineData(true, EFlags.None)]
        [InlineData(false, ENotFlags.None)]
        public void IsFlags(bool expected, Enum value) => value.IsFlags().Should().Be(expected);
    }
}
