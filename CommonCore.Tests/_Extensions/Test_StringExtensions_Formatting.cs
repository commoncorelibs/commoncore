﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests._Extensions
{
    [ExcludeFromCodeCoverage]
    public class Test_StringExtensions_Formatting
    {

        #region AsFormat Tests

        [Theory]
        [InlineData("a b", "{0} {1}", "a", "b")]
        public void AsFormat(string expected, string format, params object[] args)
            => format.AsFormat(args).Should().Be(expected);

        #endregion

        #region Align Tests

        [Theory, Obsolete]
        [InlineData(null, 4, "    ")]
        [InlineData("", 4, "    ")]
        [InlineData("aa", 4, "aa  ")]
        [InlineData("aaa", 4, "aaa ")]
        [InlineData("aaaa", 4, "aaaa")]
        [InlineData("aaaaa", 4, "aaaaa")]
        public void AlignLeft(string subject, int width, string expected)
            => subject.AlignLeft(width).Should().Be(expected);

        [Theory, Obsolete]
        [InlineData(null, 4, "    ")]
        [InlineData("", 4, "    ")]
        [InlineData("aa", 4, "  aa")]
        [InlineData("aaa", 4, " aaa")]
        [InlineData("aaaa", 4, "aaaa")]
        [InlineData("aaaaa", 4, "aaaaa")]
        public void AlignRight(string subject, int width, string expected)
            => subject.AlignRight(width).Should().Be(expected);

        [Theory]
        [InlineData(null, 4, "    ")]
        [InlineData("", 4, "    ")]
        [InlineData("a", 4, " a  ")]
        [InlineData("aa", 4, " aa ")]
        [InlineData("aaa", 4, "aaa ")]
        [InlineData("aaaa", 4, "aaaa")]
        [InlineData("aaaaa", 4, "aaaaa")]
        public void AlignCenter(string subject, int width, string expected)
            => subject.AlignCenter(width).Should().Be(expected);

        #endregion

        #region Trim Tests

        [Theory]
        [InlineData("aba", 0, "aba")]
        [InlineData("", 1, "")]
        [InlineData("a", 1, "")]
        [InlineData("ab", 1, "")]
        [InlineData("aba", 1, "b")]
        [InlineData("aabbaa", 2, "bb")]
        public void Trim_Count1(string subject, int count, string expected)
            => subject.Trim(count).Should().Be(expected);

        [Theory]
        [InlineData("aba", 0, 0, "aba")]
        [InlineData("", 1, 0, "")]
        [InlineData("a", 1, 0, "")]
        [InlineData("ab", 1, 1, "")]
        [InlineData("aba", 1, 1, "b")]
        [InlineData("aabbaa", 2, 2, "bb")]
        [InlineData("aabbaa", 1, 2, "abb")]
        [InlineData("aabbaa", 2, 1, "bba")]
        public void Trim_Count2(string subject, int startCount, int endCount, string expected)
            => subject.Trim(startCount, endCount).Should().Be(expected);

        [Theory]
        [InlineData("", null, null, "")]
        [InlineData("", "", null, "")]
        [InlineData("", null, "", "")]
        [InlineData("", "", "", "")]
        [InlineData("", "x", "", "")]
        [InlineData("", "", "x", "")]
        [InlineData("", "x", "x", "")]
        [InlineData("abc", null, null, "abc")]
        [InlineData("abc", "", null, "abc")]
        [InlineData("abc", null, "", "abc")]
        [InlineData("abc", "", "", "abc")]
        [InlineData("ababcbc", "x", "x", "ababcbc")]
        [InlineData("ababcbc", "a", "x", "babcbc")]
        [InlineData("ababcbc", "x", "c", "ababcb")]
        [InlineData("ababcbc", "a", "c", "babcb")]
        [InlineData("ababcbc", "ab", "x", "cbc")]
        [InlineData("ababcbc", "x", "bc", "aba")]
        [InlineData("ababcbc", "ab", "bc", "")]
        [InlineData("ababbcbc", "ab", "bc", "")]
        public void Trim_String2(string subject, string startValue, string endValue, string expected)
            => subject.Trim(startValue, endValue).Should().Be(expected);

        [Theory]
        [InlineData("", null, null, -1, -1, "")]
        [InlineData("", "", null, -1, -1, "")]
        [InlineData("", null, "", -1, -1, "")]
        [InlineData("", "", "", -1, -1, "")]
        [InlineData("", "x", "", -1, -1, "")]
        [InlineData("", "", "x", -1, -1, "")]
        [InlineData("", "x", "x", -1, -1, "")]
        [InlineData("abc", null, null, -1, -1, "abc")]
        [InlineData("abc", "", null, -1, -1, "abc")]
        [InlineData("abc", null, "", -1, -1, "abc")]
        [InlineData("abc", "", "", -1, -1, "abc")]
        [InlineData("ababcbc", "x", "x", -1, -1, "ababcbc")]
        [InlineData("ababcbc", "a", "x", -1, -1, "babcbc")]
        [InlineData("ababcbc", "x", "c", -1, -1, "ababcb")]
        [InlineData("ababcbc", "a", "c", -1, -1, "babcb")]
        [InlineData("ababcbc", "ab", "x", -1, -1, "cbc")]
        [InlineData("ababcbc", "x", "bc", -1, -1, "aba")]
        [InlineData("ababcbc", "ab", "bc", -1, -1, "")]
        [InlineData("ababbcbc", "ab", "bc", -1, -1, "")]
        [InlineData("ababcbc", "ab", "bc", 0, 0, "ababcbc")]
        [InlineData("ababbcbc", "ab", "bc", 0, 0, "ababbcbc")]
        [InlineData("ababcbc", "ab", "bc", 1, 1, "abc")]
        [InlineData("ababbcbc", "ab", "bc", 1, 1, "abbc")]
        [InlineData("ababcbc", "ab", "bc", 2, 1, "c")]
        [InlineData("ababbcbc", "ab", "bc", 2, 1, "bc")]
        [InlineData("ababcbc", "ab", "bc", 1, 2, "a")]
        [InlineData("ababbcbc", "ab", "bc", 1, 2, "ab")]
        [InlineData("ababcbc", "ab", "bc", 2, 2, "")]
        [InlineData("ababbcbc", "ab", "bc", 2, 2, "")]
        [InlineData("ababcbc", "ab", "bc", 3, 2, "")]
        [InlineData("ababbcbc", "ab", "bc", 3, 2, "")]
        [InlineData("ababcbc", "ab", "bc", 2, 3, "")]
        [InlineData("ababbcbc", "ab", "bc", 2, 3, "")]
        [InlineData("ababcbc", "ab", "bc", 3, 3, "")]
        [InlineData("ababbcbc", "ab", "bc", 3, 3, "")]
        public void Trim_Max_String2(string subject, string startValue, string endValue, int maxStartCount, int maxEndCount, string expected)
            => subject.Trim(startValue, endValue, maxStartCount, maxEndCount).Should().Be(expected);

        [Theory]
        [InlineData("", null, "")]
        [InlineData("", "", "")]
        [InlineData("", "x", "")]
        [InlineData("aba", "x", "aba")]
        [InlineData("aba", "a", "b")]
        [InlineData("aaabaaa", "a", "b")]
        [InlineData("aaaaabaaaaa", "aa", "aba")]
        public void Trim_String(string subject, string value, string expected)
            => subject.Trim(value).Should().Be(expected);

        [Theory]
        [InlineData("", null, -1, "")]
        [InlineData("", "", -1, "")]
        [InlineData("", "x", -1, "")]
        [InlineData("aba", "x", -1, "aba")]
        [InlineData("aba", "a", -1, "b")]
        [InlineData("aaabaaa", "a", -1, "b")]
        [InlineData("aaaaabaaaaa", "aa", -1, "aba")]
        [InlineData("aaaaabaaaaa", "aa", 0, "aaaaabaaaaa")]
        [InlineData("aaaaabaaaaa", "aa", 1, "aaabaaa")]
        [InlineData("aaaaabaaaaa", "aa", 2, "aba")]
        [InlineData("aaaaabaaaaa", "aa", 3, "aba")]
        public void Trim_Max_String(string subject, string value, int max, string expected)
            => subject.Trim(value, max).Should().Be(expected);

        #endregion

        #region TrimEnd Tests

        [Theory]
        [InlineData("abcd", 0, "abcd")]
        [InlineData("", 4, "")]
        [InlineData("abc", 4, "")]
        [InlineData("abcd", 4, "")]
        [InlineData("abcde", 4, "a")]
        public void TrimEnd_Count(string subject, int count, string expected)
            => subject.TrimEnd(count).Should().Be(expected);

        [Theory]
        [InlineData("", null, "")]
        [InlineData("abc", null, "abc")]
        [InlineData("abc", "", "abc")]
        [InlineData("abc", "x", "abc")]
        [InlineData("abcbc", "xc", "abcbc")]
        [InlineData("abcbc", "c", "abcb")]
        [InlineData("abcbc", "bc", "a")]
        public void TrimEnd_String(string subject, string value, string expected)
            => subject.TrimEnd(value).Should().Be(expected);

        [Theory]
        [InlineData("", null, -1, "")]
        [InlineData("", null, 0, "")]
        [InlineData("abc", null, -1, "abc")]
        [InlineData("abc", "", -1, "abc")]
        [InlineData("abc", "x", -1, "abc")]
        [InlineData("abcbc", "xc", -1, "abcbc")]
        [InlineData("abcbc", "c", -1, "abcb")]
        [InlineData("abcbc", "bc", -1, "a")]
        [InlineData("abcbc", "bc", 0, "abcbc")]
        [InlineData("abcbc", "bc", 1, "abc")]
        [InlineData("abcbc", "bc", 2, "a")]
        [InlineData("abcbc", "bc", 3, "a")]
        public void TrimEnd_Max_String(string subject, string value, int max, string expected)
            => subject.TrimEnd(value, max).Should().Be(expected);

        [Theory]
        [InlineData("", null, "")]
        [InlineData("", new string[] { }, "")]
        [InlineData("", new[] { "x", "y", "z" }, "")]
        [InlineData("abcdefefghefghgh", new[] { "ef", "gh", "c" }, "abcd")]
        [InlineData("abcdefefghefghgh", new[] { "ef", "gh", "d" }, "abc")]
        public void TrimEnd_String_Params(string subject, string[] values, string expected)
        {
            subject.TrimEnd(values).Should().Be(expected);
            subject.TrimEnd((IEnumerable<string>)values).Should().Be(expected);
        }

        [Theory]
        [InlineData("", null, -1, "")]
        [InlineData("", new string[] { }, -1, "")]
        [InlineData("", new[] { "x", "y", "z" }, -1, "")]
        [InlineData("abcdefefghefghgh", new[] { "ef", "gh", "c" }, -1, "abcd")]
        [InlineData("abcdefefghefghgh", new[] { "ef", "gh", "d" }, -1, "abc")]
        [InlineData("abcdefefghefghgh", new[] { "ef", "gh" }, 0, "abcdefefghefghgh")]
        [InlineData("abcdefefghefghgh", new[] { "ef", "gh" }, 1, "abcdefefghefgh")]
        [InlineData("abcdefefghefghgh", new[] { "ef", "gh" }, 5, "abcdef")]
        [InlineData("abcdefefghefghgh", new[] { "ef", "gh" }, 6, "abcd")]
        [InlineData("abcdefefghefghgh", new[] { "ef", "gh" }, 7, "abcd")]
        public void TrimEnd_Max_String_Params(string subject, string[] values, int max, string expected)
        {
            subject.TrimEnd(max, values).Should().Be(expected);
            subject.TrimEnd((IEnumerable<string>)values, max).Should().Be(expected);
        }

        #endregion

        #region TrimStart Tests

        [Theory]
        [InlineData("abcd", 0, "abcd")]
        [InlineData("", 4, "")]
        [InlineData("abc", 4, "")]
        [InlineData("abcd", 4, "")]
        [InlineData("abcde", 4, "e")]
        public void TrimStart_Count(string subject, int count, string expected)
            => subject.TrimStart(count).Should().Be(expected);

        [Theory]
        [InlineData("", null, "")]
        [InlineData("abc", null, "abc")]
        [InlineData("abc", "", "abc")]
        [InlineData("abc", "x", "abc")]
        [InlineData("ababc", "ax", "ababc")]
        [InlineData("ababc", "a", "babc")]
        [InlineData("ababc", "ab", "c")]
        public void TrimStart_String(string subject, string value, string expected)
            => subject.TrimStart(value).Should().Be(expected);

        [Theory]
        [InlineData("", null, -1, "")]
        [InlineData("", null, 0, "")]
        [InlineData("abc", null, -1, "abc")]
        [InlineData("abc", "", -1, "abc")]
        [InlineData("abc", "x", -1, "abc")]
        [InlineData("ababc", "ax", -1, "ababc")]
        [InlineData("ababc", "a", -1, "babc")]
        [InlineData("ababc", "ab", -1, "c")]
        [InlineData("ababc", "ab", 0, "ababc")]
        [InlineData("ababc", "ab", 1, "abc")]
        [InlineData("ababc", "ab", 2, "c")]
        [InlineData("ababc", "ab", 3, "c")]
        public void TrimStart_Max_String(string subject, string value, int max, string expected)
            => subject.TrimStart(value, max).Should().Be(expected);

        [Theory]
        [InlineData("", null, "")]
        [InlineData("", new string[] { }, "")]
        [InlineData("", new[] { "x", "y", "z" }, "")]
        [InlineData("ababcdabcdcdefgh", new[] { "cd", "ab", "f" }, "efgh")]
        [InlineData("ababcdabcdcdefgh", new[] { "cd", "ab", "e" }, "fgh")]
        public void TrimStart_String_Params(string subject, string[] values, string expected)
        {
            subject.TrimStart(values).Should().Be(expected);
            subject.TrimStart((IEnumerable<string>)values).Should().Be(expected);
        }

        [Theory]
        [InlineData("", null, -1, "")]
        [InlineData("", new string[] { }, -1, "")]
        [InlineData("", new[] { "x", "y", "z" }, -1, "")]
        [InlineData("ababcdabcdcdefgh", new[] { "cd", "ab", "f" }, -1, "efgh")]
        [InlineData("ababcdabcdcdefgh", new[] { "cd", "ab", "e" }, -1, "fgh")]
        [InlineData("ababcdabcdcdefgh", new[] { "cd", "ab" }, 0, "ababcdabcdcdefgh")]
        [InlineData("ababcdabcdcdefgh", new[] { "cd", "ab" }, 1, "abcdabcdcdefgh")]
        [InlineData("ababcdabcdcdefgh", new[] { "cd", "ab" }, 5, "cdefgh")]
        [InlineData("ababcdabcdcdefgh", new[] { "cd", "ab" }, 6, "efgh")]
        [InlineData("ababcdabcdcdefgh", new[] { "cd", "ab" }, 7, "efgh")]
        public void TrimStart_Max_String_Params(string subject, string[] values, int max, string expected)
        {
            subject.TrimStart(max, values).Should().Be(expected);
            subject.TrimStart((IEnumerable<string>)values, max).Should().Be(expected);
        }

        #endregion

        #region Remove Tests

        [Theory]
        [InlineData("", null, "")]
        [InlineData("ababa", null, "ababa")]
        [InlineData("ababa", "", "ababa")]
        [InlineData("ababa", "x", "ababa")]
        [InlineData("ababa", "a", "bb")]
        public void Remove_String(string subject, string value, string expected)
            => subject.Remove(value).Should().Be(expected);

        [Theory]
        [InlineData("", null, "")]
        [InlineData("ababa", null, "ababa")]
        [InlineData("ababa", "", "ababa")]
        [InlineData("ababa", "x", "ababa")]
        [InlineData("ababa", "a", "bb")]
        [InlineData("ababa", "A", "bb")]
        public void Remove_Insensitive_String(string subject, string value, string expected)
            => subject.Remove(value, StringComparison.InvariantCultureIgnoreCase).Should().Be(expected);

        [Theory]
        [InlineData("", null, -1, "")]
        [InlineData("ababa", null, -1, "ababa")]
        [InlineData("ababa", "", -1, "ababa")]
        [InlineData("ababa", "x", -1, "ababa")]
        [InlineData("ababa", "a", -1, "bb")]
        [InlineData("ababa", "a", 0, "ababa")]
        [InlineData("ababa", "a", 1, "baba")]
        [InlineData("ababa", "a", 2, "bba")]
        [InlineData("ababa", "a", 3, "bb")]
        [InlineData("ababa", "a", 4, "bb")]
        public void Remove_Count_String(string subject, string value, int count, string expected)
            => subject.Remove(value, count).Should().Be(expected);

        [Theory]
        [InlineData("", null, -1, "")]
        [InlineData("ababa", null, -1, "ababa")]
        [InlineData("ababa", "", -1, "ababa")]
        [InlineData("ababa", "x", -1, "ababa")]
        [InlineData("ababa", "a", -1, "bb")]
        [InlineData("ababa", "A", -1, "bb")]
        [InlineData("ababa", "A", 0, "ababa")]
        [InlineData("ababa", "A", 1, "baba")]
        [InlineData("ababa", "A", 2, "bba")]
        [InlineData("ababa", "A", 3, "bb")]
        [InlineData("ababa", "A", 4, "bb")]
        public void Remove_Count_Insensitive_String(string subject, string value, int count, string expected)
            => subject.Remove(value, count, StringComparison.InvariantCultureIgnoreCase).Should().Be(expected);

        #endregion

        #region RemoveAll Tests

        [Theory]
        [InlineData("", null, "")]
        [InlineData("abcacba", null, "abcacba")]
        [InlineData("abcacba", new string[0], "abcacba")]
        [InlineData("abcacba", new[] { "" }, "abcacba")]
        [InlineData("abcacba", new[] { "x" }, "abcacba")]
        [InlineData("abcacba", new[] { "a" }, "bccb")]
        [InlineData("abcacba", new[] { "a", "c" }, "bb")]
        public void RemoveAll_String(string subject, string[] values, string expected)
        {
            subject.RemoveAll(values).Should().Be(expected);
            subject.RemoveAll((IEnumerable<string>)values).Should().Be(expected);
        }

        [Theory]
        [InlineData("", null, "")]
        [InlineData("abcacba", null, "abcacba")]
        [InlineData("abcacba", new string[0], "abcacba")]
        [InlineData("abcacba", new[] { "" }, "abcacba")]
        [InlineData("abcacba", new[] { "x" }, "abcacba")]
        [InlineData("abcacba", new[] { "a" }, "bccb")]
        [InlineData("abcacba", new[] { "A" }, "bccb")]
        [InlineData("abcacba", new[] { "a", "C" }, "bb")]
        [InlineData("abcacba", new[] { "A", "c" }, "bb")]
        public void RemoveAll_Insensitive_String(string subject, string[] values, string expected)
        {
            subject.RemoveAll(StringComparison.InvariantCultureIgnoreCase, values).Should().Be(expected);
            subject.RemoveAll((IEnumerable<string>)values, StringComparison.InvariantCultureIgnoreCase).Should().Be(expected);
        }

        #endregion

        #region Replace Tests

        [Theory]
        [InlineData("", null, null, -1, "", "subject is empty")]
        [InlineData("ababa", null, null, -1, "ababa", "oldValue is null")]
        [InlineData("ababa", "", null, -1, "ababa", "oldValue is empty")]
        [InlineData("ababa", "ababab", null, -1, "ababa", "oldValue is longer than subject")]
        [InlineData("ababa", "x", null, -1, "ababa", "oldValue is not found")]
        [InlineData("aaaaa", "a", null, -1, "", "result width equals zero")]
        [InlineData("ababa", "a", null, -1, "bb")]
        [InlineData("ababa", "a", "", -1, "bb")]
        [InlineData("ababa", "a", "x", -1, "xbxbx")]
        [InlineData("ababa", "a", "xtx", -1, "xtxbxtxbxtx")]
        [InlineData("ababa", "a", "x", 0, "ababa")]
        [InlineData("ababa", "a", "x", 1, "xbaba")]
        [InlineData("ababa", "a", "x", 2, "xbxba")]
        [InlineData("ababa", "a", "x", 3, "xbxbx")]
        [InlineData("ababa", "a", "x", 4, "xbxbx")]
        public void Replace_Count(string subject, string oldValue, string newValue, int count, string expected, string because = "")
            => subject.Replace(oldValue, newValue, count).Should().Be(expected, because: because);

        [Theory]
        [InlineData("", null, null, -1, "", "subject is empty")]
        [InlineData("ababa", null, null, -1, "ababa", "oldValue is null")]
        [InlineData("ababa", "", null, -1, "ababa", "oldValue is empty")]
        [InlineData("ababa", "ababab", null, -1, "ababa", "oldValue is longer than subject")]
        [InlineData("ababa", "x", null, -1, "ababa", "oldValue is not found")]
        [InlineData("aaaaa", "a", null, -1, "", "result width equals zero")]
        [InlineData("ababa", "a", null, -1, "bb")]
        [InlineData("ababa", "a", "", -1, "bb")]
        [InlineData("ababa", "a", "x", -1, "xbxbx")]
        [InlineData("ababa", "a", "xtx", -1, "xtxbxtxbxtx")]
        [InlineData("ababa", "A", null, -1, "bb")]
        [InlineData("ababa", "A", "", -1, "bb")]
        [InlineData("ababa", "A", "x", -1, "xbxbx")]
        [InlineData("ababa", "A", "xtx", -1, "xtxbxtxbxtx")]
        [InlineData("ababa", "a", "x", 0, "ababa")]
        [InlineData("ababa", "a", "x", 1, "xbaba")]
        [InlineData("ababa", "a", "x", 2, "xbxba")]
        [InlineData("ababa", "a", "x", 3, "xbxbx")]
        [InlineData("ababa", "a", "x", 4, "xbxbx")]
        [InlineData("ababa", "A", "x", 0, "ababa")]
        [InlineData("ababa", "A", "x", 1, "xbaba")]
        [InlineData("ababa", "A", "x", 2, "xbxba")]
        [InlineData("ababa", "A", "x", 3, "xbxbx")]
        [InlineData("ababa", "A", "x", 4, "xbxbx")]
        public void Replace_Count_Insensitive(string subject, string oldValue, string newValue, int count, string expected, string because = "")
            => subject.Replace(oldValue, newValue, count, StringComparison.InvariantCultureIgnoreCase).Should().Be(expected, because: because);

        #endregion

        #region ReplaceAll Tests

        [Theory]
        [InlineData("", null, null, "")]
        [InlineData("ababa", null, null, "ababa")]
        [InlineData("ababa", "x", null, "ababa")]
        [InlineData("ababa", "x", new string[0], "ababa")]
        [InlineData("ababa", "x", new[] { null, "" }, "ababa")]
        [InlineData("ababa", "x", new[] { "a" }, "xbxbx")]
        [InlineData("ababa", "x", new[] { "a", "b" }, "xxxxx")]
        [InlineData("ababa", "x", new[] { "a", "b", "c", null, "" }, "xxxxx")]
        public void ReplaceAll_Params(string subject, string newValue, string[] oldValues, string expected)
        {
            subject.ReplaceAll(newValue, oldValues).Should().Be(expected);
            subject.ReplaceAll(newValue, (IEnumerable<string>)oldValues).Should().Be(expected);
        }

        [Theory]
        [InlineData("", null, null, "")]
        [InlineData("ababa", null, null, "ababa")]
        [InlineData("ababa", "x", null, "ababa")]
        [InlineData("ababa", "x", new string[0], "ababa")]
        [InlineData("ababa", "x", new[] { null, "" }, "ababa")]
        [InlineData("ababa", "x", new[] { "a" }, "xbxbx")]
        [InlineData("ababa", "x", new[] { "a", "b" }, "xxxxx")]
        [InlineData("ababa", "x", new[] { "a", "b", "c", null, "" }, "xxxxx")]
        [InlineData("ababa", "x", new[] { "A" }, "xbxbx")]
        [InlineData("ababa", "x", new[] { "A", "B" }, "xxxxx")]
        [InlineData("ababa", "x", new[] { "A", "B", "c", null, "" }, "xxxxx")]
        public void ReplaceAll_Insensitive_Params(string subject, string newValue, string[] oldValues, string expected)
        {
            subject.ReplaceAll(newValue, StringComparison.InvariantCultureIgnoreCase, oldValues).Should().Be(expected);
            subject.ReplaceAll(newValue, (IEnumerable<string>)oldValues, StringComparison.InvariantCultureIgnoreCase).Should().Be(expected);
        }

        #endregion

        #region ReplaceAny Tests

        [Theory]
        [InlineData("", null, "")]
        [InlineData("ababa", null, "ababa")]
        [MemberData(nameof(ReplaceAny_Dict_Data))]
        public void ReplaceAny_Dict(string subject, Dictionary<string, string> values, string expected)
        {
            subject.ReplaceAny(values).Should().Be(expected);
            subject.ReplaceAny(values?.Keys, values?.Values).Should().Be(expected);
        }

        public static IEnumerable<object[]> ReplaceAny_Dict_Data
            => new[]
            {
                new object[] { "ababa", new Dictionary<string, string>(), "ababa" },
                new object[] { "ababa", new Dictionary<string, string>() { { "", "x" } }, "ababa" },
                new object[] { "ababa", new Dictionary<string, string>() { { "a", "x" } }, "xbxbx" },
                new object[] { "ababa", new Dictionary<string, string>() { { "b", "x" } }, "axaxa" },
                new object[] { "ababa", new Dictionary<string, string>() { { "a", "x" }, { "b", "x" } }, "xxxxx" }
            };

        [Theory]
        [InlineData("", null, "")]
        [InlineData("ababa", null, "ababa")]
        [MemberData(nameof(ReplaceAny_Insensitive_Dict_Data))]
        public void ReplaceAny_Insensitive_Dict(string subject, Dictionary<string, string> values, string expected)
        {
            subject.ReplaceAny(values, StringComparison.InvariantCultureIgnoreCase).Should().Be(expected);
            subject.ReplaceAny(values?.Keys, values?.Values, StringComparison.InvariantCultureIgnoreCase).Should().Be(expected);
        }

        public static IEnumerable<object[]> ReplaceAny_Insensitive_Dict_Data
            => new[]
            {
                new object[] { "ababa", new Dictionary<string, string>(), "ababa" },
                new object[] { "ababa", new Dictionary<string, string>() { { "", "x" } }, "ababa" },
                new object[] { "ababa", new Dictionary<string, string>() { { "a", "x" } }, "xbxbx" },
                new object[] { "ababa", new Dictionary<string, string>() { { "b", "x" } }, "axaxa" },
                new object[] { "ababa", new Dictionary<string, string>() { { "a", "x" }, { "b", "x" } }, "xxxxx" },
                new object[] { "ababa", new Dictionary<string, string>() { { "A", "x" } }, "xbxbx" },
                new object[] { "ababa", new Dictionary<string, string>() { { "B", "x" } }, "axaxa" },
                new object[] { "ababa", new Dictionary<string, string>() { { "A", "x" }, { "B", "x" } }, "xxxxx" }
            };

        [Theory]
        [InlineData("", null, null, "")]
        [InlineData("ababa", null, null, "ababa")]
        [InlineData("ababa", null, new string[0], "ababa")]
        [InlineData("ababa", new string[0], null, "ababa")]
        [MemberData(nameof(ReplaceAny_Enumerables_Data))]
        public void ReplaceAny_Enumerables(string subject, IEnumerable<string> oldValues, IEnumerable<string> newValues, string expected)
            => subject.ReplaceAny(oldValues, newValues).Should().Be(expected);

        public static IEnumerable<object[]> ReplaceAny_Enumerables_Data
            => new[]
            {
                new object[] { "ababa", new[] { null, "" }, new[] { "x", "y" }, "ababa" },
                new object[] { "ababa", new[] { "a" }, new[] { "x" }, "xbxbx" },
                new object[] { "ababa", new[] { "a", "b" }, new[] { "x" }, "xbxbx" },
                new object[] { "ababa", new[] { "a", "b" }, new[] { "x", null }, "xxx" },
                new object[] { "ababa", new[] { "a", "b" }, new[] { "x", "" }, "xxx" },
                new object[] { "ababa", new[] { "a", "b" }, new[] { "x", "y", "z" }, "xyxyx" },
                new object[] { "ababa", new[] { "a", "b", "x" }, new[] { "x", "y", "z" }, "zyzyz" },
            };

        [Theory]
        [InlineData("", null, null, "")]
        [InlineData("ababa", null, null, "ababa")]
        [InlineData("ababa", null, new string[0], "ababa")]
        [InlineData("ababa", new string[0], null, "ababa")]
        [MemberData(nameof(ReplaceAny_Insensitive_Enumerables_Data))]
        public void ReplaceAny_Insensitive_Enumerables(string subject, IEnumerable<string> oldValues, IEnumerable<string> newValues, string expected)
            => subject.ReplaceAny(oldValues, newValues, StringComparison.InvariantCultureIgnoreCase).Should().Be(expected);

        public static IEnumerable<object[]> ReplaceAny_Insensitive_Enumerables_Data
            => new[]
            {
                new object[] { "ababa", new[] { null, "" }, new[] { "x", "y" }, "ababa" },
                new object[] { "ababa", new[] { "a" }, new[] { "x" }, "xbxbx" },
                new object[] { "ababa", new[] { "a", "b" }, new[] { "x" }, "xbxbx" },
                new object[] { "ababa", new[] { "a", "b" }, new[] { "x", null }, "xxx" },
                new object[] { "ababa", new[] { "a", "b" }, new[] { "x", "" }, "xxx" },
                new object[] { "ababa", new[] { "a", "b" }, new[] { "x", "y", "z" }, "xyxyx" },
                new object[] { "ababa", new[] { "a", "b", "x" }, new[] { "x", "y", "z" }, "zyzyz" },
                new object[] { "ababa", new[] { "A" }, new[] { "x" }, "xbxbx" },
                new object[] { "ababa", new[] { "A", "B" }, new[] { "x" }, "xbxbx" },
                new object[] { "ababa", new[] { "A", "B" }, new[] { "x", null }, "xxx" },
                new object[] { "ababa", new[] { "A", "B" }, new[] { "x", "" }, "xxx" },
                new object[] { "ababa", new[] { "A", "B" }, new[] { "x", "y", "z" }, "xyxyx" },
                new object[] { "ababa", new[] { "A", "B", "X" }, new[] { "x", "y", "z" }, "zyzyz" },
            };

        #endregion

    }
}
