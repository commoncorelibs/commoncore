﻿using System.Text;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests._Extensions
{
    public class Test_StringBuilderExtensions_NormalizeLineEndings
    {
        [Theory]
        [InlineData(ELineEnding.N, "\ra\rb\r", "\na\nb\n")]
        [InlineData(ELineEnding.N, "a\r\nb\n \r   \n\nc\n\rd", "a\nb\n \n   \n\nc\nd")]
        [InlineData(ELineEnding.R, "a\r\nb\n \r   \n\nc\n\rd", "a\rb\r \r   \r\rc\rd")]
        [InlineData(ELineEnding.RN, "a\r\nb\n \r   \n\nc\n\rd", "a\r\nb\r\n \r\n   \r\n\r\nc\r\nd")]
        [InlineData(ELineEnding.NR, "a\r\nb\n \r   \n\nc\n\rd", "a\n\rb\n\r \n\r   \n\r\n\rc\n\rd")]
        public void NormalizeLineEndings(ELineEnding ending, string subject, string expected)
            => new StringBuilder(subject).NormalizeLineEndings(ending).ToString().Should().Be(expected);

        [Theory]
        [InlineData(ELineEnding.N, "\ra\r \rb\r", "\na\nb\n", "\na\n \nb\n")]
        [InlineData(ELineEnding.N, "a\r\nb\n \r   \n\nc\n\rd", "a\nb\nc\nd", "a\nb\n \n   \nc\nd")]
        [InlineData(ELineEnding.R, "a\r\nb\n \r   \n\nc\n\rd", "a\rb\rc\rd", "a\rb\r \r   \rc\rd")]
        [InlineData(ELineEnding.RN, "a\r\nb\n \r   \n\nc\n\rd", "a\r\nb\r\nc\r\nd", "a\r\nb\r\n \r\n   \r\nc\r\nd")]
        [InlineData(ELineEnding.NR, "a\r\nb\n \r   \n\nc\n\rd", "a\n\rb\n\rc\n\rd", "a\n\rb\n\r \n\r   \n\rc\n\rd")]
        public void NormalizeMergeLineEndings(ELineEnding ending, string subject, string expectedTrue, string expectedFalse)
        {
            new StringBuilder(subject).NormalizeMergeLineEndings(ending, true).ToString().Should().Be(expectedTrue);
            new StringBuilder(subject).NormalizeMergeLineEndings(ending, false).ToString().Should().Be(expectedFalse);
        }
    }
}
