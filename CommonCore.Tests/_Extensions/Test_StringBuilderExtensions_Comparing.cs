﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Text;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests._Extensions
{
    [ExcludeFromCodeCoverage]
    public class Test_StringBuilderExtensions_Comparing
    {

        #region Contains(char) Tests

        [Theory]
        [InlineData("abcabcabcabc", 'x', false)]
        [InlineData("abcabcabcabc", 'A', false)]
        [InlineData("abcabcabcabc", 'a', true)]
        public void Contains1_Char(string subject, char value, bool expected)
            => new StringBuilder(subject).Contains(value).Should().Be(expected);

        [Theory]
        [InlineData("abcabcabcabc", 'x', 0, false)]
        [InlineData("abcabcabcabc", 'A', 0, false)]
        [InlineData("abcabcabcabc", 'a', 0, true)]
        public void Contains2_Char(string subject, char value, int startIndex, bool expected)
            => new StringBuilder(subject).Contains(value, startIndex).Should().Be(expected);

        [Theory]
        [InlineData("abcabcabcabc", 'x', 0, -1, false)]
        [InlineData("abcabcabcabc", 'A', 0, -1, false)]
        [InlineData("abcabcabcabc", 'a', 0, -1, true)]
        public void Contains3_Char(string subject, char value, int startIndex, int maxCount, bool expected)
            => new StringBuilder(subject).Contains(value, startIndex, maxCount).Should().Be(expected);

        [Theory]
        [InlineData("abcabcabcabc", 'x', ECharComparison.CaseSensitive, false)]
        [InlineData("abcabcabcabc", 'A', ECharComparison.CaseSensitive, false)]
        [InlineData("abcabcabcabc", 'a', ECharComparison.CaseSensitive, true)]
        [InlineData("abcabcabcabc", 'x', ECharComparison.CurrentCultureIgnoreCase, false)]
        [InlineData("abcabcabcabc", 'A', ECharComparison.CurrentCultureIgnoreCase, true)]
        [InlineData("abcabcabcabc", 'a', ECharComparison.CurrentCultureIgnoreCase, true)]
        [InlineData("abcabcabcabc", 'x', ECharComparison.InvariantCultureIgnoreCase, false)]
        [InlineData("abcabcabcabc", 'A', ECharComparison.InvariantCultureIgnoreCase, true)]
        [InlineData("abcabcabcabc", 'a', ECharComparison.InvariantCultureIgnoreCase, true)]
        public void Contains1_Char_Comparison(string subject, char value, ECharComparison comparison, bool expected)
            => new StringBuilder(subject).Contains(value, comparison).Should().Be(expected);

        [Theory]
        [InlineData("abcabcabcabc", 'x', 0, ECharComparison.CaseSensitive, false)]
        [InlineData("abcabcabcabc", 'A', 0, ECharComparison.CaseSensitive, false)]
        [InlineData("abcabcabcabc", 'a', 0, ECharComparison.CaseSensitive, true)]
        [InlineData("abcabcabcabc", 'x', 0, ECharComparison.CurrentCultureIgnoreCase, false)]
        [InlineData("abcabcabcabc", 'A', 0, ECharComparison.CurrentCultureIgnoreCase, true)]
        [InlineData("abcabcabcabc", 'a', 0, ECharComparison.CurrentCultureIgnoreCase, true)]
        [InlineData("abcabcabcabc", 'x', 0, ECharComparison.InvariantCultureIgnoreCase, false)]
        [InlineData("abcabcabcabc", 'A', 0, ECharComparison.InvariantCultureIgnoreCase, true)]
        [InlineData("abcabcabcabc", 'a', 0, ECharComparison.InvariantCultureIgnoreCase, true)]
        public void Contains2_Char_Comparison(string subject, char value, int startIndex, ECharComparison comparison, bool expected)
            => new StringBuilder(subject).Contains(value, startIndex, comparison).Should().Be(expected);

        [Theory]
        [InlineData("abcabcabcabc", 'x', 0, -1, ECharComparison.CaseSensitive, false)]
        [InlineData("abcabcabcabc", 'A', 0, -1, ECharComparison.CaseSensitive, false)]
        [InlineData("abcabcabcabc", 'a', 0, -1, ECharComparison.CaseSensitive, true)]
        [InlineData("abcabcabcabc", 'x', 0, -1, ECharComparison.CurrentCultureIgnoreCase, false)]
        [InlineData("abcabcabcabc", 'A', 0, -1, ECharComparison.CurrentCultureIgnoreCase, true)]
        [InlineData("abcabcabcabc", 'a', 0, -1, ECharComparison.CurrentCultureIgnoreCase, true)]
        [InlineData("abcabcabcabc", 'x', 0, -1, ECharComparison.InvariantCultureIgnoreCase, false)]
        [InlineData("abcabcabcabc", 'A', 0, -1, ECharComparison.InvariantCultureIgnoreCase, true)]
        [InlineData("abcabcabcabc", 'a', 0, -1, ECharComparison.InvariantCultureIgnoreCase, true)]
        public void Contains3_Char_Comparison(string subject, char value, int startIndex, int maxCount, ECharComparison comparison, bool expected)
            => new StringBuilder(subject).Contains(value, startIndex, maxCount, comparison).Should().Be(expected);

        [Theory]
        [InlineData("abcabcabcabc", 'x', null, false)]
        [InlineData("abcabcabcabc", 'A', null, false)]
        [InlineData("abcabcabcabc", 'a', null, true)]
        [InlineData("abcabcabcabc", 'x', "current", false)]
        [InlineData("abcabcabcabc", 'A', "current", true)]
        [InlineData("abcabcabcabc", 'a', "current", true)]
        [InlineData("abcabcabcabc", 'x', "invariant", false)]
        [InlineData("abcabcabcabc", 'A', "invariant", true)]
        [InlineData("abcabcabcabc", 'a', "invariant", true)]
        public void Contains1_Char_Culture(string subject, char value, string culture, bool expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).Contains(value, info).Should().Be(expected);
        }

        [Theory]
        [InlineData("abcabcabcabc", 'x', 0, null, false)]
        [InlineData("abcabcabcabc", 'A', 0, null, false)]
        [InlineData("abcabcabcabc", 'a', 0, null, true)]
        [InlineData("abcabcabcabc", 'x', 0, "current", false)]
        [InlineData("abcabcabcabc", 'A', 0, "current", true)]
        [InlineData("abcabcabcabc", 'a', 0, "current", true)]
        [InlineData("abcabcabcabc", 'x', 0, "invariant", false)]
        [InlineData("abcabcabcabc", 'A', 0, "invariant", true)]
        [InlineData("abcabcabcabc", 'a', 0, "invariant", true)]
        public void Contains2_Char_Culture(string subject, char value, int startIndex, string culture, bool expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).Contains(value, startIndex, info).Should().Be(expected);
        }

        [Theory]
        [InlineData("abcabcabcabc", 'x', 0, -1, null, false)]
        [InlineData("abcabcabcabc", 'A', 0, -1, null, false)]
        [InlineData("abcabcabcabc", 'a', 0, -1, null, true)]
        [InlineData("abcabcabcabc", 'x', 0, -1, "current", false)]
        [InlineData("abcabcabcabc", 'A', 0, -1, "current", true)]
        [InlineData("abcabcabcabc", 'a', 0, -1, "current", true)]
        [InlineData("abcabcabcabc", 'x', 0, -1, "invariant", false)]
        [InlineData("abcabcabcabc", 'A', 0, -1, "invariant", true)]
        [InlineData("abcabcabcabc", 'a', 0, -1, "invariant", true)]
        public void Contains3_Char_Culture(string subject, char value, int startIndex, int maxCount, string culture, bool expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).Contains(value, startIndex, maxCount, info).Should().Be(expected);
        }

        #endregion

        #region Contains(string) Tests

        [Theory]
        [InlineData("abcabcabcabc", "xbc", false)]
        [InlineData("abcabcabcabc", "Abc", false)]
        [InlineData("abcabcabcabc", "abc", true)]
        public void Contains1_String(string subject, string value, bool expected)
            => new StringBuilder(subject).Contains(value).Should().Be(expected);

        [Theory]
        [InlineData("abcabcabcabc", "xbc", 0, false)]
        [InlineData("abcabcabcabc", "Abc", 0, false)]
        [InlineData("abcabcabcabc", "abc", 0, true)]
        public void Contains2_String(string subject, string value, int startIndex, bool expected)
            => new StringBuilder(subject).Contains(value, startIndex).Should().Be(expected);

        [Theory]
        [InlineData("abcabcabcabc", "xbc", 0, -1, false)]
        [InlineData("abcabcabcabc", "Abc", 0, -1, false)]
        [InlineData("abcabcabcabc", "abc", 0, -1, true)]
        public void Contains3_String(string subject, string value, int startIndex, int maxCount, bool expected)
            => new StringBuilder(subject).Contains(value, startIndex, maxCount).Should().Be(expected);

        [Theory]
        [InlineData("abcabcabcabc", "xbc", ECharComparison.CaseSensitive, false)]
        [InlineData("abcabcabcabc", "Abc", ECharComparison.CaseSensitive, false)]
        [InlineData("abcabcabcabc", "abc", ECharComparison.CaseSensitive, true)]
        [InlineData("abcabcabcabc", "xbc", ECharComparison.CurrentCultureIgnoreCase, false)]
        [InlineData("abcabcabcabc", "Abc", ECharComparison.CurrentCultureIgnoreCase, true)]
        [InlineData("abcabcabcabc", "abc", ECharComparison.CurrentCultureIgnoreCase, true)]
        [InlineData("abcabcabcabc", "xbc", ECharComparison.InvariantCultureIgnoreCase, false)]
        [InlineData("abcabcabcabc", "Abc", ECharComparison.InvariantCultureIgnoreCase, true)]
        [InlineData("abcabcabcabc", "abc", ECharComparison.InvariantCultureIgnoreCase, true)]
        public void Contains1_String_Comparison(string subject, string value, ECharComparison comparison, bool expected)
            => new StringBuilder(subject).Contains(value, comparison).Should().Be(expected);

        [Theory]
        [InlineData("abcabcabcabc", "xbc", 0, ECharComparison.CaseSensitive, false)]
        [InlineData("abcabcabcabc", "Abc", 0, ECharComparison.CaseSensitive, false)]
        [InlineData("abcabcabcabc", "abc", 0, ECharComparison.CaseSensitive, true)]
        [InlineData("abcabcabcabc", "xbc", 0, ECharComparison.CurrentCultureIgnoreCase, false)]
        [InlineData("abcabcabcabc", "Abc", 0, ECharComparison.CurrentCultureIgnoreCase, true)]
        [InlineData("abcabcabcabc", "abc", 0, ECharComparison.CurrentCultureIgnoreCase, true)]
        [InlineData("abcabcabcabc", "xbc", 0, ECharComparison.InvariantCultureIgnoreCase, false)]
        [InlineData("abcabcabcabc", "Abc", 0, ECharComparison.InvariantCultureIgnoreCase, true)]
        [InlineData("abcabcabcabc", "abc", 0, ECharComparison.InvariantCultureIgnoreCase, true)]
        public void Contains2_String_Comparison(string subject, string value, int startIndex, ECharComparison comparison, bool expected)
            => new StringBuilder(subject).Contains(value, startIndex, comparison).Should().Be(expected);

        [Theory]
        [InlineData("abcabcabcabc", "xbc", 0, -1, ECharComparison.CaseSensitive, false)]
        [InlineData("abcabcabcabc", "Abc", 0, -1, ECharComparison.CaseSensitive, false)]
        [InlineData("abcabcabcabc", "abc", 0, -1, ECharComparison.CaseSensitive, true)]
        [InlineData("abcabcabcabc", "xbc", 0, -1, ECharComparison.CurrentCultureIgnoreCase, false)]
        [InlineData("abcabcabcabc", "Abc", 0, -1, ECharComparison.CurrentCultureIgnoreCase, true)]
        [InlineData("abcabcabcabc", "abc", 0, -1, ECharComparison.CurrentCultureIgnoreCase, true)]
        [InlineData("abcabcabcabc", "xbc", 0, -1, ECharComparison.InvariantCultureIgnoreCase, false)]
        [InlineData("abcabcabcabc", "Abc", 0, -1, ECharComparison.InvariantCultureIgnoreCase, true)]
        [InlineData("abcabcabcabc", "abc", 0, -1, ECharComparison.InvariantCultureIgnoreCase, true)]
        public void Contains3_String_Comparison(string subject, string value, int startIndex, int maxCount, ECharComparison comparison, bool expected)
            => new StringBuilder(subject).Contains(value, startIndex, maxCount, comparison).Should().Be(expected);

        [Theory]
        [InlineData("abcabcabcabc", "xbc", null, false)]
        [InlineData("abcabcabcabc", "Abc", null, false)]
        [InlineData("abcabcabcabc", "abc", null, true)]
        [InlineData("abcabcabcabc", "xbc", "current", false)]
        [InlineData("abcabcabcabc", "Abc", "current", true)]
        [InlineData("abcabcabcabc", "abc", "current", true)]
        [InlineData("abcabcabcabc", "xbc", "invariant", false)]
        [InlineData("abcabcabcabc", "Abc", "invariant", true)]
        [InlineData("abcabcabcabc", "abc", "invariant", true)]
        public void Contains1_String_Culture(string subject, string value, string culture, bool expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).Contains(value, info).Should().Be(expected);
        }

        [Theory]
        [InlineData("abcabcabcabc", "xbc", 0, null, false)]
        [InlineData("abcabcabcabc", "Abc", 0, null, false)]
        [InlineData("abcabcabcabc", "abc", 0, null, true)]
        [InlineData("abcabcabcabc", "xbc", 0, "current", false)]
        [InlineData("abcabcabcabc", "Abc", 0, "current", true)]
        [InlineData("abcabcabcabc", "abc", 0, "current", true)]
        [InlineData("abcabcabcabc", "xbc", 0, "invariant", false)]
        [InlineData("abcabcabcabc", "Abc", 0, "invariant", true)]
        [InlineData("abcabcabcabc", "abc", 0, "invariant", true)]
        public void Contains2_String_Culture(string subject, string value, int startIndex, string culture, bool expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).Contains(value, startIndex, info).Should().Be(expected);
        }

        [Theory]
        [InlineData("abcabcabcabc", "xbc", 0, -1, null, false)]
        [InlineData("abcabcabcabc", "Abc", 0, -1, null, false)]
        [InlineData("abcabcabcabc", "abc", 0, -1, null, true)]
        [InlineData("abcabcabcabc", "xbc", 0, -1, "current", false)]
        [InlineData("abcabcabcabc", "Abc", 0, -1, "current", true)]
        [InlineData("abcabcabcabc", "abc", 0, -1, "current", true)]
        [InlineData("abcabcabcabc", "xbc", 0, -1, "invariant", false)]
        [InlineData("abcabcabcabc", "Abc", 0, -1, "invariant", true)]
        [InlineData("abcabcabcabc", "abc", 0, -1, "invariant", true)]
        public void Contains3_String_Culture(string subject, string value, int startIndex, int maxCount, string culture, bool expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).Contains(value, startIndex, maxCount, info).Should().Be(expected);
        }

        #endregion

        #region ContainsAll(char) Tests

        [Fact]
        public void ContainsAll_Char_Wrappers()
        {
            var b = new StringBuilder("xab");
            var v = new char[0];
            new StringBuilder("xab").ContainsAll(new char[0]).Should().Be(false);
            new StringBuilder("xab").ContainsAll(new char[0], 0).Should().Be(false);
            new StringBuilder("xab").ContainsAll(new char[0], 0, -1).Should().Be(false);
            new StringBuilder("xab").ContainsAll(new char[0], ECharComparison.CaseSensitive).Should().Be(false);
            new StringBuilder("xab").ContainsAll(new char[0], 0, ECharComparison.CaseSensitive).Should().Be(false);
            new StringBuilder("xab").ContainsAll(new char[0], 0, -1, ECharComparison.CaseSensitive).Should().Be(false);
            new StringBuilder("xab").ContainsAll(new char[0], null).Should().Be(false);
            new StringBuilder("xab").ContainsAll(new char[0], 0, null).Should().Be(false);
            new StringBuilder("xab").ContainsAll(new char[0], 0, -1, null).Should().Be(false);
        }

        [Theory]
        [InlineData("xab", new char[0], 0, -1, null, false)]
        [InlineData("xab", new[] { 'a' }, 0, 0, null, false)]
        [InlineData("xab", new[] { 'a' }, 3, -1, null, false)]
        [InlineData("xab", new[] { 'c' }, 0, -1, null, false)]
        [InlineData("xab", new[] { 'c', 'x', 'a' }, 0, -1, null, false)]
        [InlineData("xab", new[] { 'b', 'x', 'a' }, 0, -1, null, true)]
        [InlineData("xab", new[] { 'b', 'a', 'x' }, 0, -1, null, true)]
        public void ContainsAll3_Char_Culture(string subject, IEnumerable<char> values, int startIndex, int maxCount, string culture, bool expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).ContainsAll(values, startIndex, maxCount, info).Should().Be(expected);
        }

        #endregion

        #region ContainsAll(string) Tests

        [Fact]
        public void ContainsAll_String_Wrappers()
        {
            var b = new StringBuilder("xab");
            var v = new string[0];
            b.ContainsAll(v).Should().Be(false);
            b.ContainsAll(v, 0).Should().Be(false);
            b.ContainsAll(v, 0, -1).Should().Be(false);
            b.ContainsAll(v, ECharComparison.CaseSensitive).Should().Be(false);
            b.ContainsAll(v, 0, ECharComparison.CaseSensitive).Should().Be(false);
            b.ContainsAll(v, 0, -1, ECharComparison.CaseSensitive).Should().Be(false);
            b.ContainsAll(v, null).Should().Be(false);
            b.ContainsAll(v, 0, null).Should().Be(false);
            b.ContainsAll(v, 0, -1, null).Should().Be(false);
        }

        [Theory]
        [InlineData("xab", new string[0], 0, -1, null, false)]
        [InlineData("xab", new[] { "ab" }, 0, 0, null, false)]
        [InlineData("xab", new[] { "ab" }, 3, -1, null, false)]
        [InlineData("xab", new[] { "cd" }, 0, -1, null, false)]
        [InlineData("xab", new[] { "xa", "ab" }, 0, -1, null, true)]
        [InlineData("xab", new[] { "ab", "xa" }, 0, -1, null, true)]
        public void ContainsAll3_String_Culture(string subject, IEnumerable<string> values, int startIndex, int maxCount, string culture, bool expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).ContainsAll(values, startIndex, maxCount, info).Should().Be(expected);
        }

        #endregion

        #region ContainsAny(char) Tests

        [Fact]
        public void ContainsAny_Char_Wrappers()
        {
            var b = new StringBuilder("xab");
            var v = new char[0];
            b.ContainsAny(v).Should().Be(false);
            b.ContainsAny(v, 0).Should().Be(false);
            b.ContainsAny(v, 0, -1).Should().Be(false);
            b.ContainsAny(v, ECharComparison.CaseSensitive).Should().Be(false);
            b.ContainsAny(v, 0, ECharComparison.CaseSensitive).Should().Be(false);
            b.ContainsAny(v, 0, -1, ECharComparison.CaseSensitive).Should().Be(false);
            b.ContainsAny(v, null).Should().Be(false);
            b.ContainsAny(v, 0, null).Should().Be(false);
            b.ContainsAny(v, 0, -1, null).Should().Be(false);
        }

        [Theory]
        [InlineData("xab", new char[0], 0, -1, null, false)]
        [InlineData("xab", new[] { 'a' }, 0, 0, null, false)]
        [InlineData("xab", new[] { 'a' }, 3, -1, null, false)]
        [InlineData("xab", new[] { 'c' }, 0, -1, null, false)]
        [InlineData("xab", new[] { 'c', 'x', 'a' }, 0, -1, null, true)]
        public void ContainsAny3_Char_Culture(string subject, IEnumerable<char> values, int startIndex, int maxCount, string culture, bool expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).ContainsAny(values, startIndex, maxCount).Should().Be(expected);
        }

        #endregion

        #region ContainsAny(string) Tests

        [Fact]
        public void ContainsAny_String_Wrappers()
        {
            var b = new StringBuilder("xab");
            var v = new string[0];
            b.ContainsAny(v).Should().Be(false);
            b.ContainsAny(v, 0).Should().Be(false);
            b.ContainsAny(v, 0, -1).Should().Be(false);
            b.ContainsAny(v, ECharComparison.CaseSensitive).Should().Be(false);
            b.ContainsAny(v, 0, ECharComparison.CaseSensitive).Should().Be(false);
            b.ContainsAny(v, 0, -1, ECharComparison.CaseSensitive).Should().Be(false);
            b.ContainsAny(v, null).Should().Be(false);
            b.ContainsAny(v, 0, null).Should().Be(false);
            b.ContainsAny(v, 0, -1, null).Should().Be(false);
        }

        [Theory]
        [InlineData("xab", new string[0], 0, -1, false)]
        [InlineData("xab", new[] { "ab" }, 0, 0, false)]
        [InlineData("xab", new[] { "ab" }, 3, -1, false)]
        [InlineData("xab", new[] { "cd" }, 0, -1, false)]
        [InlineData("xab", new[] { "cd", "xa", "ab" }, 0, -1, true)]
        [InlineData("xab", new[] { "cd", "ab", "xa" }, 0, -1, true)]
        public void ContainsAny3_String_Culture(string subject, IEnumerable<string> values, int startIndex, int maxCount, bool expected)
            => new StringBuilder(subject).ContainsAny(values, startIndex, maxCount).Should().Be(expected);

        #endregion

        #region EndsWith Tests

        [Fact]
        public void EndsWith_Wrappers()
        {
            var b = new StringBuilder("aaabbbccc");
            var v = string.Empty;
            b.EndsWith(v).Should().Be(false);
            b.EndsWith(v, ECharComparison.CaseSensitive).Should().Be(false);
            b.EndsWith(v, null).Should().Be(false);
        }

        [Theory]
        [InlineData("", null, null, false)]
        [InlineData("aaabbbccc", null, null, false)]
        [InlineData("aaabbbccc", "", null, false)]
        [InlineData("aaabbbccc", "c", null, true)]
        [InlineData("aaabbbccc", "cc", null, true)]
        [InlineData("aaabbbccc", "ccc", null, true)]
        [InlineData("aaabbbccc", "cccc", null, false)]
        public void EndsWith_Culture(string subject, string value, string culture, bool expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).EndsWith(value, info).Should().Be(expected);
        }

        #endregion

        #region EndsWithAny Tests

        [Fact]
        public void EndsWithAny_Wrappers()
        {
            var b = new StringBuilder("aaabbbccc");
            var v = new string[0];
            b.EndsWithAny(v).Should().Be(false);
            b.EndsWithAny(v, ECharComparison.CaseSensitive).Should().Be(false);
            b.EndsWithAny(v, null).Should().Be(false);
        }

        [Theory]
        [InlineData("", new string[0], null, false)]
        [InlineData("aaabbbccc", new string[0], null, false)]
        [InlineData("aaabbbccc", new[] { "aaa", "bbb", "c" }, null, true)]
        [InlineData("aaabbbccc", new[] { "aaa", "bbb", "cc" }, null, true)]
        [InlineData("aaabbbccc", new[] { "aaa", "bbb", "ccc" }, null, true)]
        [InlineData("aaabbbccc", new[] { "aaa", "bbb", "cccc" }, null, false)]
        public void EndsWithAny_Culture(string subject, IEnumerable<string> values, string culture, bool expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).EndsWithAny(values, info).Should().Be(expected);
        }

        #endregion

        #region IndexOf(char) Tests

        [Theory]
        [InlineData("", 'x', -1)]
        [InlineData("abcabcabcabc", 'x', -1)]
        [InlineData("abcabcabcabc", 'A', -1)]
        [InlineData("abcabcabcabc", 'a', 0)]
        public void IndexOf1_Char(string subject, char value, int expected)
            => new StringBuilder(subject).IndexOf(value).Should().Be(expected);

        [Theory]
        [InlineData("", 'x', 0, -1)]
        [InlineData("abcabcabcabc", 'x', 0, -1)]
        [InlineData("abcabcabcabc", 'A', 0, -1)]
        [InlineData("abcabcabcabc", 'a', 12, -1)]
        [InlineData("abcabcabcabc", 'a', 0, 0)]
        [InlineData("abcabcabcabc", 'a', 1, 3)]
        [InlineData("abcabcabcabc", 'a', 5, 6)]
        public void IndexOf2_Char(string subject, char value, int startIndex, int expected)
            => new StringBuilder(subject).IndexOf(value, startIndex).Should().Be(expected);

        [Theory]
        [InlineData("", 'x', 0, -1, -1)]
        [InlineData("abcabcabcabc", 'x', 0, 0, -1)]
        [InlineData("abcabcabcabc", 'x', 0, -1, -1)]
        [InlineData("abcabcabcabc", 'A', 0, -1, -1)]
        [InlineData("abcabcabcabc", 'a', 10, -1, -1)]
        [InlineData("abcabcabcabc", 'a', 12, -1, -1)]
        [InlineData("abcabcabcabc", 'a', 1, 2, -1)]
        [InlineData("abcabcabcabc", 'a', 0, -1, 0)]
        [InlineData("abcabcabcabc", 'a', 0, 1, 0)]
        [InlineData("abcabcabcabc", 'a', 1, -1, 3)]
        [InlineData("abcabcabcabc", 'a', 1, 10, 3)]
        [InlineData("abcabcabcabc", 'a', 1, 20, 3)]
        [InlineData("abcabcabcabc", 'a', 5, 2, 6)]
        public void IndexOf3_Char(string subject, char value, int startIndex, int maxCount, int expected)
            => new StringBuilder(subject).IndexOf(value, startIndex, maxCount).Should().Be(expected);

        [Theory]
        [InlineData("", 'x', ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", 'x', ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", 'A', ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", 'a', ECharComparison.CaseSensitive, 0)]

        [InlineData("abcabcabcabc", 'A', ECharComparison.CurrentCultureIgnoreCase, 0)]

        [InlineData("abcabcabcabc", 'A', ECharComparison.InvariantCultureIgnoreCase, 0)]
        public void IndexOf1_Char_Comparison(string subject, char value, ECharComparison comparison, int expected)
            => new StringBuilder(subject).IndexOf(value, comparison).Should().Be(expected);

        [Theory]
        [InlineData("", 'x', 0, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", 'x', 0, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", 'A', 0, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", 'a', 12, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", 'a', 0, ECharComparison.CaseSensitive, 0)]
        [InlineData("abcabcabcabc", 'a', 1, ECharComparison.CaseSensitive, 3)]
        [InlineData("abcabcabcabc", 'a', 5, ECharComparison.CaseSensitive, 6)]

        [InlineData("abcabcabcabc", 'A', 12, ECharComparison.CurrentCultureIgnoreCase, -1)]
        [InlineData("abcabcabcabc", 'A', 0, ECharComparison.CurrentCultureIgnoreCase, 0)]
        [InlineData("abcabcabcabc", 'A', 1, ECharComparison.CurrentCultureIgnoreCase, 3)]
        [InlineData("abcabcabcabc", 'A', 5, ECharComparison.CurrentCultureIgnoreCase, 6)]

        [InlineData("abcabcabcabc", 'A', 12, ECharComparison.InvariantCultureIgnoreCase, -1)]
        [InlineData("abcabcabcabc", 'A', 0, ECharComparison.InvariantCultureIgnoreCase, 0)]
        [InlineData("abcabcabcabc", 'A', 1, ECharComparison.InvariantCultureIgnoreCase, 3)]
        [InlineData("abcabcabcabc", 'A', 5, ECharComparison.InvariantCultureIgnoreCase, 6)]
        public void IndexOf2_Char_Comparison(string subject, char value, int startIndex, ECharComparison comparison, int expected)
            => new StringBuilder(subject).IndexOf(value, startIndex, comparison).Should().Be(expected);

        [Theory]
        [InlineData("", 'x', 0, -1, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", 'x', 0, 0, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", 'x', 0, -1, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", 'A', 0, -1, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", 'a', 10, -1, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", 'a', 12, -1, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", 'a', 1, 2, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", 'a', 0, -1, ECharComparison.CaseSensitive, 0)]
        [InlineData("abcabcabcabc", 'a', 0, 1, ECharComparison.CaseSensitive, 0)]
        [InlineData("abcabcabcabc", 'a', 1, -1, ECharComparison.CaseSensitive, 3)]
        [InlineData("abcabcabcabc", 'a', 1, 10, ECharComparison.CaseSensitive, 3)]
        [InlineData("abcabcabcabc", 'a', 1, 20, ECharComparison.CaseSensitive, 3)]
        [InlineData("abcabcabcabc", 'a', 5, 2, ECharComparison.CaseSensitive, 6)]

        [InlineData("abcabcabcabc", 'A', 10, -1, ECharComparison.CurrentCultureIgnoreCase, -1)]
        [InlineData("abcabcabcabc", 'A', 12, -1, ECharComparison.CurrentCultureIgnoreCase, -1)]
        [InlineData("abcabcabcabc", 'A', 1, 2, ECharComparison.CurrentCultureIgnoreCase, -1)]
        [InlineData("abcabcabcabc", 'A', 0, -1, ECharComparison.CurrentCultureIgnoreCase, 0)]
        [InlineData("abcabcabcabc", 'A', 0, 1, ECharComparison.CurrentCultureIgnoreCase, 0)]
        [InlineData("abcabcabcabc", 'A', 1, -1, ECharComparison.CurrentCultureIgnoreCase, 3)]
        [InlineData("abcabcabcabc", 'A', 1, 10, ECharComparison.CurrentCultureIgnoreCase, 3)]
        [InlineData("abcabcabcabc", 'A', 1, 20, ECharComparison.CurrentCultureIgnoreCase, 3)]
        [InlineData("abcabcabcabc", 'A', 5, 2, ECharComparison.CurrentCultureIgnoreCase, 6)]

        [InlineData("abcabcabcabc", 'A', 10, -1, ECharComparison.InvariantCultureIgnoreCase, -1)]
        [InlineData("abcabcabcabc", 'A', 12, -1, ECharComparison.InvariantCultureIgnoreCase, -1)]
        [InlineData("abcabcabcabc", 'A', 1, 2, ECharComparison.InvariantCultureIgnoreCase, -1)]
        [InlineData("abcabcabcabc", 'A', 0, -1, ECharComparison.InvariantCultureIgnoreCase, 0)]
        [InlineData("abcabcabcabc", 'A', 0, 1, ECharComparison.InvariantCultureIgnoreCase, 0)]
        [InlineData("abcabcabcabc", 'A', 1, -1, ECharComparison.InvariantCultureIgnoreCase, 3)]
        [InlineData("abcabcabcabc", 'A', 1, 10, ECharComparison.InvariantCultureIgnoreCase, 3)]
        [InlineData("abcabcabcabc", 'A', 1, 20, ECharComparison.InvariantCultureIgnoreCase, 3)]
        [InlineData("abcabcabcabc", 'A', 5, 2, ECharComparison.InvariantCultureIgnoreCase, 6)]
        public void IndexOf3_Char_Comparison(string subject, char value, int startIndex, int maxCount, ECharComparison comparison, int expected)
            => new StringBuilder(subject).IndexOf(value, startIndex, maxCount, comparison).Should().Be(expected);

        [Theory]
        [InlineData("", 'x', null, -1)]
        [InlineData("abcabcabcabc", 'x', null, -1)]
        [InlineData("abcabcabcabc", 'A', null, -1)]
        [InlineData("abcabcabcabc", 'a', null, 0)]

        [InlineData("abcabcabcabc", 'A', "current", 0)]

        [InlineData("abcabcabcabc", 'A', "invariant", 0)]
        public void IndexOf1_Char_Culture(string subject, char value, string culture, int expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).IndexOf(value, info).Should().Be(expected);
        }

        [Theory]
        [InlineData("", 'x', 0, null, -1)]
        [InlineData("abcabcabcabc", 'x', 0, null, -1)]
        [InlineData("abcabcabcabc", 'A', 0, null, -1)]
        [InlineData("abcabcabcabc", 'a', 12, null, -1)]
        [InlineData("abcabcabcabc", 'a', 0, null, 0)]
        [InlineData("abcabcabcabc", 'a', 1, null, 3)]
        [InlineData("abcabcabcabc", 'a', 5, null, 6)]

        [InlineData("abcabcabcabc", 'A', 12, "current", -1)]
        [InlineData("abcabcabcabc", 'A', 0, "current", 0)]
        [InlineData("abcabcabcabc", 'A', 1, "current", 3)]
        [InlineData("abcabcabcabc", 'A', 5, "current", 6)]

        [InlineData("abcabcabcabc", 'A', 12, "invariant", -1)]
        [InlineData("abcabcabcabc", 'A', 0, "invariant", 0)]
        [InlineData("abcabcabcabc", 'A', 1, "invariant", 3)]
        [InlineData("abcabcabcabc", 'A', 5, "invariant", 6)]
        public void IndexOf2_Char_Culture(string subject, char value, int startIndex, string culture, int expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).IndexOf(value, startIndex, info).Should().Be(expected);
        }

        [Theory]
        [InlineData("", 'x', 0, -1, null, -1)]
        [InlineData("abcabcabcabc", 'x', 0, 0, null, -1)]
        [InlineData("abcabcabcabc", 'x', 0, -1, null, -1)]
        [InlineData("abcabcabcabc", 'A', 0, -1, null, -1)]
        [InlineData("abcabcabcabc", 'a', 10, -1, null, -1)]
        [InlineData("abcabcabcabc", 'a', 12, -1, null, -1)]
        [InlineData("abcabcabcabc", 'a', 1, 2, null, -1)]
        [InlineData("abcabcabcabc", 'a', 0, -1, null, 0)]
        [InlineData("abcabcabcabc", 'a', 0, 1, null, 0)]
        [InlineData("abcabcabcabc", 'a', 1, -1, null, 3)]
        [InlineData("abcabcabcabc", 'a', 1, 10, null, 3)]
        [InlineData("abcabcabcabc", 'a', 1, 20, null, 3)]
        [InlineData("abcabcabcabc", 'a', 5, 2, null, 6)]

        [InlineData("abcabcabcabc", 'A', 10, -1, "current", -1)]
        [InlineData("abcabcabcabc", 'A', 12, -1, "current", -1)]
        [InlineData("abcabcabcabc", 'A', 1, 2, "current", -1)]
        [InlineData("abcabcabcabc", 'A', 0, -1, "current", 0)]
        [InlineData("abcabcabcabc", 'A', 0, 1, "current", 0)]
        [InlineData("abcabcabcabc", 'A', 1, -1, "current", 3)]
        [InlineData("abcabcabcabc", 'A', 1, 10, "current", 3)]
        [InlineData("abcabcabcabc", 'A', 1, 20, "current", 3)]
        [InlineData("abcabcabcabc", 'A', 5, 2, "current", 6)]

        [InlineData("abcabcabcabc", 'A', 10, -1, "invariant", -1)]
        [InlineData("abcabcabcabc", 'A', 12, -1, "invariant", -1)]
        [InlineData("abcabcabcabc", 'A', 1, 2, "invariant", -1)]
        [InlineData("abcabcabcabc", 'A', 0, -1, "invariant", 0)]
        [InlineData("abcabcabcabc", 'A', 0, 1, "invariant", 0)]
        [InlineData("abcabcabcabc", 'A', 1, -1, "invariant", 3)]
        [InlineData("abcabcabcabc", 'A', 1, 10, "invariant", 3)]
        [InlineData("abcabcabcabc", 'A', 1, 20, "invariant", 3)]
        [InlineData("abcabcabcabc", 'A', 5, 2, "invariant", 6)]
        public void IndexOf3_Char_Culture(string subject, char value, int startIndex, int maxCount, string culture, int expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).IndexOf(value, startIndex, maxCount, info).Should().Be(expected);
        }

        #endregion

        #region IndexOf(string) Tests

        [Theory]
        [InlineData("", "", -1)]
        [InlineData("abcabcabcabc", "", -1)]
        [InlineData("abcabcabcabc", "xabcabcabcabcx", -1)]
        [InlineData("abcabcabcabc", "xbc", -1)]
        [InlineData("abcabcabcabc", "Abc", -1)]
        [InlineData("abcabcabcabc", "abc", 0)]
        [InlineData("abc", "bc", 1)]
        [InlineData("abc", "c", 2)]
        public void IndexOf1_String(string subject, string value, int expected)
            => new StringBuilder(subject).IndexOf(value).Should().Be(expected);

        [Theory]
        [InlineData("", "", 0, -1)]
        [InlineData("abcabcabcabc", "", 0, -1)]
        [InlineData("abcabcabcabc", "xabcabcabcabcx", 0, -1)]
        [InlineData("abcabcabcabc", "xbc", 0, -1)]
        [InlineData("abcabcabcabc", "Abc", 0, -1)]
        [InlineData("abcabcabcabc", "abc", 12, -1)]
        [InlineData("abcabcabcabc", "abc", 0, 0)]
        [InlineData("abcabcabcabc", "abc", 1, 3)]
        [InlineData("abcabcabcabc", "abc", 5, 6)]
        public void IndexOf2_String(string subject, string value, int startIndex, int expected)
            => new StringBuilder(subject).IndexOf(value, startIndex).Should().Be(expected);

        [Theory]
        [InlineData("", "", 0, -1, -1)]
        [InlineData("abcabcabcabc", "", 0, -1, -1)]
        [InlineData("abcabcabcabc", "xabcabcabcabcx", 0, -1, -1)]
        [InlineData("abcabcabcabc", "xbc", 0, 0, -1)]
        [InlineData("abcabcabcabc", "xbc", 0, -1, -1)]
        [InlineData("abcabcabcabc", "Abc", 0, -1, -1)]
        [InlineData("abcabcabcabc", "abc", 10, -1, -1)]
        [InlineData("abcabcabcabc", "abc", 12, -1, -1)]
        [InlineData("abcabcabcabc", "abc", 1, 2, -1)]
        [InlineData("abcabcabcabc", "abc", 0, -1, 0)]
        [InlineData("abcabcabcabc", "abc", 0, 1, 0)]
        [InlineData("abcabcabcabc", "abc", 1, -1, 3)]
        [InlineData("abcabcabcabc", "abc", 1, 10, 3)]
        [InlineData("abcabcabcabc", "abc", 1, 20, 3)]
        [InlineData("abcabcabcabc", "abc", 5, 2, 6)]
        public void IndexOf3_String(string subject, string value, int startIndex, int maxCount, int expected)
            => new StringBuilder(subject).IndexOf(value, startIndex, maxCount).Should().Be(expected);

        [Theory]
        [InlineData("", "", ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", "", ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", "xabcabcabcabcx", ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", "xbc", ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", "Abc", ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", "abc", ECharComparison.CaseSensitive, 0)]

        [InlineData("abcabcabcabc", "Abc", ECharComparison.CurrentCultureIgnoreCase, 0)]

        [InlineData("abcabcabcabc", "Abc", ECharComparison.InvariantCultureIgnoreCase, 0)]
        public void IndexOf1_String_Comparison(string subject, string value, ECharComparison comparison, int expected)
            => new StringBuilder(subject).IndexOf(value, comparison).Should().Be(expected);

        [Theory]
        [InlineData("", "", 0, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", "", 0, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", "xabcabcabcabcx", 0, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", "xbc", 0, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", "Abc", 0, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", "abc", 12, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", "abc", 0, ECharComparison.CaseSensitive, 0)]
        [InlineData("abcabcabcabc", "abc", 1, ECharComparison.CaseSensitive, 3)]
        [InlineData("abcabcabcabc", "abc", 5, ECharComparison.CaseSensitive, 6)]

        [InlineData("abcabcabcabc", "Abc", 12, ECharComparison.CurrentCultureIgnoreCase, -1)]
        [InlineData("abcabcabcabc", "Abc", 0, ECharComparison.CurrentCultureIgnoreCase, 0)]
        [InlineData("abcabcabcabc", "Abc", 1, ECharComparison.CurrentCultureIgnoreCase, 3)]
        [InlineData("abcabcabcabc", "Abc", 5, ECharComparison.CurrentCultureIgnoreCase, 6)]

        [InlineData("abcabcabcabc", "Abc", 12, ECharComparison.InvariantCultureIgnoreCase, -1)]
        [InlineData("abcabcabcabc", "Abc", 0, ECharComparison.InvariantCultureIgnoreCase, 0)]
        [InlineData("abcabcabcabc", "Abc", 1, ECharComparison.InvariantCultureIgnoreCase, 3)]
        [InlineData("abcabcabcabc", "Abc", 5, ECharComparison.InvariantCultureIgnoreCase, 6)]
        public void IndexOf2_String_Comparison(string subject, string value, int startIndex, ECharComparison comparison, int expected)
            => new StringBuilder(subject).IndexOf(value, startIndex, comparison).Should().Be(expected);

        [Theory]
        [InlineData("", "", 0, -1, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", "", 0, -1, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", "xabcabcabcabcx", 0, -1, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", "xbc", 0, 0, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", "xbc", 0, -1, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", "Abc", 0, -1, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", "abc", 10, -1, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", "abc", 12, -1, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", "abc", 1, 2, ECharComparison.CaseSensitive, -1)]
        [InlineData("abcabcabcabc", "abc", 0, -1, ECharComparison.CaseSensitive, 0)]
        [InlineData("abcabcabcabc", "abc", 0, 1, ECharComparison.CaseSensitive, 0)]
        [InlineData("abcabcabcabc", "abc", 1, -1, ECharComparison.CaseSensitive, 3)]
        [InlineData("abcabcabcabc", "abc", 1, 10, ECharComparison.CaseSensitive, 3)]
        [InlineData("abcabcabcabc", "abc", 1, 20, ECharComparison.CaseSensitive, 3)]
        [InlineData("abcabcabcabc", "abc", 5, 2, ECharComparison.CaseSensitive, 6)]

        [InlineData("abcabcabcabc", "Abc", 10, -1, ECharComparison.CurrentCultureIgnoreCase, -1)]
        [InlineData("abcabcabcabc", "Abc", 12, -1, ECharComparison.CurrentCultureIgnoreCase, -1)]
        [InlineData("abcabcabcabc", "Abc", 1, 2, ECharComparison.CurrentCultureIgnoreCase, -1)]
        [InlineData("abcabcabcabc", "Abc", 0, -1, ECharComparison.CurrentCultureIgnoreCase, 0)]
        [InlineData("abcabcabcabc", "Abc", 0, 1, ECharComparison.CurrentCultureIgnoreCase, 0)]
        [InlineData("abcabcabcabc", "Abc", 1, -1, ECharComparison.CurrentCultureIgnoreCase, 3)]
        [InlineData("abcabcabcabc", "Abc", 1, 10, ECharComparison.CurrentCultureIgnoreCase, 3)]
        [InlineData("abcabcabcabc", "Abc", 1, 20, ECharComparison.CurrentCultureIgnoreCase, 3)]
        [InlineData("abcabcabcabc", "Abc", 5, 2, ECharComparison.CurrentCultureIgnoreCase, 6)]

        [InlineData("abcabcabcabc", "Abc", 10, -1, ECharComparison.InvariantCultureIgnoreCase, -1)]
        [InlineData("abcabcabcabc", "Abc", 12, -1, ECharComparison.InvariantCultureIgnoreCase, -1)]
        [InlineData("abcabcabcabc", "Abc", 1, 2, ECharComparison.InvariantCultureIgnoreCase, -1)]
        [InlineData("abcabcabcabc", "Abc", 0, -1, ECharComparison.InvariantCultureIgnoreCase, 0)]
        [InlineData("abcabcabcabc", "Abc", 0, 1, ECharComparison.InvariantCultureIgnoreCase, 0)]
        [InlineData("abcabcabcabc", "Abc", 1, -1, ECharComparison.InvariantCultureIgnoreCase, 3)]
        [InlineData("abcabcabcabc", "Abc", 1, 10, ECharComparison.InvariantCultureIgnoreCase, 3)]
        [InlineData("abcabcabcabc", "Abc", 1, 20, ECharComparison.InvariantCultureIgnoreCase, 3)]
        [InlineData("abcabcabcabc", "Abc", 5, 2, ECharComparison.InvariantCultureIgnoreCase, 6)]
        public void IndexOf3_String_Comparison(string subject, string value, int startIndex, int maxCount, ECharComparison comparison, int expected)
            => new StringBuilder(subject).IndexOf(value, startIndex, maxCount, comparison).Should().Be(expected);

        [Theory]
        [InlineData("", "", null, -1)]
        [InlineData("abcabcabcabc", "", null, -1)]
        [InlineData("abcabcabcabc", "xabcabcabcabcx", null, -1)]
        [InlineData("abcabcabcabc", "xbc", null, -1)]
        [InlineData("abcabcabcabc", "Abc", null, -1)]
        [InlineData("abcabcabcabc", "abc", null, 0)]

        [InlineData("abcabcabcabc", "Abc", "current", 0)]

        [InlineData("abcabcabcabc", "Abc", "invariant", 0)]
        public void IndexOf1_String_Culture(string subject, string value, string culture, int expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).IndexOf(value, info).Should().Be(expected);
        }

        [Theory]
        [InlineData("", "", 0, null, -1)]
        [InlineData("abcabcabcabc", "", 0, null, -1)]
        [InlineData("abcabcabcabc", "xabcabcabcabcx", 0, null, -1)]
        [InlineData("abcabcabcabc", "xbc", 0, null, -1)]
        [InlineData("abcabcabcabc", "Abc", 0, null, -1)]
        [InlineData("abcabcabcabc", "abc", 12, null, -1)]
        [InlineData("abcabcabcabc", "abc", 0, null, 0)]
        [InlineData("abcabcabcabc", "abc", 1, null, 3)]
        [InlineData("abcabcabcabc", "abc", 5, null, 6)]

        [InlineData("abcabcabcabc", "Abc", 12, "current", -1)]
        [InlineData("abcabcabcabc", "Abc", 0, "current", 0)]
        [InlineData("abcabcabcabc", "Abc", 1, "current", 3)]
        [InlineData("abcabcabcabc", "Abc", 5, "current", 6)]

        [InlineData("abcabcabcabc", "Abc", 12, "invariant", -1)]
        [InlineData("abcabcabcabc", "Abc", 0, "invariant", 0)]
        [InlineData("abcabcabcabc", "Abc", 1, "invariant", 3)]
        [InlineData("abcabcabcabc", "Abc", 5, "invariant", 6)]
        public void IndexOf2_String_Culture(string subject, string value, int startIndex, string culture, int expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).IndexOf(value, startIndex, info).Should().Be(expected);
        }

        [Theory]
        [InlineData("", "", 0, -1, null, -1)]
        [InlineData("abcabcabcabc", "", 0, -1, null, -1)]
        [InlineData("abcabcabcabc", "xabcabcabcabcx", 0, -1, null, -1)]
        [InlineData("abcabcabcabc", "xbc", 0, 0, null, -1)]
        [InlineData("abcabcabcabc", "xbc", 0, -1, null, -1)]
        [InlineData("abcabcabcabc", "Abc", 0, -1, null, -1)]
        [InlineData("abcabcabcabc", "abc", 10, -1, null, -1)]
        [InlineData("abcabcabcabc", "abc", 12, -1, null, -1)]
        [InlineData("abcabcabcabc", "abc", 1, 2, null, -1)]
        [InlineData("abcabcabcabc", "abc", 0, -1, null, 0)]
        [InlineData("abcabcabcabc", "abc", 0, 1, null, 0)]
        [InlineData("abcabcabcabc", "abc", 1, -1, null, 3)]
        [InlineData("abcabcabcabc", "abc", 1, 10, null, 3)]
        [InlineData("abcabcabcabc", "abc", 1, 20, null, 3)]
        [InlineData("abcabcabcabc", "abc", 5, 2, null, 6)]

        [InlineData("abcabcabcabc", "Abc", 10, -1, "current", -1)]
        [InlineData("abcabcabcabc", "Abc", 12, -1, "current", -1)]
        [InlineData("abcabcabcabc", "Abc", 1, 2, "current", -1)]
        [InlineData("abcabcabcabc", "Abc", 0, -1, "current", 0)]
        [InlineData("abcabcabcabc", "Abc", 0, 1, "current", 0)]
        [InlineData("abcabcabcabc", "Abc", 1, -1, "current", 3)]
        [InlineData("abcabcabcabc", "Abc", 1, 10, "current", 3)]
        [InlineData("abcabcabcabc", "Abc", 1, 20, "current", 3)]
        [InlineData("abcabcabcabc", "Abc", 5, 2, "current", 6)]

        [InlineData("abcabcabcabc", "Abc", 10, -1, "invariant", -1)]
        [InlineData("abcabcabcabc", "Abc", 12, -1, "invariant", -1)]
        [InlineData("abcabcabcabc", "Abc", 1, 2, "invariant", -1)]
        [InlineData("abcabcabcabc", "Abc", 0, -1, "invariant", 0)]
        [InlineData("abcabcabcabc", "Abc", 0, 1, "invariant", 0)]
        [InlineData("abcabcabcabc", "Abc", 1, -1, "invariant", 3)]
        [InlineData("abcabcabcabc", "Abc", 1, 10, "invariant", 3)]
        [InlineData("abcabcabcabc", "Abc", 1, 20, "invariant", 3)]
        [InlineData("abcabcabcabc", "Abc", 5, 2, "invariant", 6)]
        public void IndexOf3_String_Culture(string subject, string value, int startIndex, int maxCount, string culture, int expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).IndexOf(value, startIndex, maxCount, info).Should().Be(expected);
        }

        #endregion

        #region IndexOfAny(char) Tests

        [Fact]
        public void IndexOfAny_Char_Wrappers()
        {
            var b = new StringBuilder("xab");
            var v = new char[0];
            b.IndexOfAny(v).Should().Be(-1);
            b.IndexOfAny(v, 0).Should().Be(-1);
            b.IndexOfAny(v, 0, -1).Should().Be(-1);
            b.IndexOfAny(v, ECharComparison.CaseSensitive).Should().Be(-1);
            b.IndexOfAny(v, 0, ECharComparison.CaseSensitive).Should().Be(-1);
            b.IndexOfAny(v, 0, -1, ECharComparison.CaseSensitive).Should().Be(-1);
            b.IndexOfAny(v, null).Should().Be(-1);
            b.IndexOfAny(v, 0, null).Should().Be(-1);
            b.IndexOfAny(v, 0, -1, null).Should().Be(-1);
        }

        [Theory]
        [InlineData("xab", new char[0], 0, -1, null, -1)]
        [InlineData("xab", new[] { 'a' }, 0, 0, null, -1)]
        [InlineData("xab", new[] { 'a' }, 3, -1, null, -1)]
        [InlineData("xab", new[] { 'c' }, 0, -1, null, -1)]
        [InlineData("xab", new[] { 'c', 'x', 'a' }, 0, -1, null, 0)]// Breadth and Depth search would be 0
        [InlineData("xab", new[] { 'c', 'a', 'x' }, 0, -1, null, 0)]// Breadth search would be 0 and Depth search would be 1
        public void IndexOfAny3_Char_Culture(string subject, IEnumerable<char> values, int startIndex, int maxCount, string culture, int expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).IndexOfAny(values, startIndex, maxCount).Should().Be(expected);
        }

        #endregion

        #region IndexOfAny(string) Tests

        [Fact]
        public void IndexOfAny_String_Wrappers()
        {
            var b = new StringBuilder("xab");
            var v = new string[0];
            b.IndexOfAny(v).Should().Be(-1);
            b.IndexOfAny(v, 0).Should().Be(-1);
            b.IndexOfAny(v, 0, -1).Should().Be(-1);
            b.IndexOfAny(v, ECharComparison.CaseSensitive).Should().Be(-1);
            b.IndexOfAny(v, 0, ECharComparison.CaseSensitive).Should().Be(-1);
            b.IndexOfAny(v, 0, -1, ECharComparison.CaseSensitive).Should().Be(-1);
            b.IndexOfAny(v, null).Should().Be(-1);
            b.IndexOfAny(v, 0, null).Should().Be(-1);
            b.IndexOfAny(v, 0, -1, null).Should().Be(-1);
        }

        [Theory]
        [InlineData("xab", new string[0], 0, -1, -1)]
        [InlineData("xab", new[] { "ab" }, 0, 0, -1)]
        [InlineData("xab", new[] { "ab" }, 3, -1, -1)]
        [InlineData("xab", new[] { "cd" }, 0, -1, -1)]
        [InlineData("xab", new[] { "cd", "xa", "ab" }, 0, -1, 0)]// Breadth and Depth search would be 0
        [InlineData("xab", new[] { "cd", "ab", "xa" }, 0, -1, 0)]// Breadth search would be 0 and Depth search would be 1
        public void IndexOfAny3_String_Culture(string subject, IEnumerable<string> values, int startIndex, int maxCount, int expected)
            => new StringBuilder(subject).IndexOfAny(values, startIndex, maxCount).Should().Be(expected);

        #endregion

        #region HasIndex Tests

        [Theory]
        [InlineData(null, 0, false)]
        [InlineData("", 0, false)]
        [InlineData("test", -1, false)]
        [InlineData("test", 4, false)]
        [InlineData("test", 0, true)]
        [InlineData("test", 3, true)]
        public void HasIndex(string subject, int index, bool expected)
            => new StringBuilder(subject).HasIndex(index).Should().Be(expected);

        #endregion

        #region IsChar Tests

        [Theory]
        [InlineData("abcdefg", 1, 'b', true)]
        [InlineData("abcdefg", 1, 'B', false)]
        [InlineData(null, 0, '\0', false)]
        [InlineData("abcdefg", -1, '\0', false)]
        [InlineData("abcdefg", 7, '\0', false)]
        public void IsChar(string subject, int index, char value, bool expected)
            => new StringBuilder(subject).IsChar(index, value).Should().Be(expected);

        [Theory]
        [InlineData("abcdefg", 1, 'b', ECharComparison.CaseSensitive, true)]
        [InlineData("abcdefg", 1, 'B', ECharComparison.CaseSensitive, false)]
        [InlineData(null, 0, '\0', ECharComparison.CaseSensitive, false)]
        [InlineData("abcdefg", -1, '\0', ECharComparison.CaseSensitive, false)]
        [InlineData("abcdefg", 7, '\0', ECharComparison.CaseSensitive, false)]
        [InlineData("abcdefg", 1, 'B', ECharComparison.CurrentCultureIgnoreCase, true)]
        [InlineData("abcdefg", 1, 'B', ECharComparison.InvariantCultureIgnoreCase, true)]
        public void IsChar_Comparison(string subject, int index, char value, ECharComparison comparison, bool expected)
            => new StringBuilder(subject).IsChar(index, value, comparison).Should().Be(expected);

        [Theory]
        [InlineData("abcdefg", 1, 'b', null, true)]
        [InlineData("abcdefg", 1, 'B', null, false)]
        [InlineData(null, 0, '\0', null, false)]
        [InlineData("abcdefg", -1, '\0', null, false)]
        [InlineData("abcdefg", 7, '\0', null, false)]
        [InlineData("abcdefg", 1, 'B', "current", true)]
        [InlineData("abcdefg", 1, 'B', "invariant", true)]
        public void IsChar_Culture(string subject, int index, char value, string culture, bool expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).IsChar(index, value, info).Should().Be(expected);
        }

        #endregion

        #region IsPrevChar Tests

        [Theory]
        [InlineData("abcdefg", 1, 'a', true)]
        [InlineData("abcdefg", 1, 'A', false)]
        [InlineData(null, 0, '\0', false)]
        [InlineData("abcdefg", -1, '\0', false)]
        [InlineData("abcdefg", 7, '\0', false)]
        public void IsPrevChar(string subject, int index, char value, bool expected)
            => new StringBuilder(subject).IsPrevChar(index, value).Should().Be(expected);

        [Theory]
        [InlineData("abcdefg", 1, 'a', ECharComparison.CaseSensitive, true)]
        [InlineData("abcdefg", 1, 'A', ECharComparison.CaseSensitive, false)]
        [InlineData(null, 0, '\0', ECharComparison.CaseSensitive, false)]
        [InlineData("abcdefg", -1, '\0', ECharComparison.CaseSensitive, false)]
        [InlineData("abcdefg", 7, '\0', ECharComparison.CaseSensitive, false)]
        [InlineData("abcdefg", 1, 'A', ECharComparison.CurrentCultureIgnoreCase, true)]
        [InlineData("abcdefg", 1, 'A', ECharComparison.InvariantCultureIgnoreCase, true)]
        public void IsPrevChar_Comparison(string subject, int index, char value, ECharComparison comparison, bool expected)
            => new StringBuilder(subject).IsPrevChar(index, value, comparison).Should().Be(expected);

        [Theory]
        [InlineData("abcdefg", 1, 'a', null, true)]
        [InlineData("abcdefg", 1, 'A', null, false)]
        [InlineData(null, 0, '\0', null, false)]
        [InlineData("abcdefg", -1, '\0', null, false)]
        [InlineData("abcdefg", 7, '\0', null, false)]
        [InlineData("abcdefg", 1, 'A', "current", true)]
        [InlineData("abcdefg", 1, 'A', "invariant", true)]
        public void IsPrevChar_Culture(string subject, int index, char value, string culture, bool expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).IsPrevChar(index, value, info).Should().Be(expected);
        }

        #endregion

        #region IsNextChar Tests

        [Theory]
        [InlineData("abcdefg", 1, 'c', true)]
        [InlineData("abcdefg", 1, 'C', false)]
        [InlineData(null, 0, '\0', false)]
        [InlineData("abcdefg", -1, '\0', false)]
        [InlineData("abcdefg", 7, '\0', false)]
        public void IsNextChar(string subject, int index, char value, bool expected)
            => new StringBuilder(subject).IsNextChar(index, value).Should().Be(expected);

        [Theory]
        [InlineData("abcdefg", 1, 'c', ECharComparison.CaseSensitive, true)]
        [InlineData("abcdefg", 1, 'C', ECharComparison.CaseSensitive, false)]
        [InlineData(null, 0, '\0', ECharComparison.CaseSensitive, false)]
        [InlineData("abcdefg", -1, '\0', ECharComparison.CaseSensitive, false)]
        [InlineData("abcdefg", 7, '\0', ECharComparison.CaseSensitive, false)]
        [InlineData("abcdefg", 1, 'C', ECharComparison.CurrentCultureIgnoreCase, true)]
        [InlineData("abcdefg", 1, 'C', ECharComparison.InvariantCultureIgnoreCase, true)]
        public void IsNextChar_Comparison(string subject, int index, char value, ECharComparison comparison, bool expected)
            => new StringBuilder(subject).IsNextChar(index, value, comparison).Should().Be(expected);

        [Theory]
        [InlineData("abcdefg", 1, 'c', null, true)]
        [InlineData("abcdefg", 1, 'C', null, false)]
        [InlineData(null, 0, '\0', null, false)]
        [InlineData("abcdefg", -1, '\0', null, false)]
        [InlineData("abcdefg", 7, '\0', null, false)]
        [InlineData("abcdefg", 1, 'C', "current", true)]
        [InlineData("abcdefg", 1, 'C', "invariant", true)]
        public void IsNextChar_Culture(string subject, int index, char value, string culture, bool expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).IsNextChar(index, value, info).Should().Be(expected);
        }

        #endregion

        #region IsEmpty Tests

        [Theory]
        [InlineData(null, true)]
        [InlineData("", true)]
        [InlineData("text", false)]
        public void IsEmpty(string subject, bool expected)
        {
            StringBuilder builder = subject == null ? null : new StringBuilder(subject);
            builder.IsEmpty().Should().Be(expected);
            builder.IsNotEmpty().Should().Be(!expected);
        }

        [Theory]
        [InlineData(null, true)]
        [InlineData("", true)]
        [InlineData("    ", true)]
        [InlineData("    \t    ", true)]
        [InlineData("\t\t", true)]
        [InlineData("\t    \t", true)]
        [InlineData("text", false)]
        [InlineData("    text", false)]
        [InlineData("text    ", false)]
        [InlineData("    text    ", false)]
        public void IsEmptyOrWhitespace(string subject, bool expected)
        {
            StringBuilder builder = subject == null ? null : new StringBuilder(subject);
            builder.IsEmptyOrWhitespace().Should().Be(expected);
            builder.IsNotEmptyOrWhitespace().Should().Be(!expected);
        }

        #endregion

        #region IsMatch Tests

        [Theory]
        [InlineData(null, null, -1, false)]
        [InlineData(null, "", -1, false)]
        [InlineData("", null, -1, false)]
        [InlineData("", "", -1, false)]
        [InlineData("a", "aa", -1, false)]
        [InlineData("abcdef", "aa", -1, false)]
        [InlineData("abcdef", "aa", 5, false)]
        [InlineData("abcdef", "aa", 1, false)]
        [InlineData("abcdef", "bc", 0, false)]
        [InlineData("abcdef", "bc", 1, true)]
        public void IsMatch(string subject, string value, int index, bool expected)
            => new StringBuilder(subject).IsMatch(value, index).Should().Be(expected);

        [Theory]
        [InlineData(null, null, -1, ECharComparison.CaseSensitive, false)]
        [InlineData(null, "", -1, ECharComparison.CaseSensitive, false)]
        [InlineData("", null, -1, ECharComparison.CaseSensitive, false)]
        [InlineData("", "", -1, ECharComparison.CaseSensitive, false)]
        [InlineData("a", "aa", -1, ECharComparison.CaseSensitive, false)]
        [InlineData("abcdef", "aa", -1, ECharComparison.CaseSensitive, false)]
        [InlineData("abcdef", "aa", 5, ECharComparison.CaseSensitive, false)]
        [InlineData("abcdef", "aa", 1, ECharComparison.CaseSensitive, false)]
        [InlineData("abcdef", "bc", 0, ECharComparison.CaseSensitive, false)]
        [InlineData("abcdef", "bc", 1, ECharComparison.CaseSensitive, true)]
        [InlineData("abcdef", "Bc", 1, ECharComparison.CurrentCultureIgnoreCase, true)]
        [InlineData("abcdef", "Bc", 1, ECharComparison.InvariantCultureIgnoreCase, true)]
        public void IsMatch_Comparison(string subject, string value, int index, ECharComparison comparison, bool expected)
            => new StringBuilder(subject).IsMatch(value, index, comparison).Should().Be(expected);

        [Theory]
        [InlineData(null, null, -1, null, false)]
        [InlineData(null, "", -1, null, false)]
        [InlineData("", null, -1, null, false)]
        [InlineData("", "", -1, null, false)]
        [InlineData("a", "aa", -1, null, false)]
        [InlineData("abcdef", "aa", -1, null, false)]
        [InlineData("abcdef", "aa", 5, null, false)]
        [InlineData("abcdef", "aa", 1, null, false)]
        [InlineData("abcdef", "bc", 0, null, false)]
        [InlineData("abcdef", "bc", 1, null, true)]
        [InlineData("abcdef", "Bc", 1, "current", true)]
        [InlineData("abcdef", "Bc", 1, "invariant", true)]
        public void IsMatch_Culture(string subject, string value, int index, string culture, bool expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).IsMatch(value, index, info).Should().Be(expected);
        }

        #endregion

        #region StartsWith Tests

        [Fact]
        public void StartsWith_Wrappers()
        {
            var b = new StringBuilder("aaabbbccc");
            var v = string.Empty;
            b.StartsWith(v).Should().Be(false);
            b.StartsWith(v, ECharComparison.CaseSensitive).Should().Be(false);
            b.StartsWith(v, null).Should().Be(false);
        }

        [Theory]
        [InlineData("", null, null, false)]
        [InlineData("aaabbbccc", null, null, false)]
        [InlineData("aaabbbccc", "", null, false)]
        [InlineData("aaabbbccc", "a", null, true)]
        [InlineData("aaabbbccc", "aa", null, true)]
        [InlineData("aaabbbccc", "aaa", null, true)]
        [InlineData("aaabbbccc", "aaaa", null, false)]
        public void StartsWith_Culture(string subject, string value, string culture, bool expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).StartsWith(value, info).Should().Be(expected);
        }

        #endregion

        #region StartsWithAny Tests

        [Fact]
        public void StartsWithAny_Wrappers()
        {
            var b = new StringBuilder("aaabbbccc");
            var v = new string[0];
            b.StartsWithAny(v).Should().Be(false);
            b.StartsWithAny(v, ECharComparison.CaseSensitive).Should().Be(false);
            b.StartsWithAny(v, null).Should().Be(false);
        }

        [Theory]
        [InlineData("", new string[0], null, false)]
        [InlineData("aaabbbccc", new string[0], null, false)]
        [InlineData("aaabbbccc", new[] { "a", "bbb", "ccc" }, null, true)]
        [InlineData("aaabbbccc", new[] { "aa", "bbb", "ccc" }, null, true)]
        [InlineData("aaabbbccc", new[] { "aaa", "bbb", "ccc" }, null, true)]
        [InlineData("aaabbbccc", new[] { "aaaa", "bbb", "ccc" }, null, false)]
        public void StartsWithAny_Culture(string subject, IEnumerable<string> values, string culture, bool expected)
        {
            CultureInfo info;
            if (culture == null) { info = null; }
            else if (culture == "current") { info = CultureInfo.CurrentCulture; }
            else if (culture == "invariant") { info = CultureInfo.InvariantCulture; }
            else { info = CultureInfo.CreateSpecificCulture(culture); }
            new StringBuilder(subject).StartsWithAny(values, info).Should().Be(expected);
        }

        #endregion

    }
}
