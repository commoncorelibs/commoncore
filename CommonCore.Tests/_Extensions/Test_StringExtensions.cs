﻿using System;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests._Extensions
{
    public class Test_StringExtensions
    {

        //[Theory]
        //[InlineData(null, null, -1, new int[] { })]
        //[InlineData("", null, -1, new int[] { })]
        //[InlineData("abc", null, -1, new int[] { })]
        //[InlineData("abc", "", -1, new int[] { })]
        //[InlineData("abc", "x", -1, new int[] { })]
        //[InlineData("abc", "abcd", -1, new int[] { })]
        //[InlineData("abc", "a", 0, new int[] { })]
        //[InlineData("abc", "a", -1, new[] { 0 })]
        //[InlineData("aabc", "a", -1, new[] { 0, 1 })]
        //[InlineData("aabca", "a", -1, new[] { 0, 1, 4 })]
        //[InlineData("abcabcababc", "ab", -1, new[] { 0, 3, 6, 8 })]
        //[InlineData("abcabcababc", "ab", 0, new int[] { })]
        //[InlineData("abcabcababc", "ab", 1, new[] { 0 })]
        //[InlineData("abcabcababc", "ab", 2, new[] { 0, 3 })]
        //[InlineData("abcabcababc", "ab", 3, new[] { 0, 3, 6 })]
        //[InlineData("abcabcababc", "ab", 4, new[] { 0, 3, 6, 8 })]
        //[InlineData("abcabcababc", "ab", 5, new[] { 0, 3, 6, 8 })]
        //public void GenMatchIndexSequence(string subject, string value, int max, int[] expected)
        //    => subject.GenMatchIndexSequence(value, max).Should().BeEquivalentTo(expected);

        #region Count Tests

        [Theory]
        [InlineData("", null, 0, "subject is empty")]
        [InlineData("abc", null, 0, "value is null")]
        [InlineData("abc", "", 0, "value is empty")]
        [InlineData("abc", "x", 0, "value is not found")]
        [InlineData("abc", "abcd", 0, "value is longer than subject")]
        [InlineData("abc", "a", 1)]
        [InlineData("aabc", "a", 2)]
        [InlineData("aabca", "a", 3)]
        [InlineData("abcabcababc", "ab", 4)]
        public void CountMatches_String(string subject, string value, int expected, string because = "")
            => subject.CountMatches(value).Should().Be(expected, because: because);

        [Theory]
        [InlineData("", null, -1, 0, "subject is empty")]
        [InlineData("abc", null, -1, 0, "value is null")]
        [InlineData("abc", "", -1, 0, "value is empty")]
        [InlineData("abc", "x", -1, 0, "value is not found")]
        [InlineData("abc", "abcd", -1, 0, "value is longer than subject")]
        [InlineData("abc", "a", 0, 0)]
        [InlineData("abc", "a", -1, 1)]
        [InlineData("aabc", "a", -1, 2)]
        [InlineData("aabca", "a", -1, 3)]
        [InlineData("abcabcababc", "ab", -1, 4)]
        [InlineData("abcabcababc", "ab", 0, 0)]
        [InlineData("abcabcababc", "ab", 1, 1)]
        [InlineData("abcabcababc", "ab", 2, 2)]
        [InlineData("abcabcababc", "ab", 3, 3)]
        [InlineData("abcabcababc", "ab", 4, 4)]
        [InlineData("abcabcababc", "ab", 5, 4)]
        public void CountMatches_Count_String(string subject, string value, int max, int expected, string because = "")
            => subject.CountMatches(value, max).Should().Be(expected, because: because);

        [Theory]
        [InlineData("", null, 0, "subject is empty")]
        [InlineData("abc", null, 0, "value is null")]
        [InlineData("abc", "", 0, "value is empty")]
        [InlineData("abc", "x", 0, "value is not found")]
        [InlineData("abc", "abcd", 0, "value is longer than subject")]
        [InlineData("abc", "a", 1)]
        [InlineData("aabc", "a", 2)]
        [InlineData("aabca", "a", 3)]
        [InlineData("abcabcababc", "ab", 4)]
        [InlineData("abc", "A", 1)]
        [InlineData("aabc", "A", 2)]
        [InlineData("aabca", "A", 3)]
        [InlineData("abcabcababc", "AB", 4)]
        public void CountMatches_Insensitive_String(string subject, string value, int expected, string because = "")
            => subject.CountMatches(value, StringComparison.CurrentCultureIgnoreCase).Should().Be(expected, because: because);

        [Theory]
        [InlineData("", null, -1, 0, "subject is empty")]
        [InlineData("abc", null, -1, 0, "value is null")]
        [InlineData("abc", "", -1, 0, "value is empty")]
        [InlineData("abc", "x", -1, 0, "value is not found")]
        [InlineData("abc", "abcd", -1, 0, "value is longer than subject")]
        [InlineData("abc", "a", 0, 0)]
        [InlineData("abc", "a", -1, 1)]
        [InlineData("aabc", "a", -1, 2)]
        [InlineData("aabca", "a", -1, 3)]
        [InlineData("abcabcababc", "ab", -1, 4)]
        [InlineData("abcabcababc", "ab", 0, 0)]
        [InlineData("abcabcababc", "ab", 1, 1)]
        [InlineData("abcabcababc", "ab", 2, 2)]
        [InlineData("abcabcababc", "ab", 3, 3)]
        [InlineData("abcabcababc", "ab", 4, 4)]
        [InlineData("abcabcababc", "ab", 5, 4)]
        [InlineData("abcabcababc", "AB", -1, 4)]
        [InlineData("abcabcababc", "AB", 0, 0)]
        [InlineData("abcabcababc", "AB", 1, 1)]
        [InlineData("abcabcababc", "AB", 2, 2)]
        [InlineData("abcabcababc", "AB", 3, 3)]
        [InlineData("abcabcababc", "AB", 4, 4)]
        [InlineData("abcabcababc", "AB", 5, 4)]
        public void CountMatches_Count_Insensitive_String(string subject, string value, int max, int expected, string because = "")
            => subject.CountMatches(value, max, StringComparison.CurrentCultureIgnoreCase).Should().Be(expected, because: because);

        //[Theory]
        //[InlineData(null, null, 0)]
        //[InlineData("", null, 0)]
        //[InlineData("abc", null, 0)]
        //[InlineData("abc", "", 0)]
        //[InlineData("abc", "x", 0)]
        //[InlineData("abc", "c", 1)]
        //[InlineData("abcc", "c", 2)]
        //[InlineData("abc", "bc", 1)]
        //[InlineData("bcabcbc", "bc", 2)]
        //public void CountEndMatches_String(string subject, string value, int expected)
        //    => subject.CountEndMatches(value).Should().Be(expected);

        //[Theory]
        //[InlineData(null, null, 0)]
        //[InlineData("", null, 0)]
        //[InlineData("abc", null, 0)]
        //[InlineData("abc", "", 0)]
        //[InlineData("abc", "x", 0)]
        //[InlineData("abc", "a", 1)]
        //[InlineData("aabc", "a", 2)]
        //[InlineData("abc", "ab", 1)]
        //[InlineData("ababcab", "ab", 2)]
        //public void CountStartMatches_String(string subject, string value, int expected)
        //    => subject.CountStartMatches(value).Should().Be(expected);

        #endregion

        #region Split Tests

        [Theory]
        [InlineData("a b", null)]
        [InlineData("a b", "")]
        [InlineData("a b", " ")]
        [InlineData("a b", "a")]
        [InlineData("a b", "b")]
        [InlineData("a b", "a", "b")]
        public void Split(string subject, params string[] args)
            => subject.Split(args).Should().BeEquivalentTo(subject.Split(args, StringSplitOptions.None));

        #endregion

    }
}
