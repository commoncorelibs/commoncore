﻿using System.Text;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests._Extensions
{
    public class Test_StringBuilderExtensions
    {

        #region AppendFile Methods

        //[Theory]
        //[InlineData("", "")]
        //[InlineData("The ", "")]
        //public void AppendFile(string subject, string path)
        //{
        //    var file = new FileInfo(path);
        //    var expected = subject + (file.Exists ? File.ReadAllText(file.FullName) : "") + System.Environment.NewLine;
        //    new StringBuilder(subject).AppendFile(file).ToString().Should().Be(expected);
        //}

        #endregion

        #region AppendFormatLine Methods

        [Theory]
        [InlineData("", "{0}st, {1}nd, and {2}rd.", 1, 2, 3)]
        [InlineData("The ", "{0}st, {1}nd, and {2}rd.", 1, 2, 3)]
        public void AppendFormatLine(string subject, string format, params object[] objs)
        {
            var expected = subject + string.Format(format, objs) + System.Environment.NewLine;
            new StringBuilder(subject).AppendFormatLine(format, objs).ToString().Should().Be(expected);
        }

        #endregion

        #region AppendLine Methods

        private class TestAppendLine { public override string ToString() => nameof(TestAppendLine); }

        [Theory]
        [InlineData("")]
        [InlineData("a")]
        public void AppendLine(string subject)
        {
            object obj = new TestAppendLine();
            var expected = subject + obj.ToString() + System.Environment.NewLine;
            new StringBuilder(subject).AppendLine(obj).ToString().Should().Be(expected);

            expected = subject + System.Environment.NewLine;
            new StringBuilder(subject).AppendLine((object)null).ToString().Should().Be(expected);
        }

        #endregion

        #region CountMatches Tests

        [Theory]
        [InlineData("", null, -1, 0, "subject is empty")]
        [InlineData("abc", null, -1, 0, "value is null")]
        [InlineData("abc", "", -1, 0, "value is empty")]
        [InlineData("abc", "x", -1, 0, "value is not found")]
        [InlineData("abc", "abcd", -1, 0, "value is longer than subject")]
        [InlineData("abc", "a", 0, 0)]
        [InlineData("abc", "a", -1, 1)]
        [InlineData("aabc", "a", -1, 2)]
        [InlineData("aabca", "a", -1, 3)]
        [InlineData("abcabcababc", "ab", -1, 4)]
        [InlineData("abcabcababc", "ab", 0, 0)]
        [InlineData("abcabcababc", "ab", 1, 1)]
        [InlineData("abcabcababc", "ab", 2, 2)]
        [InlineData("abcabcababc", "ab", 3, 3)]
        [InlineData("abcabcababc", "ab", 4, 4)]
        [InlineData("abcabcababc", "ab", 5, 4)]
        public void CountMatches_String(string subject, string value, int maxCount, int expected, string because = "")
            => new StringBuilder(subject).CountMatches(value, maxCount, ECharComparison.CaseSensitive).Should().Be(expected, because: because);

        [Theory]
        [InlineData("", null, -1, 0, "subject is empty")]
        [InlineData("abc", null, -1, 0, "value is null")]
        [InlineData("abc", "", -1, 0, "value is empty")]
        [InlineData("abc", "x", -1, 0, "value is not found")]
        [InlineData("abc", "abcd", -1, 0, "value is longer than subject")]
        [InlineData("abc", "a", 0, 0, "maxCount is zero")]
        [InlineData("abc", "a", -1, 1)]
        [InlineData("aabc", "a", -1, 2)]
        [InlineData("aabca", "a", -1, 3)]
        [InlineData("abcabcababc", "ab", -1, 4)]
        [InlineData("abcabcababc", "ab", 0, 0)]
        [InlineData("abcabcababc", "ab", 1, 1)]
        [InlineData("abcabcababc", "ab", 2, 2)]
        [InlineData("abcabcababc", "ab", 3, 3)]
        [InlineData("abcabcababc", "ab", 4, 4)]
        [InlineData("abcabcababc", "ab", 5, 4)]
        [InlineData("abcabcababc", "AB", -1, 4)]
        [InlineData("abcabcababc", "AB", 0, 0)]
        [InlineData("abcabcababc", "AB", 1, 1)]
        [InlineData("abcabcababc", "AB", 2, 2)]
        [InlineData("abcabcababc", "AB", 3, 3)]
        [InlineData("abcabcababc", "AB", 4, 4)]
        [InlineData("abcabcababc", "AB", 5, 4)]
        public void CountMatches_String_Insensitive(string subject, string value, int maxCount, int expected, string because = "")
            => new StringBuilder(subject).CountMatches(value, maxCount, ECharComparison.InvariantCultureIgnoreCase).Should().Be(expected, because: because);

        #endregion

        #region CountEndMatches Tests

        [Theory]
        [InlineData("", null, -1, 0, "subject is empty")]
        [InlineData("abc", null, -1, 0, "value is null")]
        [InlineData("abc", "", -1, 0, "value is empty")]
        [InlineData("abc", "x", -1, 0, "value is not found")]
        [InlineData("abc", "b", -1, 0, "value is not found at subject end")]
        [InlineData("abc", "abcd", -1, 0, "value is longer than subject")]
        [InlineData("abc", "c", 0, 0, "maxCount is zero")]

        [InlineData("abababcdcdcd", "cd", -1, 3)]
        [InlineData("abababcdcdcd", "cd", 1, 1, "maxCount limits to one")]
        public void CountEndMatches_String(string subject, string value, int maxCount, int expected, string because = "")
            => new StringBuilder(subject).CountEndMatches(value, maxCount).Should().Be(expected, because: because);

        #endregion

        #region CountStartMatches Tests

        [Theory]
        [InlineData("", null, -1, 0, "subject is empty")]
        [InlineData("abc", null, -1, 0, "value is null")]
        [InlineData("abc", "", -1, 0, "value is empty")]
        [InlineData("abc", "x", -1, 0, "value is not found")]
        [InlineData("abc", "b", -1, 0, "value is not found at subject start")]
        [InlineData("abc", "abcd", -1, 0, "value is longer than subject")]
        [InlineData("abc", "a", 0, 0, "maxCount is zero")]

        [InlineData("abababcdcdcd", "ab", -1, 3)]
        [InlineData("abababcdcdcd", "ab", 1, 1, "maxCount limits to one")]
        public void CountStartMatches_String(string subject, string value, int maxCount, int expected, string because = "")
            => new StringBuilder(subject).CountStartMatches(value, maxCount).Should().Be(expected, because: because);

        #endregion

    }
}
