﻿using System.Collections;
using System.Collections.Generic;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests._Extensions
{
    public class Test_CollectionExtensions
    {

        #region Contains Tests

        public const string SharedString = "test";

        [Theory]
        [InlineData(new string[] { }, null, false)]
        [InlineData(new string[] { }, "", false)]
        [InlineData(new[] { SharedString }, SharedString, true)]
        [InlineData(new[] { "a" }, "a", true)]
        [InlineData(new[] { "a" }, "A", true)]
        [InlineData(new[] { "soMe tExt" }, "SomE TexT", true)]
        [InlineData(new[] { "test", null, "" }, null, true)]
        [InlineData(new[] { "test", null, "" }, "", true)]
        [InlineData(new[] { "text value" }, "other value", false)]
        [InlineData(new[] { "test" }, null, false)]
        public void ContainsIgnoreCase(string[] subject, string value, bool expected)
            => subject.ContainsIgnoreCase(value).Should().Be(expected);

        #endregion

        #region IsEmpty Tests

        [Theory]
        [InlineData(null, true)]
        [InlineData(new object[0], true)]
        [InlineData(new object[] { "" }, false)]
        public void IsEmpty_ICollection(ICollection subject, bool expected)
        {
            subject.IsEmpty().Should().Be(expected);
            subject.IsNotEmpty().Should().Be(!expected);
        }

        [Theory]
        [InlineData(null, true)]
        [InlineData(new object[0], true)]
        [InlineData(new object[] { "" }, false)]
        public void IsEmpty_IEnumerable(IEnumerable subject, bool expected)
        {
            subject.IsEmpty().Should().Be(expected);
            subject.IsNotEmpty().Should().Be(!expected);
        }

        #endregion

        #region Join Tests

        private static string Converter(object item) => item.ToString().ToUpper();

        [Theory]
        [InlineData(new string[] { }, ", ", "")]
        [InlineData(new[] { "apples" }, ", ", "apples")]
        [InlineData(new[] { "apples", "bison" }, ", ", "apples, bison")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", "apples, bison, carrots")]
        public void Join_IEnumerable1(IEnumerable subject, string separator, string expected)
        {
            subject.Join(separator).Should().Be(expected);
            subject.Join(separator, Converter).Should().Be(expected.ToUpper());
        }

        [Theory]
        [InlineData(new string[] { }, ", ", 0, "")]
        [InlineData(new[] { "apples" }, ", ", 0, "apples")]
        [InlineData(new[] { "apples", "bison" }, ", ", 0, "apples, bison")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 0, "apples, bison, carrots")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 1, "bison, carrots")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 2, "carrots")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 3, "")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 4, "")]
        public void Join_IEnumerable2(IEnumerable subject, string separator, int startIndex, string expected)
        {
            subject.Join(separator, startIndex).Should().Be(expected);
            subject.Join(separator, startIndex, Converter).Should().Be(expected.ToUpper());
        }

        [Theory]
        [InlineData(new string[] { }, ", ", 0, -1, "")]
        [InlineData(new[] { "apples" }, ", ", 0, -1, "apples")]
        [InlineData(new[] { "apples", "bison" }, ", ", 0, -1, "apples, bison")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 0, -1, "apples, bison, carrots")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 1, -1, "bison, carrots")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 2, -1, "carrots")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 3, -1, "")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 4, -1, "")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 0, 0, "")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 0, 1, "apples")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 0, 2, "apples, bison")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 0, 3, "apples, bison, carrots")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 0, 4, "apples, bison, carrots")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 1, 1, "bison")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 1, 2, "bison, carrots")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 1, 3, "bison, carrots")]
        public void Join_IEnumerable3(IEnumerable subject, string separator, int startIndex, int maxCount, string expected)
        {
            subject.Join(separator, startIndex, maxCount).Should().Be(expected);
            subject.Join(separator, startIndex, maxCount, Converter).Should().Be(expected.ToUpper());
        }

        [Theory]
        [InlineData(new string[] { }, ", ", "")]
        [InlineData(new[] { "apples" }, ", ", "apples")]
        [InlineData(new[] { "apples", "bison" }, ", ", "apples, bison")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", "apples, bison, carrots")]
        public void Join_IEnumerable_Generic1(object[] subject, string separator, string expected)
        {
            subject.Join(separator).Should().Be(expected);
            subject.Join(separator, Converter).Should().Be(expected.ToUpper());
        }

        [Theory]
        [InlineData(new string[] { }, ", ", 0, "")]
        [InlineData(new[] { "apples" }, ", ", 0, "apples")]
        [InlineData(new[] { "apples", "bison" }, ", ", 0, "apples, bison")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 0, "apples, bison, carrots")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 1, "bison, carrots")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 2, "carrots")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 3, "")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 4, "")]
        public void Join_IEnumerable_Generic2(object[] subject, string separator, int startIndex, string expected)
        {
            subject.Join(separator, startIndex).Should().Be(expected);
            subject.Join(separator, startIndex, Converter).Should().Be(expected.ToUpper());
        }

        [Theory]
        [InlineData(new string[] { }, ", ", 0, -1, "")]
        [InlineData(new[] { "apples" }, ", ", 0, -1, "apples")]
        [InlineData(new[] { "apples", "bison" }, ", ", 0, -1, "apples, bison")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 0, -1, "apples, bison, carrots")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 1, -1, "bison, carrots")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 2, -1, "carrots")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 3, -1, "")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 4, -1, "")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 0, 0, "")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 0, 1, "apples")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 0, 2, "apples, bison")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 0, 3, "apples, bison, carrots")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 0, 4, "apples, bison, carrots")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 1, 1, "bison")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 1, 2, "bison, carrots")]
        [InlineData(new[] { "apples", "bison", "carrots" }, ", ", 1, 3, "bison, carrots")]
        public void Join_IEnumerable_Generic3(object[] subject, string separator, int startIndex, int maxCount, string expected)
        {
            subject.Join(separator, startIndex, maxCount).Should().Be(expected);
            subject.Join(separator, startIndex, maxCount, Converter).Should().Be(expected.ToUpper());
        }

        #endregion

        #region Subset Tests

        [Theory]
        [InlineData(new int[0], 0, -1, new int[0])]
        [InlineData(new[] { 0, 1, 2, 3, 4, 5 }, 0, 0, new int[0])]
        [InlineData(new[] { 0, 1, 2, 3, 4, 5 }, 6, -1, new int[0])]
        [InlineData(new[] { 0, 1, 2, 3, 4, 5 }, 1, 4, new[] { 1, 2, 3, 4 })]
        public void Subset_IEnumerable(IEnumerable subject, int startIndex, int maxCount, IEnumerable expected)
            => subject.Subset(startIndex, maxCount).Should().BeEquivalentTo(expected);

        [Theory]
        [InlineData(new int[0], 0, -1, new int[0])]
        [InlineData(new[] { 0, 1, 2, 3, 4, 5 }, 0, 0, new int[0])]
        [InlineData(new[] { 0, 1, 2, 3, 4, 5 }, 6, -1, new int[0])]
        [InlineData(new[] { 0, 1, 2, 3, 4, 5 }, 1, 4, new[] { 1, 2, 3, 4 })]
        public void Subset_IEnumerable_Generic(IEnumerable<int> subject, int startIndex, int maxCount, IEnumerable<int> expected)
            => subject.Subset(startIndex, maxCount).Should().BeEquivalentTo(expected);

        #endregion

        #region ToListString Tests

        [Theory]
        [InlineData(new string[] { }, "")]
        [InlineData(new[] { "apples" }, "apples")]
        [InlineData(new[] { "apples", "bison" }, "apples and bison")]
        [InlineData(new[] { "apples", "bison", "carrots" }, "apples, bison, and carrots")]
        public void ToListString_IList(string[] subject, string expected)
        {
            ((IList<object>)subject).ToListString().Should().Be(expected);
            ((IList<string>)subject).ToListString().Should().Be(expected);
            ((object[])subject).ToListString().Should().Be(expected);
            ((string[])subject).ToListString().Should().Be(expected);
        }

        [Theory]
        [InlineData(new string[] { }, "")]
        [InlineData(new[] { "apples" }, "apples")]
        [InlineData(new[] { "apples", "bison" }, "apples and bison")]
        [InlineData(new[] { "apples", "bison", "carrots" }, "apples, bison, and carrots")]
        public void ToListString_ICollection(string[] subject, string expected)
        {
            ((ICollection<object>)subject).ToListString().Should().Be(expected);
            ((ICollection<string>)subject).ToListString().Should().Be(expected);
        }

        [Theory]
        [InlineData(new string[] { }, "")]
        [InlineData(new[] { "apples" }, "apples")]
        [InlineData(new[] { "apples", "bison" }, "apples and bison")]
        [InlineData(new[] { "apples", "bison", "carrots" }, "apples, bison, and carrots")]
        public void ToListString_IEnumerable(string[] subject, string expected)
        {
            ((IEnumerable<object>)subject).ToListString().Should().Be(expected);
            ((IEnumerable<string>)subject).ToListString().Should().Be(expected);
            //((Array)subject).ToListString().Should().Be(expected);
        }

        #endregion

    }
}
