﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests._Objects
{
    [ExcludeFromCodeCoverage]
    public class Test_AObservableObject
    {
        [Fact]
        public void RaisePropertyChanged()
        {
            TestObservableObject subject;

            subject = new TestObservableObject();
            using (var monitor = subject.Monitor())
            {
                subject.Raise_Explicit();
                monitor.Should().Raise("PropertyChanged")
                    .WithSender(monitor.Subject)
                    .WithArgs<PropertyChangedEventArgs>(args => args.PropertyName == "Raise_Explicit");
            }

            subject = new TestObservableObject();
            using (var monitor = subject.Monitor())
            {
                subject.Raise_Implicit();
                monitor.Should().Raise("PropertyChanged")
                    .WithSender(monitor.Subject)
                    .WithArgs<PropertyChangedEventArgs>(args => args.PropertyName == "Raise_Implicit");
            }
        }

        [Fact]
        public void RaisePropertyChanged_Value()
        {
            TestObservableObject subject;

            subject = new TestObservableObject();
            using (var monitor = subject.Monitor())
            {
                subject.PropertyValue.Should().BeNull();
                subject.Raise_Explicit(new object());
                subject.PropertyValue.Should().NotBeNull();
                monitor.Should().Raise("PropertyChanged")
                    .WithSender(monitor.Subject)
                    .WithArgs<PropertyChangedEventArgs>(args => args.PropertyName == "Raise_Explicit");
            }

            subject = new TestObservableObject();
            using (var monitor = subject.Monitor())
            {
                subject.PropertyValue.Should().BeNull();
                subject.Raise_Implicit(new object());
                subject.PropertyValue.Should().NotBeNull();
                monitor.Should().Raise("PropertyChanged")
                    .WithSender(monitor.Subject)
                    .WithArgs<PropertyChangedEventArgs>(args => args.PropertyName == "Raise_Implicit");
            }
        }

        [Fact]
        public void RaiseIfPropertyChanged()
        {
            TestObservableObject subject;

            subject = new TestObservableObject();
            using (var monitor = subject.Monitor())
            {
                subject.PropertyValue.Should().BeNull();
                subject.RaiseIf_Explicit(new object());
                subject.PropertyValue.Should().NotBeNull();
                monitor.Should().Raise("PropertyChanged")
                    .WithSender(monitor.Subject)
                    .WithArgs<PropertyChangedEventArgs>(args => args.PropertyName == "RaiseIf_Explicit");
            }

            subject = new TestObservableObject();
            using (var monitor = subject.Monitor())
            {
                subject.PropertyValue.Should().BeNull();
                subject.RaiseIf_Implicit(new object());
                subject.PropertyValue.Should().NotBeNull();
                monitor.Should().Raise("PropertyChanged")
                    .WithSender(monitor.Subject)
                    .WithArgs<PropertyChangedEventArgs>(args => args.PropertyName == "RaiseIf_Implicit");
            }

            subject = new TestObservableObject();
            using (var monitor = subject.Monitor())
            {
                subject.PropertyValue.Should().BeNull();
                subject.RaiseIf_Explicit(null);
                subject.PropertyValue.Should().BeNull();
                monitor.Should().NotRaise("PropertyChanged");
            }

            subject = new TestObservableObject();
            using (var monitor = subject.Monitor())
            {
                subject.PropertyValue.Should().BeNull();
                subject.RaiseIf_Implicit(null);
                subject.PropertyValue.Should().BeNull();
                monitor.Should().NotRaise("PropertyChanged");
            }
        }

        [Fact]
        public void Test_SubscribePropertyChangedEvent()
        {
            Action act;
            TestObservableObject subject;

            subject = new TestObservableObject();
            act = () => subject.SubscribePropertyChangedEvent(true, this.Test_Event);
            act.Should().NotThrow();
            act = () => subject.SubscribePropertyChangedEvent(false, this.Test_Event);
            act.Should().NotThrow();
        }

        private void Test_Event(object sender, PropertyChangedEventArgs e)
        {
        }

        private class TestObservableObject : AObservableObject
        {
            public void Raise_Explicit() => this.RaisePropertyChanged("Raise_Explicit");

            public void Raise_Implicit() => this.RaisePropertyChanged();

            public void Raise_Explicit(object value) => this.RaisePropertyChanged(ref this.PropertyValue, value, "Raise_Explicit");

            public void Raise_Implicit(object value) => this.RaisePropertyChanged(ref this.PropertyValue, value);

            public void RaiseIf_Explicit(object value) => this.RaiseIfPropertyChanged(ref this.PropertyValue, value, "RaiseIf_Explicit");

            public void RaiseIf_Implicit(object value) => this.RaiseIfPropertyChanged(ref this.PropertyValue, value);

            public object PropertyValue = null;
        }

        [Fact, Obsolete]
        public void OLD_PropertyChanged_Raised()
        {
            OLD_TestObservableObject subject;

            subject = new OLD_TestObservableObject();
            using (var monitor = subject.Monitor())
            {
                subject.RaisePropertyChanged_Explicit();
                monitor.Should().Raise("PropertyChanged").WithSender(monitor.Subject)
                .WithArgs<PropertyChangedEventArgs>(args => args.PropertyName == "PropertyChangedTest");
            }

            subject = new OLD_TestObservableObject();
            using (var monitor = subject.Monitor())
            {
                subject.RaisePropertyChanged_Implicit();
                monitor.Should().Raise("PropertyChanged").WithSender(monitor.Subject)
                .WithArgs<PropertyChangedEventArgs>(args => args.PropertyName == "PropertyChangedTest");
            }

            subject = new OLD_TestObservableObject();
            using (var monitor = subject.Monitor())
            {
                subject.PropertyValue.Should().BeNull();
                subject.RaisePropertyChanged_Explicit(new object());
                subject.PropertyValue.Should().NotBeNull();
                monitor.Should().Raise("PropertyChanged").WithSender(monitor.Subject)
                .WithArgs<PropertyChangedEventArgs>(args => args.PropertyName == "PropertyChangedTest");
            }

            subject = new OLD_TestObservableObject();
            using (var monitor = subject.Monitor())
            {
                subject.PropertyValue.Should().BeNull();
                subject.RaisePropertyChanged_Implicit(new object());
                subject.PropertyValue.Should().NotBeNull();
                monitor.Should().Raise("PropertyChanged").WithSender(monitor.Subject)
                .WithArgs<PropertyChangedEventArgs>(args => args.PropertyName == "PropertyChangedTest");
            }
        }

        [Fact, Obsolete]
        public void OLD_PropertyChanged_NotRaised()
        {
            OLD_TestObservableObject subject;

            subject = new OLD_TestObservableObject();
            using (var monitor = subject.Monitor())
            {
                subject.PropertyValue.Should().BeNull();
                subject.RaisePropertyChanged_Explicit(null);
                subject.PropertyValue.Should().BeNull();
                monitor.Should().NotRaise("PropertyChanged");
            }

            subject = new OLD_TestObservableObject();
            using (var monitor = subject.Monitor())
            {
                subject.PropertyValue.Should().BeNull();
                subject.RaisePropertyChanged_Implicit(null);
                subject.PropertyValue.Should().BeNull();
                monitor.Should().NotRaise("PropertyChanged");
            }
        }

        [Obsolete]
        private class OLD_TestObservableObject : AObservableObject
        {
            public void RaisePropertyChanged_Explicit() => this.OnPropertyChanged("PropertyChangedTest");

            public void RaisePropertyChanged_Implicit() => this.PropertyChangedTest();

            public void RaisePropertyChanged_Explicit(object value) => this.OnPropertyChanged(ref this.PropertyValue, value, "PropertyChangedTest");

            public void RaisePropertyChanged_Implicit(object value) => this.PropertyChangedTest(value);

            private void PropertyChangedTest() => this.OnPropertyChanged();

            private void PropertyChangedTest(object value) => this.OnPropertyChanged(ref this.PropertyValue, value);
            public object PropertyValue = null;
        }
    }
}
