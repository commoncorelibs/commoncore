﻿#region Copyright (c) 2019 Jay Jeckel
// Copyright (c) 2019 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests._Objects
{
    [ExcludeFromCodeCoverage]
    public class Test_AObject
    {
        [Fact]
        public void ImplicitBool()
        {
            TestObject subject;
            bool exists;

            subject = new TestObject();
            exists = subject;
            exists.Should().BeTrue();

            subject = null;
            exists = subject;
            exists.Should().BeFalse();
        }

        private class TestObject : AObject { }
    }
}
