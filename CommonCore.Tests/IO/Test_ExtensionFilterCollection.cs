﻿using System.Collections.Generic;
using CommonCore.IO;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests.IO
{
    public class Test_ExtensionFilterCollection
    {
        [Fact]
        public void Constructors()
        {
            var testFilter = new ExtensionFilter("Test Files", new[] { "abc", "xyz", "ext" });
            var filters = new[] { testFilter, ExtensionFilter.Empty, ExtensionFilter.All };
            var expected = testFilter.Filter + "|" + ExtensionFilter.All.Filter;

            ExtensionFilterCollection subject;

            subject = new ExtensionFilterCollection();
            subject.ToFilter().Should().BeEmpty();
            subject.ToString().Should().BeEmpty();

            subject = new ExtensionFilterCollection((IList<ExtensionFilter>)filters);
            subject.ToFilter().Should().Be(expected);
            subject.ToString().Should().Be(expected);

            subject = new ExtensionFilterCollection((IEnumerable<ExtensionFilter>)filters);
            subject.ToFilter().Should().Be(expected);
            subject.ToString().Should().Be(expected);
        }
    }
}
