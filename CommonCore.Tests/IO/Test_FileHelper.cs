﻿using CommonCore.IO;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests.IO
{
    public class Test_FileHelper
    {
        [Theory]
        [InlineData(0, "0 B")]
        [InlineData(27, "27 B")]
        [InlineData(999, "999 B")]
        [InlineData(1000, "1000 B")]
        [InlineData(1023, "1023 B")]
        [InlineData(1024, "1 KB")]
        [InlineData(1728, "1.7 KB")]
        [InlineData(1855425871872, "1.7 TB")]
        [InlineData(long.MaxValue, "8 EB")]
        public void FormatFileSize(long bytes, string expected)
            => FileHelper.FormatFileSize(bytes, true, "{0:0.#} {1}").Should().Be(expected);
    }
}
