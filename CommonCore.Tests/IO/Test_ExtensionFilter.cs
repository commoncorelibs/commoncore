﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CommonCore.IO;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests.IO
{
    public class Test_ExtensionFilter
    {
        [Theory]
        [InlineData("No Files", new string[0], "")]
        [InlineData("Test Files", new[] { "abc", "xyz", "ext" }, "Test Files (*.abc;*.xyz;*.ext)|*.abc;*.xyz;*.ext")]
        public void Constructors(string title, IEnumerable<string> extensions, string expected)
        {
            ExtensionFilter subject;

            subject = new ExtensionFilter(title, extensions.ToArray());
            subject.Title.Should().Be(title);
            subject.Extensions.Should().NotBeNull().And.HaveSameCount(extensions);
            subject.Filter.Should().Be(expected);
            subject.ToString().Should().Be(expected);

            subject = new ExtensionFilter(title, extensions);
            subject.Title.Should().Be(title);
            subject.Extensions.Should().NotBeNull().And.HaveSameCount(extensions);
            subject.Filter.Should().Be(expected);
            subject.ToString().Should().Be(expected);
        }

        [Fact]
        public void Constants()
        {
            ExtensionFilter subject;

            subject = ExtensionFilter.Empty;
            subject.Title.Should().BeNull();
            subject.Extensions.Should().BeNull();
            subject.Filter.Should().BeNull();
            subject.ToString().Should().BeNull();
            subject.GetHashCode().Should().Be(0);

            subject = ExtensionFilter.All;
            subject.Title.Should().Be("All Files");
            subject.Extensions.Should().BeEquivalentTo(new[] { "*" });
            subject.Filter.Should().Be("All Files (*.*)|*.*");
            subject.ToString().Should().Be("All Files (*.*)|*.*");
            subject.GetHashCode().Should().NotBe(0);
        }

        [Fact]
        public void Comparisons()
        {
            ExtensionFilter.Equals(ExtensionFilter.All, ExtensionFilter.All).Should().BeTrue();
            ExtensionFilter.Equals(ExtensionFilter.Empty, ExtensionFilter.Empty).Should().BeTrue();
            ExtensionFilter.Equals(ExtensionFilter.Empty, ExtensionFilter.All).Should().BeFalse();

            ExtensionFilter.All.Equals(ExtensionFilter.All).Should().BeTrue();
            ExtensionFilter.Empty.Equals(ExtensionFilter.Empty).Should().BeTrue();
            ExtensionFilter.Empty.Equals(ExtensionFilter.All).Should().BeFalse();

            ExtensionFilter.All.Equals((object)ExtensionFilter.All).Should().BeTrue();
            ExtensionFilter.Empty.Equals((object)ExtensionFilter.Empty).Should().BeTrue();
            ExtensionFilter.Empty.Equals((object)ExtensionFilter.All).Should().BeFalse();
            ExtensionFilter.All.Equals((object)"some string").Should().BeFalse();

            (ExtensionFilter.All == ExtensionFilter.All).Should().BeTrue();
            (ExtensionFilter.Empty == ExtensionFilter.Empty).Should().BeTrue();
            (ExtensionFilter.Empty == ExtensionFilter.All).Should().BeFalse();

            (ExtensionFilter.All != ExtensionFilter.All).Should().BeFalse();
            (ExtensionFilter.Empty != ExtensionFilter.Empty).Should().BeFalse();
            (ExtensionFilter.Empty != ExtensionFilter.All).Should().BeTrue();
        }

        [Theory]
        [InlineData("No Files", new string[0])]
        [InlineData("Test Files", new[] { "abc", "xyz", "ext" })]
        public void GetEnumerator(string title, IEnumerable<string> extensions)
        {
            var subject = new ExtensionFilter(title, extensions);
            ((IEnumerable<string>)subject).Should().BeEquivalentTo(extensions);
            ((IEnumerable<string>) subject).Should().HaveSameCount(extensions);
            ((IEnumerable)subject).GetEnumerator().Should().NotBeNull();
        }

        [Fact]
        public void Concat()
        {
            var testFilter = new ExtensionFilter("Test Files", new[] { "abc", "xyz", "ext" });
            var expected = testFilter.Filter + "|" + ExtensionFilter.All.Filter;
            var filters = new[] { testFilter, ExtensionFilter.All };
            ExtensionFilter.Concat(filters).Should().Be(expected);
            ExtensionFilter.Concat((IEnumerable<ExtensionFilter>)filters).Should().Be(expected);

            ExtensionFilter.Concat(testFilter, ExtensionFilter.Empty, ExtensionFilter.All).Should().Be(expected);
        }
    }
}
