﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using CommonCore.Numerics;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests.Numerics
{
    [ExcludeFromCodeCoverage]
    public class Test_FibonacciEngine
    {
        [Fact]
        public void Sequence()
        {
            var engine = new FibonacciEngine();
            foreach (var fibonacci in engine.Take(Stored.Length))
            { fibonacci.Should().Be(Stored[(int)engine.Index]); }
        }

        [Fact]
        public void Stamps()
        {
            var engine = new FibonacciEngine();

            // Throw wrong section count.
            Action act = () => engine.LoadStamp(string.Join(':', new int[engine.StampDetails.count + 1]));
            act.Should().Throw<FormatException>();

            // Ensure index is clamped to >= 0.
            string stamp = engine.ToStamp();
            engine.LoadStamp("-1:0:0");
            stamp.Should().Be(engine.ToStamp());

            int halfCount = Stored.Length / 2;
            foreach (var value in engine.Take(halfCount)) { }
            stamp = engine.ToStamp();
            foreach (var value in engine.Reset().LoadStamp(stamp).Take(Stored.Length - halfCount))
            { value.Should().Be(Stored[(int)engine.Index]); }
        }

        internal static readonly long[] Stored
            = { 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765 };
    }
}
