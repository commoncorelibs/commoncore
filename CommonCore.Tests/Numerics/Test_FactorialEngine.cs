﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using CommonCore.Numerics;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests.Numerics
{
    [ExcludeFromCodeCoverage]
    public class Test_FactorialEngine
    {
        [Fact]
        public void Sequence()
        {
            var engine = new FactorialEngine();
            foreach (var factorial in engine.Take(Stored.Length))
            { factorial.Should().Be(Stored[(int)engine.Index]); }
        }

        [Fact]
        public void Stamps()
        {
            var engine = new FactorialEngine();

            // Throw wrong section count.
            Action act = () => engine.LoadStamp(string.Join(':', new int[engine.StampDetails.count + 1]));
            act.Should().Throw<FormatException>();

            // Ensure index is clamped to >= 0.
            string stamp = engine.ToStamp();
            engine.LoadStamp("-1:1");
            stamp.Should().Be(engine.ToStamp());

            int halfCount = Stored.Length / 2;
            foreach (var value in engine.Take(halfCount)) { }
            stamp = engine.ToStamp();
            foreach (var value in engine.Reset().LoadStamp(stamp).Take(Stored.Length - halfCount))
            { value.Should().Be(Stored[(int)engine.Index]); }
        }

        internal static readonly long[] Stored
            = new[] { 1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800, 39916800, 479001600, 6227020800, 87178291200, 1307674368000, 20922789888000, 355687428096000, 6402373705728000, 121645100408832000, 2432902008176640000 };
    }
}
