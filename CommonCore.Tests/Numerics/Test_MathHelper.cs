﻿using System.Diagnostics.CodeAnalysis;
using System.Linq;
using CommonCore.Numerics;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests.Numerics
{
    [ExcludeFromCodeCoverage]
    public class Test_MathHelper
    {
        [Theory]
        [InlineData(0, 5, 0)]
        [InlineData(10, 0, 0)]
        [InlineData(10, 5, 2)]
        [InlineData(2, 4, 0.5)]
        public void CalRatio(double left, double right, double expected)
            => MathHelper.CalRatio(left, right).Should().Be(expected);

        [Theory]
        [InlineData(0, 5, 0)]
        [InlineData(10, 0, 0)]
        [InlineData(10, 5, 200)]
        [InlineData(2, 4, 50)]
        public void CalPercent(double left, double right, double expected)
            => MathHelper.CalPercent(left, right).Should().Be(expected);

        [Fact]
        public void CalTriangularSum()
        {
            int index = 0;
            var engine = new TriangularEngine();
            foreach (var value in engine.Take(Test_TriangularEngine.Stored.Length))
            {
                var expected = Test_TriangularEngine.Stored[index];
                value.Should().Be(expected);
                MathHelper.CalTriangularSum(index).Should().Be(expected);
                MathHelper.CalTriangularSum(engine.Index).Should().Be(expected);
                index++;
            }
        }

        [Fact]
        public void IsPrime()
        {
            var primes = new[] { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97 };
            foreach (var prime in primes)
            { MathHelper.IsPrime(prime).Should().BeTrue(); }

            var notprimes = new[] { 0, 1, 4, 6, 8, 9, 10, 12, 14, 15, 16, 18, 20, 21, 22, 24, 25, 26, 27, 28, 30, 32, 33, 34, 35, 36, 38, 39, 40, 42, 44, 45, 46, 48, 49, 50, 51, 52, 54, 55, 56, 57, 58, 60, 62, 63, 64, 65, 66, 68, 69, 70, 72, 74, 75, 76, 77, 78, 80, 81, 82, 84, 85, 86, 87, 88, 90, 91, 92, 93, 94, 95, 96, 98, 99 };
            foreach (var notprime in notprimes)
            { MathHelper.IsPrime(notprime).Should().BeFalse(); }
        }

        [Theory]
        [InlineData(-1, 0)]
        [InlineData(0, 2)]
        [InlineData(1, 3)]
        [InlineData(2, 5)]
        [InlineData(3, 7)]
        [InlineData(4, 11)]
        [InlineData(1000, 8850)]
        [InlineData(8000, 81967)]
        public void CalApproximatePrime(int index, int expected)
            => MathHelper.CalApproximatePrime(index).Should().Be(expected);

        [Fact]
        public void FibonacciSequence()
        {
            int index = 0;
            foreach (var value in MathHelper.FibonacciSequence().Take(Test_FibonacciEngine.Stored.Length))
            { value.Should().Be(Test_FibonacciEngine.Stored[index++]); }
        }

        [Fact]
        public void FactorialSequence()
        {
            int index = 0;
            foreach (var value in MathHelper.FactorialSequence().Take(Test_FactorialEngine.Stored.Length))
            { value.Should().Be(Test_FactorialEngine.Stored[index++]); }
        }

        [Fact]
        public void PiSequence()
        {
            int index = 0;
            foreach (var value in MathHelper.PiSequence().Take(Test_PiEngine.Stored.Length))
            { value.Should().Be(Test_PiEngine.Stored[index++]); }
        }

        [Fact]
        public void TriangularSequence()
        {
            int index = 0;
            foreach (var value in MathHelper.TriangularSequence().Take(Test_TriangularEngine.Stored.Length))
            { value.Should().Be(Test_TriangularEngine.Stored[index++]); }
        }

        [Fact]
        public void PrimeSequence_Naive()
        {
            int index = 0;
            foreach (var value in MathHelper.PrimeSequence(EPrimeSieveType.Naive).Take(Test_PrimeEngine.Stored.Length))
            { value.Should().Be(Test_PrimeEngine.Stored[index++]); }
        }

        [Fact]
        public void PrimeSequence_Sundaram()
        {
            int index = 0;
            foreach (var value in MathHelper.PrimeSequence(EPrimeSieveType.Sundaram, Test_PrimeEngine.Stored.Length).Take(Test_PrimeEngine.Stored.Length))
            { value.Should().Be(Test_PrimeEngine.Stored[index++]); }
        }

        [Fact]
        public void PrimeSequence_Eratosthenes()
        {
            int index = 0;
            foreach (var value in MathHelper.PrimeSequence(EPrimeSieveType.Eratosthenes, Test_PrimeEngine.Stored.Length).Take(Test_PrimeEngine.Stored.Length))
            { value.Should().Be(Test_PrimeEngine.Stored[index++]); }
        }
    }
}
