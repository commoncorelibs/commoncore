﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using CommonCore.Numerics;
using FluentAssertions;
using Xunit;

namespace CommonCore.Tests.Numerics
{
    [ExcludeFromCodeCoverage]
    public class Test_TriangularEngine
    {
        [Fact]
        public void Sequence()
        {
            var engine = new TriangularEngine();
            foreach (var value in engine.Take(Stored.Length))
            { value.Should().Be(Stored[(int)engine.Index]); }
        }

        [Fact]
        public void Stamps()
        {
            var engine = new TriangularEngine();

            // Throw wrong section count.
            Action act = () => engine.LoadStamp(string.Join(':', new int[engine.StampDetails.count + 1]));
            act.Should().Throw<FormatException>();

            // Ensure index is clamped to >= 0.
            string stamp = engine.ToStamp();
            engine.LoadStamp("-1:0");
            stamp.Should().Be(engine.ToStamp());

            int halfCount = Stored.Length / 2;
            foreach (var value in engine.Take(halfCount)) { }
            stamp = engine.ToStamp();
            foreach (var value in engine.Reset().LoadStamp(stamp).Take(Stored.Length - halfCount))
            { value.Should().Be(Stored[(int)engine.Index]); }
        }

        internal static readonly int[] Stored
            = new[] { 0, 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 66, 78, 91, 105, 120, 136, 153, 171, 190 };
    }
}
