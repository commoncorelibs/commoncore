:construction: Documentation Under Construction :construction:
# How to contribute

TODO: Text Here (eg https://github.com/opengovernment/opengovernment/blob/master/CONTRIBUTING.md, https://mozillascience.github.io/working-open-workshop/contributing/, and https://mozillascience.github.io/leadership-training/02.2-roadmap.html)

Important Resources:

  * Roadmap
  * [Documentation](https://commoncorelibs.gitlab.io/commoncore/)
  * [Issues](https://gitlab.com/commoncorelibs/commoncore/issues)
  * [NuGet](https://www.nuget.org/packages/VinlandSolutions.CommonCore/)

## Testing

TODO: Text Here

## Submitting Issues

TODO: Text Here

TODO: Issue Conventions

## Submitting Changes

TODO: Text Here

TODO: Commit Conventions (eg https://github.com/atom/atom/blob/master/CONTRIBUTING.md#git-commit-messages, https://chris.beams.io/posts/git-commit/)

1. Capitalize the subject line and do not end with a period
1. Use the present tense and imperative mood in the subject line
1. Limit the subject line to 50 characters, excluding initial emoji
1. Separate subject from body with a blank line
1. Wrap the body at 72 characters
1. Use the body to explain what and why vs. how
1. Reference issues and pull requests liberally within the body

* When only changing documentation, include [ci skip] in the commit title
* Consider starting the commit message with an appropriate [emoji](https://www.webfx.com/tools/emoji-cheat-sheet/)
  * Repo Documentation: :ledger:
  * Product Documentation: :notebook:
  * Misc: :construction:
  * CI/CD: :gear:
  * Test: :syringe:
  * Patch: :wrench:
  * Feature: :bulb:
  * Performance: :watch:
  * Release: :star:
  * Add: :exclamation:
  * Remove: :x:
  * Update: :o:

:ledger: :notebook: :blue_book: :green_book: :orange_book: :closed_book: :notebook_with_decorative_cover:
:bulb: :star: :flashlight: :package:

:school_satchel: :mortar_board: :mag: :microscope: :school:



## Coding Conventions

TODO: Text Here
